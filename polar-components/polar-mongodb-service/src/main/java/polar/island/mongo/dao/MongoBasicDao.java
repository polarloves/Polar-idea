package polar.island.mongo.dao;

import com.mongodb.BasicDBObject;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.util.ReflectionUtils;
import polar.island.core.config.Constants;
import polar.island.core.dao.BasicDao;
import polar.island.core.dao.types.IdType;
import polar.island.core.dao.annotation.ClassConfig;
import polar.island.core.dao.annotation.FieldConfig;
import polar.island.core.entity.BasicEntity;
import polar.island.core.exception.FrameWorkException;
import polar.island.core.exception.ValidationException;
import polar.island.core.util.BeansUtils;
import polar.island.core.util.EntityUtil;
import polar.island.core.util.GsonUtil;
import polar.island.mongo.query.QueryFactory;
import polar.island.mongo.query.SimpleQueryFacatory;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * mongod基础增删改查类
 *
 * @param <T> 列表类型
 * @param <D> 详情类型
 */
public abstract class MongoBasicDao<T extends BasicEntity, D extends BasicEntity> implements BasicDao<T, D> {
    /**
     * 日志记录器
     **/
    public final Logger logger = LoggerFactory.getLogger(getClass());
    @Resource(name = "mongoTemplate")
    protected MongoTemplate mongoTemplate;

    /**
     * 获取数据库表名
     *
     * @return 数据库表名
     */
    public String tableName() {
        ClassConfig classConfig = proxyClass().getAnnotation(ClassConfig.class);
        if (classConfig == null) {
            throw new FrameWorkException(Constants.CODE_SERVER_ERROR, "class " + getQueryFactory() + "未配置", null, true);
        }
        return classConfig.tableName();
    }

    /**
     * 列表的class
     *
     * @return 列表的class
     */
    public Class<T> listClass() {
        ClassConfig classConfig = proxyClass().getAnnotation(ClassConfig.class);
        if (classConfig == null) {
            throw new FrameWorkException(Constants.CODE_SERVER_ERROR, "class " + getQueryFactory() + "未配置", null, true);
        }
        return (Class<T>) classConfig.listClass();
    }

    /**
     * 详情的class
     *
     * @return 详情的class
     */
    public Class<D> detailClass() {
        ClassConfig classConfig = proxyClass().getAnnotation(ClassConfig.class);
        if (classConfig == null) {
            throw new FrameWorkException(Constants.CODE_SERVER_ERROR, "class " + getQueryFactory() + "未配置", null, true);
        }
        return (Class<D>) classConfig.detailClass();
    }

    /**
     * 代理类的class，其为了与接口包解耦
     *
     * @return 代理类的class
     */
    public abstract Class<?> proxyClass();

    /**
     * 查询器
     *
     * @return 获取查询器
     */
    public QueryFactory getQueryFactory() {
        return new SimpleQueryFacatory();
    }

    /**
     * 获取真实的id值，以防id类型不匹配
     *
     * @param id 原始id
     * @return 真实的id
     */
    public Object realIdValue(Object id) {
        ClassConfig classConfig = proxyClass().getAnnotation(ClassConfig.class);
        if (classConfig != null) {
            if (classConfig.idType() == IdType.LONG) {
                return Long.parseLong(id.toString());
            } else {
                return id.toString();
            }
        }
        return Long.parseLong(id.toString());
    }

    /**
     * 获取默认排序方式
     *
     * @param query
     * @param condition
     * @param proxyClass
     * @return
     */
    public Query defaultOrder(Query query, Map<String, Object> condition, Class<?> proxyClass) {
        query.with(Sort.by(Sort.Direction.ASC, "_id"));
        return query;
    }

    /**
     * 依据id查询一个数据时的查询条件
     *
     * @param id 数据编号
     * @return 查询条件
     */
    public Query selectOneByIdQuery(String id) {
        return detailQueryFields().addCriteria(Criteria.where("_id").is(realIdValue(id)));
    }

    /**
     * 查询一个数据时的查询条件
     *
     * @param condition 查询条件
     * @return 查询条件
     */
    public Query selectOneByConditionQuery(Map<String, Object> condition) {
        return getQueryFactory().searchOneWheres(detailQueryFields(), condition, proxyClass());
    }

    @Override
    public D selectOneById(String id) {
        Object v = mongoTemplate.findOne(selectOneByIdQuery(id), proxyClass(), tableName());
        return convertToDetail(v);
    }

    @Override
    public D selectOneByCondition(Map<String, Object> condition) {
        Object v = mongoTemplate.findOne(selectOneByConditionQuery(condition), proxyClass(), tableName());
        return convertToDetail(v);
    }

    /**
     * 查询多个时的查询条件
     *
     * @param condition 查询条件
     * @return 查询条件
     */
    public Query selectListQuery(Map<String, Object> condition) {
        Query query = getQueryFactory().searchMultiWheres(listQueryFields(), condition, proxyClass());
        return query;
    }

    /**
     * 排序规则，用于分页查询以及列表查询
     *
     * @param query     原始的Query
     * @param condition 查询条件
     * @return 带有排序规则的Query
     */
    public Query orderCondition(Query query, Map<String, Object> condition) {
        return getQueryFactory().orderCondition(query, condition, proxyClass());
    }

    @Override
    public List<T> selectList(Map<String, Object> condition) {
        //condition不可以为空，处理一下
        if (condition == null) {
            condition = new HashMap<String, Object>();
        }
        Query query = selectListQuery(condition);
        if (condition.get("sort") == null) {
            // 没有要求排序，使用默认排序
            query = defaultOrder(query, condition, proxyClass());
        } else {
            //依据字段进行排序
            query = orderCondition(query, condition);
        }
        List<?> v = mongoTemplate.find(query, proxyClass(), tableName());
        return convertToList(v);
    }

    @Override
    public List<Map<String, Object>> selectExportList(Map<String, Object> condition) {
        //condition不可以为空，处理一下
        if (condition == null) {
            condition = new HashMap<String, Object>();
        }
        List<Map<String, Object>> tmp = new ArrayList<Map<String, Object>>();
        List<T> result = selectList(condition);
        for (T t : result) {
            tmp.add(EntityUtil.beanToMap(t));
        }
        return tmp;
    }


    @Override
    public List<T> selectPageList(Map<String, Object> condition) {
        //condition不可以为空，处理一下
        if (condition == null) {
            condition = new HashMap<String, Object>();
        }
        Query query = selectListQuery(condition);
        //组装排序条件
        if (condition.get("sort") == null) {
            // 没有要求排序，使用默认排序
            query = defaultOrder(query, condition, proxyClass());
        } else {
            //依据字段进行排序
            query = orderCondition(query, condition);
        }
        //分页
        query.skip(Long.parseLong(condition.get("pageStartNumber").toString())).limit(Integer.parseInt(condition.get("pageOffsetNumber").toString()));
        List<?> v = mongoTemplate.find(query, proxyClass(), tableName());
        return convertToList(v);
    }

    @Override
    public Long selectCount(Map<String, Object> condition) {
        if (condition == null) {
            condition = new HashMap<String, Object>();
        }
        Query query = selectListQuery(condition);
        return mongoTemplate.count(query, listClass(), tableName());
    }

    @Override
    public Long updateAll(Map<String, Object> condition) {
        if (condition == null) {
            condition = new HashMap<String, Object>();
        }
        Query query = new Query();
        Object id = findId(condition);
        if (id == null) {
            logger.warn("数据编号为空,无法更新!");
            return 0l;
        }
        //更新的Query
        query.addCriteria(Criteria.where("_id").is(realIdValue(id)));
        Update update = new Update();
        //获取更新字段集合
        update = getQueryFactory().updateAllField(update, condition, proxyClass());
        if (update == null) {
            throw new ValidationException("未传入数据，无法更新", null);
        }
        UpdateResult result = mongoTemplate.updateMulti(query, update, tableName());
        return result.getMatchedCount();
    }

    /**
     * 从condition中获取数据的id，默认是id字段
     *
     * @param condition 条件
     * @return 数据id
     */
    public Object findId(Map<String, Object> condition) {

        ClassConfig classConfig = proxyClass().getAnnotation(ClassConfig.class);
        if (classConfig == null) {
            return condition.get("id");
        }
        if (classConfig.idField().length() > 0) {
            return condition.get(classConfig.idField());
        }
        return null;
    }

    /**
     * 获取此类中，id的字段名称
     *
     * @return id的字段名称，默认是id
     */
    public String idField() {
        ClassConfig classConfig = proxyClass().getAnnotation(ClassConfig.class);
        if (classConfig != null && classConfig.idField().length() > 0) {
            return classConfig.idField();
        }
        return "id";
    }

    @Override
    public Long updateField(Map<String, Object> condition) {
        if (condition == null) {
            condition = new HashMap<String, Object>();
        }
        Query query = new Query();
        Object id = findId(condition);
        if (id == null) {
            logger.warn("数据编号为空,无法更新!");
            return 0l;
        }
        query.addCriteria(Criteria.where("_id").is(realIdValue(id)));
        Update update = new Update();
        update = getQueryFactory().updateSingleField(update, condition, proxyClass());
        if (update == null) {
            throw new ValidationException("未传入数据，无法更新", null);
        }
        UpdateResult result = mongoTemplate.updateMulti(query, update, tableName());
        return result.getMatchedCount();
    }

    @Override
    public Long deleteByIdPhysical(String id) {
        DeleteResult result = mongoTemplate.remove(Query.query(Criteria.where("_id").is(realIdValue(id))), tableName());
        return result.getDeletedCount();
    }

    @Override
    public Long deleteByConditionPhysical(Map<String, Object> condition) {
        if (condition == null) {
            condition = new HashMap<String, Object>();
        }
        DeleteResult result = mongoTemplate.remove(getQueryFactory().deleteWheres(new Query(), condition, proxyClass()), tableName());
        return result.getDeletedCount();
    }

    /**
     * 将指定数据转成详情类
     *
     * @param source 数据
     * @return 详情类
     */
    public D convertToDetail(Object source) {
        return EntityUtil.copy(source, detailClass());
    }

    /**
     * 将指定数据转成集合类
     *
     * @param source 数据
     * @return 集合
     */
    public List<T> convertToList(List source) {
        return EntityUtil.copyArray(source, listClass());
    }

    @Override
    public void insert(Map<String, Object> condition) {
        if (condition == null) {
            condition = new HashMap<String, Object>();
        }
        Object d = EntityUtil.mapToBean(condition, proxyClass());
        mongoTemplate.insert(d, tableName());
        try {
            // 从条件中获取设置的数据编号
            String idField = idField();
            java.lang.reflect.Field field = proxyClass().getDeclaredField(idField);
            field.setAccessible(true);
            Object idValue = field.get(d);
            condition.put(idField, idValue);
        } catch (Exception e) {
            logger.warn("设置编号失败,class:{},error:{}", proxyClass(), e);
        }
    }

    @Override
    public Long deleteByIdLogic(String id) {
        ClassConfig classConfig = proxyClass().getAnnotation(ClassConfig.class);
        Object invalidValue = getDeleteLogicValue();
        if (invalidValue == null) {
            return 0l;
        }
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(realIdValue(id)));
        Update update = Update.update(classConfig.invalidValueName(), invalidValue);
        UpdateResult result = mongoTemplate.updateMulti(query, update, tableName());
        return result.getMatchedCount();
    }

    private Object getDeleteLogicValue() {
        ClassConfig classConfig = proxyClass().getAnnotation(ClassConfig.class);
        if (classConfig == null) {
            logger.warn("类 {} 未配置逻辑删除选项,无法进行逻辑删除!", proxyClass());
            return null;
        }
        if (!classConfig.logicAble()) {
            logger.warn("类 {} 不支持逻辑删除,无法删除!", proxyClass());
            return null;
        }
        Object invalidValue = classConfig.invalidValue();
        if (classConfig.invalidValueClass() == int.class || classConfig.invalidValueClass() == Integer.class) {
            invalidValue = Integer.parseInt(invalidValue.toString());
        } else if (classConfig.invalidValueClass() == long.class || classConfig.invalidValueClass() == Long.class) {
            invalidValue = Long.parseLong(invalidValue.toString());
        } else if (classConfig.invalidValueClass() == String.class) {
            invalidValue = invalidValue;
        } else {
            logger.warn("类 {} 的逻辑删除字段不是int或者long或者string，无法删除!", proxyClass());
            return null;
        }
        return invalidValue;
    }

    @Override
    public Long deleteByConditionLogic(Map<String, Object> condition) {
        ClassConfig classConfig = proxyClass().getAnnotation(ClassConfig.class);
        Object invalidValue = getDeleteLogicValue();
        if (invalidValue == null) {
            return 0l;
        }
        Query query = getQueryFactory().deleteWheres(new Query(), condition, proxyClass());
        Update update = Update.update(classConfig.invalidValueName(), invalidValue);
        UpdateResult result = mongoTemplate.updateMulti(query, update, tableName());
        return result.getMatchedCount();
    }

    /**
     * 获取查询列表时，需要显示的字段。
     *
     * @return 需要显示的字段
     */
    public Query listQueryFields() {
        final BasicDBObject fieldsObject = new BasicDBObject();
        ReflectionUtils.doWithFields(listClass(), new ReflectionUtils.FieldCallback() {
            @Override
            public void doWith(final java.lang.reflect.Field field)
                    throws IllegalArgumentException, IllegalAccessException {
                if (!field.isAnnotationPresent(FieldConfig.class)) {
                    return;
                }
                boolean select = true;
                if (!field.getAnnotation(FieldConfig.class).queryList()) {
                    select = false;
                }
                String key = field.getName();
                if (field.isAnnotationPresent(Field.class)) {
                    if (field.getAnnotation(Field.class).value().length() > 0) {
                        key = field.getAnnotation(Field.class).value();
                    }
                }
                if (field.isAnnotationPresent(Id.class)) {
                    key = "_id";
                }
                fieldsObject.put(key, select);
            }
        });
        return new BasicQuery(null, fieldsObject.toJson());
    }
    /**
     * 获取查询详情时，需要显示的字段。
     *
     * @return 需要显示的字段
     */
    public Query detailQueryFields() {
        final BasicDBObject fieldsObject = new BasicDBObject();
        ReflectionUtils.doWithFields(detailClass(), new ReflectionUtils.FieldCallback() {
            @Override
            public void doWith(final java.lang.reflect.Field field)
                    throws IllegalArgumentException, IllegalAccessException {
                if (!field.isAnnotationPresent(FieldConfig.class)) {
                    return;
                }
                boolean select = true;
                if (!field.getAnnotation(FieldConfig.class).queryDetail()) {
                    select = false;
                }
                String key = field.getName();
                if (field.isAnnotationPresent(Field.class)) {
                    if (field.getAnnotation(Field.class).value().length() > 0) {
                        key = field.getAnnotation(Field.class).value();
                    }
                }
                if (field.isAnnotationPresent(Id.class)) {
                    key = "_id";
                }
                fieldsObject.put(key, select);
            }
        });
        return new BasicQuery(null, fieldsObject.toJson());
    }
}
