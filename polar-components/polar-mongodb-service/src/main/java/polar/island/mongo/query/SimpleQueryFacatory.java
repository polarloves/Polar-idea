package polar.island.mongo.query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.util.ReflectionUtils;
import polar.island.core.dao.annotation.FieldConfig;
import polar.island.core.dao.annotation.MatchStyle;
import polar.island.core.dao.types.Scope;
import polar.island.core.dao.types.StyleType;
import polar.island.core.util.CommonUtil;

import java.util.HashMap;
import java.util.Map;

public class SimpleQueryFacatory implements QueryFactory {
    /**
     * 日志记录器
     **/
    public final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public Query searchMultiWheres(final Query query, final Map<String, Object> value, Class<?> queryFactoryClass) {
        return getQuery(query, value, queryFactoryClass, Scope.SELECT_MULTI, StyleType.SELECT_MULTI);
    }

    private boolean isUseful(Scope scope, Scope[] scopes) {
        if (scope == null) {
            return true;
        }
        for (Scope jScope : scopes) {
            if (jScope == Scope.ALL || jScope == scope) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Query searchOneWheres(Query query, Map<String, Object> value, Class<?> queryFactoryClass) {
        return getQuery(query, value, queryFactoryClass, Scope.SELECT_ONE, StyleType.SELECT_ONE);
    }

    @Override
    public Query orderCondition(Query query, Map<String, Object> value, Class<?> queryFactoryClass) {
        try {
            java.lang.reflect.Field field = queryFactoryClass.getDeclaredField(value.get("sort").toString());
            if (field == null) {
                return query;
            }
            String order = CommonUtil.valueOf(value.get("order"));
            if (field.getAnnotation(FieldConfig.class) != null) {
                if (!field.getAnnotation(FieldConfig.class).sortable()) {
                    return query;
                }
                String key = field.getName();
                Field mappingField = queryFactoryClass.getAnnotation(Field.class);
                if (mappingField != null) {
                    if (mappingField.value().length() > 0) {
                        key = mappingField.value();
                    }
                }
                if (order != null && order.equals("DESC")) {
                    query.with(Sort.by(Sort.Direction.DESC, key));
                } else {
                    query.with(Sort.by(Sort.Direction.ASC, key));
                }
            }
        } catch (Exception e) {
            logger.error("获取排序字段失败,error: {}", e);
        }
        return query;
    }

    @Override
    public Query deleteWheres(Query query, Map<String, Object> value, Class<?> queryFactoryClass) {
        return getQuery(query, value, queryFactoryClass, Scope.DELETE, StyleType.DELETE);
    }

    @Override
    public Query updateWheres(Query query, Map<String, Object> value, Class<?> queryFactoryClass) {
        return getQuery(query, value, queryFactoryClass, Scope.UPDATE, StyleType.UPDATE);
    }

    public Map<String, Object> updateFiledValue(Map<String, Object> condition, Class<?> queryFactoryClass) {
        Map<String, Object> result = new HashMap<String, Object>();
        for (String fieldName : condition.keySet()) {
            Object mValue = condition.get(fieldName);
            java.lang.reflect.Field field = ReflectionUtils.findField(queryFactoryClass, fieldName);
            if (field == null) {
                continue;
            }
            if (field.getAnnotation(Id.class) != null) {
                continue;
            }
            if (field.getAnnotation(FieldConfig.class) == null) {
                continue;
            }
            if (!field.getAnnotation(FieldConfig.class).updateField()) {
                continue;
            }
            if (field.isAnnotationPresent(Field.class)) {
                Field re = field.getAnnotation(Field.class);
                if (re.value().length() > 0) {
                    fieldName = re.value();
                }
            }
            result.put("key", fieldName);
            result.put("value", mValue);
            return result;
        }
        return null;
    }

    @Override
    public Update updateSingleField(final Update update, Map<String, Object> value, Class<?> queryFactoryClass) {
        Map<String, Object> updateFiledValue=updateFiledValue(value,queryFactoryClass);
        update.set(updateFiledValue.get("key").toString(), updateFiledValue.get("value"));
        return null;
    }

    @Override
    public Update updateAllField(Update update, Map<String, Object> value, Class<?> queryFactoryClass) {
        boolean has = false;
        for (java.lang.reflect.Field field : queryFactoryClass.getDeclaredFields()) {
            if (field == null) {
                continue;
            }
            if (field.getAnnotation(Id.class) != null) {
                continue;
            }
            if (field.getAnnotation(FieldConfig.class) == null) {
                continue;
            }
            if (field.getAnnotation(FieldConfig.class).updateAll()) {
                String fieldName = field.getName();
                if (field.isAnnotationPresent(Field.class)) {
                    Field re = field.getAnnotation(Field.class);
                    if (re.value().length() > 0) {
                        fieldName = re.value();
                    }
                }
                update.set(fieldName, value.get(field.getName()));
                has = true;
            }
        }
        if (has) {
            return update;
        } else {
            return null;
        }
    }

    private MatchStyle findMatchStyle(FieldConfig fieldConfig, StyleType styleType) {
        for (MatchStyle style : fieldConfig.styles()) {
            if (style.type() == styleType) {
                return style;
            }
        }
        return null;
    }

    private Query getQuery(final Query query, final Map<String, Object> value, final Class<?> queryFactoryClass, final Scope scope, final StyleType styleType) {
        ReflectionUtils.doWithFields(queryFactoryClass, new ReflectionUtils.FieldCallback() {
            @Override
            public void doWith(final java.lang.reflect.Field field)
                    throws IllegalArgumentException, IllegalAccessException {
                ReflectionUtils.makeAccessible(field);
                if (field.isAnnotationPresent(FieldConfig.class)) {
                    FieldConfig fieldConfig = field.getAnnotation(FieldConfig.class);
                    if (!isUseful(scope, fieldConfig.scope())) {
                        //在此作用域无效，则其返回
                        return;
                    }
                    MatchStyle matchStyle = findMatchStyle(fieldConfig, styleType);
                    if (matchStyle == null) {
                        return;
                    }
                    Object innerValue = value.get(field.getName());
                    if (!matchStyle.nullMatches() && innerValue == null) {
                        //空值不进行匹配
                        return;
                    }
                    String key = field.getName();
                    if (field.isAnnotationPresent(Id.class)) {
                        key = "_id";
                    }
                    if (field.isAnnotationPresent(Field.class)) {
                        Field re = field.getAnnotation(Field.class);
                        if (re.value().length() > 0) {
                            key = re.value();
                        }
                    }
                    switch (matchStyle.matchStyle()) {
                        case EQUALS:
                            query.addCriteria(Criteria.where(key).is(innerValue));
                            break;
                        case BIG_EQUAL:
                            query.addCriteria(Criteria.where(key).gte(innerValue));
                            break;
                        case LESS_EQUAL:
                            query.addCriteria(Criteria.where(key).lte(innerValue));
                            break;
                        case BIG:
                            query.addCriteria(Criteria.where(key).gt(innerValue));
                            break;
                        case LESS:
                            query.addCriteria(Criteria.where(key).lt(innerValue));
                            break;
                        case LIKE:
                            query.addCriteria(Criteria.where(key).regex(".*?\\" + innerValue + ".*"));
                            break;
                        case REGEX:
                            query.addCriteria(Criteria.where(key).regex(matchStyle.regex()));
                            break;
                        case UNDEFINED:
                            query.addCriteria(Criteria.where(key).not());
                            break;
                        case NOTNULL:
                            query.addCriteria(Criteria.where(key).ne("").ne(null));
                            break;
                    }
                }
            }
        });
        return query;
    }


}
