package polar.island.mongo.query;

import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.Map;

public interface QueryFactory {

    /**
     * 查询多条数据时的条件
     *
     * @param query             预设条件
     * @param value             数据
     * @param queryFactoryClass 条件生成依据的实体
     * @return 条件
     */
    public Query searchMultiWheres(Query query, Map<String, Object> value, Class<?> queryFactoryClass);

    /**
     * 查询单条数据时的条件
     *
     * @param query             预设条件
     * @param value             数据
     * @param queryFactoryClass 条件生成依据的实体
     * @return 条件
     */
    public Query searchOneWheres(Query query, Map<String, Object> value, Class<?> queryFactoryClass);

    /**
     * 排序条件
     *
     * @param query             预设条件
     * @param value             数据
     * @param queryFactoryClass 条件生成依据的实体
     * @return 条件
     */
    public Query orderCondition(Query query, Map<String, Object> value, Class<?> queryFactoryClass);


    /**
     * 删除条件(物理删除的删除条件)
     *
     * @param query             预设条件
     * @param value             数据
     * @param queryFactoryClass 条件生成依据的实体
     * @return 条件
     */
    public Query deleteWheres(Query query, Map<String, Object> value, Class<?> queryFactoryClass);

    /**
     * 更新时的条件
     *
     * @param query             预设条件
     * @param value             数据
     * @param queryFactoryClass 条件生成依据的实体
     * @return 条件
     */
    public Query updateWheres(Query query, Map<String, Object> value, Class<?> queryFactoryClass);

    /**
     * 获取更新单个字段时更新数据，如果不允许更新或者发生错误，返回空
     *
     * @param update            预设条件
     * @param value             数据
     * @param queryFactoryClass 条件生成依据的实体
     * @return 条件
     */
    public Update updateSingleField(Update update, Map<String, Object> value, Class<?> queryFactoryClass);


    /**
     * 获取更新全部字段时更新数据，如果不允许更新或者发生错误，返回空
     *
     * @param update            预设条件
     * @param value             数据
     * @param queryFactoryClass 条件生成依据的实体
     * @return 条件
     */
    public Update updateAllField(Update update, Map<String, Object> value, Class<?> queryFactoryClass);

    /**
     * 获取更新单个字段时的数据，包括key，value
     *
     * @param condition         数据
     * @param queryFactoryClass 条件生成依据的实体
     * @return 数据
     */
    public Map<String, Object> updateFiledValue(Map<String, Object> condition, Class<?> queryFactoryClass);
}
