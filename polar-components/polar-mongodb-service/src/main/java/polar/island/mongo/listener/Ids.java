package polar.island.mongo.listener;

import org.springframework.data.annotation.Id;

public class Ids {
    @Id
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
