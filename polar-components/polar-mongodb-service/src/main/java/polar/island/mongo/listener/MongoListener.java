package polar.island.mongo.listener;

import com.mongodb.BasicDBObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;
import polar.island.core.dao.annotation.AutoIncrease;
import polar.island.core.dao.annotation.ClassConfig;
import polar.island.core.listener.InitializeListener;
import polar.island.core.serializer.Serializer;
import polar.island.mongo.dao.MongoBasicDao;
import polar.island.redis.serializer.impl.SerializerWrapper;
import polar.island.redis.service.RedisService;

import javax.annotation.Resource;
import java.lang.reflect.Field;
import java.util.Map;

@Component
public class MongoListener implements InitializeListener {
    @Value("${mongo.id.key}")
    private String prefix = "mongo_ids_";
    @Resource(name = "mongoTemplate")
    protected MongoTemplate mongoTemplate;

    @Autowired(required = false)
    private Map<String, MongoBasicDao> beans;
    private RedisService redisService;
    private Serializer keySerializer;
    private Serializer valueSerializer;
    private Serializer defaultKeySerializer=new SerializerWrapper(new StringRedisSerializer());
    private Serializer defaultValueSerializer=new SerializerWrapper(new JdkSerializationRedisSerializer());

    /**
     * 日志记录器
     **/
    public final Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public int sort () {
        return 3;
    }

    @Override
    public void initialize(ApplicationContext context) {
        for (String key : beans.keySet()) {
            MongoBasicDao dao = beans.get(key);
            Class<?> proxyClass = dao.proxyClass();
            if (proxyClass.getAnnotation(ClassConfig.class) != null) {
                String tableName = proxyClass.getAnnotation(ClassConfig.class).tableName();
                boolean exist = false;
                for (String name : mongoTemplate.getCollectionNames()) {
                    if (name.equals(tableName)) {
                        exist = true;
                        break;
                    }
                }
                if (!exist) {
                    logger.debug("表{}不存在，创建表", tableName);
                    mongoTemplate.createCollection(tableName);
                }
                for (Field field : proxyClass.getDeclaredFields()) {
                    if (field.getAnnotation(Id.class) != null) {
                        if (field.getAnnotation(AutoIncrease.class) != null) {
                            if (!redisService.exists(getKeySerializer().serialize(prefix + tableName))) {
                                logger.debug("名称为:{}的table在redis中没有主键序列,开始增加", tableName);
                                BasicDBObject fieldsObject = new BasicDBObject();
                                fieldsObject.put("_id", true);
                                Query query = new BasicQuery(null, fieldsObject.toJson()).with(Sort.by(Sort.Direction.DESC, "_id")).limit(1);
                                Ids ids = mongoTemplate.findOne(query, Ids.class, tableName);
                                Long max = 0l;
                                if (ids != null) {
                                    max = ids.getId();
                                }
                                logger.debug("开始为{}的table设置主键，初始主键为：{}", tableName, max);
                                redisService.set(getKeySerializer().serialize(prefix + tableName),getValueSerializer().serialize(max+""));
                            }
                        } else {
                            break;
                        }
                    }
                }
            }
        }
    }

    public Serializer getKeySerializer() {
        if(keySerializer==null){
            return defaultKeySerializer;
        }
        return keySerializer;
    }

    public void setKeySerializer(Serializer keySerializer) {
        this.keySerializer = keySerializer;
    }

    public Serializer getValueSerializer() {
        if(valueSerializer==null){
            return defaultValueSerializer;
        }
        return valueSerializer;
    }

    public void setValueSerializer(Serializer valueSerializer) {
        this.valueSerializer = valueSerializer;
    }

    public void setRedisService(RedisService redisService) {
        this.redisService = redisService;
    }

}
