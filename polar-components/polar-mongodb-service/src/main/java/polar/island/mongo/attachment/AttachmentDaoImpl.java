package polar.island.mongo.attachment;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;
import polar.island.core.dao.AttachmentDao;
import polar.island.core.entity.AttachmentEntity;
import polar.island.core.util.EntityUtil;
import polar.island.mongo.dao.MongoBasicDao;

import java.util.ArrayList;
import java.util.List;

@Repository(value = "mongoAttachmentDao")
public class AttachmentDaoImpl extends MongoBasicDao<AttachmentEntity, AttachmentEntity> implements AttachmentDao {

    @Override
    public void insert(AttachmentEntity entity) {
        AttachmentProxyEntity attachmentProxyEntity = EntityUtil.copy(entity, AttachmentProxyEntity.class);
        mongoTemplate.insert(attachmentProxyEntity);
    }

    @Override
    public Long deleteAttachment(String type, String attachmentId) {
        Query query = Query.query(Criteria.where("type").is(type).and("attachmentId").is(attachmentId));
        return mongoTemplate.remove(query, tableName()).getDeletedCount();
    }

    @Override
    public List<String> selectAttachment(String type, String attachmentId) {
        Query query = Query.query(Criteria.where("type").is(type).and("attachmentId").is(attachmentId)).with(Sort.by(Sort.Direction.ASC, "createDateMillions"));
        List<AttachmentProxyEntity> entities = mongoTemplate.find(query, AttachmentProxyEntity.class, tableName());
        List<String> result = new ArrayList<String>();
        if (!CollectionUtils.isEmpty(entities)) {
            for (AttachmentProxyEntity entity : entities) {
                result.add(entity.getVisitPath());
            }
        }
        return result;
    }

    @Override
    public Long deleteAttachmentByPid(String type, String pid) {
        Query query = Query.query(Criteria.where("type").is(type).and("pid").is(pid));
        return mongoTemplate.remove(query, tableName()).getDeletedCount();
    }

    @Override
    public Class<?> proxyClass() {
        return AttachmentProxyEntity.class;
    }
}
