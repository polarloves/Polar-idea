package polar.island.mongo.util;

import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.HashMap;
import java.util.Map;

public class MongoDBUtil {
    public static  Long deleteCollectionItem(MongoTemplate mongoTemplate, String collectionName, String itemName, Object id) {
        Update update = new Update();
        Map<String, Object> v = new HashMap<>();
        v.put("_id", id);
        update.pull(itemName, v);
        return mongoTemplate.updateMulti(new Query(), update, collectionName).getMatchedCount();
    }
}
