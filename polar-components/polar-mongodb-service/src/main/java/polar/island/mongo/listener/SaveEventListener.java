package polar.island.mongo.listener;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.event.AbstractMongoEventListener;
import org.springframework.data.mongodb.core.mapping.event.BeforeConvertEvent;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;
import polar.island.core.dao.annotation.AutoIncrease;
import polar.island.core.serializer.Serializer;
import polar.island.core.util.CommonUtil;
import polar.island.redis.serializer.impl.SerializerWrapper;
import polar.island.redis.service.RedisService;


public class SaveEventListener extends AbstractMongoEventListener<Object> {
    @Value("${mongo.id.key}")
    private String prefix = "mongo_ids_";
    private RedisService redisService;
    private Serializer keySerializer;
    private Serializer defaultKeySerializer=new SerializerWrapper(new StringRedisSerializer());

    @Override
    public void onBeforeConvert(final BeforeConvertEvent<Object> event) {
        final Object source = event.getSource();
        if (source != null) {
            ReflectionUtils.doWithFields(source.getClass(), new ReflectionUtils.FieldCallback() {
                @Override
                public void doWith(final java.lang.reflect.Field field)
                        throws IllegalArgumentException, IllegalAccessException {
                    ReflectionUtils.makeAccessible(field);
                    if (field.isAnnotationPresent(Id.class) && field.isAnnotationPresent(AutoIncrease.class) && field.get(source) == null) {
                        field.set(source, generateAutoIncreaseId(event.getCollectionName()));
                    }
                }
            });
        }
    }

    private Long generateAutoIncreaseId(final String collectionName) {
        return redisService.autoIncreaseKey(getKeySerializer().serialize(prefix + collectionName));
    }



    public void setRedisService(RedisService redisService) {
        this.redisService = redisService;
    }
    public void setKeySerializer(Serializer keySerializer) {
        this.keySerializer = keySerializer;
    }

    public Serializer getKeySerializer() {
        if(keySerializer==null){
            return defaultKeySerializer;
        }
        return keySerializer;
    }
}
