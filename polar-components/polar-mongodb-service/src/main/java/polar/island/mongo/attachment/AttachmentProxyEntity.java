package polar.island.mongo.attachment;

import org.springframework.data.annotation.Id;
import polar.island.core.dao.annotation.AutoIncrease;
import polar.island.core.dao.annotation.ClassConfig;
import polar.island.core.dao.annotation.FieldConfig;
import polar.island.core.entity.AttachmentEntity;

import java.util.Date;
@ClassConfig(tableName = "t_polar_attachment", listClass = AttachmentEntity.class, detailClass = AttachmentEntity.class)
public class AttachmentProxyEntity {
    /** 主键编号	**/
    @Id
    @AutoIncrease
    @FieldConfig
    private Long id;
    /** 访问路径 **/
    @FieldConfig
    private String visitPath;
    /** 外键编号 **/
    @FieldConfig
    private String attachmentId;
    /** 文件类型，一般以表名+字段名 **/
    @FieldConfig
    private String type;
    @FieldConfig
    private String pid;
    @FieldConfig
    private Date createDate;
    @FieldConfig
    private Long createDateMillions;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVisitPath() {
        return visitPath;
    }

    public void setVisitPath(String visitPath) {
        this.visitPath = visitPath;
    }

    public String getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(String attachmentId) {
        this.attachmentId = attachmentId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getCreateDateMillions() {
        return createDateMillions;
    }

    public void setCreateDateMillions(Long createDateMillions) {
        this.createDateMillions = createDateMillions;
    }
}
