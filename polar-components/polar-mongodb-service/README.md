# 一、使用方式

## 1.mongodb事务使用

* 必须使用复制集，复制集安装，参考：https://blog.csdn.net/forever19911314/article/details/51177102
* 使用事务之前（@Transaction）,必须先创建collection,创建方法：`db.createCollection(name, options)`
* 数据库创建用户密码：`db.createUser({user:"root",pwd:"root",roles:["readWrite"]})`

## 2.service与dao

此处，service没有与dao进行分离，作为NoSql数据库,其和sql的逻辑、表设计有巨大差距，因此，未做分离