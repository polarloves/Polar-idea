package polar.island.mybatis.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;
import polar.island.core.service.BasicService;
import polar.island.core.util.ExceptionUtil;
import polar.island.mybatis.common.MybatisDao;

import java.util.List;
import java.util.Map;


/**
 * Service层的实现类,如果使用/清空缓存，使用如下方式：
 * 清空：@CacheEvict(value = "cacheName", key = "#condition['uuid']")
 * 使用：@Cacheable(value = "cacheName", key = "#condition['uuid']")
 *
 * @param <T> 列表结果集泛化类型
 * @param <F> 详情结果集泛化类型
 * @param <D> Dao类型
 * @author PolarLoves
 */

public abstract class MybatisService<T, F, D extends MybatisDao<T, F>> implements BasicService<T, F> {
    public final Logger logger = LoggerFactory.getLogger(getClass());

    public abstract D getDao();

    public F selectOneById(String id) {
        return getDao().selectOneById(id);
    }

    @Transactional(readOnly = false)
    public void importExcell(List<Map<String, Object>> condition) {
        try {
            for (Map<String, Object> arg : condition) {
                getDao().insert(arg);
            }
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    public F selectOneByCondition(Map<String, Object> condition) {
        try {
            return getDao().selectOneByCondition(condition);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }

    }

    public List<T> selectList(Map<String, Object> condition) {
        try {
            return getDao().selectList(condition);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    public List<T> selectPageList(Map<String, Object> condition) {
        try {
            return getDao().selectPageList(condition);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    public Long selectCount(Map<String, Object> condition) {
        try {
            return getDao().selectCount(condition);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Transactional(readOnly = false)
    public Long updateAll(Map<String, Object> condition) {
        try {
            return getDao().updateAll(condition);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Transactional(readOnly = false)
    public Long updateField(Map<String, Object> condition) {
        try {
            return getDao().updateField(condition);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Transactional(readOnly = false)
    public Long deleteByIdPhysical(String id) {
        try {
            return getDao().deleteByIdPhysical(id);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Transactional(readOnly = false)
    public Long deleteByIdLogic(String id) {
        try {
            return getDao().deleteByIdLogic(id);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Transactional(readOnly = false)
    public Long deleteMulitByIdPhysical(String[] ids) {
        try {
            Long result = 0L;

            for (String id : ids) {
                result = result + getDao().deleteByIdPhysical(id);
            }
            return result;
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Transactional(readOnly = false)
    public Long deleteMulitByIdLogic(String[] ids) {
        try {
            Long result = 0L;

            for (String id : ids) {
                result = result + getDao().deleteByIdLogic(id);
            }
            return result;
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Transactional(readOnly = false)
    public Long deleteByConditionPhysical(Map<String, Object> condition) {
        try {
            return getDao().deleteByConditionPhysical(condition);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Transactional(readOnly = false)
    public Long deleteByConditionLogic(Map<String, Object> condition) {
        try {
            return getDao().deleteByConditionLogic(condition);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    public List<Map<String, Object>> selectExportList(Map<String, Object> condition) {
        try {
            return getDao().selectExportList(condition);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }
}
