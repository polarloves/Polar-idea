package polar.island.payment.dispatcher;

import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradePrecreateModel;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradePrecreateRequest;
import com.alipay.api.response.AlipayTradePrecreateResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import polar.island.core.config.Constants;
import polar.island.core.exception.FrameWorkException;
import polar.island.core.exception.ValidationException;
import polar.island.core.payment.config.AliConfig;
import polar.island.core.payment.config.WechatConfig;
import polar.island.core.payment.service.AliConfigService;
import polar.island.core.payment.service.WechatConfigService;
import polar.island.core.payment.callback.PaymentRequest;
import polar.island.core.payment.callback.PaymentResponse;
import polar.island.core.payment.config.PaymentType;

import java.util.HashMap;
import java.util.Map;

/**
 * 支付的调度器,此调度器应该由Controller来调用.其再将各种数据分发给各个service,理应配置各个service
 */
@Component(value = "paymentDispatcher")
public class PaymentDispatcher {
    private String aliKey = "out_trade_no";
    /**
     * 日志记录器
     **/
    public final Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired(required = false)
    private PaymentRequest paymentRequest;
    @Autowired(required = false)
    private PaymentResponse paymentResponse;
    @Autowired
    private AliConfigService aliConfigService;
    @Autowired
    private WechatConfigService wechatConfigService;

    /**
     * 发起支付请求
     *
     * @param extendsArgs 扩展的一些参数
     * @param money       支付金额
     * @param paymentType 支付类型
     * @return 支付成功后的数据
     */
    public Map<String, String> request(Map<String, Object> extendsArgs, String money, PaymentType paymentType) {
        if (paymentRequest == null) {
            logger.error("支付请求的回调函数未配置，请配置请求回调函数");
            throw new ValidationException("支付请求的回调函数未配置", null);
        }
        Map<String, String> payJson = new HashMap<String, String>();
        String payId = paymentRequest.generatePayId(paymentType, extendsArgs);
        if (paymentType == PaymentType.AliPay) {
            //支付宝支付
            AliConfig aliConfig = aliConfigService.get();
            if (aliConfig == null) {
                throw new ValidationException("支付宝未配置，请先配置支付宝", null);
            } else {
                try {
                    AlipayClient alipayClient = new DefaultAlipayClient(aliConfig.getServerUrl(), aliConfig.getAppId(),
                            aliConfig.getPrivateKey(), "json", "UTF-8", aliConfig.getPublicKey(), "RSA2");
                    AlipayTradePrecreateRequest request = new AlipayTradePrecreateRequest();
                    AlipayTradePrecreateModel model = new AlipayTradePrecreateModel();
                    request.setBizModel(model);
                    model.setOutTradeNo(payId);
                    model.setTotalAmount(Double.parseDouble(money) / 100 + "");
                    model.setSubject("支付订单");
                    AlipayTradePrecreateResponse response = alipayClient.execute(request);
                    if (response.isSuccess()) {
                        payJson.put("body", response.getBody());
                        payJson.put("qrcode", response.getQrCode());
                    } else {
                        throw new FrameWorkException(Constants.CODE_PAY, null, null, true);
                    }
                } catch (Exception e) {
                    if (e instanceof FrameWorkException) {
                        throw (FrameWorkException) e;
                    } else {
                        throw new FrameWorkException(Constants.CODE_PAY, null, e, true);
                    }
                }
            }
        } else if (paymentType == PaymentType.WeChatPay) {
            //微信支付
        }
        paymentRequest.onPayRequest(extendsArgs, money, paymentType, payId);
        return payJson;
    }

    /**
     * 支付响应回调，应在controller调用，由其分发给各个服务
     *
     * @param params
     * @param paymentType
     * @return
     */
    public boolean response(Map<String, String> params, PaymentType paymentType) {
        if (paymentResponse == null) {
            logger.error("支付的响应回调函数未设置，请设置响应回调函数");
            return false;
        }
        String payId = null;
        if (paymentType == PaymentType.AliPay) {
            //支付宝支付
            AliConfig aliConfig = aliConfigService.get();
            if (aliConfig == null) {
                logger.error("支付宝参数未配置");
                return false;
            }
            try {
                boolean signCheck = AlipaySignature.rsaCheckV1(params, aliConfig.getPublicKey(), "UTF-8");
                if (!signCheck) {
                    logger.error("非法的支付宝签名!");
                    return false;
                }
            } catch (Exception e) {
                logger.error("支付宝签名验证失败,error:{}", e);
                return false;
            }
        } else if (paymentType == PaymentType.WeChatPay) {
            //微信支付
        }
        return paymentResponse.onPayResponse(paymentType, payId);
    }

    /**
     * 获取支付回调后的订单编号
     *
     * @param params      支付回调参数
     * @param paymentType 支付类型
     * @return 订单编号
     */
    public String findPayId(Map<String, String> params, PaymentType paymentType) {
        if (paymentType == PaymentType.AliPay) {
            return params.get(aliKey);
        }
        return null;
    }

}

