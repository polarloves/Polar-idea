package polar.island.payment.entity;

import polar.island.core.payment.config.AliConfig;
import polar.island.core.payment.config.WechatConfig;

import java.io.Serializable;

public class PayStorageEntity implements Serializable {
    private AliConfig aliConfig;
    private WechatConfig wechatConfig;

    public AliConfig getAliConfig() {
        return aliConfig;
    }

    public void setAliConfig(AliConfig aliConfig) {
        this.aliConfig = aliConfig;
    }

    public WechatConfig getWechatConfig() {
        return wechatConfig;
    }

    public void setWechatConfig(WechatConfig wechatConfig) {
        this.wechatConfig = wechatConfig;
    }
}
