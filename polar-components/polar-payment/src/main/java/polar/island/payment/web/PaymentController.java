package polar.island.payment.web;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import polar.island.core.config.Constants;
import polar.island.core.entity.ResponseJson;
import polar.island.core.res.ResMsg;
import polar.island.core.res.ResType;
import polar.island.core.payment.service.AliConfigService;
import polar.island.core.payment.service.WechatConfigService;
import polar.island.payment.entity.PayStorageEntity;
import polar.island.web.controller.BasicController;

import javax.servlet.http.HttpServletRequest;

@Controller(value = "paymentWebController")
@RequestMapping(value = "/pay/config")
public class PaymentController extends BasicController {
    @Autowired(required = false)
    private AliConfigService aliConfigService;
    @Autowired(required = false)
    private WechatConfigService wechatConfigService;

    @RequiresUser
     @RequiresPermissions(value = { "polar:backstage", "polar:payconfig" })
    @ResMsg (tag = "查看支付配置", type = ResType.JSON)
    @RequestMapping(value = "/edit", produces = "text/html;charset=utf-8")
    public String edit(HttpServletRequest request) {
        request.setAttribute("ali", aliConfigService.get());
        request.setAttribute("wechat", wechatConfigService.get());
        return "/view/pay/form.jsp";
    }

    @ResponseBody
    @RequiresUser
    @RequiresPermissions(value = { "polar:backstage", "polar:payconfig" })
    @ResMsg (tag = "修改支付配置", type = ResType.JSON)
    @RequestMapping(value = "/save", produces = "application/json;charset=utf-8")
    public ResponseJson save(PayStorageEntity entity) {
        aliConfigService.save(entity.getAliConfig());
        wechatConfigService.save(entity.getWechatConfig());
        return new ResponseJson(Constants.CODE_SUCCESS, null);
    }
}
