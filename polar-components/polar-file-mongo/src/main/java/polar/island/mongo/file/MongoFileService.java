package polar.island.mongo.file;


import com.mongodb.client.gridfs.model.GridFSFile;
import org.apache.commons.io.IOUtils;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Service;
import polar.island.core.config.Constants;
import polar.island.core.exception.FrameWorkException;
import polar.island.core.file.FileService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;

/**
 * 使用Mongodb对文件进行上传、下载,由于数据流在web端，因此其不能做成一个服务
 *
 * @author polarloves
 */
@Service(value = "mongoFileService")
public class MongoFileService implements FileService {
    @Resource(name = "gridFsTemplate")
    protected GridFsTemplate gridFsTemplate;

    @Override
    public String storeFile(String fileName, InputStream inputStream, String contentType) {
        try {
            ObjectId id = gridFsTemplate.store(inputStream, fileName, contentType);
            return id.toString();
        } catch (Exception e) {
            throw new FrameWorkException(Constants.CODE_SERVER_ERROR, "文件存储失败", e, true);
        }
    }

    public void writeFile(String id, HttpServletResponse response) {
        Query query = Query.query(Criteria.where("_id").is(id));
        // 查询单个文件
        GridFSFile gfsFile = gridFsTemplate.findOne(query);
        if (gfsFile != null) {
            try {
                GridFsResource gridFsResource = gridFsTemplate.getResource(gfsFile);
                response.setHeader("Content-Disposition", "attachment;filename=\"" + gfsFile.getFilename() + "\"");
                IOUtils.copy(gridFsResource.getInputStream(), response.getOutputStream());
            } catch (Exception e) {

            }
        } else {
            throw new FrameWorkException(Constants.CODE_SERVER_ERROR, "文件不存在", null, true);
        }
    }
}
