package polar.island.mybatis.support;
import java.util.List;
import java.util.Map;

public class MultiDataSourceConfig {
    private List<Map<String, String>> values;

    public List<Map<String, String>> getValues() {
        return values;
    }

    public void setValues(List<Map<String, String>> values) {
        this.values = values;
    }
}
