package polar.island.mybatis.support;

import org.mybatis.spring.mapper.ClassPathMapperScanner;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.core.type.filter.AssignableTypeFilter;
import org.springframework.core.type.filter.TypeFilter;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.Map;

public class MultiClassPathMapperScanner extends ClassPathMapperScanner {
    private Class<?> markerInterface;
    private Class<? extends Annotation> annotationClass;
    private String id;
    //是否扫描默认的dao
    private boolean scanDefaultDao = false;

    public MultiClassPathMapperScanner(BeanDefinitionRegistry registry) {
        super(registry);
    }

    @Override
    public void setMarkerInterface(Class<?> markerInterface) {
        super.setMarkerInterface(markerInterface);
        this.markerInterface = markerInterface;
    }

    public void setAnnotationClass(Class<? extends Annotation> annotationClass) {
        super.setAnnotationClass(annotationClass);
        this.annotationClass = annotationClass;
    }

    public void registerFilters() {
        boolean acceptAllInterfaces = true;
        if (this.annotationClass != null) {
            addIncludeFilter(new AnnotationTypeFilter(this.annotationClass) {
                @Override
                protected boolean matchSelf(MetadataReader metadataReader) {
                    boolean result = super.matchSelf(metadataReader);
                    if (!result) {
                        return false;
                    }
                    Map<String, Object> value = metadataReader.getAnnotationMetadata().getAnnotationAttributes(annotationClass.getName());
                    if (!scanDefaultDao && id != null) {
                        //进行扫描判断
                        if (value != null && value.containsKey("dataSource") && id.equals(value.get("dataSource"))) {
                            return true;
                        }
                        return false;
                    }
                    return true;
                }
            });
            acceptAllInterfaces = false;
        }

        if (this.markerInterface != null) {
            addIncludeFilter(new AssignableTypeFilter(this.markerInterface) {
                @Override
                protected boolean matchClassName(String className) {
                    return false;
                }
            });
            acceptAllInterfaces = false;
        }

        if (acceptAllInterfaces) {
            // default include filter that accepts all classes
            addIncludeFilter(new TypeFilter() {
                @Override
                public boolean match(MetadataReader metadataReader, MetadataReaderFactory metadataReaderFactory) throws IOException {
                    return true;
                }
            });
        }

        // exclude package-info.java
        addExcludeFilter(new TypeFilter() {
            @Override
            public boolean match(MetadataReader metadataReader, MetadataReaderFactory metadataReaderFactory) throws IOException {
                String className = metadataReader.getClassMetadata().getClassName();
                return className.endsWith("package-info");
            }
        });
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setScanDefaultDao(boolean scanDefaultDao) {
        this.scanDefaultDao = scanDefaultDao;
    }
}
