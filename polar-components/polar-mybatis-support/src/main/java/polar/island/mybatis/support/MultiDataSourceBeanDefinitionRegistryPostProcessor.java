package polar.island.mybatis.support;

import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.beans.BeansException;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.PropertyValue;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.TypedStringValue;
import org.springframework.beans.factory.support.*;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

@Component
public class MultiDataSourceBeanDefinitionRegistryPostProcessor implements BeanDefinitionRegistryPostProcessor {
    private Map<String, String> convert(ManagedMap<TypedStringValue, TypedStringValue> map) {
        Map<String, String> result = new HashMap<>(map.size());
        for (TypedStringValue key : map.keySet()) {
            result.put(key.getValue(), map.get(key).getValue());
        }
        return result;
    }

    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {

        if (!registry.isBeanNameInUse("multiDataSourceConfig")) {
            return;
        }
        BeanDefinition multiDataSourceConfig = registry.getBeanDefinition("multiDataSourceConfig");
        MutablePropertyValues mutablePropertyValues = multiDataSourceConfig.getPropertyValues();
        PropertyValue propertyValue = mutablePropertyValues.getPropertyValue("values");

        ManagedList<ManagedMap<TypedStringValue, TypedStringValue>> list = (ManagedList<ManagedMap<TypedStringValue, TypedStringValue>>) propertyValue.getValue();
        if (CollectionUtils.isEmpty(list)) {
            return;
        }

        for (ManagedMap<TypedStringValue, TypedStringValue> value : list) {
            //定义sqlSessionFactory
            Map<String, String> map = convert(value);
            String id = map.get("id");
            boolean scanDefaultDao = false;
            try {
                scanDefaultDao = Boolean.parseBoolean(map.get("default"));
            } catch (Exception e) {
            }
            if (StringUtils.isEmpty(id)) {
                throw new RuntimeException("唯一编号不能为空！");
            }
            String typeAliasesPackage = map.get("typeAliasesPackage");
            if (StringUtils.isEmpty(typeAliasesPackage)) {
                throw new RuntimeException("别名扫描包不能为空！");
            }
            String typeAliasesSuperType = map.get("typeAliasesSuperType");
            if (StringUtils.isEmpty(typeAliasesSuperType)) {
                typeAliasesSuperType = "polar.island.core.entity.BasicEntity";
            }
            String dataSource = map.get("dataSource");
            if (StringUtils.isEmpty(dataSource)) {
                throw new RuntimeException("数据源不能为空！");
            }
            String mapperLocations = map.get("mapperLocations");
            if (StringUtils.isEmpty(mapperLocations)) {
                throw new RuntimeException("资源文件路径不能为空！");
            }
            String configLocation = map.get("configLocation");
            if (StringUtils.isEmpty(configLocation)) {
                throw new RuntimeException("配置文件路径不能为空！");
            }
            String basePackage = map.get("basePackage");
            if (StringUtils.isEmpty(basePackage)) {
                throw new RuntimeException("dao基础路径不能为空！");
            }
            String annotationClass = map.get("annotationClass");
            if (StringUtils.isEmpty(annotationClass)) {
                annotationClass = "polar.island.mybatis.annotations.MybatisStore";
            }
            String factoryName = id + "SqlSessionFactory";
            BeanDefinition sqlSessionFactory = BeanDefinitionBuilder.genericBeanDefinition(SqlSessionFactoryBean.class)
                    .addPropertyValue("typeAliasesPackage", typeAliasesPackage)
                    .addPropertyValue("typeAliasesSuperType", typeAliasesSuperType)
                    .addPropertyValue("mapperLocations", mapperLocations)
                    .addPropertyValue("configLocation", configLocation)
                    .addPropertyReference("dataSource", dataSource)
                    .getBeanDefinition();
            registry.registerBeanDefinition(factoryName, sqlSessionFactory);

            BeanDefinition beanDefinition = BeanDefinitionBuilder.genericBeanDefinition(MultiMapperScannerConfigurer.class)
                    .addPropertyValue("sqlSessionFactoryBeanName", factoryName)
                    .addPropertyValue("basePackage", basePackage)
                    .addPropertyValue("annotationClass", annotationClass)
                    .addPropertyValue("id", id)
                    .addPropertyValue("scanDefaultDao", scanDefaultDao)
                    .getBeanDefinition();
            registry.registerBeanDefinition(id + "MapperScannerConfigurer", beanDefinition);
            String transactionManagerName = "transactionManager";
            if (!scanDefaultDao) {
                transactionManagerName = id + "TransactionManager";
            }
            BeanDefinition transactionManager = BeanDefinitionBuilder.genericBeanDefinition(DataSourceTransactionManager.class)
                    .addPropertyReference("dataSource", dataSource)
                    .getBeanDefinition();
            registry.registerBeanDefinition(transactionManagerName, transactionManager);
        }
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {

    }
}
