package polar.island.queue.consumer.listener;

import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.common.message.MessageExt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import polar.island.core.queue.receiver.MessageHandler;
import polar.island.core.queue.type.MessageType;

import java.util.List;

/**
 * 广播消息监听器，注意，每次只消费一条消息，以防各种问题!
 *
 * @author polarloves
 */
public class RocketBroadCastListener implements MessageListenerConcurrently {
    /**
     * 日志记录器
     **/
    public final Logger logger = LoggerFactory.getLogger(getClass());
    private MessageHandler messageHandler;
    private int retryTimes = 5;

    public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> list, ConsumeConcurrentlyContext consumeConcurrentlyContext) {
        logger.debug("begin custom broadcast message,message: {}",list);
        if (list.size() > 1) {
            logger.warn("receive broadcast size > 1 ,the real size is {} ,it may lost some messages! all message is :{}", list.size(), list);
        }
        MessageExt message = list.get(0);
        try {
            messageHandler.handlerBroadCastMessage(message.getTopic(), message.getTags(), new String(message.getBody()));
        } catch (Exception e) {
            logger.warn("handlerBroadCastMessage failed ,message: {},error:{}",message,e);
            if (message.getReconsumeTimes() > retryTimes) {
                logger.error("custom broadcast failed in {} times !");
                messageHandler.saveErrorMessage(message.getTopic(), message.getTags(), message.getBody(), MessageType.BroadCast,e);
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
            return ConsumeConcurrentlyStatus.RECONSUME_LATER;
        }
        return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
    }

    public MessageHandler getMessageHandler() {
        return messageHandler;
    }

    public void setMessageHandler(MessageHandler messageHandler) {
        this.messageHandler = messageHandler;
    }

    public int getRetryTimes() {
        return retryTimes;
    }

    public void setRetryTimes(int retryTimes) {
        this.retryTimes = retryTimes;
    }
}
