package polar.island.queue.producer.impl;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.common.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import polar.island.core.queue.sender.Sender;
import polar.island.core.serializer.Serializer;


public class RocketSenderImpl implements Sender {
    /**
     * 日志记录器
     **/
    public final Logger logger = LoggerFactory.getLogger(getClass());
    private Serializer serializer;

    private DefaultMQProducer producer;

    public void sendQueue(String topic, String tags, Object data) {
        try {
            Message message = new Message(topic, tags, serializer.serialize(data));
            producer.send(message);
        } catch (Exception e) {
            //记录错误日志
            logger.error("sendQueue failed, topic: {},tags: {},data: {},error :{}", topic, tags, data, e);
        }
    }

    public void sendTransactionQueue(String className, String methodName, Object data) {
        try {
            Message message = new Message(className, methodName, serializer.serialize(data));
            producer.send(message);
        } catch (Exception e) {
            //记录错误日志
            logger.error("sendTransactionQueue failed, className: {},method: {},data: {},error :{}", className, methodName, data, e);
        }
    }

    public void sendBroadCast(String topic, String tag, String data) {
        try {
            Message message = new Message(topic, tag, data.getBytes("UTF-8"));
            producer.send(message);
        } catch (Exception e) {
            //记录错误日志
            logger.error("sendBroadCast failed, topic: {},tag: {},data: {},error :{}", topic, tag, data, e);
        }
    }

    public DefaultMQProducer getProducer() {
        return producer;
    }

    public void setSerializer(Serializer serializer) {
        this.serializer = serializer;
    }

    public void setProducer(DefaultMQProducer producer) {
        this.producer = producer;
    }
}
