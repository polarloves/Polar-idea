package polar.island.redis.service.impl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import polar.island.redis.service.RedisListener;
import polar.island.redis.service.RedisService;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPubSub;
import redis.clients.jedis.exceptions.JedisException;
import java.util.*;

public class JedisServiceImpl implements RedisService {
    public static final int TIMEOUT_NEVER = 0;
    private Logger logger = LoggerFactory.getLogger(getClass());
    private JedisPool jedisPool;

    /**
     * 获取资源
     *
     * @return 获取的Jedis对象
     * @throws JedisException 获取资源失败
     */
    public Jedis getResource() throws JedisException {
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
        } catch (JedisException e) {
            logger.error("can not get resource from pool,error:{}", e);
            returnBrokenResource(jedis);
            throw e;
        }
        return jedis;
    }

    @Override
    public boolean monopolyLock(byte[] key, int timeout) {
        Jedis jedis = null;
        try {
            jedis = getResource();
            Long result = jedis.setnx(key, "1".getBytes());
            boolean returnValue = (result == 1);
            if (returnValue && timeout != TIMEOUT_NEVER) {
                jedis.expire(key, timeout);
            }
            return returnValue;
        } finally {
            returnResource(jedis);
        }
    }

    @Override
    public void monopolyUnLock(byte[] key) {
        del(key);
    }

    @Override
    public boolean parallelLock(byte[] key, long count, int timeout) {
        Jedis jedis = null;
        try {
            jedis = getResource();
            Long currentCount = jedis.incr(key);
            if (currentCount.compareTo(count) > 0) {
                if (timeout != TIMEOUT_NEVER) {
                    jedis.expire(key, timeout);
                }
                return false;
            } else {
                return true;
            }

        } finally {
            returnResource(jedis);
        }
    }

    @Override
    public void parallelUnLock(byte[] key, long count) {
        Jedis jedis = null;
        try {
            jedis = getResource();
            jedis.decrBy(key, count);
        } finally {
            returnResource(jedis);
        }
    }

    @Override
    public long autoIncreaseKey(byte[] key) {
        Jedis jedis = null;
        try {
            jedis = getResource();
            return jedis.incr(key);
        } finally {
            returnResource(jedis);
        }
    }

    @Override
    public void publish(String channel, String message) {
        Jedis jedis = null;
        try {
            jedis = getResource();
            jedis.publish(channel, message);
        } finally {
            returnResource(jedis);
        }
    }

    @Override
    public void subscribe(final String channel, final RedisListener listener) {
        final JedisPubSub j = new JedisPubSub() {
            public void onMessage(String channel, String message) {
                listener.onMessage(channel, message);
            }
        };
        new Thread() {
            @Override
            public void run() {
                Jedis jedis = null;
                try {
                    jedis = getResource();
                    jedis.subscribe(j, channel);
                } finally {
                    returnResource(jedis);
                }

            }
        }.start();
    }


    @Override
    public byte[] get(byte[] key) {
        Jedis jedis = null;
        try {
            jedis = getResource();
            return jedis.get(key);
        } finally {
            returnResource(jedis);
        }
    }



    @Override
    public boolean set(byte[] key, byte[] value) {
        Jedis jedis = null;
        try {
            jedis = getResource();
            jedis.set(key,value);
            return true;
        } finally {
            returnResource(jedis);
        }

    }


    @Override
    public  Set<byte[]> sget(byte[] key) {
        Jedis jedis = null;
        try {
            jedis = getResource();
            return jedis.smembers(key);
        } finally {
            returnResource(jedis);
        }
    }

  
    @Override
    public Long sadd(byte[] key, byte[]... value) {
        Jedis jedis = null;
        try {
            jedis = getResource();
            return jedis.sadd(key, value);
        } finally {
            returnResource(jedis);
        }
    }

    @Override
    public  List<byte[]> lget(byte[] key) {
        List<byte[]> value = null;
        Jedis jedis = null;
        try {
            jedis = getResource();
            if (jedis.exists(key)) {
                List<byte[]> bytes = jedis.lrange(key, 0, -1);
                if (bytes != null && bytes.size() > 0) {
                    value = new ArrayList(bytes.size());
                    for (byte[] b : bytes) {
                        value.add(b);
                    }
                }
            }
        } finally {
            returnResource(jedis);
        }
        return value;
    }

    

    @Override
    public Long llength(byte[] key) {
        Jedis jedis = null;
        try {
            jedis = getResource();
            return jedis.llen(key);
        } finally {
            returnResource(jedis);
        }
    }

    @Override
    public Long lrpush(byte[] key, byte[]... value) {
        Jedis jedis = null;
        try {
            jedis = getResource();
            return jedis.rpush(key, value);
        } finally {
            returnResource(jedis);
        }
    }

    public Long llpush(byte[] key, byte[]... value) {
        Jedis jedis = null;
        try {
            jedis = getResource();
            return jedis.lpush(key, value);
        } finally {
            returnResource(jedis);
        }
    }

    @Override
    public boolean lset(byte[] key, long index, byte[] value) {
        Jedis jedis = null;
        try {
            jedis = getResource();
            jedis.lset(key, index, value);
            return true;
        } finally {
            returnResource(jedis);
        }
    }

    @Override
    public byte[] llpop(byte[] key) {
        Jedis jedis = null;
        try {
            jedis = getResource();
            return jedis.lpop(key);
        } finally {
            returnResource(jedis);
        }
    }
    @Override
    public byte[] lrpop(byte[] key) {
        Jedis jedis = null;
        try {
            jedis = getResource();
            return jedis.rpop(key);
        } finally {
            returnResource(jedis);
        }
    }


    @Override
    public  Map<byte[], byte[]> hgetAll(byte[] key) {
        Jedis jedis = null;

        try {
            jedis = getResource();
            return jedis.hgetAll(key);
        } finally {
            returnResource(jedis);
        }
    }



    @Override
    public byte[] hget(byte[] key, byte[] field) {
        Jedis jedis = null;
        try {
            jedis = getResource();
            return jedis.hget(key,field);
        } finally {
            returnResource(jedis);
        }
    }


    @Override
    public boolean hset(byte[] key, Map<byte[], byte[]> value) {
        Jedis jedis = null;
        try {
            jedis = getResource();
            for (byte[] field : value.keySet()) {
                byte[] v = value.get(field);
                jedis.hset(key, field, v);
            }
            return true;
        } finally {
            returnResource(jedis);
        }
    }

    @Override
    public boolean hset(byte[] key, byte[] field, byte[] value) {
        Jedis jedis = null;
        try {
            jedis = getResource();
            jedis.hset(key, field, value);
            return true;
        } finally {
            returnResource(jedis);
        }
    }

    @Override
    public Long hlen(byte[] key) {
        Jedis jedis = null;
        try {
            jedis = getResource();
            return jedis.hlen(key);
        } finally {
            returnResource(jedis);
        }
    }

    @Override
    public  Set<byte[]> hkeys(byte[] key) {
        Jedis jedis = null;
        try {
            jedis = getResource();
            return jedis.hkeys(key);
        } finally {
            returnResource(jedis);
        }
    }

    @Override
    public boolean hdel(byte[] key, byte[] mapKey) {
        Jedis jedis = null;
        try {
            jedis = getResource();
            Long r = jedis.hdel(key, mapKey);
            return r > 0;
        } finally {
            returnResource(jedis);
        }
    }

    @Override
    public boolean hexists(byte[] key, byte[] mapKey) {
        Jedis jedis = null;
        try {
            jedis = getResource();
            return jedis.hexists(key, mapKey);
        } finally {
            returnResource(jedis);
        }
    }

    @Override
    public boolean del(byte[] key) {
        Jedis jedis = null;
        try {
            jedis = getResource();
            Long r = jedis.del(key);
            return r > 0;
        } finally {
            returnResource(jedis);
        }
    }

    @Override
    public boolean exists(byte[] key) {
        Jedis jedis = null;
        try {
            jedis = getResource();
            return jedis.exists(key);
        } finally {
            returnResource(jedis);
        }
    }

    @Override
    public void expire(byte[] key, int time) {
        Jedis jedis = null;
        try {
            jedis = getResource();
            jedis.expire(key, time);
        } finally {
            returnResource(jedis);
        }
    }

    @Override
    public  Set<byte[]> keys(byte[] pattern) {
        Jedis jedis = null;
        try {
            jedis = getResource();
            return jedis.keys(pattern);
        } finally {
            returnResource(jedis);
        }
    }


    public void setJedisPool(JedisPool jedisPool) {
        this.jedisPool = jedisPool;
    }

    /**
     * 释放资源
     *
     * @param jedis jedis对象
     */
    private void returnResource(Jedis jedis) {
        if (jedis != null) {
            jedis.close();
        }
    }

    /**
     * 归还资源
     *
     * @param jedis jedis对象
     */
    private void returnBrokenResource(Jedis jedis) {
        if (jedis != null) {
            jedis.close();
        }
    }


}
