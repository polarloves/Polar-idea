package polar.island.redis.serializer.impl;

import org.springframework.data.redis.serializer.RedisSerializer;
import polar.island.core.exception.SerializeException;
import polar.island.core.serializer.Serializer;

public class SerializerWrapper implements Serializer {
    private RedisSerializer serializer;
    @Override
    public byte[] serialize(Object t) throws SerializeException {
        return serializer.serialize(t);
    }
    public SerializerWrapper(RedisSerializer serializer){
        this.serializer=serializer;
    }
    @Override
    public Object deserialize(byte[] bytes) throws SerializeException {
        return serializer.deserialize(bytes);
    }

}
