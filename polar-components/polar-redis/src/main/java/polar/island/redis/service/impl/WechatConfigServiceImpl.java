package polar.island.redis.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.connection.jedis.JedisUtils;
import org.springframework.data.redis.serializer.RedisSerializer;
import polar.island.core.payment.config.WechatConfig;
import polar.island.core.serializer.Serializer;
import polar.island.redis.service.RedisService;


public class WechatConfigServiceImpl implements polar.island.core.payment.service.WechatConfigService {
    @Value("${cache.wxkey}")
    private String key = "polar_we_config";
    private RedisService redisService;
     private Serializer valueSerializer;
    @Override
    public WechatConfig get() {
        return (WechatConfig) valueSerializer.deserialize( redisService.get(key.getBytes()));
    }

    @Override
    public void save(WechatConfig wechatConfig) {
        redisService.set(key.getBytes(),valueSerializer.serialize(wechatConfig));
    }

    public Serializer getValueSerializer() {
        return valueSerializer;
    }

    public void setValueSerializer(Serializer valueSerializer) {
        this.valueSerializer = valueSerializer;
    }

    public void setRedisService(RedisService redisService) {
        this.redisService = redisService;
    }
}
