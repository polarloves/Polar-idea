package polar.island.redis.lock;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 分布式锁,使用于方法接口处,默认使用类名+方法名称进行加锁,当业务逻辑执行完毕后,其会自动释放锁
 *
 * @author polarloves
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface RunLock {
    /**
     * 获取锁时,最大的超时时间,超出这个时间会抛出异常,单位为毫秒
     *
     * @return 超时时间，单位毫秒
     */
    long timeout() default 2000;

    /**
     * 最多允许多少个进程获取此锁,业务场景常用于抢购等操作,其支持spEl,可以从方法参数中获取最大的进程,比如：商品抢购时，其最大数量可能和业务逻辑相关
     *
     * @return 最大并行人数
     */
    String maxActive() default "";

    /**
     * 锁的key,其支持spEl.
     *
     * @return 锁的key
     */
    String key() default "";

    /**
     * 获取不到锁时的重试时间间隔，单位为毫秒
     *
     * @return 重试时间间隔
     */
    long retryTimeLimite() default 200;

    /**
     * 当请求超时时，是否尝试重试，只有其值为true时，tryTimeLimite()才会生效
     *
     * @return 是否重试
     */
    boolean retry() default true;

    /**
     * 锁自动释放的时间(即锁空闲一定时间后自动释放),单位为秒,0表示不自动释放
     *
     * @return 释放的时间，单位秒
     */
    int releaseSecond() default 0;

    /**
     * 当此逻辑结束后，是否释放锁
     *
     * @return 是否释放
     */
    boolean releaseAfterComplete() default true;

    /**
     * 当业务逻辑发生异常时，是否释放锁
     *
     * @return 是否释放锁
     */
    boolean releaseOnError() default true;

    /**
     * 当业务繁忙时，提示信息，当独占锁时，表示此方法已经被占用，当并发锁时，表示占用的个数已经超过了设置的限制数.
     *
     * @return 业务繁忙的提示语
     */
    String busyMessage() default "";
}
