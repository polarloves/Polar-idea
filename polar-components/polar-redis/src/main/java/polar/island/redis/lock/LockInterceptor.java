package polar.island.redis.lock;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.CodeSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.stereotype.Component;
import polar.island.core.config.Constants;
import polar.island.core.exception.FrameWorkException;
import polar.island.core.util.ExceptionUtil;
import polar.island.redis.service.RedisService;
@Component
@Aspect
public class LockInterceptor {
    @Value("${lock.key}")
    private String prefix;
    private RedisService redisService;
    /**
     * 日志记录器
     **/
    public final Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * 分布式锁切面
     *
     * @param runLock 注解对象
     */
    @Pointcut(value = "@annotation(runLock)")
    public void pointcutName(RunLock runLock) {
    }

    /**
     * 执行方法体之前需要执行的操作
     *
     * @param jp      切点
     * @param runLock 注解对象
     */
    @Before(value = "pointcutName(runLock)")
    public void before(JoinPoint jp, RunLock runLock) {
        String key = getKey(runLock, jp);
        try {
            logger.debug("start get lock ... key: {}", key);
            long current = System.currentTimeMillis();
            long maxActive = getMaxActive(runLock, jp);
            long count = 0;
            while (true) {
                //尝试获取锁
                boolean locked;
                if (maxActive > 1) {
                    locked = redisService.parallelLock(key.getBytes(), maxActive, runLock.releaseSecond());
                } else {
                    locked = redisService.monopolyLock(key.getBytes(), runLock.releaseSecond());
                }
                if (!runLock.retry()) {
                    //不进行重试
                    if (locked) {
                        //获取到了锁，直接返回
                        break;
                    } else {
                        //未获取到锁,抛出繁忙异常
                        throw new FrameWorkException(Constants.CODE_BUSY, runLock.busyMessage().length() == 0 ? null : runLock.busyMessage(), null, false);
                    }
                }
                if (locked) {
                    logger.debug("get lock in {} times, key: {}", count + 1, key);
                    break;
                } else {
                    logger.debug("retry count:{},key: {}", count, key);
                    count++;
                }

                try {
                    Thread.sleep(runLock.retryTimeLimite());
                } catch (Exception ef) {
                    logger.warn("retry and wait failed! key: {} , error: {}", key, ef);
                }
                if ((System.currentTimeMillis() - runLock.timeout()) > current) {
                    //锁请求超时了
                    logger.error("get lock timeout ... key: {}", key);
                    throw new FrameWorkException(Constants.CODE_BUSY, runLock.busyMessage().length() == 0 ? null : runLock.busyMessage(), null, false);
                }

            }
        } catch (FrameWorkException e) {
            throw e;
        } catch (Exception e) {
            logger.error("get lock error ... key: {}", key);
            throw ExceptionUtil.assembleException(e);
        }

    }

    /**
     * 方法体返回数据后执行的操作
     *
     * @param jp      切点
     * @param runLock 注解对象
     */
    @AfterReturning(value = "pointcutName(runLock)")
    public void afterReturning(JoinPoint jp, RunLock runLock) {
        if (runLock.releaseAfterComplete()) {
            logger.debug("unlock key ,it is normal!");
            unLock(jp, runLock);
        }

    }

    /**
     * 方法体异常时，执行的操作,如果执行了此方法，则afterReturning不会执行
     *
     * @param jp      切点
     * @param runLock 注解对象
     * @param e       异常
     */
    @AfterThrowing(value = "pointcutName(runLock)", throwing = "e")
    public void afterThrowing(JoinPoint jp, RunLock runLock, Exception e) {
        if (e instanceof FrameWorkException && ((FrameWorkException) e).getCode().equals(Constants.CODE_BUSY)) {
            //请求超时时不释放锁
        } else {
            if (runLock.releaseOnError()) {
                logger.debug(" unlock key,because an unknow error happend !");
                unLock(jp, runLock);
            }
        }
    }

    /**
     * 获取分布式锁的key
     *
     * @param runLock 注解对象
     * @param jp      切点
     * @return 分布式的key
     */
    private String getKey(RunLock runLock, JoinPoint jp) {
        if (!runLock.key().equals("")) {
            return prefix + "_" + parseValue(jp, runLock.key(), String.class);
        } else {
            String className = jp.getTarget().getClass().getName();//获取目标类名
            String signature = jp.getSignature().toString();//获取目标方法签名
            String methodName = signature.substring(signature.lastIndexOf(".") + 1, signature.indexOf("("));
            return prefix + "_" + className.replace(".", "_") + "_" + methodName;
        }
    }

    /**
     * 根据内容，解析表达式，获取值
     *
     * @param jp     切点
     * @param value  表达式内容
     * @param tClass 需要获取的值的类型
     * @param <T>    需要获取的值的类型
     * @return 获取到的值
     */
    private <T> T parseValue(JoinPoint jp, String value, Class<T> tClass) {
        Object[] params = jp.getArgs();//获取目标方法体参数
        String[] paramNames = ((CodeSignature) jp
                .getSignature()).getParameterNames();
        StandardEvaluationContext itemContext = new StandardEvaluationContext();
        for (int i = 0; i < paramNames.length; i++) {
            itemContext.setVariable(paramNames[i], params[i]);
        }
        ExpressionParser parser = new SpelExpressionParser();
        Expression exp = parser.parseExpression(value);
        return exp.getValue(itemContext, tClass);
    }

    /**
     * 获取最大并发数量
     *
     * @param runLock 注解对象
     * @param jp      切点
     * @return 最大并发数量
     */
    private long getMaxActive(RunLock runLock, JoinPoint jp) {
        if (!runLock.maxActive().equals("")) {
            return parseValue(jp, runLock.maxActive(), Long.class);
        }
        return 1;
    }

    /**
     * 解锁
     *
     * @param jp      切点
     * @param runLock 注解对象
     */
    private void unLock(JoinPoint jp, RunLock runLock) {
        String key = getKey(runLock, jp);
        logger.debug("start unlock... key: {}", key);
        try {
            long maxActive = getMaxActive(runLock, jp);
            if (maxActive > 1) {
                //独占锁，独占解除
                redisService.parallelUnLock(key.getBytes(), maxActive);
            } else {
                //并发锁，并发解除
                redisService.monopolyUnLock(key.getBytes());
            }
            logger.debug("unLock successed! key: {}");
        } catch (Exception e) {
            logger.error("unLock failed... key: {}", key);
            throw new FrameWorkException(Constants.CODE_BUSY_RELEASE, null, e, true);
        }
    }

    public void setRedisService(RedisService redisService) {
        this.redisService = redisService;
    }
}
