package polar.island.redis.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.support.SimpleValueWrapper;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import polar.island.core.serializer.Serializer;
import polar.island.redis.serializer.impl.SerializerWrapper;
import polar.island.redis.service.RedisService;

import java.util.concurrent.Callable;

public class SpringRedisCache implements org.springframework.cache.Cache {
    private String name;
    private Logger logger = LoggerFactory.getLogger(getClass());
    private RedisService redisService;
    private Serializer keySerializer;
    private Serializer valueSerializer;
    private Serializer defaultKeySerializer = new SerializerWrapper(new StringRedisSerializer());
    private Serializer defaultValueSerializer = new SerializerWrapper(new JdkSerializationRedisSerializer());
    private String cahceKey;

    public SpringRedisCache(String name, String cahceKey, RedisService redisService, Serializer keySerializer, Serializer valueSerializer) {
        this.name = name;
        this.cahceKey = cahceKey;
        this.redisService = redisService;
        this.keySerializer = keySerializer;
        this.valueSerializer = valueSerializer;
    }

    /**
     * 获取 cache 名称
     */
    @Override
    public String getName() {
        return name;
    }

    /**
     * 获取真正的缓存提供方案
     */
    @Override
    public Object getNativeCache() {
        return null;
    }

    /**
     * 从缓存中获取 key 对应的值（包含在一个 ValueWrapper 中）
     **/
    @Override
    public ValueWrapper get(Object key) {
        ValueWrapper result = null;
        try {
            if (redisService.hexists(getKeySerializer().serialize(cahceKey + name), getKeySerializer().serialize(key))) {
                result = new SimpleValueWrapper(getValueSerializer().deserialize(
                        redisService.hget(getKeySerializer().serialize(cahceKey + name), getKeySerializer().serialize(key))));
            }
        } catch (Exception e) {
            logger.error("get name:{},key:{},error:{}", name, key, e);
            throw new polar.island.core.exception.CacheException(null, e);
        }
        return result;
    }

    /**
     * 从缓存中获取 key 对应的指定类型的值（4.0版本新增）
     */
    @SuppressWarnings("unchecked")
    @Override
    public <T> T get(Object key, Class<T> type) {
        T result = null;
        try {
            if (redisService.hexists(getKeySerializer().serialize(cahceKey + name), getKeySerializer().serialize(key))) {
                result = (T) new SimpleValueWrapper(getValueSerializer().deserialize(
                        redisService.hget(getKeySerializer().serialize(cahceKey + name), getKeySerializer().serialize(key))));
            }
        } catch (Exception e) {
            logger.error("getT name:{},key:{},error:{}", name, key, e);
            throw new polar.island.core.exception.CacheException(null, e);
        }
        return result;
    }

    /**
     * 从缓存中获取 key 对应的值，如果缓存没有命中，则添加缓存， 此时可异步地从 valueLoader 中获取对应的值（4.3版本新增）
     */
    @SuppressWarnings("unchecked")
    @Override
    public <T> T get(Object key, Callable<T> valueLoader) {
        T value = null;
        try {
            byte[] rKey = getKeySerializer().serialize(cahceKey + name);
            byte[] field = getKeySerializer().serialize(key);
            if (!redisService.hexists(rKey, field)) {
                value = valueLoader.call();
                redisService.hset(rKey, field, getValueSerializer().serialize(value));
            } else {
                value = (T) getValueSerializer().deserialize(redisService.hget(rKey, field));
            }
        } catch (Exception e) {
            logger.error("getT with valueLoader, name:{},key:{},error:{}", name, key, e);
            throw new polar.island.core.exception.CacheException(null, e);
        }
        return value;
    }

    /**
     * 缓存 key-value，如果缓存中已经有对应的 key，则替换其 value
     */
    @Override
    public void put(Object key, Object value) {
        if (value == null) {
            return;
        }
        try {
            byte[] rKey = getKeySerializer().serialize(cahceKey + name);
            byte[] field = getKeySerializer().serialize(key);
            redisService.hset(rKey, field, getValueSerializer().serialize(value));
        } catch (Exception e) {
            logger.error("put, name:{},key:{},value:{},error:{}", name, key, value, e);
            throw new polar.island.core.exception.CacheException(null, e);
        }

    }

    /**
     * 缓存 key-value，如果缓存中已经有对应的 key，则返回已有的 value，不做替换
     */
    @Override
    public ValueWrapper putIfAbsent(Object key, Object value) {
        if (value == null) {
            return new SimpleValueWrapper(null);
        }
        ValueWrapper result = null;
        try {
            byte[] rKey = getKeySerializer().serialize(cahceKey + name);
            byte[] field = getKeySerializer().serialize(key);
            if (redisService.hexists(rKey, field)) {
                value = getValueSerializer().deserialize(redisService.hget(rKey, field));
            } else {
                redisService.hset(rKey, field, getValueSerializer().serialize(value));
            }
            result = new SimpleValueWrapper(value);
        } catch (Exception e) {
            logger.error("putIfAbsent, name:{},key:{},value:{},error:{}", name, key, value, e);
            throw new polar.island.core.exception.CacheException(null, e);
        }
        return result;
    }

    /**
     * 从缓存中移除对应的 key
     */
    @Override
    public void evict(Object key) {
        try {
            byte[] rKey = getKeySerializer().serialize(cahceKey + name);
            byte[] field = getKeySerializer().serialize(key);
            redisService.hdel(rKey, field);
        } catch (Exception e) {
            logger.error("evict, name:{},key:{},error:{}", name, key, e);
            throw new polar.island.core.exception.CacheException(null, e);
        }

    }

    /**
     * 清空缓存
     */
    @Override
    public void clear() {
        try {
            redisService.del(getKeySerializer().serialize(cahceKey + name));
        } catch (Exception e) {
            logger.error("clear name:{},error:{}", name, e);
            throw new polar.island.core.exception.CacheException(null, e);
        }
    }

    public Serializer getKeySerializer() {
        if (keySerializer == null) {
            return defaultKeySerializer;
        }
        return keySerializer;
    }

    public void setKeySerializer(Serializer keySerializer) {
        this.keySerializer = keySerializer;
    }

    public Serializer getValueSerializer() {
        if (valueSerializer == null) {
            return defaultValueSerializer;
        }
        return valueSerializer;
    }

    public void setValueSerializer(Serializer valueSerializer) {
        this.valueSerializer = valueSerializer;
    }

    public void setRedisService(RedisService redisService) {
        this.redisService = redisService;
    }
}
