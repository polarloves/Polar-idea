package polar.island.redis.cache;

import org.springframework.cache.Cache;
import polar.island.core.serializer.Serializer;
import polar.island.redis.service.RedisService;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class SpringRedisCacheManager implements org.springframework.cache.CacheManager {

    private Set<String> names;
    private ConcurrentMap<String, SpringRedisCache> caches;
    private String cahceKey ="caches_jedis_spring_";
    private RedisService redisService;
    private Serializer keySerializer;
    private Serializer valueSerializer;
    public String getCahceKey() {
        return cahceKey;
    }

    public void setCahceKey(String cahceKey) {
        this.cahceKey = cahceKey;
    }

    public SpringRedisCacheManager () {
        names = new HashSet<String>();
        caches = new ConcurrentHashMap<String, SpringRedisCache>();
    }

    private synchronized void loadCaches(String name) {
        names.add(name);
        caches.put(name, new SpringRedisCache(name, cahceKey,redisService,keySerializer,valueSerializer));
    }

    @Override
    public Cache getCache(String name) {
        if (!names.contains(name)) {
            loadCaches(name);
        }
        return caches.get(name);
    }

    @Override
    public Collection<String> getCacheNames() {
        return names;
    }

    public void setRedisService(RedisService redisService) {
        this.redisService = redisService;
    }

    public void setKeySerializer(Serializer keySerializer) {
        this.keySerializer = keySerializer;
    }

    public void setValueSerializer(Serializer valueSerializer) {
        this.valueSerializer = valueSerializer;
    }
}
