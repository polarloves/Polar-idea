package polar.island.redis.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * redis服务接口
 */
public interface RedisService {
    /**
     * 独占锁定,锁定后只有一个用户可以
     *
     * @param key 键
     * @param timeout 超时时间
     * @return 锁定成功或者失败
     */
    public boolean monopolyLock(byte[] key, int timeout);

    /**
     * 独占解锁
     * @param key 需要解锁的key
     */
    public void monopolyUnLock(byte[] key);

    /**
     * 并行锁定,一次可以有多个用户获取锁
     *
     * @param key
     * @param count
     * @param timeout 过期时间
     * @return
     */
    public boolean parallelLock(byte[] key, long count, int timeout);

    /**
     * 并行解锁
     *
     * @param key
     * @param count
     * @return
     */
    public void parallelUnLock(byte[] key, long count);

    /**
     * 获取一个自动增长的key
     *
     * @param key 键
     * @return 获取到的值
     */
    public long autoIncreaseKey(byte[] key);

    /**
     * 使用redis发布消息
     *
     * @param channel 频道
     * @param message 消息
     */
    public void publish(String channel, String message);

    /**
     * 使用redis订阅消息
     *
     * @param channel  频道
     * @param listener 监听器
     */
    public void subscribe(String channel, RedisListener listener);


    /**
     * 获取值
     *
     * @param key        键
     * @return 值
     */
    public byte[] get(byte[] key);
    /**
     * 设置值
     *
     * @param key   键
     * @param value 值
     * @return 是否成功
     */
    public boolean set(byte[] key, byte[] value);


    /**
     * 获取set
     *
     * @param key 键
     * @return 值
     */
    public  Set<byte[]> sget(byte[] key);

    /**
     * 向Set缓存中添加值
     *
     * @param key   键
     * @param value 值
     * @return 设置的条目数量
     */
    public Long sadd(byte[] key, byte[]... value);

    /**
     * 获取集合中所有元素
     *
     * @param key        键
     * @return 集合中所有元素
     */
    public List<byte[]> lget(byte[] key);

    /**
     * 获取集合长度
     *
     * @param key 键
     * @return 集合长度
     */
    public Long llength(byte[] key);

    /**
     * 向集合结束添加值
     *
     * @param key   键
     * @param value 值
     * @return 添加后集合长度
     */
    public Long lrpush(byte[] key, byte[]... value);

    /**
     * 向集合开始添加值
     *
     * @param key   键
     * @param value 值
     * @return 添加后集合长度
     */
    public Long llpush(byte[] key, byte[]... value);

    /**
     * 设置集合元素
     *
     * @param key   键
     * @param index 集合索引
     * @param value 值
     * @return 是否成功
     */
    public boolean lset(byte[] key, long index, byte[] value);

    /**
     * 移除集合开始元素，并且返回
     *
     * @param key        键
     * @return 开始元素
     */
    public byte[] llpop(byte[] key);

    /**
     * 移除集合结束元素，并且返回
     *
     * @param key        键
     * @return 开始元素r
     */
    public byte[] lrpop(byte[] key);

    /**
     * 获取Map缓存
     *
     * @param key 键
     * @return 值
     */
    public  Map<byte[], byte[]> hgetAll(byte[] key);

    public byte[] hget(byte[] key, byte[] field);

    /**
     * 向Map缓存中添加值
     *
     * @param key   键
     * @param value 值
     * @return 设置的结果
     */
    public boolean hset(byte[] key, Map<byte[], byte[]> value);

    /**
     * 向Map缓存中添加值
     *
     * @param key   键
     * @param field map中的key
     * @param value 值
     * @return 设置的结果
     */
    public boolean hset(byte[] key, byte[] field, byte[] value);
    public Long hlen(byte[] key);
    /**
     * 获取map中所有key
     *
     * @param key      键
     * @return 所有的key
     */
    public Set<byte[]> hkeys(byte[] key);

    /**
     * 移除Map缓存中的值
     *
     * @param key    键
     * @param mapKey 值
     * @return 是否删除成功
     */
    public boolean hdel(byte[] key, byte[] mapKey);

    /**
     * 判断Map缓存中的Key是否存在
     *
     * @param key    键
     * @param mapKey 值
     * @return 是否存在
     */
    public boolean hexists(byte[] key, byte[] mapKey);

    /**
     * 删除缓存
     *
     * @param key 键
     * @return 是否删除成功
     */
    public boolean del(byte[] key);


    /**
     * 缓存是否存在
     *
     * @param key 键
     * @return 是否存在
     */
    public boolean exists(byte[] key);


    /**
     * 设置过期时间
     *
     * @param key  键
     * @param time 过期时间
     */
    public void expire(byte[] key, int time);

    /**
     * 获取key
     *
     * @param pattern  键
     * @return 所有的key
     */
    public  Set<byte[]> keys(byte[] pattern);

}
