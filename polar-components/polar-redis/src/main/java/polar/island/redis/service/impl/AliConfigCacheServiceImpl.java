package polar.island.redis.service.impl;

import org.springframework.beans.factory.annotation.Value;
import polar.island.core.payment.config.AliConfig;
import polar.island.core.payment.service.AliConfigService;
import polar.island.core.serializer.Serializer;
import polar.island.redis.service.RedisService;

public class AliConfigCacheServiceImpl implements AliConfigService {
    @Value("${cache.alikey}")
    private String key = "polar_ali_config";
    private Serializer valueSerializer;
    private RedisService redisService;
    @Override
    public AliConfig get() {
        return (AliConfig)valueSerializer.deserialize( redisService.get(key.getBytes()));
    }

    @Override
    public void save(AliConfig aliConfig) {
        redisService.set(key.getBytes(),valueSerializer.serialize(aliConfig));

    }

    public void setValueSerializer(Serializer valueSerializer) {
        this.valueSerializer = valueSerializer;
    }

    public void setRedisService(RedisService redisService) {
        this.redisService = redisService;
    }
}
