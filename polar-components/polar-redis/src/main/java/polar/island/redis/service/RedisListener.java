package polar.island.redis.service;

public interface RedisListener {
    public void onMessage(String channel, String message);
}
