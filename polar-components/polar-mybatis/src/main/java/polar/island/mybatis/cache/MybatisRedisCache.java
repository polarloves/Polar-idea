package polar.island.mybatis.cache;

import org.apache.ibatis.cache.Cache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import polar.island.core.exception.CacheException;
import polar.island.core.serializer.Serializer;
import polar.island.redis.serializer.impl.SerializerWrapper;
import polar.island.redis.service.RedisService;
import redis.clients.jedis.Jedis;

import java.util.Set;
import java.util.concurrent.locks.ReadWriteLock;

public class MybatisRedisCache implements Cache {
    private final ReadWriteLock readWriteLock = new DummyReadWriteLock ( );
    private String id;
    private String cahceKey = "caches_jedis_mybatis_";
    private Logger logger = LoggerFactory.getLogger ( getClass ( ) );
    private RedisService redisService;
    private Serializer keySerializer;
    private Serializer valueSerializer;
    private Serializer defaultKeySerializer=new SerializerWrapper(new StringRedisSerializer());
    private Serializer defaultValueSerializer=new SerializerWrapper(new JdkSerializationRedisSerializer());

    public MybatisRedisCache ( String id ) {
        if ( id == null ) {
            throw new IllegalArgumentException ( "Cache instances require an ID" );
        } else {
            this.id = id;
        }
    }

    public String getId () {
        return this.id;
    }

    public int getSize () {
        return redisService.hkeys(getKeySerializer().serialize(cahceKey + this.id )).size();
    }

    public void putObject ( final Object key , final Object value ) {
        redisService.hset(getKeySerializer().serialize(cahceKey + this.id ),getKeySerializer().serialize(key ),getValueSerializer().serialize(value ));
    }

    public Object getObject ( final Object key ) {
        return valueSerializer.deserialize(redisService.hget(getKeySerializer().serialize(cahceKey + this.id ),getKeySerializer().serialize(key ))) ;


    }

    public Object removeObject ( final Object key ) {
        return redisService.hdel(getKeySerializer().serialize(cahceKey + this.id ),getKeySerializer().serialize(key ));
    }

    public void clear () {
        redisService.del(getKeySerializer().serialize(cahceKey + this.id ));
    }

    public ReadWriteLock getReadWriteLock () {
        return this.readWriteLock;
    }

    public String toString () {
        return "MybatisRedisCache {" + this.id + "}";
    }


    public Serializer getKeySerializer() {
        if(keySerializer==null){
            return defaultKeySerializer;
        }
        return keySerializer;
    }

    public void setKeySerializer(Serializer keySerializer) {
        this.keySerializer = keySerializer;
    }

    public Serializer getValueSerializer() {
        if(valueSerializer==null){
            return defaultValueSerializer;
        }
        return valueSerializer;
    }

    public void setValueSerializer(Serializer valueSerializer) {
        this.valueSerializer = valueSerializer;
    }

    public void setRedisService(RedisService redisService) {
        this.redisService = redisService;
    }

}
