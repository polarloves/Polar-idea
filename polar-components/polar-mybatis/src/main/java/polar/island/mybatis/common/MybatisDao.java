package polar.island.mybatis.common;

import org.apache.ibatis.annotations.Param;
import polar.island.core.dao.BasicDao;

public interface MybatisDao<T, D> extends BasicDao <T, D> {

    public D selectOneById ( @Param ( value = "id" ) String id );


    public Long deleteByIdPhysical ( @Param ( value = "id" ) String id );


    public Long deleteByIdLogic ( @Param ( value = "id" ) String id );

}
