package polar.island.shiro.token;

import org.apache.shiro.authc.UsernamePasswordToken;

public class ShiroToken extends UsernamePasswordToken {

    /**
     * 是否校验用户密码
     */
    private boolean validatePassword = true;


    public ShiroToken() {
    }

    public ShiroToken(String username, char[] password) {
        super(username, password);
    }

    public ShiroToken(String username, String password) {
        super(username, password);
    }

    public ShiroToken(String username, char[] password, String host) {
        super(username, password, host);
    }

    public ShiroToken(String username, String password, String host) {
        super(username, password, host);
    }

    public ShiroToken(String username, char[] password, boolean rememberMe) {
        super(username, password, rememberMe);
    }

    public ShiroToken(String username, String password, boolean rememberMe) {
        super(username, password, rememberMe);
    }

    public ShiroToken(String username, char[] password, boolean rememberMe, String host) {
        super(username, password, rememberMe, host);
    }

    public ShiroToken(String username, String password, boolean rememberMe, String host) {
        super(username, password, rememberMe, host);
    }


    public boolean isValidatePassword() {
        return validatePassword;
    }

    public void setValidatePassword(boolean validatePassword) {
        this.validatePassword = validatePassword;
    }
}
