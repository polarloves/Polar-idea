package polar.island.shiro.service;

import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.session.ExpiredSessionException;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.DefaultSessionKey;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.support.DefaultSubjectContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import polar.island.core.config.Constants;
import polar.island.core.exception.ValidationException;
import polar.island.core.queue.sender.Sender;
import polar.island.core.security.entity.UserEntity;
import polar.island.core.security.service.SecurityService;
import polar.island.core.util.PropertieUtil;
import polar.island.redis.service.RedisService;
import polar.island.shiro.encry.EncryManager;
import polar.island.shiro.filter.DatabaseResouceFilter;
import polar.island.shiro.realm.ShiroPrincipal;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.Deque;
@Service(value = "shiroSecurityService")
public class ShiroSecurityService implements SecurityService {
    public final Logger logger = LoggerFactory.getLogger(getClass());
    @Resource(name = "shiroCacheManager")
    private CacheManager cacheManager;
    @Resource(name = "dataBaseResourceFilter")
    private DatabaseResouceFilter databaseResouceFilter;
    @Resource(name = "sessionManager")
    private SessionManager sessionManager;
    @Value ( value = "${shiro.cache.infoCacheName}" )
    private String infoCacheName;
    @Value ( value = "${shiro.cache.permissionCacheName}" )
    private String permissionCacheName;
    @Value ( value = "${shiro.cache.activeUserCacheName}" )
    private String activeUserCacheName;
    @Resource(name = "encryManager")
    private EncryManager encryManager;
    @Autowired(required = false)
    private RedisService redisService;
    @Autowired(required = false)
    private Sender sender;
    private long globalSessionTimeout=1800000;
    /**
     * 移除一个会话
     *
     * @param sessionId 会话编号
     * @param message   移除原因
     */
    public void kickOutSession(Serializable sessionId, String message) {
        if(sessionId==null){
            logger.warn ( "can not kick out session ,the sessionId is null!" );
            return;
        }
        try {
            Session session = sessionManager.getSession(new DefaultSessionKey(sessionId));
            if (session != null) {
                session.setAttribute(Constants.FORCE_DOWNLINE_FLAG, true);
                session.setAttribute(Constants.FORCE_DOWNLINE_MSG, message);
                if (session.getTimeout() < 0) {
                    //session永不过时，让其30分钟后过时
                    session.setTimeout(globalSessionTimeout);
                }
            }

        } catch (ExpiredSessionException e) {
            //用户过期，不做操作
        } catch (Exception e) {
            logger.error("踢出用户异常,error:{}", e);
        }
    }

    /**
     * 踢出用户，并且清空用户的权限、相信缓存
     *
     * @param userEntity
     * @param message
     */
    public void kickOutUser(UserEntity userEntity, String message) {
        if (userEntity == null || userEntity.getId() == null) {
            throw new ValidationException("用户不存在", null);
        }
        Cache<String, Deque<Serializable>> activeUserCache = cacheManager.getCache(activeUserCacheName);
        if (activeUserCache == null) {
            logger.warn("activeUserCache is null ,can not kick use out!");
            return;
        }
        Deque<Serializable> caches = activeUserCache.get(userEntity.getId() + "");
        if (caches != null) {
            while (caches.size() > 0) {
                Serializable kickoutSessionId = caches.removeFirst();
                kickOutSession(kickoutSessionId, message);
            }
        }
        if (userEntity != null) {
            //移除用户信息缓存
            String userName = userEntity.getUserName();
            cacheManager.getCache(infoCacheName).remove(userName);
            cacheManager.getCache(permissionCacheName).remove(userEntity.getId() + "");
        }
        //移除活跃用户
        activeUserCache.remove(userEntity.getId() + "");
        //更新活跃用户缓存
        if (caches != null) {
            activeUserCache.put(userEntity.getId() + "", caches);
        } else {
            activeUserCache.remove(userEntity.getId() + "");
        }
    }

    public void reloadResource(boolean publish) {
        if (publish) {
            if (sender == null) {
                redisService.publish(PropertieUtil.getSetting("LOAD_CHANNEL"), "1");
            } else {
                sender.sendBroadCast(PropertieUtil.getSetting("LOAD_CHANNEL"), "reloadResource", "1");
            }
        } else {
            reloadResourceTruely();
        }
    }

    private void reloadResourceTruely() {
        databaseResouceFilter.loadDataBasePermissions();
    }

    /**
     * 清除用户权限缓存
     *
     * @param userId 用户编号
     */
    public void clearUserPermissionCache(String userId) {
        cacheManager.getCache(permissionCacheName).remove(userId);
    }

    /**
     * 清除所有用户权限缓存
     */
    public void clearAllUserPermissionCache() {
        cacheManager.getCache(permissionCacheName).clear();
        cacheManager.getCache(infoCacheName).clear();
    }

    /**
     * 清除用户信息缓存，并且更新所有活跃用户的session
     *
     * @param userName   用户登录账户
     * @param userEntity 需要更新的实体类
     */
    public void clearUserInfoCache(String userName, UserEntity userEntity) {
        cacheManager.getCache(infoCacheName).remove(userName);
        if (userEntity == null) {
            return;
        }
        Cache<String, Deque<Serializable>> activeUserCache = cacheManager.getCache(activeUserCacheName);
        Deque<Serializable> caches = activeUserCache.get(userEntity.getId() + "");
        if (caches == null || caches.size() == 0) {
            return;
        }
        //同步修改session中保存的用户信息
        for (Serializable serializable : caches) {
            //修改session
            Session session = null;
            try {
                session = sessionManager.getSession(new DefaultSessionKey(serializable));
            } catch (ExpiredSessionException e) {
                continue;
            }
            if (session != null) {
                PrincipalCollection principalCollection = (PrincipalCollection) session.getAttribute(DefaultSubjectContext.PRINCIPALS_SESSION_KEY);
                ShiroPrincipal principal = (ShiroPrincipal) principalCollection.getPrimaryPrincipal();
                principal.setUser(userEntity);
                session.setAttribute(DefaultSubjectContext.PRINCIPALS_SESSION_KEY, principalCollection);
            }
        }
    }

    public void clearAllUserInfoCache() {
        cacheManager.getCache(infoCacheName).clear();
    }

    @Override
    public String encryPassword ( String password , String salt ) {
        return encryManager.encry ( password,salt );
    }

    @Override
    public String generateSalt () {
        return encryManager.generateSalt ();
    }


}