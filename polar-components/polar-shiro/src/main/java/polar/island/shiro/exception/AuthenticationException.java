package polar.island.shiro.exception;

public class AuthenticationException extends org.apache.shiro.authc.AuthenticationException {
    private static final long serialVersionUID = -2706346606153075352L;
    private String code;
    private String frameWorkMessage;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    public String getFrameWorkMessage() {
        return frameWorkMessage;
    }

    public void setFrameWorkMessage(String frameWorkMessage) {
        this.frameWorkMessage = frameWorkMessage;
    }

    public AuthenticationException(String code, String message) {
        super();
        this.code = code;
        this.frameWorkMessage = message;
    }

}
