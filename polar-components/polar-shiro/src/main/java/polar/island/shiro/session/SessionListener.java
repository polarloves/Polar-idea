package polar.island.shiro.session;

import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.support.DefaultSubjectContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import polar.island.shiro.realm.ShiroPrincipal;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.Serializable;
import java.util.Deque;

/**
 * 使用session.getAttribute(DefaultSubjectContext.PRINCIPALS_SESSION_KEY)来获取用户凭证。
 * 详见org.apache.shiro.mgt.DefaultSubjectDAO
 *
 * @author N
 */
@Component ( value = "sessionListener" )
public class SessionListener implements org.apache.shiro.session.SessionListener {
    // 活动的用户的缓存
    private Cache <String, Deque <Serializable>> onlineUserCache;
    // 活动的会话缓存
    @Resource ( name = "shiroCacheManager" )
    private CacheManager cacheManager;
    @Value ( value = "${shiro.cache.onlineUserCacheName}" )
    private String onlineUserCacheName;


    @PostConstruct
    public void initBean () {
        this.onlineUserCache = cacheManager.getCache ( onlineUserCacheName );
    }

    @Override
    public void onStart ( Session session ) {// 会话创建触发 已进入shiro的过滤连就触发这个方法

    }

    @Override
    public void onStop ( Session session ) {// 退出
        PrincipalCollection principalCollection = ( PrincipalCollection ) session.getAttribute ( DefaultSubjectContext.PRINCIPALS_SESSION_KEY );
        if ( principalCollection != null ) {
            ShiroPrincipal principal = ( ShiroPrincipal ) principalCollection.getPrimaryPrincipal ( );
            if ( principal != null ) {
                Deque <Serializable> sessions = onlineUserCache.get ( principal.getUser ( ).getId ( ) + "" );
                if ( sessions != null && !sessions.isEmpty ( ) ) {
                    sessions.remove ( session.getId ( ) );
                    onlineUserCache.put ( principal.getUser ( ).getId ( ) + "" , sessions );
                }
            }
        }
    }

    @Override
    public void onExpiration ( Session session ) {// 会话过期时触发
        PrincipalCollection principalCollection = ( PrincipalCollection ) session.getAttribute ( DefaultSubjectContext.PRINCIPALS_SESSION_KEY );
        if ( principalCollection != null ) {
            ShiroPrincipal principal = ( ShiroPrincipal ) principalCollection.getPrimaryPrincipal ( );
            if ( principal != null ) {
                Deque <Serializable> sessions = onlineUserCache.get ( principal.getUser ( ).getId ( ) + "" );
                if ( sessions != null && !sessions.isEmpty ( ) ) {
                    sessions.remove ( session.getId ( ) );
                    onlineUserCache.put ( principal.getUser ( ).getId ( ) + "" , sessions );
                }
            }
        }
    }

}
