package polar.island.shiro.filter;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.servlet.AdviceFilter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import polar.island.core.security.service.SecurityService;
import polar.island.shiro.realm.ShiroPrincipal;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.Serializable;
import java.util.Deque;

/**
 * 并发人数控制，拦截其登录请求
 *
 * @author N
 */
@Component ( value = "concurrentControlFilter" )
public class ConcurrentControlFilter extends AdviceFilter {
    // 活动的用户的缓存
    private Cache <String, Deque <Serializable>> onlineUserCache;
    @Value ( value = "${shiro.session.maxSession}" )
    private int maxSession = 1;
    @Resource ( name = "shiroSecurityService" )
    private SecurityService securityService;
    @Resource ( name = "shiroCacheManager" )
    private CacheManager cacheManager;
    @Value ( value = "${shiro.cache.onlineUserCacheName}" )
    private String onlineUserCacheName;

    @PostConstruct
    public void init () {
        onlineUserCache = cacheManager.getCache ( onlineUserCacheName );
    }

    @Override
    protected void postHandle ( ServletRequest request , ServletResponse response ) throws Exception {
        Subject subject = SecurityUtils.getSubject ( );
        if ( !subject.isAuthenticated ( ) && !subject.isRemembered ( ) ) {
            // 如果没有登录，直接进行之后的流程
            return;
        }
        ShiroPrincipal principal = ( ShiroPrincipal ) subject.getPrincipal ( );
        String userId = principal.getUser ( ).getId ( ) + "";
        Deque <Serializable> caches = onlineUserCache.get ( userId );
        if ( maxSession > 0 && caches != null && caches.size ( ) > 0 ) {
            boolean flag = false;
            while (caches.size ( ) > maxSession) {
                flag = true;
                Serializable kickoutSessionId = caches.removeLast ( );
                securityService.kickOutSession ( kickoutSessionId , "您的账号在另外一台设备登录，您已被强制下线." );
            }
            if ( flag ) {
                onlineUserCache.put ( userId , caches );
            }
        }
    }

}
