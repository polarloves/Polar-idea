package polar.island.shiro.matches;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import polar.island.core.config.Constants;
import polar.island.shiro.realm.ShiroPrincipal;
import polar.island.shiro.token.ShiroToken;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.Serializable;
import java.util.Date;
import java.util.Deque;
import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 凭证匹配器，如果密码输入次数大于设置的值，则抛出异常。
 *
 * @author PolarLoves
 */

public class ShiroLimitRetryCountMatcher extends HashedCredentialsMatcher {
    private Logger logger = LoggerFactory.getLogger(getClass());

    // AtomicInteger 线程安全的int类，passwordRetryCache为重新输入次数
    private Cache<String, FaultMessage> retryCache;

    private long lockTime = 60 * 1000 * 30l;
    //活动的用户的缓存
    private Cache<String, Deque<Serializable>> onLineUserCache;
    private String forceDownLineName = Constants.FORCE_DOWNLINE_MSG;
    private String forceDownLineFlag = Constants.FORCE_DOWNLINE_FLAG;
    @Resource(name = "shiroCacheManager")
    private CacheManager cacheManager;
    @Value ( value = "${shiro.cache.retryCacheName}" )
    private String retryCacheName;
    @Value ( value = "${shiro.cache.onlineUserCacheName}" )
    private String onlineUserCacheName;
    @Value ( value = "${shiro.session.retryCount}" )
    private int retryCout;


    private static class FaultMessage implements Serializable {
        private static final long serialVersionUID = 1726833032596862634L;
        private AtomicInteger num;
        private long lastTime;
    }

    @PostConstruct
    public void  init(){
        retryCache = cacheManager.getCache(retryCacheName);
        onLineUserCache = cacheManager.getCache(onlineUserCacheName);
    }

    public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
        ShiroToken realToken = (ShiroToken) token;
        String username = (String) token.getPrincipal();
        boolean matches = false;
        if (realToken.isValidatePassword()) {
            matches = super.doCredentialsMatch(token, info);
        } else {
            //不校验密码，则返回true
            matches = true;
        }
        if (retryCout > 0) {
            boolean faultMessageExist = false;
            FaultMessage faultMessage = retryCache.get(username);
            if (faultMessage == null) {
                faultMessage = new FaultMessage();
                faultMessage.num = new AtomicInteger(0);
                faultMessage.lastTime = new Date().getTime();
                faultMessageExist = false;
            } else {
                if (faultMessage.lastTime + lockTime < new Date().getTime()) {
                    // 已经过期啦~
                    faultMessage = new FaultMessage();
                    faultMessage.num = new AtomicInteger(0);
                    faultMessage.lastTime = new Date().getTime();
                    retryCache.remove(username);
                } else {
                    faultMessageExist = true;
                }
            }
            if (faultMessage.num.intValue() >= retryCout) {
                //账号被锁定
                long time = (faultMessage.lastTime + lockTime - new Date().getTime()) / 1000 / 60;
                throw new polar.island.shiro.exception.AuthenticationException(Constants.CODE_LOCKED_USER, "账号已经被锁定,将于" + time + "分钟内解锁");
            }
            if (matches) {
                // 账号密码匹配
                if (faultMessageExist) {
                    retryCache.remove(username);
                }
            } else {
                //存储失败次数
                int num = faultMessage.num.incrementAndGet();
                faultMessage.lastTime = new Date().getTime();
                retryCache.put(username, faultMessage);
                if (num == retryCout) {
                    //账号已经被禁用啦
                    long time = (faultMessage.lastTime + lockTime - new Date().getTime()) / 1000 / 60;
                    throw new polar.island.shiro.exception.AuthenticationException(Constants.CODE_LOCKED_USER, "账号已经被锁定,将于" + time + "分钟内解锁");
                } else {
                    throw new polar.island.shiro.exception.AuthenticationException(Constants.CODE_UNCORRENT_CREDENTIALS,
                            "账号密码不匹配，您还有" + (retryCout - num) + "次机会重新输入!");
                }

            }
        }
        if (matches) {
            //保存活跃用户
            String userId = (( ShiroPrincipal ) info.getPrincipals().getPrimaryPrincipal()).getUser().getId() + "";
            Deque<Serializable> caches = onLineUserCache.get(userId);
            if (caches == null) {
                caches = new LinkedList<Serializable>();
            }
            Subject subject = SecurityUtils.getSubject();
            Session session = subject.getSession();
            Serializable sessionId = session.getId();
            // 如果队列里没有此sessionId,放入队列
            if (!caches.contains(sessionId) && session.getAttribute(forceDownLineFlag) == null) {
                caches.push(sessionId);
            }
            onLineUserCache.put(userId, caches);
        }
        return matches;
    }
}
