package polar.island.shiro.cache;

import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.cache.CacheManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import polar.island.core.serializer.Serializer;
import polar.island.redis.serializer.impl.SerializerWrapper;
import polar.island.redis.service.RedisService;
import java.util.*;

/**
 * jedis缓存器，用于缓存用户凭证，session等数据。
 *
 * @author PolarLoves
 */
public class ShiroRedisCacheManager implements CacheManager {
    private RedisService redisService;
    private Serializer keySerializer;
    private Serializer valueSerializer;
    private Serializer defaultKeySerializer = new SerializerWrapper(new StringRedisSerializer());
    private Serializer defaultValueSerializer = new SerializerWrapper(new JdkSerializationRedisSerializer());

    private String cahceKey = "caches_jedis_shiro_";

    public String getCahceKey() {
        return cahceKey;
    }

    public void setCahceKey(String cahceKey) {
        this.cahceKey = cahceKey;
    }

    public <K, V> Cache<K, V> getCache(String arg0) throws CacheException {
        return new JedisCache<K, V>(cahceKey + arg0);
    }

    public class JedisCache<K, V> implements Cache<K, V> {
        private Logger logger = LoggerFactory.getLogger(getClass());
        private String name;

        public JedisCache(String name) {
            this.name = name;
        }

        public void clear() throws CacheException {
            redisService.del(getKeySerializer().serialize(name));
        }

        @SuppressWarnings("unchecked")
        public V get(K key) throws CacheException {
            try {
                return (V) getValueSerializer().deserialize(redisService.hget(getKeySerializer().serialize(name), getKeySerializer().serialize(key)));
            } catch (Exception e) {
                logger.error("get name:{},key:{},error:{}", name, key, e);
                throw new polar.island.core.exception.CacheException(null, e);
            }

        }

        @SuppressWarnings("unchecked")

        public Set<K> keys() {
            Set<K> keys = new HashSet<K>();
            try {
                Set<byte[]> set =redisService.hkeys(getKeySerializer().serialize(name));
                for (byte[] key : set) {
                    keys.add((K) getKeySerializer().serialize(key));
                }
                logger.debug("keys {} {} ", name, keys);
                return keys;
            } catch (Exception e) {
                logger.error("keys ,name:{},error:{}", name, e);
                throw new polar.island.core.exception.CacheException(null, e);
            }
        }

        public V put(K key, V value) throws CacheException {
            if (key == null) {
                return null;
            }
            try {
                redisService.hset(getKeySerializer().serialize(name), getKeySerializer().serialize(key), getValueSerializer().serialize(value));
            } catch (Exception e) {
                logger.error("put name:{},key:{},value:{},error:{}", name, key, value, e);
                throw new polar.island.core.exception.CacheException(null, e);
            }
            return value;
        }

        @SuppressWarnings("unchecked")
        public V remove(K key) throws CacheException {
            V value = null;
            try {
                value = (V) getValueSerializer().deserialize(redisService.hget(getKeySerializer().serialize(name), getKeySerializer().serialize(key)));
                redisService.hdel(getKeySerializer().serialize(name), getKeySerializer().serialize(key));
                logger.debug("remove {} {}", name, key);
            } catch (Exception e) {
                logger.warn("remove ,name:{},key:{},error:{}", name, key, e);
                throw new polar.island.core.exception.CacheException(null, e);
            }
            return value;
        }

        public int size() {
            int size = 0;
            try {
                size = redisService.hlen(getKeySerializer().serialize(name)).intValue();
                logger.debug("size {} {} ", name, size);
                return size;
            } catch (Exception e) {
                logger.error("size ,name:{},error:{}", name, e);
                throw new polar.island.core.exception.CacheException(null, e);
            }
        }

        @SuppressWarnings("unchecked")

        public Collection<V> values() {
            List<V> vals = new ArrayList<V>();
            try {

                Map<byte[],byte[]> col = redisService.hgetAll(getKeySerializer().serialize(name));
                for (byte[] val : col.keySet()) {
                    vals.add((V) getValueSerializer().serialize(col.get(val)));
                }
                logger.debug("values {} {} ", name, vals);
                return vals;
            } catch (Exception e) {
                logger.error("values ,name:{},error:{}", name, e);
                throw new polar.island.core.exception.CacheException(null, e);
            }
        }

    }


    public Serializer getKeySerializer() {
        if (keySerializer == null) {
            return defaultKeySerializer;
        }
        return keySerializer;
    }

    public void setKeySerializer(Serializer keySerializer) {
        this.keySerializer = keySerializer;
    }

    public Serializer getValueSerializer() {
        if (valueSerializer == null) {
            return defaultValueSerializer;
        }
        return valueSerializer;
    }

    public void setValueSerializer(Serializer valueSerializer) {
        this.valueSerializer = valueSerializer;
    }

    public void setRedisService(RedisService redisService) {
        this.redisService = redisService;
    }

}
