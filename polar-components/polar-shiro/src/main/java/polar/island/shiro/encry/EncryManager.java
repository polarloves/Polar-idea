package polar.island.shiro.encry;

import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component(value = "encryManager")
public class EncryManager {
    @Value ( "${shiro.pwd.algorithmName}" )
    private String algorithmName;
    @Value ( "${shiro.pwd.hashIterations}" )
    private int hashIterations;

    public EncryManager () {

    }

    public String encry ( String password , String salt ) {
        String newPassword = new SimpleHash ( algorithmName , password , ByteSource.Util.bytes ( salt ) , hashIterations )
                .toHex ( );
        return newPassword;
    }

    /**
     * 生成一个随机的盐值
     *
     * @return
     */
    public String generateSalt () {
        return UUID.randomUUID ( ).toString ( ).replace ( "-" , "" ).toUpperCase ( );
    }

}
