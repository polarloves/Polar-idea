package polar.island.shiro.filter;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.PathMatchingFilter;
import org.springframework.stereotype.Component;
import polar.island.core.exception.NoPermissionException;
import polar.island.core.exception.NotLoginException;
import polar.island.core.security.service.ResourceService;

import javax.annotation.Resource;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component ( value = "dataBaseResourceFilter" )
public class DatabaseResouceFilter extends PathMatchingFilter {
    @Resource ( name = "resourceService" )
    private ResourceService resourceService;
    private Map <String, List <String>> sourcePermissions;

    public void loadDataBasePermissions () {
        // 从数据库读取资源权限列表
        sourcePermissions = resourceService.resourcePermissions ( );
    }

    private List <String> findRequiredPermissions ( ServletRequest request ) {
        List <String> result = new ArrayList <> ( );
        Map <String, List <String>> sourcePermissions = this.sourcePermissions;
        if ( sourcePermissions != null && sourcePermissions.size ( ) > 0 ) {
            for (String path : sourcePermissions.keySet ( )) {
                if ( pathsMatch ( path , request ) ) {
                    result.addAll ( sourcePermissions.get ( path ) );
                }
            }
        }
        return result;
    }

    @Override
    protected boolean onPreHandle ( ServletRequest request , ServletResponse response , Object mappedValue )
            throws Exception {
        Subject currentUser = SecurityUtils.getSubject ( );
        if ( sourcePermissions != null && sourcePermissions.size ( ) > 0 ) {
            List <String> requiredPermissions = findRequiredPermissions ( request );
            if ( requiredPermissions.size ( ) > 0 ) {
                //需要校验权限
                if ( !currentUser.isAuthenticated ( ) ) {
                    // 用户未登录
                    request.setAttribute ( "exception" , new NotLoginException ( null , null ) );
                } else {
                    // 开始验证权限
                    boolean flag = false;
                    for (String permission : requiredPermissions) {
                        if ( !currentUser.isPermitted ( permission ) ) {
                            flag = true;
                            break;
                        }
                    }
                    if ( flag ) {
                        // 没有权限访问
                        request.setAttribute ( "exception" , new NoPermissionException ( null , null ) );
                    }
                }
            }
        }
        return true;
    }
}