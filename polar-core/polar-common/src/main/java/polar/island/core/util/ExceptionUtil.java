package polar.island.core.util;

import polar.island.core.config.Constants;
import polar.island.core.exception.FrameWorkException;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ExceptionUtil {
    private ExceptionUtil() {

    }

    public static String getExceptionAllinformation(Exception e) {
        if (e == null) {
            return "";
        }
        StringWriter stringWriter = new StringWriter();
        e.printStackTrace(new PrintWriter(stringWriter));
        return stringWriter.toString();
    }

    public static RuntimeException assembleException(Throwable e) {
        if (e instanceof FrameWorkException) {
            return (FrameWorkException) e;
        }
        RuntimeException ru=null;
        if (e.getCause() != null) {
            ru=assembleException(e.getCause());
        }
        FrameWorkException ex = new FrameWorkException(Constants.CODE_SERVER_ERROR, null, ru, true, e.getClass().getName() + ":" + e.getMessage());
        //设置堆栈信息
        ex.setStackTrace(e.getStackTrace());
        return ex;
    }

}
