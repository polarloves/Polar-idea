package polar.island.core.exception;

import polar.island.core.config.Constants;

public class SerializeException extends FrameWorkException {
    public SerializeException(Exception causeMessage) {
        super(Constants.CODE_SERVER_ERROR, null, causeMessage,true);
    }
    public SerializeException(String msg) {
        super(Constants.CODE_SERVER_ERROR, msg, null,true);
    }
}
