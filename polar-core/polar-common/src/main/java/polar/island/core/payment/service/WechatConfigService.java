package polar.island.core.payment.service;

import polar.island.core.payment.config.AliConfig;
import polar.island.core.payment.config.WechatConfig;

public interface WechatConfigService {
    public WechatConfig get();

    public void save(WechatConfig wechatConfig);
}
