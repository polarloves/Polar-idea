package polar.island.core.file;

import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;

public interface FileService {
    public String storeFile(String fileName, InputStream inputStream, String contentType);

    public void writeFile(String id, HttpServletResponse response);
}
