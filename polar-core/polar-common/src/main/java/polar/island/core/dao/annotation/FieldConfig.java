package polar.island.core.dao.annotation;

import polar.island.core.dao.types.Scope;
import polar.island.core.dao.types.StyleType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface FieldConfig {

    MatchStyle[] styles() default {
            //作为查询列表时的条件
            @MatchStyle(type = StyleType.SELECT_MULTI),
            //作为查询单个时的条件
            @MatchStyle(type = StyleType.SELECT_ONE),
            //作为删除时的条件
            @MatchStyle(type = StyleType.DELETE),
            //作为更新时的条件
            @MatchStyle(type = StyleType.UPDATE)
    };

    Scope[] scope() default {Scope.ALL};

    /**
     * 是否可以排序
     *
     * @return
     */
    boolean sortable() default true;

    /**
     * 列表查询时，是否返回数据
     *
     * @return
     */
    boolean queryList() default true;

    /**
     * 详情查询时，是否返回数据
     *
     * @return
     */
    boolean queryDetail() default true;

    /**
     * 更新单个时，是否可被更新
     *
     * @return
     */
    boolean updateField() default true;

    /**
     * 更新全部时是否可被更新
     * @return
     */
    boolean updateAll() default true;
}
