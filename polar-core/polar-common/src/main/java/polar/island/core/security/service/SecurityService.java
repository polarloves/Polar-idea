package polar.island.core.security.service;

import polar.island.core.security.entity.UserEntity;

import java.io.Serializable;

public interface SecurityService {
    /**
     * 移除一个会话
     *
     * @param sessionId 会话编号
     * @param message   移除原因
     */
    public void kickOutSession( Serializable sessionId, String message);
    /**
     * 踢出用户，并且清空用户的权限、相信缓存
     *
     * @param userEntity
     * @param message
     */
    public void kickOutUser( UserEntity userEntity, String message);

    public void reloadResource(boolean publish);


    /**
     * 清除用户权限缓存
     *
     * @param userId 用户编号
     */
    public void clearUserPermissionCache(String userId);

    /**
     * 清除所有用户权限缓存
     */
    public void clearAllUserPermissionCache();

    /**
     * 清除用户信息缓存，并且更新所有活跃用户的session
     *
     * @param userName   用户登录账户
     * @param userEntity 需要更新的实体类
     */
    public void clearUserInfoCache(String userName, UserEntity userEntity);

    public void clearAllUserInfoCache();

    public String encryPassword( String password , String salt);

    public String generateSalt( );

}
