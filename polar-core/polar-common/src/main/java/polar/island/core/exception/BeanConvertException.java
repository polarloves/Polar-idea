package polar.island.core.exception;

import polar.island.core.config.Constants;


/**
 * 实体转换异常。
 *
 * @author PolarLoves
 */
public class BeanConvertException extends FrameWorkException {
    private static final long serialVersionUID = 6649998389051775763L;

    public BeanConvertException(String message, Exception causeMessage) {
        super(Constants.CODE_BEAN_CONVERT, message, causeMessage);
    }

}
