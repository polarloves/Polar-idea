package polar.island.core.queue.type;

public enum MessageType {
    Transaction, Queue, BroadCast
}
