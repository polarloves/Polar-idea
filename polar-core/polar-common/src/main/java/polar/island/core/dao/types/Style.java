package polar.island.core.dao.types;

public enum Style {
    EQUALS, LESS, LESS_EQUAL, BIG_EQUAL, BIG, LIKE, REGEX, UNDEFINED, NOTNULL
}
