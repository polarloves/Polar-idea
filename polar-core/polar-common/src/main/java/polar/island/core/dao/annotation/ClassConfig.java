package polar.island.core.dao.annotation;

import polar.island.core.dao.types.IdType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ClassConfig {
    /**
     * 逻辑删除时，无效字段
     *
     * @return
     */
    String invalidValue() default "0";

    /**
     * 逻辑删除时,无效字段名称,其指的是数据库字段名称
     *
     * @return
     */
    String invalidValueName() default "useable";

    /**
     * 逻辑删除时,无效字段的类型
     *
     * @return
     */
    Class invalidValueClass() default int.class;

    /**
     * 是否可以逻辑删除
     *
     * @return
     */
    boolean logicAble() default false;

    /**
     * 表征此类在javaBean中,id的字段名称
     *
     * @return
     */
    String idField() default "id";

    IdType idType() default IdType.LONG;

    Class<?> detailClass();

    Class<?> listClass();

    String tableName();
}
