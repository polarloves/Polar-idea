package polar.island.core.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import polar.island.core.util.PropertieUtil;

import java.io.Serializable;

/**
 * 通用的返回数据实体类
 */

@ApiModel(value = "ResponseJson", description = "学生信息描述")
public class ResponseJson implements Serializable {
    @ApiModelProperty("状态码")
    private String code;
    @ApiModelProperty(value="数据",name="data")
    private Object data;
    @ApiModelProperty("消息内容")
    private String msg;
    @ApiModelProperty("数据条目")
    private Long count;

    public ResponseJson(String code) {
        this(code, null, null);
    }

    public ResponseJson(String code, Object data) {
        this(code, data, null);
    }

    public ResponseJson(String code, Object data, String msg) {
        this(code, data, msg, null);
    }

    public ResponseJson(String code, Object data, String msg, Long count) {
        this.code = code;
        this.data = data;
        this.msg = msg;
        this.count = count;
        if (this.msg == null) {
            this.msg = PropertieUtil.getMsg(this.code);
        }
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }
}
