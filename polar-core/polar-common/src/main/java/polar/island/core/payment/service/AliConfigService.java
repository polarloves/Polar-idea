package polar.island.core.payment.service;

import polar.island.core.payment.config.AliConfig;

public interface AliConfigService {
    public AliConfig get();

    public void save(AliConfig aliConfig);
}
