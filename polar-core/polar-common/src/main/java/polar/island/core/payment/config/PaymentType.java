package polar.island.core.payment.config;

/**
 * 支付类型,分为微信、支付宝
 */
public enum PaymentType {
    WeChatPay, AliPay
}
