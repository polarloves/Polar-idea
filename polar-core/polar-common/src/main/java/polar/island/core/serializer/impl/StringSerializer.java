package polar.island.core.serializer.impl;

import polar.island.core.exception.SerializeException;
import polar.island.core.serializer.Serializer;

public class StringSerializer implements Serializer {
    @Override
    public byte[] serialize(Object t) throws SerializeException {
        return ((String)t).getBytes();
    }

    @Override
    public Object deserialize(byte[] bytes) throws SerializeException {
        return new String(bytes);
    }
}
