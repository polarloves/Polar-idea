package polar.island.core.serializer.impl;

import org.springframework.core.convert.converter.Converter;
import org.springframework.core.serializer.support.DeserializingConverter;
import org.springframework.core.serializer.support.SerializingConverter;
import polar.island.core.exception.SerializeException;
import polar.island.core.serializer.Serializer;

public class JdkSerializer implements Serializer {
    private  Converter<Object, byte[]> serializer;
    private  Converter<byte[], Object> deserializer;
    public JdkSerializer(){
        serializer=  new SerializingConverter();
        deserializer=new DeserializingConverter();
    }
    @Override
    public byte[] serialize(Object t) throws SerializeException {
        return serializer.convert(t);
    }

    @Override
    public Object deserialize(byte[] bytes) throws SerializeException {
        return deserializer.convert(bytes);
    }
}