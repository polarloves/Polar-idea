package polar.island.core.security.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.ibatis.type.Alias;
import polar.island.core.config.Constants;
import polar.island.core.entity.BasicEntity;
import polar.island.core.validator.TAG;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Date;

/**
 * 用户的实体类。 用户基本信息管理
 *
 * @author PolarLoves
 */
@Alias(value = "userEntity")
@ApiModel(value = "polar.island.core.security.entity.UserEntity", description = "用户信息")
public class UserEntity extends BasicEntity implements Serializable {
    private static final long serialVersionUID = -7480139832109967015L;
    /**
     * 主键编号
     **/
    @TAG(value = {"updateAllById", "updateField"})
    @NotNull(message = "编号不能为空")
    @ApiModelProperty(value = "数据编号", example = "1")
    private Long id;
    /**
     * 有效性字段，有效值：1，无效值：0。
     **/
    @ApiModelProperty(value = "账号状态，有效值：1，无效值：0")
    private Integer state;
    /**
     * 用户名
     **/
    @TAG(value = {"add"})
    @NotNull(message = "用户名不能为空")
    @ApiModelProperty(value = "用户名")
    private String userName;
    /**
     * 密码
     **/
    @ApiModelProperty(value = "密码")
    private String password;
    /**
     * 登陆次数
     **/
    @ApiModelProperty(value = "登陆次数")
    private Long logCount;
    /**
     * 头像
     **/
    @ApiModelProperty(value = "头像")
    private String headUrl;
    /**
     * 昵称
     **/
    @ApiModelProperty(value = "昵称")
    private String nickName;
    /**
     * 最后一次登陆的IP
     **/
    @ApiModelProperty(value = "最后一次登陆的IP")
    private String logInIp;
    /**
     * 创建日期
     **/
    @ApiModelProperty(value = "创建日期")
    private Date createDate;
    @ApiModelProperty(value = "登录日期")
    private Date logInDate;
    /**
     * 手机号
     **/
    @ApiModelProperty(value = "手机号")
    @Pattern(regexp = Constants.REG_PHONE, message = "手机号格式不正确")
    private String phone;
    /**
     * 邮箱
     **/
    @ApiModelProperty(value = "邮箱")
    @Pattern(regexp = Constants.REG_EMAIL, message = "邮箱格式不正确")
    private String email;
    @ApiModelProperty(value = "所属机构编号")
    private String orgId;
    /**
     * 密码盐值
     **/
    private String salt;
    /**
     * 用户类型 ,1-普通用户，2-第三方账号
     **/
    private Integer userType;
    /**
     * 禁用用户原因
     */
    private String disableReason;

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getLogInDate() {
        return logInDate;
    }

    public void setLogInDate(Date logInDate) {
        this.logInDate = logInDate;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Long getLogCount() {
        return logCount;
    }

    public void setLogCount(Long logCount) {
        this.logCount = logCount;
    }

    public String getHeadUrl() {
        return headUrl;
    }

    public void setHeadUrl(String headUrl) {
        this.headUrl = headUrl;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getLogInIp() {
        return logInIp;
    }

    public void setLogInIp(String logInIp) {
        this.logInIp = logInIp;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public String getDisableReason() {
        return disableReason;
    }

    public void setDisableReason(String disableReason) {
        this.disableReason = disableReason;
    }
}