package polar.island.core.dao.annotation;

import polar.island.core.dao.types.Style;
import polar.island.core.dao.types.StyleType;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface MatchStyle {
    /**
     * 当值为空值时，是否匹配
     *
     * @return
     */
    boolean nullMatches() default false;

    /**
     * 匹配的风格
     *
     * @return
     */
    Style matchStyle() default Style.EQUALS;



    /**
     * 数据匹配的正则表达式
     *
     * @return
     */
    String regex() default "";


    /**
     * 匹配的类型
     *
     * @return
     */
    StyleType type() default StyleType.SELECT_MULTI;
}
