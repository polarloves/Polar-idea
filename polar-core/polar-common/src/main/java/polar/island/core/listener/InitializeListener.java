package polar.island.core.listener;

import org.springframework.context.ApplicationContext;

public interface InitializeListener {
    public void initialize(ApplicationContext context);
    public int sort();
}
