package polar.island.core.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import polar.island.core.json.GsonIgnore;
import springfox.documentation.annotations.ApiIgnore;

import java.io.Serializable;

/**
 * 基础实体类。
 * 
 * @author PolarLoves
 *
 */
public class BasicEntity implements Serializable {
	@JsonIgnore
	@GsonIgnore
	/** 当前页码 **/
	private int page = 1;
	/** 每页条目 **/
	@JsonIgnore
	@GsonIgnore
	private int rows = 10;
	/** 排序名称 **/
	@GsonIgnore
	@JsonIgnore
	private String sort;
	/** 排序名称 **/
	@GsonIgnore
	@JsonIgnore
	private String fieldName;
	/** 排序方式 **/
	@GsonIgnore
	@JsonIgnore
	private String order="ASC";
	@JsonIgnore
	// 获取MySql开始页码
	public int getPageStartNumber() {
		return (page - 1) * rows;
	}
	@JsonIgnore
	// 获取MySql偏移页面
	public int getPageOffsetNumber() {
		return rows;
	}

	public int getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(Integer rows) {
		this.rows = rows;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	/**
	 * 如果字段名称和实体类中不一致，则需要重写此方法
	 * 
	 * @return 排序的字段名称
	 */
	public String getSort() {
		return sort;
	}

	public void setSort(String sort) {
		this.sort = sort;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		if(order!=null&&order.equalsIgnoreCase("desc")){
			this.order ="DESC";
			return;
		}
		this.order = "ASC";
	}

}
