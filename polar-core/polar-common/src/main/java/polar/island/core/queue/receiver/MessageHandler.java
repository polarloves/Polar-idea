package polar.island.core.queue.receiver;


import polar.island.core.queue.type.MessageType;

/**
 * 消息分发器，由其进行消息的分发
 *
 * @author polarloves
 */
public interface MessageHandler {
    /**
     * 处理事务消息
     *
     * @param className  目标类名
     * @param methodName 方法名称
     * @param message    消息内容
     * @throws Exception 处理异常，所有的异常均抛出
     */
    public void handlerTransactionMessage(String className, String methodName, Object message) throws Exception;

    /**
     * 处理队列消息
     *
     * @param topic   消息的标题
     * @param tags    消息二级标题
     * @param message 消息内容
     * @throws Exception 处理异常，所有的异常均抛出
     */
    public void handlerQueueMessage(String topic, String tags, Object message) throws Exception;

    /**
     * 处理广播消息
     *
     * @param topic   消息的标题
     * @param tags    消息二级标题
     * @param message 消息内容
     * @throws Exception 处理异常，所有的异常均抛出
     */
    public void handlerBroadCastMessage(String topic, String tags, String message) throws Exception;

    /**
     * 保存处理异常的数据
     *
     * @param topic   消息的标题/目标类名
     * @param tags    消息二级标题/方法名称
     * @param message 消息内容
     * @param type    类型
     * @param e       错误信息
     */
    public void saveErrorMessage(String topic, String tags, byte[] message, MessageType type, Exception e);
}
