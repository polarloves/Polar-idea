package polar.island.core.dao.types;

public enum Scope {
    SELECT_MULTI, SELECT_ONE, DELETE, ALL,UPDATE
}