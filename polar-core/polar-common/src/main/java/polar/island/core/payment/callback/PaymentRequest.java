package polar.island.core.payment.callback;



import polar.island.core.payment.config.PaymentType;

import java.util.Map;

/**
 * 发起支付的回调接口
 */
public interface PaymentRequest {
    /**
     * 获取支付的唯一编号
     *
     * @param paymentType 支付类型，可选值：微信，支付宝
     * @param extendsArgs 一些拓展参数，可以用来传递一些其他参数,比如：用户编号等。
     * @return 支付的订单编号
     */
    public String generatePayId(PaymentType paymentType, Map<String, Object> extendsArgs);


    /**
     * 订单请求成功后处理业务流程，此处应该处理业务逻辑，如：将订单存入数据库中等
     *
     * @param extendsArgs 一些拓展参数，可以用来传递一些其他参数,比如：用户编号等。
     * @param money       支付金额,此处单位为分
     * @param paymentType 支付类型，可选值：微信，支付宝
     * @param payId       支付的订单编号
     */
    public void onPayRequest(Map<String, Object> extendsArgs, String money, PaymentType paymentType, String payId);
}
