package polar.island.core.payment.callback;


import polar.island.core.payment.config.PaymentType;

/**
 * 支付成功后回调接口
 */
public interface PaymentResponse {
    /**
     * 支付成功的回调,此处应该处理一些业务逻辑，如:更新订单状态等
     *
     * @param paymentType 支付类型，可选值：微信，支付宝
     * @param payId       支付的订单编号
     * @return 如果成功则返回true, 否则返回false
     */
    public boolean onPayResponse(PaymentType paymentType, String payId);
}
