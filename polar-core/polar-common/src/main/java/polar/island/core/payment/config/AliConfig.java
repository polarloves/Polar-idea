package polar.island.core.payment.config;

import java.io.Serializable;

public class AliConfig implements Serializable {
    /** 应用秘钥 **/
    private String privateKey;
    /** 支付宝公钥 **/
    private String publicKey;
    /** 服务网关 **/
    private String serverUrl;
    /** appId **/
    private String appId;

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }
}
