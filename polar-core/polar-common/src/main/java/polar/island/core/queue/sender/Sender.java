package polar.island.core.queue.sender;

public interface Sender {
    /**
     * 发送队列
     *
     * @param topic 一级标题
     * @param tags  二级标题
     * @param data  参数
     */
    public void sendQueue(String topic, String tags, Object data);

    /**
     * 发送队列
     *
     * @param className  目标方法名称
     * @param methodName 目标参数名称，此处要求有方法里面只有一个参数,并且参数可序列化,例如：insert
     * @param data       参数
     */
    public void sendTransactionQueue(String className, String methodName, Object data);

    /**
     * 发生广播数据
     *
     * @param topic 一级标题
     * @param tag   二级标题
     * @param data  数据
     */
    public void sendBroadCast(String topic, String tag, String data);
}
