package polar.island.core.payment.config;

import java.io.Serializable;

public class WechatConfig implements Serializable{
    /** appId **/
    private String appId;
    /** 商户号 **/
    private String mchId;
    /** 回调地址 **/
    private String noticeUrl;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getMchId() {
        return mchId;
    }

    public void setMchId(String mchId) {
        this.mchId = mchId;
    }

    public String getNoticeUrl() {
        return noticeUrl;
    }

    public void setNoticeUrl(String noticeUrl) {
        this.noticeUrl = noticeUrl;
    }
}
