package polar.island.core.dao.types;

public enum StyleType {
    SELECT_MULTI, SELECT_ONE, DELETE, UPDATE
}
