package polar.island.core.dao.types;

public enum IdType {
    LONG, STRING
}
