package polar.island.core.security.service;


import polar.island.core.security.entity.UserEntity;

import java.util.Set;

public interface UserService {
    /**
     * 根据用户名查找用户。
     *
     * @param userName 用户名。
     * @return 用户
     */
    public UserEntity selectUserByUserName ( String userName );

    /**
     * 获取用户的角色
     *
     * @param userId 用户编号
     * @return 用户角色
     */
    public Set<String> userRoles ( String userId );

    /**
     * 获取用户的权限
     *
     * @param userId 用户编号
     * @return 用户权限
     */
    public Set<String> userPermissions ( String userId );
}
