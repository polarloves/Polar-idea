package polar.island.core.serializer;

import polar.island.core.exception.SerializeException;

public interface Serializer {
    byte[] serialize(Object t) throws SerializeException;

   Object deserialize(byte[] bytes) throws SerializeException;
}
