package polar.island.core.security.service;

import java.util.List;
import java.util.Map;


public interface ResourceService {
    /**
     * 获取资源的所有权限
     *
     * @return 资源的所有权限列表
     */
    public Map <String, List <String>> resourcePermissions ();
}