package polar.island.core.queue.receiver.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.ReflectionUtils;
import polar.island.core.queue.err.QueueError;
import polar.island.core.queue.receiver.MessageHandler;
import polar.island.core.queue.type.MessageType;
import polar.island.core.util.BeansUtils;
import polar.island.core.util.CommonUtil;
import polar.island.core.util.ExceptionUtil;
import java.lang.reflect.Method;
import java.util.Date;


public class MessageHandlerImpl implements MessageHandler {
    /**
     * 日志记录器
     **/
    public final Logger logger = LoggerFactory.getLogger(getClass());
    /**
     * 队列消息发送错误时，保存的key
     */
    @Value("${queue.errorMessageKey}")
    private String errorMessageKey = "errorMessageKey";



    public void handlerTransactionMessage(String className, String methodName, Object message) throws Exception {
        Class c2 = Class.forName(className.replace("-", "."));
        Object obj = BeansUtils.getBean(c2);
        Class argClass = message.getClass();
        Method mh = ReflectionUtils.findMethod(obj.getClass(), methodName, argClass);
        ReflectionUtils.invokeMethod(mh, obj, message);
    }

    public void handlerQueueMessage(String topic, String tags, Object message) throws Exception {

    }

    public void handlerBroadCastMessage(String topic, String tags, String message) throws Exception {

    }

    public void saveErrorMessage(String topic, String tags, byte[] message, MessageType type, Exception e2) {
        String key = errorMessageKey;
        String field = CommonUtil.randomId();
        byte[] values = message;
        switch (type) {
            case Queue:
                key = key + "_Queue";
                break;
            case BroadCast:
                key = key + "_BroadCast";
                break;
            case Transaction:
                key = key + "_Transaction";
                break;
        }
        QueueError err = new QueueError();
        err.setErrMsg(ExceptionUtil.getExceptionAllinformation(e2));
        err.setData(values);
        err.setTag(tags);
        err.setTopic(topic);
        err.setType(type);
        err.setDate(new Date());
        logger.error("error happen,key:{},field:{},err:{}",key,field,err);
    }

}