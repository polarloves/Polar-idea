package polar.island.web.service;

import org.apache.commons.io.IOUtils;
import org.apache.ibatis.javassist.tools.reflect.CannotCreateException;
import org.springframework.stereotype.Service;
import polar.island.core.config.Constants;
import polar.island.core.exception.FrameWorkException;
import polar.island.core.file.FileService;
import polar.island.core.util.CommonUtil;
import polar.island.core.util.FileUtil;
import polar.island.core.util.PropertieUtil;
import polar.island.web.util.ResponseUtil;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URLEncoder;

@Service(value = "localFileService")
public class LocalFileService implements FileService {
    @Override
    public String storeFile(String fileName, InputStream inputStream, String contentType) {
        FileOutputStream out = null;
        try {
            File file2 = new File(PropertieUtil.getSetting("FILE_PATH") + "/" + CommonUtil.randomId() + "/" + fileName);
            if (!file2.getParentFile().exists()) {
                file2.getParentFile().mkdirs();
            }
            boolean flag = file2.createNewFile();
            if (!flag) {
                throw new CannotCreateException("文件无法被创建");
            }
            out = new FileOutputStream(file2);
            byte[] tmp = new byte[1024];
            int i = -1;
            while ((i = inputStream.read(tmp)) != -1) {
                out.write(tmp, 0, i);
            }
            return URLEncoder.encode(file2.getAbsolutePath(), "UTF-8");
        } catch (Exception e) {
            throw new FrameWorkException(Constants.CODE_FILE, "文件上传失败", e, false);
        } finally {
            IOUtils.closeQuietly(out);
            IOUtils.closeQuietly(inputStream);
        }
    }

    @Override
    public void writeFile(String id, HttpServletResponse response) {
        try {
            String name = id.substring(id.lastIndexOf( File.separator) + File.separator.length(), id.length());
            ResponseUtil.renderResponseFileHeader(name, response);
            FileUtil.writeFile(name, id, response.getOutputStream());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
