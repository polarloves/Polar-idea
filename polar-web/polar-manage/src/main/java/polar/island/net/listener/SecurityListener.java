package polar.island.net.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import polar.island.core.listener.InitializeListener;
import polar.island.core.security.service.SecurityService;
import polar.island.core.util.PropertieUtil;
import polar.island.redis.service.RedisListener;
import polar.island.redis.service.RedisService;
@Component
public class SecurityListener implements InitializeListener {
    @Autowired (required = false)
    private SecurityService securityService;
    @Autowired (required = false)
    private RedisService redisService;
    @Override
    public void initialize( ApplicationContext context) {
        if (securityService != null) {
            redisService.subscribe(PropertieUtil.getSetting("LOAD_CHANNEL"),new RedisListener(){
                @Override
                public void onMessage(String channel, String message) {
                    securityService.reloadResource(false);
                }
            });
        }
    }

    @Override
    public int sort () {
        return 5;
    }
}
