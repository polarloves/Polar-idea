package polar.island.net.queue;

import org.springframework.beans.factory.annotation.Autowired;
import polar.island.core.queue.receiver.impl.MessageHandlerImpl;
import polar.island.core.security.service.SecurityService;

public class WebQueueHandler extends MessageHandlerImpl {
    @Autowired(required = false)
    private SecurityService securityService;

    public void handlerBroadCastMessage(String topic, String tags, String message) throws Exception {
        if (tags.equals("reloadResource")) {
            securityService.reloadResource(false);
        }
    }

}
