package polar.island.net.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.NamedThreadLocal;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;

/**
 * 日志拦截器
 */
public class LogInterceptor implements HandlerInterceptor {
    private Logger logger = LoggerFactory.getLogger(getClass());
    private static final ThreadLocal<Long> startTimeThreadLocal = new NamedThreadLocal<Long>("ThreadLocal StartTime");
    private long diff = 1000 * 3;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
                             Object handler) throws Exception {
        long beginTime = System.currentTimeMillis();
        startTimeThreadLocal.set(beginTime);
        return true;
    }


    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response,
                                Object handler, Exception ex) throws Exception {
        long endTime = System.currentTimeMillis();
        long beginTime = startTimeThreadLocal.get();
        if ((endTime - beginTime) > diff) {
            new SimpleDateFormat("hh:mm:ss.SSS").format(endTime);
            String result = "URI:" + request.getRequestURI() +
                    "\nController(handle):" + handler +
                    "\n开始计时:" + new SimpleDateFormat("hh:mm:ss.SSS").format(beginTime) +
                    "\n结束时间:" + new SimpleDateFormat("hh:mm:ss.SSS").format(endTime) +
                    "\n耗时:" + (endTime - beginTime) + "ms" +
                    "\n最大内存:" + Runtime.getRuntime().maxMemory() / 1024 / 1024 + "M" +
                    "\n已分配内存:" + Runtime.getRuntime().totalMemory() / 1024 / 1024 + "M" +
                    "\n已分配内存的剩余空间:" + Runtime.getRuntime().freeMemory() / 1024 / 1024 + "M" +
                    "\n最大可用内存:" + Runtime.getRuntime().freeMemory() / 1024 / 1024 + "M";

            logger.warn(result);
        }

    }

}