package polar.island.net.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import polar.island.core.listener.InitializeListener;
import polar.island.inlay.dict.service.DictService;
import polar.island.inlay.menu.service.MenuService;
import polar.island.inlay.permission.service.PermissionService;
import polar.island.inlay.role.service.RoleService;
import polar.island.inlay.tree.service.TreeService;
import polar.island.net.util.DictUtil;

@Component
public class DictInitListener implements InitializeListener {
    @Autowired(required = false)
    private  DictService dictService;
    @Autowired(required = false)
    private  TreeService treeService;
    @Autowired(required = false)
    private  RoleService roleService;
    @Autowired(required = false)
    private  PermissionService permissionService;
    @Autowired(required = false)
    private  MenuService menuService;
    @Override
    public void initialize(ApplicationContext context) {
        DictUtil.setServices(treeService,dictService,roleService,permissionService,menuService);
    }

    @Override
    public int sort () {
        return 2;
    }
}
