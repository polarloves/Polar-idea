package polar.island.net.api;

import io.swagger.annotations.ApiOperation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.sql.SQLException;

/*@Configuration
@EnableSwagger2
@EnableWebMvc*/
public class DocConfig extends WebMvcConfigurationSupport {


    @Bean
    public Docket customDocket() {
        Docket docket = new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .useDefaultResponseMessages(false)
                .select().apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                .apis(RequestHandlerSelectors.basePackage("polar")).paths(PathSelectors.any()).build();
        return docket;
    }

    private ApiInfo apiInfo() {
        String describe = "在线接口文档，接口均采用统一的JSON格式:\n" +
                "{\n" +
                "   \"code\":\"000000\"\n" +
                "   \"msg\":\"响应内容\"\n" +
                "   \"data\":\"响应携带的数据\"\n" +
                "   \"count\":\"数据总共条目/此次操作处理的条目\"\n" +
                "}\n" +
                "特殊code对应关系如下：\n" +
                "   000000 -----------------请求成功\n" +
                "   000009 -----------------没有权限访问此接口\n" +
                "   000010 -----------------用户已被强制下线\n" +
                "   010002 -----------------用户已被锁定\n" +
                "   010003 -----------------用户已被删除\n" +
                "   000008 -----------------当前用户未登录（当用户会话过期时，也会返回此条代码）\n" +
                "\n" +
                "以下接口中返回参数均为data节点中的内容\n";
        return new ApiInfoBuilder()
                .title("Polar快速开发平台在线接口")
                .description(describe)
                .version("1.0.0")
                .license("暂无版权声明")
                .licenseUrl("")
                .contact(new Contact("polarloves", "", "1107061838@qq.com"))
                .build();

    }
}
