package polar.island.net.web;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.apache.shiro.session.Session;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import polar.island.core.config.Constants;
import polar.island.core.entity.ResponseJson;
import polar.island.core.res.ResMsg;
import polar.island.core.res.ResType;
import polar.island.core.exception.FrameWorkException;
import polar.island.core.exception.ValidationException;
import polar.island.core.file.FileService;
import polar.island.inlay.menu.service.MenuService;
import polar.island.inlay.user.service.UserService;

import polar.island.shiro.realm.ShiroPrincipal;
import polar.island.shiro.token.ShiroToken;
import polar.island.shiro.util.UserUtil;
import polar.island.web.controller.BasicController;
import polar.island.web.util.RequestUtil;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.List;
import java.util.Random;

@Controller(value = "mainController")
@RequestMapping(value = "/")
public class MainController extends BasicController {
    @Resource(name = "menuService")
    private MenuService menuService;
    @Resource(name = "userService")
    private UserService userService;
    @Resource(name = "localFileService")
    private FileService fileService;

    @ResMsg (tag = "接口文档", type = ResType.JSON, writeLogs = false)
    @RequestMapping(value = {"api"}, produces = "text/html;charset=utf-8")
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage"})
    public String api(HttpServletRequest request) {
        return "/view/api/api.jsp";
    }


    @ResMsg (tag = "开发文档", type = ResType.JSON, writeLogs = false)
    @RequestMapping(value = {"developmentDocument"}, produces = "text/html;charset=utf-8")
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage"})
    public String developmentDocument(HttpServletRequest request, String folder) {
        String path = folder.replace(",", "/");
        String real = request.getRealPath("/") + "view/doc/" + path + ".jsp";
        if (!new File(real).exists()) {
            throw new ValidationException("您找的东西走丢了~", null);
        }
        return "/view/doc/" + path + ".jsp";
    }

    @ResMsg (tag = "登录页", type = ResType.WEB, writeLogs = false)
    @RequestMapping(value = {"/"}, produces = "text/html;charset=utf-8")
    public String logInPage(HttpServletRequest request) {
        String userId = UserUtil.getUserId();
        if (userId != null) {
            request.setAttribute("menus", menuService.userMenus(UserUtil.getUserId()));
            request.setAttribute("user", UserUtil.getCurrentUser());
        } else {
            return "/view/sys/login.jsp";
        }
        return "/view/sys/Main.jsp";
    }

    @ResMsg (tag = "导航页", type = ResType.JSON, writeLogs = false)
    @RequestMapping(value = "/firstPage", produces = "text/html;charset=utf-8")
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage"})
    public String firstPage() {
        return "/view/sys/index.jsp";
    }

    @ResMsg (tag = "文件上传", type = ResType.JSON, writeLogs = false)
    @RequestMapping(value = "/uploadFile", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson upLoadFile(MultipartFile file, HttpServletRequest request) {
        if (file == null) {
            throw new ValidationException("文件不能为空", null);
        }
        try {
            String id = fileService.storeFile(file.getOriginalFilename(), file.getInputStream(), file.getContentType());
            String path = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
                    + request.getContextPath() + "/downFile?fileUrl="
                    + URLEncoder.encode(id, "UTF-8");
            return new ResponseJson(Constants.CODE_SUCCESS, path);
        } catch (Exception e) {
            throw new FrameWorkException(Constants.CODE_FILE, "文件上传失败", e, false);
        }
    }

    @ResMsg (tag = "查看图片", type = ResType.JSON, writeLogs = false)
    @RequestMapping(value = "/downFile", produces = "application/json;charset=utf-8")
    public void viewPic(String fileUrl, HttpServletResponse response) {
        try {
            String path = URLDecoder.decode(fileUrl, "UTF-8");
            fileService.writeFile(path, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @ResponseBody
    @ResMsg (tag = "登录", type = ResType.JSON)
    @RequestMapping(value = "/logIn", produces = "application/json;charset=utf-8", method = RequestMethod.POST)
    public ResponseJson logIn(String userName, String password, String code, HttpSession httpSession,
                              HttpServletRequest request) {

        // SecurityUtils.getSubject().getSession().setTimeout(-1000l);--设置会话永不超时
        String rrcode = (String) httpSession.getAttribute("rrCode");
        if (rrcode == null) {
            throw new FrameWorkException(Constants.CODE_SERVER_COMMON, "验证码无效或者已经过期，请重新输入", null, false);
        }
        if (code == null) {
            throw new FrameWorkException(Constants.CODE_SERVER_COMMON, "请输入验证码", null, false);
        }
        if (!rrcode.equals(code)) {
            throw new FrameWorkException(Constants.CODE_SERVER_COMMON, "验证码输入错误，请重新输入", null, false);
        }
        if (SecurityUtils.getSubject().isAuthenticated()) {
            // 如果已认证，先退出登录
            SecurityUtils.getSubject().logout();
        }
        UsernamePasswordToken token = new ShiroToken (userName, password, false);
        SecurityUtils.getSubject().login(token);
        ShiroPrincipal principal = (ShiroPrincipal) SecurityUtils.getSubject().getPrincipal();
        userService.logIn(principal.getUser().getId(), RequestUtil.getIpAddr(request));
        return new ResponseJson(Constants.CODE_SUCCESS, null, "登录成功");
    }

    @ResponseBody
    @ResMsg (tag = "退出登录", type = ResType.JSON)
    @RequestMapping(value = "/logOut", produces = "application/json;charset=utf-8")
    public ResponseJson logOut() {
        SecurityUtils.getSubject().logout();
        return new ResponseJson(Constants.CODE_SUCCESS, null, "退出登录成功");
    }

    private Color getRandColor(int fc, int bc) {
        Random random = new Random();
        if (fc > 255) {
            fc = 255;
        }
        if (bc < 255) {
            bc = 255;
        }
        int r = fc + random.nextInt(bc - fc);
        int g = fc + random.nextInt(bc - fc);
        int b = fc + random.nextInt(bc - fc);

        return new Color(r, g, b);
    }

    @ResMsg (tag = "获取验证码", type = ResType.JSON, writeLogs = false)
    @RequestMapping(value = "/code")
    public void code(HttpServletResponse response) throws Exception {
        Session session = SecurityUtils.getSubject().getSession(true);

        //设置页面不缓存
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-catch");
        response.setDateHeader("Expires", 0);
        //在内存中创建图象
        int width = 120;
        int height = 40;
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

        //创建图象
        Graphics g = image.getGraphics();
        //生成随机对象
        Random random = new Random();
        //设置背景色
        g.setColor(getRandColor(200, 250));
        g.fillRect(0, 0, width, height);
        //设置字体
        g.setFont(new Font("Tines Nev Roman", Font.PLAIN, 18));
        //随机产生干扰线
        g.setColor(getRandColor(160, 200));
        for (int i = 0; i < 255; i++) {
            int x = random.nextInt(width);
            int y = random.nextInt(height);
            int xl = random.nextInt(12);
            int yl = random.nextInt(12);
        }
        //随机产生认证码,4位数字
        String sRand = "";
        for (int i = 0; i < 4; i++) {
            String rand = String.valueOf(random.nextInt(10));
            sRand += rand;
            //将认证码显示到图象中
            g.setColor(new Color(20 + random.nextInt(110), 20 + random.nextInt(110), 20 + random.nextInt(110)));
            g.drawString(rand, 25 * i + 16, 25);
        }
        session.setAttribute("rrCode", sRand);
        //图像生效
        g.dispose();
        //输出图像到页面
        ImageIO.write(image, "JPEG", response.getOutputStream());
        response.getOutputStream().flush();
        response.getOutputStream().close();
    }

}
