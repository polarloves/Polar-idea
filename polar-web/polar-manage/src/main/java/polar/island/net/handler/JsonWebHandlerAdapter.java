package polar.island.net.handler;

import eu.bitwalker.useragentutils.DeviceType;
import eu.bitwalker.useragentutils.OperatingSystem;
import eu.bitwalker.useragentutils.UserAgent;
import org.apache.commons.io.IOUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.UnauthenticatedException;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.AntPathMatcher;
import org.apache.shiro.util.PatternMatcher;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;
import polar.island.core.config.Constants;
import polar.island.core.res.ResMsg;
import polar.island.core.res.ResType;
import polar.island.core.exception.*;
import polar.island.core.logger.LocalLogger;
import polar.island.core.logger.LogRecordsService;
import polar.island.core.logger.LoggerLevel;
import polar.island.core.logger.LoggerService;
import polar.island.core.util.CodeUtil;
import polar.island.core.util.PropertieUtil;
import polar.island.shiro.realm.ShiroPrincipal;
import polar.island.web.util.RequestUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;

public class JsonWebHandlerAdapter extends RequestMappingHandlerAdapter {
    private String MSG_DEFAULT = PropertieUtil.getDefaultMsg ( );
    private String MAPPING_DEFAULT = PropertieUtil.getDefaultMapping ( );
    private LoggerService logger = new LocalLogger ( );
    @Autowired ( required = false )
    private LogRecordsService logRecordsService;
    private List <String> logInUrls;
    private String exceptionName = "exception";
    private String forceDownLineName = Constants.FORCE_DOWNLINE_MSG;
    private String forceDownLineFlag = Constants.FORCE_DOWNLINE_FLAG;
    private PatternMatcher pathMatcher = new AntPathMatcher ( );

    public JsonWebHandlerAdapter () {
        super ( );
    }

    @Override
    protected ModelAndView handleInternal ( HttpServletRequest request , HttpServletResponse response ,
                                            HandlerMethod handlerMethod ) throws Exception {
        if ( handlerMethod != null ) {
            Exception ex = ( Exception ) getRequstAttribute ( exceptionName , request );
            if ( ex != null ) {
                return resolveException ( request , response , handlerMethod , ex );
            }
            boolean matches = false;
            if ( logInUrls != null && !logInUrls.isEmpty ( ) ) {
                String requestUri = WebUtils.getPathWithinApplication ( request );
                for (String url : logInUrls) {
                    if ( pathMatcher.matches ( url , requestUri ) ) {
                        matches = true;
                        break;
                    }
                }
            }
            if ( !matches ) {
                Subject subject = SecurityUtils.getSubject ( );
                if ( subject.isAuthenticated ( ) ) {
                    Session session = subject.getSession ( );
                    Boolean flag = ( Boolean ) getSessionAttribute ( forceDownLineFlag , session );
                    if ( flag != null && flag ) {
                        String errorMsg = ( String ) getSessionAttribute ( forceDownLineName , session );
                        //request中放置错误信息，用于显示.
                        request.setAttribute ( exceptionName ,
                                new FrameWorkException ( Constants.CODE_FORCE_DOWNLINE , errorMsg , null , false ) );
                        // 用户已经被强制下线
                        subject.logout ( );
                        return resolveException ( request , response , handlerMethod ,
                                new FrameWorkException ( Constants.CODE_FORCE_DOWNLINE , errorMsg , null , false ) );
                    }
                }
            }
        }

        try {
            String userName = getUserName ( );
            ModelAndView modelAndView = super.handleInternal ( request , response , handlerMethod );
            if ( userName == null ) {
                userName = getUserName ( );
            }
            writeRecords ( request , handlerMethod , userName );
            return modelAndView;
        } catch (UnauthenticatedException e) {
            // 用户未认证
            return resolveException ( request , response , handlerMethod , new NotLoginException ( null , e ) );
        } catch (polar.island.shiro.exception.AuthenticationException e) {
            //用户认证过程中的错误
            return resolveException ( request , response , handlerMethod , new FrameWorkException ( e.getCode ( ) , e.getFrameWorkMessage ( ) , null , false ) );
        } catch (org.apache.shiro.authz.AuthorizationException e) {
            // 无权限访问此页面
            return resolveException ( request , response , handlerMethod , new NoPermissionException ( null , e ) );
        } catch (org.apache.shiro.authc.AuthenticationException e) {
            if ( e.getCause ( ) != null && e.getCause ( ) instanceof FrameWorkException ) {
                return resolveException ( request , response , handlerMethod , ( FrameWorkException ) (e.getCause ( )) );
            } else {
                return resolveException ( request , response , handlerMethod , e );
            }
        } catch (org.springframework.web.multipart.MaxUploadSizeExceededException e) {
            // 文件过大
            return resolveException ( request , response , handlerMethod , new FileTooLargeException ( null , e ) );
        } catch (Exception e) {
            e.printStackTrace ( );
            return resolveException ( request , response , handlerMethod , e );
        }
    }

    private Object getRequstAttribute ( String name , HttpServletRequest request ) {
        return request.getAttribute ( name );
    }

    private Object getSessionAttribute ( String name , Session session ) {
        return session.getAttribute ( name );
    }


    private void writeRecords ( HttpServletRequest request , HandlerMethod handlerMethod , String userName ) {
        if ( logRecordsService != null ) {
            String pageName = null;
            final String url = request.getRequestURI ( );
            Integer platform = null;
            final Date now = new Date ( );
            final String ip = RequestUtil.getIpAddr ( request );
            ResMsg err = handlerMethod.getMethodAnnotation ( ResMsg.class );
            if ( err != null && err.tag ( ) != null ) {
                pageName = err.tag ( );
                if ( !err.writeLogs ( ) ) {
                    return;
                }
            } else {
                return;
            }
            UserAgent useragent = new UserAgent ( request.getHeader ( "user-agent" ) );
            OperatingSystem sys = useragent.getOperatingSystem ( );
            DeviceType deviceType = sys.getDeviceType ( );
            if ( deviceType == DeviceType.MOBILE ) {
                platform = 2;
            } else if ( deviceType == DeviceType.COMPUTER ) {
                platform = 1;
            } else {
                platform = -1;
            }
            final String finalPageName = pageName;
            final Integer finalPlatform = platform;
            new Thread ( ) {
                @Override
                public void run () {
                    logRecordsService.writeRecords ( finalPageName , url , finalPlatform , now , userName , ip );
                }
            }.start ( );

        }

    }

    public String getUserName () {
        ShiroPrincipal shiroPrincipal = ( ShiroPrincipal ) SecurityUtils.getSubject ( ).getPrincipal ( );
        if ( shiroPrincipal != null ) {
            return shiroPrincipal.getUser ( ).getUserName ( );
        }
        return null;
    }

    public String getUserId () {
        ShiroPrincipal shiroPrincipal = ( ShiroPrincipal ) SecurityUtils.getSubject ( ).getPrincipal ( );
        if ( shiroPrincipal != null ) {
            return shiroPrincipal.getUser ( ).getId ( ) + "";
        }
        return null;
    }

    /**
     * 处理异常信息。
     *
     * @param request       request对象
     * @param response      response对象
     * @param handlerMethod handlerMethod
     * @param exception     处理异常
     * @return 处理后定位的页面
     */
    private ModelAndView resolveException ( HttpServletRequest request , HttpServletResponse response ,
                                            HandlerMethod handlerMethod , Exception exception ) {
        ResMsg resMsg = getAnnotation ( handlerMethod );
        String userId = getUserName ( );
        writeLog ( resMsg , exception , request.getRequestURI ( ) , userId );
        String code = getErrorCode ( exception );
        String message = findErrorMessage ( resMsg , exception , code );
        ResType type = getType ( resMsg );
        if ( type == ResType.WEB || (type == ResType.BOTH && !isMobile ( request )) ) {
            String mapping = getMapper ( resMsg , code , request.getRequestURI ( ) , userId );
            ModelAndView view = new ModelAndView ( mapping );
            request.setAttribute ( "code" , code );
            request.setAttribute ( "message" , message );
            return view;
        } else {
            try {
                response.setCharacterEncoding ( "UTF-8" );
                response.setContentType ( "application/json;charset=utf-8" );
                response.getOutputStream ( ).write ( CodeUtil.generateDataStr ( code , null , message ).getBytes ( "UTF-8" ) );
                response.getOutputStream ( ).flush ( );
            } catch (Exception ex) {
                // 写出异常时，不记录日志
                // logger.writeLog(TAG, "[写出异常]", ex, LoggerLevel.ERROR,
                // request.getRequestURI(), userId);
            } finally {
                try {
                    IOUtils.closeQuietly ( response.getOutputStream ( ) );
                } catch (Exception e) {
                }
            }
            return null;
        }
    }

    /**
     * 判断请求是否为手机端。
     *
     * @param request request对象
     * @return 是否为手机端
     */
    private boolean isMobile ( HttpServletRequest request ) {
        UserAgent useragent = new UserAgent ( request.getHeader ( "user-agent" ) );
        OperatingSystem sys = useragent.getOperatingSystem ( );
        DeviceType deviceType = sys.getDeviceType ( );
        if ( deviceType == DeviceType.MOBILE ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 获取错误类型需要返回的数据格式，默认返回json串。
     *
     * @param resMsg ErrorMsg标签
     * @return 错误类型
     * @see ResMsg
     */
    private ResType getType ( ResMsg resMsg ) {
        if ( resMsg == null ) {
            return ResType.JSON;
        } else {
            return resMsg.type ( );
        }
    }

    /**
     * 获取注解标签
     *
     * @param handlerMethod handlerMethod
     * @return ErrorMsg对象
     */
    private ResMsg getAnnotation ( HandlerMethod handlerMethod ) {
        ResMsg resMsg = handlerMethod.getMethodAnnotation ( ResMsg.class );
        return resMsg;
    }

    /**
     * 根据代码获取错误信息。
     *
     * @param resMsg 标签
     * @param e      错误信息
     * @param code   错误码
     * @return 错误信息
     */
    public String findErrorMessage ( ResMsg resMsg , Exception e , String code ) {
        String message = null;
        if ( resMsg == null ) {
            if ( e instanceof FrameWorkException ) {
                FrameWorkException ex = ( FrameWorkException ) e;
                message = ex.getFrameWorkMessage ( );
                if ( message == null ) {
                    message = PropertieUtil.getMsg ( code );
                }
            } else {
                message = MSG_DEFAULT;
            }
        } else {
            if ( resMsg.overrideAll ( ) && !resMsg.msg ( ).equals ( "" ) ) {
                message = resMsg.msg ( );
            } else {
                if ( e instanceof FrameWorkException ) {
                    FrameWorkException ex = ( FrameWorkException ) e;
                    message = ex.getFrameWorkMessage ( );
                    if ( message == null ) {
                        if ( message == null ) {
                            message = PropertieUtil.getMsg ( code );
                        }
                    }
                    if ( !message.equals ( MSG_DEFAULT ) ) {
                        if ( !resMsg.prefix ( ).equals ( "" ) ) {
                            message = resMsg.prefix ( ) + "," + message;
                        }
                        if ( !resMsg.suffix ( ).equals ( "" ) ) {
                            message = message + "," + resMsg.suffix ( );
                        }
                    }
                } else {
                    message = MSG_DEFAULT;
                }
            }

        }
        return message;
    }

    /**
     * 获取错误码
     *
     * @param e 异常类型
     * @return 错误码
     */
    public String getErrorCode ( Exception e ) {
        String code = null;
        if ( e instanceof FrameWorkException ) {
            FrameWorkException ex = ( FrameWorkException ) e;
            code = ex.getCode ( );
            if ( code == null ) {
                code = Constants.CODE_SERVER_ERROR;
            }
        } else {
            return Constants.CODE_SERVER_ERROR;
        }
        return code;
    }

    /**
     * 获取重定向路径
     *
     * @param resMsg 注解
     * @param code   错误码
     * @param url    访问路径
     * @param userId 用户编号
     * @return 重定向路径
     */
    public String getMapper ( ResMsg resMsg , String code , String url , String userId ) {
        if ( resMsg != null && resMsg.mapper ( ).equals ( "" ) ) {
            return PropertieUtil.getMapping ( code );

        }
        return MAPPING_DEFAULT;
    }

    /**
     * 写入日志
     *
     * @param resMsg 注解
     * @param e      错误信息
     * @param url    访问路径
     * @param userId 用户编号
     */
    public void writeLog ( final ResMsg resMsg , final Exception e , final String url , final String userId ) {
        if ( e instanceof FrameWorkException ) {
            FrameWorkException ex = ( FrameWorkException ) e;
            if ( !ex.isLog ( ) ) {
                return;
            }
        }
        if ( resMsg == null || resMsg.tag ( ).equals ( "" ) ) {
            new Thread ( ) {
                @Override
                public void run () {
                    logger.writeLog ( "未指定的标签" , e , LoggerLevel.ERROR , url , userId );
                }
            }.start ( );

        } else {
            new Thread ( ) {
                @Override
                public void run () {
                    logger.writeLog ( resMsg.tag ( ) , e , LoggerLevel.ERROR , url , userId );
                }
            }.start ( );
        }
    }

    public List <String> getLogInUrls () {
        return logInUrls;
    }

    public void setLogInUrls ( List <String> logInUrls ) {
        this.logInUrls = logInUrls;
    }

    public LoggerService getLogger () {
        return logger;
    }

    public void setLogger ( LoggerService logger ) {
        this.logger = logger;
    }


    public LogRecordsService getLogRecordsService () {
        return logRecordsService;
    }

    public void setLogRecordsService ( LogRecordsService logRecordsService ) {
        this.logRecordsService = logRecordsService;
    }

    public String getExceptionName () {
        return exceptionName;
    }

    public void setExceptionName ( String exceptionName ) {
        this.exceptionName = exceptionName;
    }

    public String getForceDownLineName () {
        return forceDownLineName;
    }

    public void setForceDownLineName ( String forceDownLineName ) {
        this.forceDownLineName = forceDownLineName;
    }

    public String getForceDownLineFlag () {
        return forceDownLineFlag;
    }

    public void setForceDownLineFlag ( String forceDownLineFlag ) {
        this.forceDownLineFlag = forceDownLineFlag;
    }

}
