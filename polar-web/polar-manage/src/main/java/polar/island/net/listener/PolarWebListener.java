package polar.island.net.listener;

import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;

public class PolarWebListener extends ContextLoaderListener {
    @Override
    public WebApplicationContext initWebApplicationContext ( ServletContext servletContext ) {
        WebApplicationContext context = super.initWebApplicationContext ( servletContext );
        String str = "-----------------------------------\n" +
                "            _____\n" +
                "           /\\    \\\n" +
                "          /::\\____\\\n" +
                "         /:::|    |\n" +
                "        /::::|    |\n" +
                "       /:::::|    |\n" +
                "      /:::/|:|    |\n" +
                "     /:::/ |:|    |\n" +
                "    /:::/  |:|    |   _____\n" +
                "   /:::/   |:|    |  /\\    \\\n" +
                "  /:::/    |:|    | /::\\____\\\n" +
                "  \\::/    /|:|    |/:::/    /\n" +
                "   \\/____/ |:|    /:::/    /\n" +
                "  ____     |:|   /:::/    /\n" +
                " |    |    |:|  /:::/    /\n" +
                " | 恩 |    |:| /:::/    /\n" +
                " | 爷 |    |:|/:::/    /\n" +
                " | 专 |    |:|:::/    /\n" +
                " | 属 |    |:|::/    /\n" +
                " |    |    |:|:/    /\n" +
                " |____|    |:|/____/\n" +
                "--------------------------------";
        System.err.println(str);
        return context;
    }
}