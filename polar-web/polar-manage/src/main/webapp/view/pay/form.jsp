<%@page language="java" contentType="text/html; charset=utf-8"
        pageEncoding="utf-8" %>
<%@ include file="/view/includes/tag.jsp" %>
<style>
    .form-div {
        height: calc(100% - 50px);
        width: 100%;
    }

    .polar-operate {
        height: 50px;
        width: 100%;
        background-color: rgba(34, 0, 24, 0.1);
        padding-top: 10px;
        padding-bottom: 10px;
        padding-right: 30px;
    }
</style>
<div class='form-div layui-side-scroll' xmlns:t="http://www.w3.org/1999/html">
    <form method="post" class="layui-form required-validate polar-form" id="payForm" cache="true"
          lay-filter="payForm">
        <div class="layui-tab layui-tab-brief " lay-filter="tab_code">
            <ul class="layui-tab-title">
                <li class="layui-this" lay-id="tab_0">支付宝配置</li>
                <li lay-id="tab_1">微信支付配置</li>
            </ul>
            <div class="layui-tab-content">
                <div class="layui-tab-item layui-show">
                    <div class="layui-form-item">
                        <div class="layui-row">
                            <div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md6">
                                <label class="layui-form-label polar-form-title">APPID：</label>
                                <div class="layui-input-block polar-form-content">
                                    <form:input  name="aliConfig.appId" placeholder="请输入APPID" cache="true" vertify="required" tag="支付宝APPID" value="${ali.appId}"></form:input>
                                </div>
                            </div>
                            <div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md6">
                                <label class="layui-form-label polar-form-title">服务网关：</label>
                                <div class="layui-input-block polar-form-content">
                                    <form:input name="aliConfig.serverUrl" placeholder="请输入服务网关" cache="true" vertify="required" tag="支付宝服务网关" value="${ali.serverUrl}"></form:input>
                                </div>
                            </div>
                            <div class="layui-inline layui-col-xs12 layui-col-sm12 layui-col-md12">
                                <label class="layui-form-label polar-form-title">支付宝公钥：</label>
                                <div class="layui-input-block polar-form-content">
                                    <form:textarea name="aliConfig.publicKey" maxLength="-1" cache="true" value="${ali.publicKey}" tag="支付宝公钥">

                                    </form:textarea>
                                </div>
                            </div>
                            <div class="layui-inline layui-col-xs12 layui-col-sm12 layui-col-md12">
                                <label class="layui-form-label polar-form-title">应用私钥：</label>
                                <div class="layui-input-block polar-form-content">
                                    <form:textarea name="aliConfig.privateKey" maxLength="-1" cache="true"  value="${ali.privateKey}" tag="支付宝私钥">

                                    </form:textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="layui-tab-item">
                    <div class="layui-form-item">
                        <div class="layui-row">
                            <div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md6">
                                <label class="layui-form-label polar-form-title">APPID：</label>
                                <div class="layui-input-block polar-form-content">
                                    <form:input name="wechatConfig.appId" placeholder="请输入APPID" value="${wechat.appId}" cache="true" vertify="required" tag="微信APPID"></form:input>
                                </div>
                            </div>
                            <div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md6">
                                <label class="layui-form-label polar-form-title">商户号：</label>
                                <div class="layui-input-block polar-form-content">
                                    <form:input name="wechatConfig.mchId" placeholder="请输入商户号" value="${wechat.mchId}" cache="true" vertify="required" tag="微信商户号"></form:input>
                                </div>
                            </div>
                            <div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md6">
                                <label class="layui-form-label polar-form-title">回调地址：</label>
                                <div class="layui-input-block polar-form-content">
                                    <form:input name="wechatConfig.noticeUrl" placeholder="请输入微信回调地址" value="${wechat.noticeUrl}" cache="true" vertify="required" tag="微信回调地址"></form:input>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
     </div>
    </form>
</div>
<div class="polar-operate">
    <button class="layui-btn  layui-btn-sm layui-btn-normal pull-right polar-save">
        <i class="fa fa-pencil"></i> <span class="polar-btn-content">保存</span>
    </button>
</div>
<script>
    $(function () {
        var options = {
            url: "/pay/config/save",
            id: "payForm",//定义表单编号,要求lay-flter也为此
        };
        var form = polar.form.Form(options);
        form.validate = function (that) {
            return polar.form.Verification("#" + that.options.id);
        };
        form.init(form);
        polar.tab.currentForm = $("#payForm");//保存当前对象至tab中，方便缓存
        $(".polar-save").click(function () {
            var that = form;
            that.beforeValidate(that);
            if (that.validate(that)) {
                //提交表单
                var url = that.options.url;
                var data = that.params(that);
                if (that.onSubmit != null && that.onSubmit != 'undefined' && typeof that.onSubmit === 'function') {
                    that.onSubmit(that, data);
                }
                var options = {};
                options = that.onSubmitOptions(options, that);
                polar.loader.load(polar.loader.jsonSuccess, polar.loader.jsonError, url, true, options, data);
            }
        });

    });
</script>
