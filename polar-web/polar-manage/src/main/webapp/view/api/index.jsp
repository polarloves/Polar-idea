<!-- HTML for static distribution bundle build -->
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/view/includes/tag.jsp"%>
<div class="polar-content"  style="background-color: #fafafa;overflow-y:auto">
  <link rel="stylesheet" type="text/css" href="${ctxStatic}/api/swagger-ui.css" >
  <link rel="icon" type="image/png" href="${ctxStatic}/api/favicon-32x32.png" sizes="32x32" />
  <link rel="icon" type="image/png" href="${ctxStatic}/api/favicon-16x16.png" sizes="16x16" />
  <style>
    html
    {
      box-sizing: border-box;
      overflow: -moz-scrollbars-vertical;
      overflow-y: scroll;
    }
    *,
    *:before,
    *:after
    {
      box-sizing: inherit;
    }
    body
    {
      margin:0;
      background: #fafafa;
    }
    pre{
      background-color: transparent;
      border: 0px;
    }
    /* 隐藏入参详细信息 */
    td.parameters-col_name .parameter__in{
      display: none;
    }
    td.parameters-col_description .body-param{
      display: none;
    }
    td.parameters-col_description .tab{
      display: none;
    }
  /*  .try-out{
      display: none;!*!/隐藏调试按钮*!
    }*/

    table.parameters,table.parameters tr th, table.parameters tr td {
      border:1px solid gainsboro;
    }
    table.parameters tbody tr td {
      padding: 0px;
      vertical-align: middle;

    }
    table.parameters thead tr th {
      vertical-align: middle;

    }
  </style>
    <div id="swagger-ui"></div>

    <script src="${ctxStatic}/api/swagger-ui-bundle.js"> </script>
    <script src="${ctxStatic}/api/swagger-ui-standalone-preset.js"> </script>
    <script>

      // Build a system
      var ui = SwaggerUIBundle({
        url: "${ctx}/v2/api-docs.do",
        dom_id: '#swagger-ui',
        deepLinking: true,
        presets: [
          SwaggerUIBundle.presets.apis,
          SwaggerUIStandalonePreset
        ],
        plugins: [
          SwaggerUIBundle.plugins.DownloadUrl
        ],
        layout: "StandaloneLayout"
      })
    //  window.ui = ui
  </script>
</div>
