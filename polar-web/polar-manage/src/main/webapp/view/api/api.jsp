<%@ page contentType="text/html;charset=UTF-8"%>
<%@ include file="/view/includes/tag.jsp"%>
<link rel="stylesheet" type="text/css" href="${ctxStatic}/api/css/global.css">
<link rel="stylesheet" type="text/css" href="${ctxStatic}/api/css/jquery.json-viewer.css">
<style>
    .navMenubox { width: 200px;  background: #1c2229; margin: 0 auto;  overflow: hidden; }
    .navMenu-top { padding: 10px; color: #fff; border-bottom: 1px solid rgba(255,255,255,.1) }
    .navMenu> li { display: block; margin: 0; padding: 0; border: 0px; }
    .navMenu>li>a { display: block; overflow: hidden; padding-left: 0px; line-height: 40px; color: #ABB1B7; transition: all .3s; position: relative; text-decoration: none; font-size: 14px; border-top: 1px solid #222932; border-bottom: 2px solid #191e24; }
    .navMenu > li:nth-of-type(1)> a { border-top: 1px solid transparent; }
    .navMenu > li:last-child > a { border-bottom: 1px solid transparent; }
    .navMenu>li>a>i { font-size: 17px; float: left; font-style: normal; margin: 0 5px; }
    .navMenu li a .arrow:before { display: block; float: right; margin-top: 1px; margin-right: 15px; display: inline; font-size: 13px; font-family: FontAwesome; height: auto; content: "\f105"; font-weight: 300; text-shadow: none; }
    .navMenu li a .arrow.open:before { float: right; margin-top: 1px; margin-right: 15px; display: inline; font-family: FontAwesome; height: auto; font-size: 13px; content: "\f107"; font-weight: 300; text-shadow: none; }
    .navMenu>li>a.active, .navMenu>li>a:hover { color: #FFF; background: #12181b; }
    .navMenu>li>ul.sub-menu, .navMenu>li>ul.sub-menu>li>ul.sub-menu { display: none; list-style: none; clear: both; margin: 8px 0px 0px 10px; padding-bottom: 5px; }
    .navMenu>li.active > ul.sub-menu, .navMenu>li>ul.sub-menu>li.active >ul.sub-menu { }
    .navMenu>li>ul.sub-menu li { background: none; margin: 0px; padding: 0px; }
    .navMenu>li>ul.sub-menu li>a { display: block; font-size: 13px; line-height: 36px; padding-left: 20px; color: #ABB1B7; clear: both; }
    .navMenu>li>ul.sub-menu li>a.active, .navMenu>li>ul.sub-menu li>a:hover, .navMenu>li>ul.sub-menu>li.active >a { color: #FFF; background: #12181b; }
    .icon_1:before { content: "\f0ac"; }
    .icon_2:before { content: "\f0ac"; }
    .icon_3:before { content: "\f0ac"; }

    .navMenu>li>ul.sub-menu li>a{
        background-color: rgba(40,51,64,0.93);
        margin-top: 5px;
        padding-left: 5px;
    }

</style>
<div class="polar-content" id="api-div"  style="background-color: #fafafa;overflow-y:auto">
<script id="template" type="text/template">
    <div class="layui-layout layui-layout-admin"
         style="height: 100%">
        <div class="layui-header header layui-bg-blue ">
            <div class="layui-main">
                <div class="admin-login-box logo">
                    <span>{{:info.title}}<small class="version">{{:info.version}}</small></span>
                </div>

            </div>
        </div>
        <div style="width: 200px;height:calc( 100% - 60px );background: rgba(12,29,64,0.82) ">
            <div class="navMenubox" >
                <ul class="navMenu">
                    {{for tags itemVar="~tag"}}
                    {{if name != "basic-error-controller"}}
                    <li ><a href="javascript:;" style="padding-left: 10px;">
                        {{:name}}<span class="layui-nav-more"></span></a>
                        <ul class="sub-menu">
                            {{!--获取tags下面对应的方法--}}
                            {{props ~root.paths itemVar="~path"}}
                            {{!--具体方法--}}
                            {{props prop}}
                            {{if prop.tags[0] == ~tag.name}}
                            <li title="{{:key}} {{:prop.description}}">
                                <a href="javascript:;" name="a_path" path="{{:~path.key}}" method="{{:key}}"
                                   operationId="{{:prop.operationId}}">
                                    <p><span class="" style="width: 100%">{{:~path.key}}</span></p>
                                    <span class="{{:key}}_font pl10" style="width: 100%">{{:prop.summary}}</span>
                                </a>
                            </li>
                            {{/if}}
                            {{/props}}
                            {{/props}}
                        </ul>
                    </li>
                    {{/if}}
                    {{/for}}

                </ul>

            </div>
        </div>



        <div class="layui-body site-content" id="path-body"
             style="margin-left: 200px;margin-top: 45px;height:calc( 100% - 110px )">
            {{!-- body 内容 $ref = temp_body --}}
        </div>

        {{if info.license}}
        <div class="layui-footer footer">
            <div class="layui-main">
                <a href="{{:info.license.url}}" target="blank">{{:info.license.name}}</a></p>
            </div>
        </div>
        {{/if}}
    </div>
</script>
<script id="temp_body" type="text/template">
    <div class="layui-tab layui-tab-brief">
        <ul class="layui-tab-title">
            <li class="layui-this"><span class="method {{:method}}" m_operationId="{{:operationId}}" path="{{:path}}"
                                         method="{{:method}}">{{:method}}</span>{{:path}}
            </li>
            <li>Debug</li>
        </ul>
        <div class="layui-tab-content" style="min-height: 150px; padding: 5px 0px 0px; height: 803px;">
            <div class="layui-tab-item layui-show">
                <table class="layui-table">
                    <colgroup>
                        <col width="150">
                        <col width="150">
                        <col width="150">
                        <col>
                    </colgroup>
                    <tbody>
                    <tr>
                        <th>Path</th>
                        <td colspan="3">{{:path}}</td>
                    </tr>
                    <tr>
                        <th>Summary</th>
                        <td colspan="3">{{:summary}}</td>
                    </tr>
                    <tr>
                        <th>Description</th>
                        <td colspan="3">{{:description}}</td>
                    </tr>
                    <tr>
                        <th>Consumes</th>
                        <td>{{:consumes}}</td>
                        <th>Produces</th>
                        <td>{{:produces}}</td>
                    </tr>
                    </tbody>
                </table>
                <fieldset class="layui-elem-field layui-field-title">
                    <legend>Parameters</legend>
                </fieldset>
                <table class="layui-table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Parameter Type</th>
                        <th>Data Type</th>
                        <th>Default</th>
                        <th>Required</th>
                    </tr>
                    </thead>
                    <tbody>
                    {{for parameters}}
                    <tr>
                        <td>{{:name}}</td>
                        <td>{{:description}}</td>
                        <td>{{:in}}</td>
                        <td>{{:type}}</td>
                        <td>{{:default}}</td>
                        {{if required}}
                        <td><i class="layui-icon">&#xe605;</i></td>
                        {{else}}
                        <td></td>
                        {{/if}}
                    </tr>
                    {{/for}}
                    </tbody>
                </table>
                <fieldset class="layui-elem-field layui-field-title">
                    <legend>Responses</legend>
                </fieldset>
                <div>
                    <table class="layui-table">
                        <thead>
                        <tr>
                            <th>Properties</th>
                            <th>Type</th>
                            <th>Description</th>
                        </tr>
                        </thead>
                        <tbody id="path-body-response-model">
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="layui-tab-item">
                <fieldset class="layui-elem-field layui-field-title" style="display: none">
                    <legend>Parameters</legend>
                </fieldset>
                <div class="method-type" style="display: none">
                    <lable>Parameter Type :</lable>
                    <input type="hidden" id="content_type_{{:operationId}}" value="form">
                    <button id="pt_form_{{:operationId}}" type="form" operationId="{{:operationId}}"
                            onclick="changeParameterType(this)"
                            class="layui-btn layui-btn-small layui-btn-normal layui-btn-radius">Form
                    </button>
                    <button id="pt_json_{{:operationId}}" type="json" operationId="{{:operationId}}"
                            onclick="changeParameterType(this)"
                            class="layui-btn layui-btn-small layui-btn-primary layui-btn-radius">Json
                    </button>
                </div>
                <textarea class="parameter-text hide" rows="10" id="text_tp_{{:operationId}}"></textarea>
                <table class="layui-table" id="table_tp_{{:operationId}}">
                    <colgroup>
                        <col width="150">
                        <col>
                        <col>
                        <col width="150">
                        <col width="150">
                        <col>
                    </colgroup>
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Value</th>
                        <th>Description</th>
                        <th>Parameter Type</th>
                        <th>Data Type</th>
                        <th>Required</th>
                    </tr>
                    </thead>
                    <tbody>
                    {{for parameters}}
                    <tr>
                        <td>{{:name}}</td>
                        <td>
                            {{if required}}
                            <input type="text" p_operationId="{{:~root.operationId}}" name="{{:name}}" in="{{:in}}"
                                   required="required" value="{{:default}}" placeholder="required" autocomplete="off"
                                   class="layui-input">
                            {{else}}
                            <input type="text" p_operationId="{{:~root.operationId}}" name="{{:name}}" in="{{:in}}"
                                   autocomplete="off" value="{{:default}}" class="layui-input">
                            {{/if}}
                        </td>
                        <td>{{:description}}</td>
                        <td>{{:in}}</td>
                        <td>{{:type}}</td>
                        {{if required}}
                        <td><i class="layui-icon">&#xe605;</i></td>
                        {{else}}
                        <td></td>
                        {{/if}}
                    </tr>
                    {{/for}}
                    </tbody>
                </table>
                <div>
                    <button class="layui-btn" name="btn_submit" onclick="getData('{{:operationId}}')"> Submit</button>
                </div>
                <fieldset class="layui-elem-field layui-field-title">
                    <legend>Responses</legend>
                </fieldset>
                <div class="responseJson">
                    <pre id="json-response"></pre>
                </div>
            </div>
        </div>
    </div>
</script>
<script id="temp_body_response_model" type="text/template">
    <tr class="response_entity">
        <td colspan="3">{{:name}} : </td>
    </tr>
    {{props properties}}
    <tr>
        <td>{{:key}}</td>
        <td>{{:prop.type}}</td>
        <td>{{:prop.description}}</td>
    </tr>
    {{/props}}
</script>
</div>
<script src="${ctxStatic}/api/js/jsrender.min.js"></script>
<script src="${ctxStatic}/api/js/jquery.json-viewer.js"></script>
<script src="${ctxStatic}/api/js/docs.js"></script>