<%@page language="java" contentType="text/html; charset=utf-8"
        pageEncoding="utf-8" %>
<%@ include file="/view/includes/tag.jsp" %>
<style>
    .title li {
        padding: 5px;
        font-style: normal;
        list-style-type: circle;
        list-style-position: inside;
    }

    .title em {
        font-style: normal;
    }

    .bg-gray {
        background-color: whitesmoke;
    }

    .form-div .layui-inline {
        margin-bottom: 5px;
        margin-right: 0px;
        padding: 5px;
    }

</style>
<div class="layui-side-scroll" style="width: 100%;background-color: whitesmoke;">

    <div class='form-div bg-gray'>
        <div class="layui-form-item ">
            <div class="layui-fluid">
                <div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md4">
                    <div class="layui-card">
                        <div class="layui-card-header layui-bg-black">系统缓存</div>
                        <div class="layui-card-body">
                            <ul>
                                <li><span class="layui-badge-dot layui-bg-black"></span>&nbsp;集成redis、ehcache两种缓存;</li>
                                <li><span class="layui-badge-dot layui-bg-black"></span>&nbsp;redis连接池,缓存更加可靠、稳定;</li>
                                <li><span class="layui-badge-dot layui-bg-black"></span>&nbsp;缓存切换方便,配置透明;</li>
                                <li><span class="layui-badge-dot layui-bg-black"></span>&nbsp;多个项目可同时使用同一个redis,各个项目间胡不影响;
                                </li>
                                <li><span class="layui-badge-dot layui-bg-black"></span>&nbsp;支持redis无密码、有密码配置,无需改动配置文件;
                                </li>
                                <li><span class="layui-badge-dot layui-bg-black"></span>&nbsp;支持redis消息队列,用于负载均衡过程中协调各个容器;
                                </li>
                            </ul>
                            <br/>
                            &nbsp;&nbsp;在项目研发过程中,缓存是影响整个项目运行效率的一个重要指标,系统为了支持负载均衡以及单系统部署,分别提供了两种方案:redis、ehcache。<br/>
                            &nbsp;&nbsp;当系统压力较小,不需要redis进行缓存时,可以将redis缓存替换成ehcache缓存,这也降低了部署的难度以及系统的压力.
                        </div>
                    </div>
                </div>
                <div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md4">
                    <div class="layui-card ">
                        <div class="layui-card-header layui-bg-green">权限控制</div>
                        <div class="layui-card-body">
                            <ul>
                                <li><span class="layui-badge-dot layui-bg-green"></span>&nbsp;经典的用户->权限->资源五张表设计规则;</li>
                                <li><span class="layui-badge-dot layui-bg-green"></span>&nbsp;整合shiro权限管理框架,支持细粒度权限控制,支持权限缓存、登录凭证缓存,登录效率更快,权限处理更快;
                                </li>
                                <li><span class="layui-badge-dot layui-bg-green"></span>&nbsp;并发人数控制,可以控制单个账号同时登录平台的人数;
                                </li>
                                <li><span class="layui-badge-dot layui-bg-green"></span>&nbsp;在线用户管理,通过用户会话,可以对在线的用户进行管理,实时对其强制下线的功能;
                                </li>
                                <li><span class="layui-badge-dot layui-bg-green"></span>&nbsp;接口动态管理,对某些特殊的接口,可以通过数据库配置的方式实现动态的管理,此处也内置缓存,用于减轻服务器压力;
                                </li>
                                <li><span class="layui-badge-dot layui-bg-green"></span>&nbsp;系统加入了组织机构权限,其与普通的权限为"和"关系,由于组织机构为树形结构,在不影响效率的情况下,子机构不会集成父机构的权限;
                                </li>
                            </ul>
                            <br/>
                            &nbsp;&nbsp;每个系统都有自己的权限管理体系,都有自己的权限设计思路,本系统使用经典的五张表设计,即：用户表->用户角色表->角色表->角色权限表->权限表,其中更加入了组织机构的权限,其结构为：用户表->组织机构权限表->权限表<br/>
                        </div>
                    </div>
                </div>
                <div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md4">
                    <div class="layui-card">
                        <div class="layui-card-header layui-bg-blue">代码生成</div>
                        <div class="layui-card-body">
                            <ul>
                                <li><span class="layui-badge-dot layui-bg-green"></span>&nbsp;三种常用的表单：树结构表单、普通表单、父子表单;
                                </li>
                                <li><span class="layui-badge-dot layui-bg-green"></span>&nbsp;以freemarker作为模板引擎,模板透明,可随时更改;
                                </li>
                                <li><span class="layui-badge-dot layui-bg-green"></span>&nbsp;内置15种表单元素,代码生成时更加方便;</li>
                                <li><span class="layui-badge-dot layui-bg-green"></span>&nbsp;简单而又复杂的代码生成器,不仅可以简单的生成代码,也可以对其进行高级定制;
                                </li>
                                <li><span class="layui-badge-dot layui-bg-green"></span>&nbsp;一键配置用户权限,无需手动录入;</li>
                            </ul>
                            <br/>
                            &nbsp;&nbsp;系统遵循着"简单而又复杂"的设计原则对代码生成器进行设计.<br/>
                            &nbsp;&nbsp;当功能模块过于简单时,可以通过：设计表->导入表->生成代码这三步即可完成一个模块的编写,而功能过于复杂时,可以通过对导入表后的数据进行灵活的配置,从而实现复杂模块的编写.<br/>
                            &nbsp;&nbsp;系统遵循着"简单而又复杂"的设计原则对代码生成器进行设计.<br/>
                        </div>
                    </div>
                </div>
                <div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md4">
                    <div class="layui-card">
                        <div class="layui-card-header  layui-bg-red">高效、低负载</div>
                        <div class="layui-card-body">
                            <ul>
                                <li><span class="layui-badge-dot layui-bg-green"></span>&nbsp;前端摒弃掉"iframe"模式，使用div来代替iframe;
                                </li>
                                <li><span class="layui-badge-dot layui-bg-green"></span>&nbsp;无重复加载的资源文件,降低服务器压力;
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
