<%@page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="/view/includes/tag.jsp"%>
<div class='form-div' xmlns:t="http://www.w3.org/1999/html">
    <div style="display: none" id="childDetailBtn">
        <button class="layui-btn    layui-btn-primary layui-btn-xs polar-detail-child-delete-inner" polar-data="[id]"  type="button">
            <i class="fa fa-remove"></i> <span class="polar-btn-content">删除</span>
        </button>
    </div>
	<form method="post" class="layui-form required-validate polar-form" id="groupForm" lay-filter="groupForm">

		<input type="hidden" name="id" value='${group.id}' />
        <div class="layui-form-item ">
            <div class="layui-row">
				<div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md6">
                    <label class="layui-form-label polar-form-title">分组名称：</label>
                    <div class="layui-input-block polar-form-content">
					<form:input name="groupName" placeholder="请输入分组名称"   tag="分组名称" value='${group.groupName}' vertify="maxLength,minLength,required"/>
                    </div>
                </div>
				<div class="layui-inline layui-col-xs12 layui-col-sm12 layui-col-md12">
                    <label class="layui-form-label polar-form-title">组描述：</label>
                    <div class="layui-input-block polar-form-content">
					<form:textarea name="groupDecribe"    tag="组描述" value='${group.groupDecribe}' vertify="maxLength,minLength"/>
                    </div>
                </div>
			</div>
		</div>

        <div class="layui-tab layui-tab-brief " >
            <ul class="layui-tab-title">
                <li class="layui-this" lay-id="tab_user">用户列表</li>
        </ul>
        <div class="layui-tab-content" >
            <div class="layui-tab-item layui-show">
                <div id="userChildToolbar">
                    <button class="layui-btn    layui-btn-primary layui-btn-sm polar-child-add-row" type="button">
                        <i class="fa fa-plus"></i>&nbsp;<span class="polar-btn-content">添加</span>
                    </button>
                    <button class="layui-btn layui-btn-primary layui-btn-sm polar-child-delete-row" type="button">
                        <i class="fa fa-minus"></i>&nbsp;<span class="polar-btn-content">删除</span>
                    </button>
                </div>
                <div style="width: 100%;overflow-x: auto" >
                    <table  class="layui-table" id="userTable"></table>
                </div>
            </div>
        </div>
        </div>
	</form>
</div>
<script>
	$(function(){
		var options={
			addUrl:"/group/json/add",//定义新增的url
			upDateUrl:"/group/json/updateAllById",//定义修改的url
			id:"groupForm",//定义表单编号,要求lay-flter也为此
			unionId:"id"//主键编号
		};
		var form=polar.form.Form(options);
		form.validate=function(that){
			return polar.form.Verification("#"+that.options.id);
		};
		form.done=function(that,layuiForm){//加载完成的回调
			var layedit = layui.layedit;
			that.indexs={};
		};
		form.onSubmit=function(that,data){
			var layedit = layui.layedit;
			return data;
		}
        form.beforeValidate=function(that){
            var layedit = layui.layedit;
        }
        var usercolumns=[ {
                checkbox : true,
                tableWidth : 50
            }
            , {
                "field" : "userId",
                "title" : "用户编号",
                minWidth:100,
                weigth:1
            }  , {
                "field" : "userName",
                "title" : "用户名称",
                minWidth:100,
                weigth:1
            }  , {
                "field" : "nickName",
                "title" : "用户昵称",
                minWidth:100,
                weigth:1
            }
            , {
            field : 'operate',
            title : '操作',
            formatter : function(row,options){
                return $("#childDetailBtn").html().split('[id]').join(row.userId);
            }
            ,tableWidth : 80
            ,miniTableWidth : 160
        } ];
        var useroptions={
            toolbar:$("#userChildToolbar"),//工具条的dom对象
            table:$("#userTable"),//表格的dom对象
            formWidth:"1000px",//页面宽度
            formHeight:"800px",//页面高度
            listName:"users",//列表name
            formFilter:"groupForm",//外面form的filter属性
            unionId:"id",//数据唯一标识
            name:"用户",//模块名称
            id:"users"
        };
        var userurls={
            addUrl:"/group/web/addGroupUser"
        };
        var datas=JSON.parse('${users}');
        for(var p in datas){
           datas[p].id=datas[p].userId;
        }
        var st1=polar.table.staticTable(useroptions,userurls,usercolumns,datas);
        st1.onInitDone=function () {
            var that = st1;
            that.options.toolbar.find(".polar-child-add-row").bind('click', function () {
                polar.layer.loadLayer(that.urls.addUrl, true,
                    {
                        width: that.options.formWidth,
                        height: that.options.formHeight,
                        yesContent: "添加用户",
                        yesBtn: true,
                        title: "添加" + that.options.name,
                        clearForm: false,
                        yesCall: function (index, layero) {
                            var row = polar.form.tmp.submit(index);
                            if (row != null){
                                for(var index=0;index<row.length;index++){
                                    row[index].userId=row[index].id;
                                }
                                that.addRow(row);
                            }
                        },end:function () {
                            polar.form.tmp = null;
                            polar.table.targetSecondTable=null;
                        }

                    }, null);
            });
            st1.options.toolbar.find(".polar-child-delete-row").bind('click', function () {
                var rows = that.getSelectRow();
                if (rows.length == 0) {
                    polar.dialog.alert("提示", "请选择至少一条数据");
                } else {
                    var ids = [];
                    for (var index = 0; index < rows.length; index++) {
                        ids.push(rows[index][that.options.unionId]);
                    }
                    that.deleteRow(ids);
                }
            });
            layui.form.on('checkbox(' + st1.options.id + '_polar-check-all)', function (data) {
                that.options.table.find(".select-item").attr("checked", data.elem.checked);
                layui.form.render("checkbox", that.options.formFilter);
            });
        }
        st1.addRow=function (row) {
            for(var index=0;index<row.length;index++){
                var has=false;
                for(var index2=0;index2<st1.data.length;index2++){
                    if(st1.data[index2].userId==row[index].userId){
                        has=true;
                        break;
                    }
                }
                if(!has){
                    st1.data.push(row[index]);
                }
            }
            st1.renderData();
        }
        st1.init();
        form.init(form);
	});
</script>
