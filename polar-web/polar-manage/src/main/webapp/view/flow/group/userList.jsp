<%@page language="java" contentType="text/html; charset=utf-8"
        pageEncoding="utf-8" %>
<%@ include file="/view/includes/tag.jsp" %>
<div class="polar-content"
     cache='true' id="usertableListDiv"  >
    <input type="hidden" value='1' class="polar-page"/>
    <input type="hidden" value='10' class="polar-rows"/>
    <form class=" polar-list-form layui-form" role="form" lay-filter='usertableListFormFilter'>
        <div style="display: none" id="listBtn2">
            <div class="polar-detail-inner" polar-data="[id]" style="cursor:pointer">
                <span style="color:#019858">[userName]</span>
            </div>
        </div>
        <div style="display: none" class="polar-btn-inner-template-disable">

        </div>

        <div class="layui-form-item polar-scroll layui-row">
            <div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md6">
                <label class="layui-form-label">用户状态：</label>
                <div class="layui-input-block">
                    <select name='state' cache="true">
                        <option value="">全部</option>
                        <option value="0">已禁用</option>
                        <option value="1">正常</option>
                    </select>
                </div>
            </div>
            <div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md6">
                <label class="layui-form-label">昵称：</label>
                <div class="layui-input-block">
                    <form:input name="nickName" placeholder="请输入昵称" cache="true"/>
                </div>
            </div>

            <div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md6">
                <label class="layui-form-label">手机号：</label>
                <div class="layui-input-block">
                    <form:input name="phone" placeholder="请输入手机号" cache="true"/>
                </div>
            </div>
            <div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md6">
                <label class="layui-form-label">邮箱：</label>
                <div class="layui-input-block">
                    <form:input name="email" placeholder="请输入邮箱" cache="true"/>
                </div>
            </div>

        </div>
    </form>
    <div  class="layui-inline polar-toolbar">

        <div class="pull-right">
            <button class="layui-btn  layui-btn-sm layui-btn-primary polar-refresh">
                <i class="fa fa-refresh"></i> 刷新
            </button>
            <button class="layui-btn  layui-btn-sm layui-btn-primary polar-reset">
                <i class="fa fa-circle-o-notch"></i> 重置
            </button>
            <button class="layui-btn  layui-btn-sm layui-btn-primary polar-search">
                <i class="fa fa-search-plus"></i> 搜索
            </button>
        </div>
    </div>
    <table data-striped="true" id="usercontent_table" lay-filter="usercontent_table"></table>
    <script type="text/javascript">
        $(function () {
            var columns = [[{
                checkbox: true,
                tableWidth: 50
            }
                , {
                    "field": "userName",
                    "title": "用户名"
                    ,minWidth:150
                } , {
                    "field": "userType",
                    "title": "用户类型",
                    formatter: function (row, options) {
                        if (row.userType == 1) {
                            return '普通用户';
                        } else {
                            return '第三方用户';
                        }
                    }
                    ,minWidth:150
                }, {
                    "field": "state",
                    "title": "状态",
                    formatter: function (row, options) {
                        if (row.state == 0) {
                            return '<span style="color:#F00">已禁用</span>';
                        } else {
                            return '<span style="color:#019858">正常</span>';
                        }
                    }
                    ,minWidth:100
                }
                , {
                    "field": "logCount",
                    "title": "登陆次数"
                    , miniTableWidth: 100
                    ,minWidth:150
                }
                , {
                    "field": "nickName",
                    "title": "昵称"
                    ,minWidth:150
                }
                , {
                    "field": "logInIp",
                    "title": "登陆IP"
                    ,minWidth:150
                }, {
                    "field": "logInDate",
                    "title": "登陆日期"
                    ,minWidth:150
                }
                , {
                    "field": "createDate",
                    "title": "创建日期"
                    ,minWidth:150
                }
                , {
                    "field": "phone",
                    "title": "手机号"
                    ,minWidth:150
                }
               ]];
            var options = { //配置文件
                id: "usercontent_table",//表编号
                outDivId: "usertableListDiv",//最外层的div编号，用来重绘表格高度
                formFilter:"usertableListFormFilter",
                unionId: "id",//数据的唯一编号
                netIdKey: "id",//网络请求时，传入的主键编号
                netMulitKey: "ids",//网络请求时，一组主键编号的key
                name: "用户",//此模块的名称
                formWidth: '1000px',//表单宽度
                formHeight: '600px',//表单高度
                limits: [10, 20, 30, 50, 100]//页码数据

            };
            var urls = {
                listUrl: '/group/json/userList'
            };
            var table = polar.table.layui(columns, options, urls);
            table.onQuery = function (that, args) {
                if (!polar.isNull(args.createDate)) {
                    args.createDate = args.createDate + " 00:00:00";
                }
                if (!polar.isNull(args.logInDate)) {
                    args.logInDate = args.logInDate + " 00:00:00";
                }
                return args;
            }
            table.onDataDone = function (that) {

            };
            table.onInitDone = function (that) {

            }
            var form={
                submit:function (index) {
                    var rows=table.getSelection(table);
                    if(rows.length==0){
                        polar.dialog.alert("提示","请至少选择一行数据")
                        return null;
                    }else{
                        layui.layer.close(index);//关闭图层
                        return rows;
                    }
                }
            };
            polar.form.tmp = form;
            table.init(table,true);
        });
    </script>
</div>