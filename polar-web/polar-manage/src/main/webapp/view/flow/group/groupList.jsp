<%@page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="/view/includes/tag.jsp"%>
<div class="polar-content"
	cache='true' id="groupOuterDiv" >
    <input type="hidden" value='1' class="polar-page" />
    <input type="hidden" value='10' class="polar-rows" />
	<form class="polar-list-form layui-form" role="form" lay-filter="groupFormFilter" >
		<blockquote class="layui-elem-quote polar-title">操作组</blockquote>
		<div class="polar-btn-inner-template">
			<shiro:hasPermission name="polar:group:view:detail">
			<button class="layui-btn   layui-btn-primary layui-btn-xs  polar-detail-inner" polar-data="[id]">
				<i class="fa fa-eye"></i> <span class="polar-btn-content">查看</span>
			</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="polar:group:edit">
			<button class="layui-btn  layui-btn-primary layui-btn-xs polar-edit-inner" polar-data="[id]">
				<i class="fa fa-edit"></i> <span class="polar-btn-content">修改</span>
			</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="polar:group:delete">
			<button class="layui-btn    layui-btn-primary layui-btn-xs polar-delete-inner" polar-data="[id]">
				<i class="fa fa-remove"></i> <span class="polar-btn-content">删除</span>
			</button>
			</shiro:hasPermission>
		</div>
		<div class="layui-form-item polar-scroll  layui-row">
			<div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md4 layui-col-lg3">
				<label class="layui-form-label">分组名称：</label>
				<div class="layui-input-block">
					<form:input name="groupName" placeholder="请输入分组名称" cache="true" />
				</div>
			</div>

		</div>
	</form>
    <div class="layui-inline polar-toolbar">
		<shiro:hasPermission name="polar:group:add">
		<button class="layui-btn  layui-btn-sm layui-btn-primary polar-add">
			<i class="fa fa-plus"></i> <span class="polar-btn-content">新增</span>
		</button>
		</shiro:hasPermission>
		<shiro:hasPermission name="polar:group:view:detail">
		<button class="layui-btn  layui-btn-sm layui-btn-primary  polar-detail">
			<i class="fa fa-eye"></i> <span class="polar-btn-content">查看</span>
		</button>
		</shiro:hasPermission>
		<shiro:hasPermission name="polar:group:edit">
		<button class="layui-btn  layui-btn-sm layui-btn-primary polar-edit">
			<i class="fa fa-pencil"></i> <span class="polar-btn-content">编辑</span>
		</button>
		</shiro:hasPermission>
		<shiro:hasPermission name="polar:group:delete">
		<button class="layui-btn  layui-btn-sm layui-btn-primary polar-delete">
			<i class="fa fa-remove"></i> <span class="polar-btn-content">删除</span>
		</button>
		</shiro:hasPermission>
		<div class="pull-right">
			<button class="layui-btn  layui-btn-sm layui-btn-primary polar-refresh">
				<i class="fa fa-refresh"></i> <span class="polar-btn-content">刷新</span>
			</button>
			<button class="layui-btn  layui-btn-sm layui-btn-primary polar-reset">
				<i class="fa fa-circle-o-notch"></i> <span class="polar-btn-content">重置</span>
			</button>
			<button class="layui-btn  layui-btn-sm layui-btn-primary polar-search">
				<i class="fa fa-search-plus"></i> <span class="polar-btn-content">搜索</span>
			</button>
		</div>
	</div>
	<table data-striped="true" id="groupTable" lay-filter="groupTable"></table>
	<script type="text/javascript">
	$(function(){
		var columns=[[ {
							checkbox : true,
							tableWidth : 50
						}
							, {
							"field" : "groupName",
							"title" : "分组名称",
							sort : true
                            ,minWidth:100
							,weigth:1
						}
							, {
							"field" : "groupDecribe",
							"title" : "组描述",
							sort : true
                            ,minWidth:100
							,weigth:1
						}
						, {
							field : 'operate',
							title : '操作',
							formatter : function(row,options){
							 return $("#groupOuterDiv .polar-list-form .polar-btn-inner-template").html().split('[id]').join(row.id);
							}
							,tableWidth : $("#groupOuterDiv .polar-list-form .polar-btn-inner-template").width()+30
                			,miniTableWidth:$("#groupOuterDiv .polar-list-form .polar-btn-inner-template").width()+30
						} ]];
			var options={ //配置文件
				formFilter:"groupFormFilter",
				id:"groupTable",//表编号
				outDivId:"groupOuterDiv",//最外层的div编号，用来重绘表格高度
				unionId:"id",//数据的唯一编号
				netIdKey:"id",//网络请求时，传入的主键编号
				netMulitKey:"ids",//网络请求时，一组主键编号的key
				name:"操作组",//此模块的名称
				formWidth:'1000px',//表单宽度
				formHeight:'600px',//表单高度
				limits:[ 10, 20, 30, 50, 100 ]//页码数据
			};	
			var urls={
				listUrl:'/group/json/pageList',//列表的url
				addPage:"/group/web/add",//新增页面的url
				viewPage:"/group/web/detail",//查看页面的url
				deleteSingle:"/group/json/deleteById",//删除单条的url
				deleteMulit:"/group/json/deleteMulitById",//删除多条数据的url
				updatePage:"/group/web/update",//编辑页面的url
				updateField:"/group/json/updateField", //更新单个字段的url	
				exportExcell:"/group/json/exportExcell",//导出excell的路径
				importExcell:"/group/json/importExcell", //导入excell的路径
				importExcellModel:"/group/json/importExcellModel" //获取导入模板的路径
			};
			var table=polar.table.layui(columns,options,urls);
			table.init(table);
			$(window).resize(function() { 
				table.resizeTable();
			});	
	});
	</script>
</div>