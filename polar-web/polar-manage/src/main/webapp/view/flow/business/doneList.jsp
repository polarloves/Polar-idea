<%@page language="java" contentType="text/html; charset=utf-8"
        pageEncoding="utf-8" %>
<%@ include file="/view/includes/tag.jsp" %>
<%@ page import="java.util.*" %>
<%@ page import="polar.island.core.entity.DictEntity" %>
<%
    List stateType = new ArrayList();
    stateType.add(new DictEntity("全部", ""));
    stateType.add(new DictEntity("已召回", "-1"));
    stateType.add(new DictEntity("已发起", "0"));
    stateType.add(new DictEntity("进行中", "1"));
    stateType.add(new DictEntity("已完成", "2"));
    request.setAttribute("stateType", stateType);
%>
<div class="polar-content"
     cache='true' id="businessDoneOuterDiv">
    <input type="hidden" value='1' class="polar-page"/>
    <input type="hidden" value='10' class="polar-rows"/>
    <form class="polar-list-form layui-form" role="form" lay-filter="businessDoneFormFilter">
        <blockquote class="layui-elem-quote polar-title">${form.functionName}</blockquote>
        <div class="polar-btn-inner-template">
            <button class="layui-btn   layui-btn-primary layui-btn-xs  polar-detail-inner" polar-data="[id]">
                <i class="fa fa-eye"></i> <span class="polar-btn-content">查看</span>
            </button>
            <button class="layui-btn   layui-btn-primary layui-btn-xs  polar-history-inner" polar-data="[id]">
                <i class="fa fa-history"></i> <span class="polar-btn-content">历史记录</span>
            </button>
        </div>
        <div class="layui-form-item polar-scroll  layui-row">
            <div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md4 layui-col-lg3">
                <label class="layui-form-label">状态：</label>
                <div class="layui-input-block">
                    <form:select itemValues="${stateType}" name="state" cache="true"></form:select>
                </div>
            </div>
            <div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md4 layui-col-lg3">
                <label class="layui-form-label">流程标题：</label>
                <div class="layui-input-block">
                    <form:input name="title" placeholder="请输入流程标题" cache="true"/>
                </div>
            </div>
            <div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md4 layui-col-lg3">
                <label class="layui-form-label">发起时间：</label>
                <div class="layui-input-block">
                    <form:inputTime name="searchDate" placeholder="请输入发起时间" cache="true" format="yyyy-MM-dd" range="~"
                                    id="createDateList"/>
                </div>
            </div>
        </div>
    </form>
    <div class="layui-inline polar-toolbar">
        <div class="pull-right">
            <button class="layui-btn  layui-btn-sm layui-btn-primary polar-refresh">
                <i class="fa fa-refresh"></i> <span class="polar-btn-content">刷新</span>
            </button>
            <button class="layui-btn  layui-btn-sm layui-btn-primary polar-reset">
                <i class="fa fa-circle-o-notch"></i> <span class="polar-btn-content">重置</span>
            </button>
            <button class="layui-btn  layui-btn-sm layui-btn-primary polar-search">
                <i class="fa fa-search-plus"></i> <span class="polar-btn-content">搜索</span>
            </button>
        </div>
    </div>
    <table data-striped="true" id="businessDoneTable" lay-filter="businessDoneTable"></table>
    <script type="text/javascript">
        $(function () {
            var columns = [[{
                checkbox: true,
                tableWidth: 50
            }
                , {
                    "field": "title",
                    "title": "流程标题",
                    sort: true
                    , minWidth: 100
                    , weigth: 1
                }, {
                    "field": "state",
                    "title": "状态",
                    sort: true
                    , minWidth: 100
                    , weigth: 1
                    , formatter: function (row, options) {
                        <c:forEach items="${stateType}" var="item">
                        <c:if test="${not empty item.value}">
                        if (row.state ==${item.value}) {
                            return "${item.text}";
                        }
                        </c:if>
                        </c:forEach>
                        return "";
                    }
                }
                , {
                    "field": "createDate",
                    "title": "发起时间",
                    sort: true
                    , minWidth: 100
                    , weigth: 1
                }

                , {
                    field: 'operate',
                    title: '操作',
                    formatter: function (row, options) {
                        var str = $("#businessDoneOuterDiv .polar-list-form .polar-btn-inner-template").html().split('[id]').join(row.id);
                        return str;
                    }
                    , tableWidth: 260
                    , miniTableWidth: 150
                }]];
            var options = { //配置文件
                formFilter: "businessDoneFormFilter",
                id: "businessDoneTable",//表编号
                outDivId: "businessDoneOuterDiv",//最外层的div编号，用来重绘表格高度
                unionId: "id",//数据的唯一编号
                netIdKey: "id",//网络请求时，传入的主键编号
                netMulitKey: "ids",//网络请求时，一组主键编号的key
                name: "${form.functionName}",//此模块的名称
                formWidth: '${form.formWidth}',//表单宽度
                formHeight: '${form.formHeight}',//表单高度
                limits: [10, 20, 30, 50, 100]//页码数据
            };
            var urls = {
                listUrl: '/business/json/doneList/${flowId}',//列表的url
                viewPage: "/business/web/detail",//查看页面的url
                history: "/business/web/history",
            };
            var table = polar.table.layui(columns, options, urls);
            table.onDataDone = function (that) {
                $("#" + that.options.outDivId).find("td .polar-history-inner").bind("click", function () {
                    var id = $(this).attr('polar-data');
                    var data = {};
                    data[that.options.unionId] = id;
                    polar.layer.loadLayer(that.urls.history, true,
                        {
                            width: '600px',
                            height: '400px',
                            yesBtn: false,
                            title: "历史记录",
                            clearForm: false
                        }, data);
                });
            };
            table.init(table);
            $(window).resize(function () {
                table.resizeTable();
            });
        });
    </script>
</div>