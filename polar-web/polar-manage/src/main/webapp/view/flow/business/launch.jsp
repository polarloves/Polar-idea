<%@page language="java" contentType="text/html; charset=utf-8"
        pageEncoding="utf-8" %>
<%@ include file="/view/includes/tag.jsp" %>
<style>
    .form-div {
        height: calc(100% - 50px);
        width: 100%;
    }

    .polar-operate {
        height: 50px;
        width: 100%;
        background-color: rgba(34, 0, 24, 0.1);
        padding-top: 10px;
        padding-bottom: 10px;
        padding-right: 30px;
    }
</style>
<div class='form-div layui-side-scroll' xmlns:t="http://www.w3.org/1999/html">
    <form method="post" class="layui-form required-validate polar-form" id="flowForm" cache="true"
          lay-filter="flowForm">
        <input type="hidden" name="id" value='${flow.id}' cache="true"/>
        <div class="layui-form-item ">
            <div class="layui-row">
                <div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md4 layui-col-lg3">
                    <label class="layui-form-label polar-form-title">标题：</label>
                    <div class="layui-input-block polar-form-content">
                    <form:input name="title" placeholder="请输入标题" cache="true" vertify="required" tag="标题" maxLength="50" minLength="3"></form:input>
                    </div>
                </div>
                <c:forEach items="${columns}" var="item">
                <div class="layui-inline layui-col-xs${item.small} layui-col-sm${item.middle} layui-col-md${item.big}  layui-col-lg${item.larger}">
                    <label class="layui-form-label polar-form-title">${item.label}：</label>
                    <div class="layui-input-block polar-form-content">
                    <c:choose>
                        <c:when test="${item.type==0}">
                            <!-- 单行文本 -->
                             <form:input name="${item.key}" placeholder="${item.placeholder}" cache="true" vertify="${item.validate}" tag="${item.label}" maxLength="${item.extendsObject.maxLength}" minLength="${item.extendsObject.minLength}"></form:input>
                        </c:when>
                        <c:when test="${item.type==1}">
                            <!-- 文本域 -->
                            <form:textarea cache="true" name="${item.key}"  vertify="${item.validate}"  tag="${item.label}" maxLength="${item.extendsObject.maxLength}" minLength="${item.extendsObject.minLength}"></form:textarea>
                        </c:when>
                        <c:when test="${item.type==2}">
                            <!-- 整数 -->
                            <form:inputNumber name="${item.key}" placeholder="${item.placeholder}" cache="true" vertify="${item.validate}" tag="${item.label}" maxLength="${item.extendsObject.maxLength}" minLength="${item.extendsObject.minLength}"></form:inputNumber>
                        </c:when>
                        <c:when test="${item.type==3}">
                            <!-- 浮点数 -->
                            <form:inputNumeric name="${item.key}" placeholder="${item.placeholder}" cache="true" vertify="${item.validate}" tag="${item.label}" maxLength="${item.extendsObject.maxLength}" minLength="${item.extendsObject.minLength}"></form:inputNumeric>
                        </c:when>
                        <c:when test="${item.type==4}">
                            <!-- 下拉列表 -->
                            <form:select itemValues="${fns:getDict(item.extendsObject.name)}" name="${item.key}" cache="true" emptyValue="${item.extendsObject.emptyValue}"></form:select>
                        </c:when>
                        <c:when test="${item.type==5}">
                            <!-- 富文本编辑器 -->
                            <form:richtext cache="true" name="${item.key}"  vertify="${item.validate}"  tag="${item.label}" maxLength="${item.extendsObject.maxLength}" minLength="${item.extendsObject.minLength}"></form:richtext>
                        </c:when>
                        <c:when test="${item.type==6}">
                            <!-- 日期 -->
                            <form:inputTime format="${item.extendsObject.format}" type="${item.extendsObject.type}" name="${item.key}" placeholder="${item.placeholder}" cache="true" tag="${item.label}"></form:inputTime>
                        </c:when>
                        <c:when test="${item.type==8}">
                            <!-- 单图片 -->
                            <form:img name="${item.key}" width="${item.extendsObject.width}" height="${item.extendsObject.height}"></form:img>
                        </c:when>
                        <c:when test="${item.type==9}">
                            <!-- 多图片 -->
                            <form:multiImg name="${item.key}" width="${item.extendsObject.width}" height="${item.extendsObject.height}" max="${item.extendsObject.max}" ></form:multiImg>
                        </c:when>
                        <c:when test="${item.type==10}">
                            <!-- 单文件 -->
                            <form:file name="${item.key}" type="${item.extendsObject.type}"></form:file>
                        </c:when>
                        <c:when test="${item.type==11}">
                            <!-- 多文件 -->
                            <form:files name="${item.key}" type="${item.extendsObject.type}"  max="${item.extendsObject.max}"></form:files>
                        </c:when>
                        <c:when test="${item.type==12}">
                            <!-- 单选树形结构 -->
                            <form:codeSingleTree id="${item.key}" title="${item.extendsObject.title}" name="${item.key}" values="${fns:getTreeJson(item.extendsObject.name)}"  width="${item.extendsObject.width}" height="${item.extendsObject.height}"></form:codeSingleTree>
                        </c:when>
                        <c:when test="${item.type==13}">
                            <!-- 多选树形结构 -->
                            <form:codeTree id="${item.key}" title="${item.extendsObject.title}" name="${item.key}" values="${fns:getTreeJson(item.extendsObject.name)}"  width="${item.extendsObject.width}" height="${item.extendsObject.height}"></form:codeTree>
                        </c:when>
                        <c:when test="${item.type==14}">
                            <!-- 省市区选择 -->
                            <form:area name="${item.key}" id="${item.key}" ></form:area>
                        </c:when>
                        <c:when test="${item.type==15}">
                            <form:ck   name="${item.key}" title="" ></form:ck>
                        </c:when>
                    </c:choose>
                    </div>
                </div>
                </c:forEach>

            </div>
        </div>
    </form>
</div>
<div class="polar-operate">
    <button class="layui-btn  layui-btn-sm layui-btn-normal pull-right polar-save">
        <i class="fa fa-pencil"></i> <span class="polar-btn-content">保存</span>
    </button>
</div>
<script>
    $(function () {
        var options = {
            url: "/business/json/start",
            id: "flowForm",//定义表单编号,要求lay-flter也为此
            unionId: "id"//主键编号
        };
        var form = polar.form.Form(options);
        form.validate = function (that) {
            return polar.form.Verification("#" + that.options.id);
        };
        form.done = function (that, layuiForm) {//加载完成的回调
            var layedit = layui.layedit;
            that.indexs = {};
            <c:forEach items="${columns}" var="item">
            <c:if test="${item.type==5}">
            that.indexs["${item.key}"]=layedit.build('${item.key}');
            </c:if>
            </c:forEach>
        };
        form.onBackground=function(that){
            var layedit = layui.layedit;
            <c:forEach items="${columns}" var="item">
                <c:if test="${item.type==5}">
                $("textarea[name='${item.key}']").html(layedit.getContent(that.indexs['${item.key}']));
                </c:if>
            </c:forEach>
        };
        form.beforeValidate = function (that) {
            var layedit = layui.layedit;
        }
        function findSelectText(ele,defaultValue){
            var value=ele.text();
            if(!polar.isNull(defaultValue)){
                if(value==defaultValue){
                    return "";
                }
            }
            return value;
        }
        function findFileStr(name){
           var result=[];
           var index=0;
           while(true){
               var ele=$('#flowForm input[name="'+name+'['+index+']"]');
               if(ele.length <= 0){
                   break;
               }else {
                   result.push(ele.val());
               }
               index++;
           }
           return result;
        }
        function findProvinceText(name){
            var item=$('#flowForm  input[name="'+name+'"]');
            var str="";
            if(!polar.isNull(item.attr("province"))){
                str=item.attr("province");
            }
            if(!polar.isNull(item.attr("city"))){
                str=str+item.attr("city");
            }
            if(!polar.isNull(item.attr("area"))){
                str=str+item.attr("area");
            }
            return str;
        }
        form.onSubmit=function(that,data){
            var layedit = layui.layedit;
            var tmpData=[];
            tmpData.push({small:"12",middle:"6",big:"4",type:-1,key:"title",label:"标题",value:data.title});
        <c:forEach items="${columns}" var="item">
            <c:if test="${item.type==5}">
            data["${item.key}"]=layedit.getContent(that.indexs['${item.key}']);
            </c:if>
            <c:choose>
                <c:when test="${item.type==0||item.type==1||item.type==2||item.type==3||item.type==5||item.type==6||item.type==8||item.type==10||item.type==15}">
            tmpData.push( {small:"${item.small}",big:"${item.big}",middle:"${item.middle}",type:${item.type},key:"${item.key}",label:"${item.label}",value:polar.isNull(data.${item.key})?"":data.${item.key}});
                </c:when>
                <c:when test="${item.type==4}">
            tmpData.push( {small:"${item.small}",big:"${item.big}",middle:"${item.middle}",type:${item.type},key:"${item.key}",label:"${item.label}",value:findSelectText($('#flowForm select[name="${item.key}"] option:selected'),"${item.extendsObject.emptyValue}")});
                </c:when>
                <c:when test="${item.type==14}">
            tmpData.push( {small:"${item.small}",big:"${item.big}",middle:"${item.middle}",type:${item.type},key:"${item.key}",label:"${item.label}",value:findProvinceText("${item.key}")});
                </c:when>
                <c:when test="${item.type==12||item.type==13}">
            tmpData.push( {small:"${item.small}",big:"${item.big}",middle:"${item.middle}",type:${item.type},key:"${item.key}",label:"${item.label}",value:$('#flowForm .polar-tree-${item.key} .polar-tree-display').val()});
                </c:when>
                <c:when test="${item.type==9||item.type==11}">
            tmpData.push( {small:"${item.small}",big:"${item.big}",middle:"${item.middle}",type:${item.type},key:"${item.key}",label:"${item.label}",value:findFileStr("${item.key}")});
                </c:when>

            </c:choose>
        </c:forEach>
            data["displayJson"]=JSON.stringify(tmpData);
            return data;
        }
        form.init(form);
        polar.tab.currentForm = $("#flowForm");//保存当前对象至tab中，方便缓存
        $(".polar-save").click(function () {
            var that = form;
            that.beforeValidate(that);
            if (that.validate(that)) {
                //提交表单
                var id = $("input[name='" + that.options.unionId + "']").val();
                var url = that.options.url;
                var data = that.params(that);
                if (that.onSubmit != null && that.onSubmit != 'undefined' && typeof that.onSubmit === 'function') {
                    that.onSubmit(that, data);
                }
                var options = {};
                options = that.onSubmitOptions(options, that);
                polar.loader.load(polar.loader.jsonSuccess, polar.loader.jsonError, url, true, options, data);
            }
        });

    });
</script>
