<%@page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="/view/includes/tag.jsp"%>
<div class='form-div' xmlns:t="http://www.w3.org/1999/html">
	<form method="post" class="layui-form required-validate polar-form" id="histroyDetailForm" lay-filter="histroyDetailForm">
        <div class="layui-form-item ">
            <div class="layui-row">
            <c:forEach items="${datas}" var="item">
                <div class="layui-inline layui-col-xs${item.small} layui-col-sm${item.middle} layui-col-md${item.big}  layui-col-lg${item.larger}">
                    <label class="layui-form-label polar-form-title">${item.label}：</label>
                    <div class="layui-input-block polar-form-content">
                        <c:choose>
                            <c:when test="${item.type==1}">
                                <form:textarea name="${item.key}" value="${item.value}" ></form:textarea>
                            </c:when>
                            <c:when test="${item.type==5}">
                                <form:richtext name="${item.key}" value="${item.value}" ></form:richtext>
                            </c:when>
                            <c:when test="${item.type==8}">
                                <form:img name="${item.key}" width="80px" height="80px" value="${item.value}"></form:img>
                            </c:when>
                            <c:when test="${item.type==9}">
                                <form:multiImg  max="-1" name="${item.key}" width="80px" height="80px" value="${item.value}"></form:multiImg>
                            </c:when>
                            <c:when test="${item.type==10}">
                                <form:file name="${item.key}" type="file" value="${item.value}"></form:file>
                            </c:when>
                            <c:when test="${item.type==11}">
                                <form:files  max="-1" name="${item.key}" type="file"  value="${item.value}"></form:files>
                            </c:when>
                            <c:when test="${item.type==15}">
                                <form:ck   name="${item.key}" title=""  value="${item.value}"></form:ck>
                            </c:when>
                            <c:otherwise>
                                <form:input name="${item.key}" value="${item.value}" readonly="readonly"></form:input>
                            </c:otherwise>
                        </c:choose>

                    </div>
                </div>
            </c:forEach>
			</div>
		</div>
	</form>
</div>
<script>
	$(function(){
        var layedit = layui.layedit;
        <c:forEach items="${datas}" var="item">
        <c:if test="${item.type==5}">
        layedit.build('${item.key}');
        </c:if>
        </c:forEach>
        layui.form.render(null,"histroyDetailForm");
	});
</script>
