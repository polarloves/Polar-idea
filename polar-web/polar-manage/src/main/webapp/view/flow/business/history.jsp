<%@page language="java" contentType="text/html; charset=utf-8"
        pageEncoding="utf-8" %>
<%@ include file="/view/includes/tag.jsp" %>
<div class='form-div' xmlns:t="http://www.w3.org/1999/html">

    <ul class="layui-timeline">
        <c:forEach items="${history}" var="item">
            <li class="layui-timeline-item polar-flow-history-detail" formwidth="${item.formWidth}"
                formheight="${item.formHeight}" >
                <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                <div class="layui-timeline-content layui-text">
                    <h3 class="layui-timeline-title">
                        <fmt:formatDate value="${item.createDate}"
                                                                     pattern="yyyy年MM月dd日 HH:mm:ss"></fmt:formatDate>
                    </h3>
                    <div style="display: none" class="displayJson">${item.displayJson}</div>
                    <p>
                            ${item.logContent}
                    </p>
                </div>
            </li>
        </c:forEach>
    </ul>
</div>
<script>

    $(function () {
        $(".polar-flow-history-detail").click(function () {
            var displayJson = $(this).find(".displayJson").html();
            var formWidth = $(this).attr("formwidth");
            var formHeight = $(this).attr("formheight");
            if (!polar.isNull(displayJson)) {
                polar.layer.loadLayer("/business/web/historyDetail", true,
                    {
                        width: formWidth,
                        height: formHeight,
                        yesBtn: false,
                        title: "历史详情",
                        clearForm: false
                    }, {displayJson:displayJson});
            }
        });
    });
</script>
