<%@page language="java" contentType="text/html; charset=utf-8"
        pageEncoding="utf-8" %>
<%@ include file="/view/includes/tag.jsp" %>

<div class='form-div'>
    <form method="post" class="layui-form required-validate polar-form" id="columnForm" lay-filter="columnForm">
        <input type="hidden" name="id" value='${flowMatches.id}'/>
        <div class="layui-form-item ">
            <div class="layui-row">
                <div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md6">
                    <label class="layui-form-label polar-form-title">任务编号：</label>
                    <div class="layui-input-block polar-form-content">
                        <form:input name="key" placeholder="请输入任务编号" tag="任务编号" value='${flowMatches.key}'
                                    vertify="maxLength,minLength,required"/>
                    </div>
                </div>
                <div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md6">
                    <label class="layui-form-label polar-form-title">对应表单：</label>
                    <div class="layui-input-block polar-form-content">
                        <form:select itemValues="${allForms}" value='${flowMatches.formId}' name="formId"></form:select>
                    </div>
                </div>
                <div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md6">
                    <label class="layui-form-label polar-form-title">操作用户：</label>
                    <div class="layui-input-block polar-form-content">
                        <form:select itemValues="${allGroups}" value='${flowMatches.groupId}' name="groupId"></form:select>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    $(function () {
        var options = {
            id: "columnForm",//定义表单编号,要求lay-flter也为此
            unionId: "id"//主键编号
        };
        var form = polar.form.TmpForm(options);
        form.validate = function (that) {
            return polar.form.Verification("#" + that.options.id);
        };
        form.done = function (that, layuiForm) {//加载完成的回调
            var layedit = layui.layedit;
            that.indexs = {};
        };
        form.beforeValidate = function (that) {
            var layedit = layui.layedit;
        }
        form.onSubmit = function (that, data) {
            var layedit = layui.layedit;
            return data;
        }
        form.init(form);
    });
</script>
