<%@page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="/view/includes/tag.jsp"%>
<div class="polar-content"
	cache='true' id="flowOuterDiv" >
    <input type="hidden" value='1' class="polar-page" />
    <input type="hidden" value='10' class="polar-rows" />
	<form class="polar-list-form layui-form" role="form" lay-filter="flowFormFilter" >
		<blockquote class="layui-elem-quote polar-title">流程管理</blockquote>
		<div class="polar-btn-inner-template">
			<shiro:hasPermission name="polar:flow:edit">
				<button class="layui-btn   layui-btn-primary layui-btn-xs  polar-menu-inner" polar-data="[id]">
					<i class="fa fa-link"></i> <span class="polar-btn-content">配置链接</span>
				</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="polar:flow:view:detail">
			<button class="layui-btn   layui-btn-primary layui-btn-xs  polar-publish-inner" polar-data="[id]" polar-state="[state]">
				<i class="fa fa-level-up"></i> <span class="polar-btn-content">发布</span>
			</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="polar:flow:edit">
				<button class="layui-btn  layui-btn-primary layui-btn-xs polar-form-inner" polar-data="[id]">
					<i class="fa fa-table"></i> <span class="polar-btn-content">匹配表单</span>
				</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="polar:flow:edit">
				<button class="layui-btn  layui-btn-primary layui-btn-xs polar-bpmn-inner" polar-data="[id]">
					<i class="fa fa-list-alt"></i> <span class="polar-btn-content">流程图</span>
				</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="polar:flow:edit">
			<button class="layui-btn  layui-btn-primary layui-btn-xs polar-edit-inner" polar-data="[id]">
				<i class="fa fa-edit"></i> <span class="polar-btn-content">修改</span>
			</button>
			</shiro:hasPermission>
			<shiro:hasPermission name="polar:flow:delete">
			<button class="layui-btn    layui-btn-primary layui-btn-xs polar-delete-inner" polar-data="[id]">
				<i class="fa fa-remove"></i> <span class="polar-btn-content">删除</span>
			</button>
			</shiro:hasPermission>
		</div>
		<div class="layui-form-item polar-scroll  layui-row">
			<div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md4 layui-col-lg3">
				<label class="layui-form-label">流程名称：</label>
				<div class="layui-input-block">
					<form:input name="flowName" placeholder="请输入流程名称" cache="true" />
				</div>
			</div>
			<div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md4 layui-col-lg3">
				<label class="layui-form-label">流程状态：</label>
				<div class="layui-input-block">
                    <select name="state">
						<option value="">全部</option>
						<option value="0">未发布</option>
						<option value="1">已发布</option>
					</select>
				</div>
			</div>
		</div>
	</form>
    <div class="layui-inline polar-toolbar">
		<shiro:hasPermission name="polar:flow:add">
		<button class="layui-btn  layui-btn-sm layui-btn-primary polar-add">
			<i class="fa fa-plus"></i> <span class="polar-btn-content">新增</span>
		</button>
		</shiro:hasPermission>
		<shiro:hasPermission name="polar:flow:view:detail">
		<button class="layui-btn  layui-btn-sm layui-btn-primary  polar-detail">
			<i class="fa fa-eye"></i> <span class="polar-btn-content">查看</span>
		</button>
		</shiro:hasPermission>
		<shiro:hasPermission name="polar:flow:edit">
		<button class="layui-btn  layui-btn-sm layui-btn-primary polar-edit">
			<i class="fa fa-pencil"></i> <span class="polar-btn-content">编辑</span>
		</button>
		</shiro:hasPermission>
		<shiro:hasPermission name="polar:flow:delete">
		<button class="layui-btn  layui-btn-sm layui-btn-primary polar-delete">
			<i class="fa fa-remove"></i> <span class="polar-btn-content">删除</span>
		</button>
		</shiro:hasPermission>

		<div class="pull-right">
			<button class="layui-btn  layui-btn-sm layui-btn-primary polar-refresh">
				<i class="fa fa-refresh"></i> <span class="polar-btn-content">刷新</span>
			</button>
			<button class="layui-btn  layui-btn-sm layui-btn-primary polar-reset">
				<i class="fa fa-circle-o-notch"></i> <span class="polar-btn-content">重置</span>
			</button>
			<button class="layui-btn  layui-btn-sm layui-btn-primary polar-search">
				<i class="fa fa-search-plus"></i> <span class="polar-btn-content">搜索</span>
			</button>
		</div>
	</div>
	<table data-striped="true" id="flowTable" lay-filter="flowTable"></table>
	<script type="text/javascript">
	$(function(){
		var columns=[[ {
							checkbox : true,
							tableWidth : 50
						}
							, {
							"field" : "flowName",
							"title" : "流程名称",
							sort : true
                            ,minWidth:100
							,weigth:1
						}
							, {
							"field" : "state",
							"title" : "流程状态",
							sort : true
                            ,minWidth:100
							,weigth:1
								,formatter : function(row,options){
							    if(row.state==0){
							        return "未发布";
								}
								return '已发布';
							}
						}
							, {
							"field" : "userName",
							"title" : "创建人",
							sort : true
                            ,minWidth:100
							,weigth:1
						}
							, {
							"field" : "createDate",
							"title" : "创建时间",
							sort : true
                            ,minWidth:100
							,weigth:1
						}
						, {
							field : 'operate',
							title : '操作',
							formatter : function(row,options){
							    var str=$("#flowOuterDiv .polar-list-form .polar-btn-inner-template").html().split('[id]').join(row.id);
							    str=str.split('[state]').join(row.state);
							 return str;
							}
							,tableWidth : $("#flowOuterDiv .polar-list-form .polar-btn-inner-template").width()+30
                			,miniTableWidth: $("#flowOuterDiv .polar-list-form .polar-btn-inner-template").width()+30
						} ]];
			var options={ //配置文件
				formFilter:"flowFormFilter",
				id:"flowTable",//表编号
				outDivId:"flowOuterDiv",//最外层的div编号，用来重绘表格高度
				unionId:"id",//数据的唯一编号
				netIdKey:"id",//网络请求时，传入的主键编号
				netMulitKey:"ids",//网络请求时，一组主键编号的key
				name:"流程定义表",//此模块的名称
				formWidth:'1000px',//表单宽度
				formHeight:'600px',//表单高度
				limits:[ 10, 20, 30, 50, 100 ]//页码数据
			};	
			var urls={
				listUrl:'/flow/json/pageList',//列表的url
				addPage:"/flow/web/add",//新增页面的url
				viewPage:"/flow/web/detail",//查看页面的url
				deleteSingle:"/flow/json/deleteById",//删除单条的url
				deleteMulit:"/flow/json/deleteMulitById",//删除多条数据的url
				updatePage:"/flow/web/update",
				updateBpmnPage:"/flow/web/bpmnData",
                matchForm:"/flow/web/matchForm",
                publish:"/flow/json/publish"

            };
			var table=polar.table.layui(columns,options,urls);
			table.onDataDone = function (that) {
                $("#" + that.options.outDivId).find("td .polar-bpmn-inner").bind("click", function () {
                    var id = $(this).attr('polar-data');
                    var data = {};
                    data[that.options.unionId] = id;
                    polar.layer.loadLayer(that.urls.updateBpmnPage, true,
                        {
                            width: '100%',
                            height:  '100%',
                            yesContent: "修改",
                            yesBtn: true,
                            title: "修改流程图",
                            clearForm: true
                        }, data);
                });
                $("#" + that.options.outDivId).find("td .polar-publish-inner").bind("click", function () {
                    var id = $(this).attr('polar-data');
              		var state=$(this).attr('polar-state');
              		if(state==1){
                        polar.dialog.confirm("提示", "流程已发布，重新发布会生成一条新的流程，您确定执行此操作吗？", function () {
                            polar.loader.load(polar.loader.jsonSuccess, polar.loader.jsonError, that.urls.publish, true, {refresh: true}, {id:id});
                        });
					}else{
                        polar.loader.load(polar.loader.jsonSuccess, polar.loader.jsonError, that.urls.publish, true, {refresh: true}, {id:id});
                    }
                });

                $("#" + that.options.outDivId).find("td .polar-menu-inner").bind("click", function () {
                    var id = $(this).attr('polar-data');
                    var str="发起流程：/business/web/launchPage/"+id+"<br/>";
                    str=str+"我发起的流程：/business/web/myList/"+id+"<br/>";
                    str=str+"我的待办：/business/web/toDoList/"+id+"<br/>";
                    str=str+"我办理的：/business/web/doneList/"+id+"<br/>";
                    str=str+"全部流程：/business/web/allList/"+id+"<br/>";
                    str='<div style="padding:20px">'+str+"</div>";
                    polar.layer.layer(str,"400px","300px",{
                        yesBtn: false,
                        title: "链接地址",
                        clearForm: false
					});
                });
                $("#" + that.options.outDivId).find("td .polar-form-inner").bind("click", function () {
                    var id = $(this).attr('polar-data');
                    var data = {};
                    data[that.options.unionId] = id;
                    polar.layer.loadLayer(that.urls.matchForm, true,
                        {
                            width: table.options.formWidth,
                            height:  table.options.formHeight,
                            yesContent: "保存",
                            yesBtn: true,
                            title: "匹配表单",
                            clearForm: true
                        }, data);
                });
			}
			table.init(table);
			$(window).resize(function() { 
				table.resizeTable();
			});	
	});
	</script>
</div>