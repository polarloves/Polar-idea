<%@page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="/view/includes/tag.jsp"%>
<div style="display: none" id="childDetailBtn">
    <button class="layui-btn   layui-btn-primary layui-btn-xs  polar-detail-child-detail-inner" polar-data="[id]" type="button">
        <i class="fa fa-eye"></i>  <span class="polar-btn-content">查看</span>
    </button>
    <button class="layui-btn  layui-btn-primary layui-btn-xs polar-detail-child-edit-inner" polar-data="[id]"  type="button">
        <i class="fa fa-edit"></i> <span class="polar-btn-content">修改</span>
    </button>
    <button class="layui-btn    layui-btn-primary layui-btn-xs polar-detail-child-delete-inner" polar-data="[id]"  type="button">
        <i class="fa fa-remove"></i> <span class="polar-btn-content">删除</span>
    </button>
</div>
<div class='form-div' xmlns:t="http://www.w3.org/1999/html">
	<form method="post" class="layui-form required-validate polar-form" id="flowForm" lay-filter="flowForm">
        <input type="hidden" name="id" value="${id}">
        <div id="columnChildToolbar">
            <button class="layui-btn    layui-btn-primary layui-btn-sm polar-child-add-row" type="button">
                <i class="fa fa-plus"></i>&nbsp;<span class="polar-btn-content">添加</span>
            </button>
            <button class="layui-btn layui-btn-primary layui-btn-sm polar-child-delete-row" type="button">
                <i class="fa fa-minus"></i>&nbsp;<span class="polar-btn-content">删除</span>
            </button>
        </div>
        <div style="width: 100%;overflow-x: auto" >
            <table  class="layui-table" id="columnTable"></table>
        </div>
	</form>
</div>
<script>
	$(function(){
		var options={
			addUrl:"/flow/json/updateFlowMatches",//定义新增的url
			upDateUrl:"/flow/json/updateFlowMatches",//定义修改的url
			id:"flowForm",//定义表单编号,要求lay-flter也为此
			unionId:"id"//主键编号
		};
		var form=polar.form.Form(options);
		form.validate=function(that){
			return polar.form.Verification("#"+that.options.id);
		};
		form.done=function(that,layuiForm){//加载完成的回调
			var layedit = layui.layedit;
			that.indexs={};
		};
		form.onSubmit=function(that,data){
			var layedit = layui.layedit;
			return data;
		}
        form.beforeValidate=function(that){
            var layedit = layui.layedit;
        }
        var columncolumns=[ {
            checkbox : true,
            tableWidth : 50
        }
            , {
                "field" : "key",
                "title" : "任务编号",
                sort : true
                ,minWidth:100
                ,weigth:1
            }
            , {
                "field" : "formId",
                "title" : "对应表单",
                sort : true
                ,minWidth:100
                ,weigth:1,
                formatter : function(row,options){
                    <c:forEach items="${allForms}" var="item">
                    if(row.formId=="${item.value}"){
                        return "${item.text}";
                    }
                    </c:forEach>
                    return "";
                }
            }
            , {
                "field" : "groupId",
                "title" : "操作用户",
                sort : true
                ,minWidth:100
                ,weigth:1,
                formatter : function(row,options){
                    <c:forEach items="${allGroups}" var="item">
                    if(row.groupId=="${item.value}"){
                        return "${item.text}";
                    }
                    </c:forEach>
                    return "";
                }
            }
            , {
                field : 'operate',
                title : '操作',
                formatter : function(row,options){
                    return $("#childDetailBtn").html().split('[id]').join(row.id);
                }
                ,tableWidth : 250
                ,miniTableWidth : 160
            } ];
        var columnoptions={
            toolbar:$("#columnChildToolbar"),//工具条的dom对象
            table:$("#columnTable"),//表格的dom对象
            formWidth:"800px",//页面宽度
            formHeight:"500px",//页面高度
            listName:"matches",//列表name
            formFilter:"flowForm",//外面form的filter属性
            unionId:"id",//数据唯一标识
            name:"匹配规则",//模块名称
            id:"column"
        };
        var columnurls={
            addUrl:"/flow/web/matchFormUpate",//添加页面的url
            detailUrl:"/flow/web/matchFormUpate",//详情页面的url
            updateUrl:"/flow/web/matchFormUpate"//修改页面的url
        };
        polar.table.staticTable(columnoptions,columnurls,columncolumns,JSON.parse('${flowMatches}')).init();
		form.init(form);
	});
</script>
