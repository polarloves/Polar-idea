<%@page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="/view/includes/tag.jsp"%>
<div class='form-div' xmlns:t="http://www.w3.org/1999/html">
	<form method="post" class="layui-form required-validate polar-form" id="flowForm" lay-filter="flowForm">

		<input type="hidden" name="id" value='${flow.id}' />
        <div class="layui-form-item ">
            <div class="layui-row">
				<div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md6">
                    <label class="layui-form-label polar-form-title">流程名称：</label>
                    <div class="layui-input-block polar-form-content">
					<form:input name="flowName" placeholder="请输入流程名称"   tag="流程名称" value='${flow.flowName}' vertify="maxLength,minLength,required"/>
                    </div>
                </div>
				<div class="layui-inline layui-col-xs12 layui-col-sm12 layui-col-md12">
                    <label class="layui-form-label polar-form-title">流程描述：</label>
                    <div class="layui-input-block polar-form-content">
					<form:textarea name="flowDescribe"    tag="流程描述" value='${flow.flowDescribe}' vertify="maxLength,minLength"/>
                    </div>
                </div>
				<div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md6">
                    <label class="layui-form-label polar-form-title">对应表单：</label>
                    <div class="layui-input-block polar-form-content">
                        <form:select itemValues="${allForms}" value='${flow.formId}' name="formId"></form:select>

                    </div>
                </div>
				<div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md6">
                    <label class="layui-form-label polar-form-title">操作用户：</label>
                    <div class="layui-input-block polar-form-content">
                        <form:select itemValues="${allGroups}" value='${flow.groupId}' name="groupId"></form:select>
                    </div>
                </div>
			</div>
		</div>
	</form>
</div>
<script>
	$(function(){
		var options={
			addUrl:"/flow/json/add",//定义新增的url
			upDateUrl:"/flow/json/updateAllById",//定义修改的url
			id:"flowForm",//定义表单编号,要求lay-flter也为此
			unionId:"id"//主键编号
		};
		var form=polar.form.Form(options);
		form.validate=function(that){
			return polar.form.Verification("#"+that.options.id);
		};
		form.done=function(that,layuiForm){//加载完成的回调
			var layedit = layui.layedit;
			that.indexs={};
		};
		form.onSubmit=function(that,data){
			var layedit = layui.layedit;
			return data;
		}
        form.beforeValidate=function(that){
            var layedit = layui.layedit;
        }
		form.init(form);
	});
</script>
