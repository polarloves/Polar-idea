<%@page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="/view/includes/tag.jsp"%>
<link rel="stylesheet" href="${ctxStatic}/bpmn/css/diagram-js.css" />
<link rel="stylesheet" href="${ctxStatic}/bpmn/vendor/bpmn-font/css/bpmn-embedded.css" />
<link rel="stylesheet" href="${ctxStatic}/bpmn/css/app.css" />
<style>
    .bjs-powered-by{
        display: none;
    }
</style>
<script src="${ctxStatic}/bpmn/bpmn.js"></script>
<div class='form-div' xmlns:t="http://www.w3.org/1999/html">
    <div class="modeler content with-diagram" id="modeler" >
        <div id="canvas" class="canvas"></div>
        <div id="properties" class="properties-panel-parent"></div>
    </div>
</div>
<script>
	$(function(){
        var bpmn=$BPMN({
            canvas:"#canvas",
            propertiesPanel:"#properties",
            container:"#modeler"
        });
        var str='${bpmnData}';
        var id="${id}";
        bpmn.create(str);
        var form={
            submit:function (index) {
                polar.dialog.loading();
                bpmn.saveXml(function (err, xml) {
                   if(polar.isNull(err)){
                       var data = {id:id,bpmnData:xml};
                       polar.loader.load(polar.loader.jsonSuccess, polar.loader.jsonError, "/flow/json/updateBpmnData", true, {refresh: true,closeIndex:index}, data);
                   }else{
                       polar.dialog.closeLoading();
                       polar.dialog.toast("流程图生成失败");
                   }
                });
            }
        };
        polar.form.target=form;
	});
</script>
