<%@page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ include file="/view/includes/tag.jsp"%>
<%@ page import="java.util.*" %>
<%@ page import="polar.island.core.entity.DictEntity" %>
<%
    List list = new ArrayList();
    list.add(new DictEntity("流程表单","0"));
    list.add(new DictEntity("操作表单","1"));
    request.setAttribute("formType", list);

    List columnType = new ArrayList();
    columnType.add(new DictEntity("单行文本","0"));
    columnType.add(new DictEntity("文本域","1"));
    columnType.add(new DictEntity("整数","2"));
    columnType.add(new DictEntity("浮点数","3"));
    columnType.add(new DictEntity("下拉列表","4"));
    columnType.add(new DictEntity("富文本编辑器","5"));
    columnType.add(new DictEntity("日期","6"));
    columnType.add(new DictEntity("大整数","7"));
    columnType.add(new DictEntity("单图片","8"));
    columnType.add(new DictEntity("多图片","9"));
    columnType.add(new DictEntity("单文件","10"));
    columnType.add(new DictEntity("多文件","11"));
    columnType.add(new DictEntity("单选树形结构","12"));
    columnType.add(new DictEntity("多选树形结构","13"));
    columnType.add(new DictEntity("省市区选择","14"));
    columnType.add(new DictEntity("复选框","15"));
    request.setAttribute("columnType", columnType);
%>
<div class='form-div'>
    <div style="display: none" id="childDetailBtn">
        <button class="layui-btn   layui-btn-primary layui-btn-xs  polar-detail-child-detail-inner" polar-data="[id]" type="button">
            <i class="fa fa-eye"></i>  <span class="polar-btn-content">查看</span>
        </button>
        <button class="layui-btn  layui-btn-primary layui-btn-xs polar-detail-child-edit-inner" polar-data="[id]"  type="button">
            <i class="fa fa-edit"></i> <span class="polar-btn-content">修改</span>
        </button>
        <button class="layui-btn    layui-btn-primary layui-btn-xs polar-detail-child-delete-inner" polar-data="[id]"  type="button">
            <i class="fa fa-remove"></i> <span class="polar-btn-content">删除</span>
        </button>
    </div>
	<form method="post" class="layui-form required-validate polar-form" id="formForm" lay-filter="formForm">
		<input type="hidden" name="id" value='${form.id}' />
        <div class="layui-form-item ">
            <div class="layui-row">
					<div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md4">
                    <label class="layui-form-label polar-form-title">表单名称：</label>
                    <div class="layui-input-block polar-form-content">
					<form:input name="formName" placeholder="请输入表单名称"   tag="表单名称" value='${form.formName}' vertify="maxLength,minLength,required"/>
                    </div>
                </div>
					<div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md4">
                    <label class="layui-form-label polar-form-title">表单类型：</label>
                    <div class="layui-input-block polar-form-content">
					<form:select itemValues="${formType}" name="formType" value='${form.formType}' />
                    </div>
                </div>
					<div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md4">
                    <label class="layui-form-label polar-form-title">表单宽度：</label>
                    <div class="layui-input-block polar-form-content">
					<form:input name="formWidth" placeholder="请输入表单宽度"   tag="表单宽度" value='${form.formWidth}' vertify="maxLength,minLength,required"/>
                    </div>
                </div>
					<div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md4">
                    <label class="layui-form-label polar-form-title">表单高度：</label>
                    <div class="layui-input-block polar-form-content">
					<form:input name="formHeight" placeholder="请输入表单高度"   tag="表单高度" value='${form.formHeight}' vertify="maxLength,minLength,required"/>
                    </div>
                </div>
					<div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md4">
                    <label class="layui-form-label polar-form-title">表单标题：</label>
                    <div class="layui-input-block polar-form-content">
					<form:input name="formTitle" placeholder="请输入表单标题"   tag="表单标题" value='${form.formTitle}' vertify="maxLength,minLength,required"/>
                    </div>
                </div>
					<div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md4">
                    <label class="layui-form-label polar-form-title">功能名称：</label>
                    <div class="layui-input-block polar-form-content">
					<form:input name="functionName" placeholder="请输入功能名称"   tag="功能名称" value='${form.functionName}' vertify="maxLength,minLength,required"/>
                    </div>
                </div>
					<div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md4">
                    <label class="layui-form-label polar-form-title">按钮名称：</label>
                    <div class="layui-input-block polar-form-content">
					<form:input name="buttonName" placeholder="请输入操作按钮名称"   tag="操作按钮名称" value='${form.buttonName}' vertify="maxLength,minLength,required"/>
                    </div>
                </div>
                <div class="layui-inline layui-col-xs12 layui-col-sm12 layui-col-md12">
                    <label class="layui-form-label polar-form-title">表单描述：</label>
                    <div class="layui-input-block polar-form-content">
					<form:textarea name="formDescribe"    tag="表单描述" value='${form.formDescribe}' vertify="maxLength,minLength"/>
                    </div>
                </div>
            </div>
            </div>
        <div class="layui-tab layui-tab-brief " >
            <ul class="layui-tab-title">
  				<li class="layui-this"  lay-id="tab_column">表单列属性</li>
            </ul>
            <div class="layui-tab-content" >
                <div class="layui-tab-item layui-show">
                    <div id="columnChildToolbar">
                        <button class="layui-btn    layui-btn-primary layui-btn-sm polar-child-add-row" type="button">
                            <i class="fa fa-plus"></i>&nbsp;<span class="polar-btn-content">添加</span>
                        </button>
                        <button class="layui-btn layui-btn-primary layui-btn-sm polar-child-delete-row" type="button">
                            <i class="fa fa-minus"></i>&nbsp;<span class="polar-btn-content">删除</span>
                        </button>
                    </div>
                    <div style="width: 100%;overflow-x: auto" >
                        <table  class="layui-table" id="columnTable"></table>
                    </div>
                </div>
            </div>
        </div>
	</form>
</div>
<script>
	$(function(){
		var options={
			addUrl:"/form/json/add",//定义新增的url
			upDateUrl:"/form/json/updateAllById",//定义修改的url
			id:"formForm",//定义表单编号,要求lay-flter也为此
			unionId:"id"//主键编号
		};
		var form=polar.form.Form(options);
		form.validate=function(that){
			return polar.form.Verification("#"+that.options.id);
		};
		form.done=function(that,layuiForm){//加载完成的回调
			var layedit = layui.layedit;
			that.indexs={};
		
		};
        form.beforeValidate=function(that){
            var layedit = layui.layedit;
        }
		form.onSubmit=function(that,data){
			var layedit = layui.layedit;
			return data;
		}
				var columncolumns=[ {
							checkbox : true,
							tableWidth : 50
						}, {
                                "field" : "key",
                                "title" : "键名",
                                sort : true
                                ,minWidth:100
                                ,weigth:1
                            }
							, {
                                "field" : "label",
                                "title" : "列名",
								sort : true
								,minWidth:100
								,weigth:1
                            }
							, {
                                "field" : "type",
                                "title" : "列类型",
								sort : true
								,minWidth:100
								,weigth:1,
                                formatter : function(row,options){
                                   <c:forEach items="${columnType}" var="item">
                                    if(row.type=="${item.value}"){
                                        return "${item.text}";
                                    }
                                    </c:forEach>
                                    return "";
                                }
                            }

                , {
                    field : 'operate',
                    title : '操作',
                    formatter : function(row,options){
                        return $("#childDetailBtn").html().split('[id]').join(row.id);
                    }
                    ,tableWidth : 250
                    ,miniTableWidth : 160
                } ];
				var columnoptions={
                    toolbar:$("#columnChildToolbar"),//工具条的dom对象
					table:$("#columnTable"),//表格的dom对象
                    formWidth:"800px",//页面宽度
                    formHeight:"420px",//页面高度
                    listName:"columnEntity",//列表name
                    formFilter:"formForm",//外面form的filter属性
                    unionId:"id",//数据唯一标识
					name:"表单列属性",//模块名称
					id:"column"
                };
				var columnurls={
                    addUrl:"/form/web/columnAdd",//添加页面的url
                    detailUrl:"/form/web/columnDetail",//详情页面的url
                    updateUrl:"/form/web/columnUpdate"//修改页面的url
                };
				polar.table.staticTable(columnoptions,columnurls,columncolumns,JSON.parse('${columnEntity}')).init();
		form.init(form);
	});
</script>
