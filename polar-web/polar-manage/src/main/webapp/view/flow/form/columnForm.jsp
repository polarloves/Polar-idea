<%@page language="java" contentType="text/html; charset=utf-8"
        pageEncoding="utf-8" %>
<%@ include file="/view/includes/tag.jsp" %>
<%@ page import="java.util.*" %>
<%@ page import="polar.island.core.entity.DictEntity" %>
<%

    List columnType = new ArrayList();
    columnType.add(new DictEntity("单行文本", "0"));
    columnType.add(new DictEntity("文本域", "1"));
    columnType.add(new DictEntity("整数", "2"));
    columnType.add(new DictEntity("浮点数", "3"));
    columnType.add(new DictEntity("下拉列表", "4"));
    columnType.add(new DictEntity("富文本编辑器", "5"));
    columnType.add(new DictEntity("日期", "6"));
    columnType.add(new DictEntity("单图片", "8"));
    columnType.add(new DictEntity("多图片", "9"));
    columnType.add(new DictEntity("单文件", "10"));
    columnType.add(new DictEntity("多文件", "11"));
    columnType.add(new DictEntity("单选树形结构", "12"));
    columnType.add(new DictEntity("多选树形结构", "13"));
    columnType.add(new DictEntity("省市区选择", "14"));
    columnType.add(new DictEntity("复选框", "15"));
    request.setAttribute("columnType", columnType);


    List validateType = new ArrayList();
    validateType.add(new DictEntity("必填", "1"));
    validateType.add(new DictEntity("手机号", "2"));
    validateType.add(new DictEntity("邮箱", "3"));
    validateType.add(new DictEntity("身份证号", "4"));
    validateType.add(new DictEntity("最小长度", "5"));
    validateType.add(new DictEntity("最大长度", "6"));
    validateType.add(new DictEntity("整数", "7"));
    validateType.add(new DictEntity("数字", "8"));
    request.setAttribute("validateType", validateType);
%>
<div class='form-div'>
    <form method="post" class="layui-form required-validate polar-form" id="columnForm" lay-filter="columnForm">
        <input type="hidden" name="id" value='${column.id}'/>
        <div class="layui-form-item ">
            <div class="layui-row">
                <div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md6">
                    <label class="layui-form-label polar-form-title">列名：</label>
                    <div class="layui-input-block polar-form-content">
                        <form:input name="label" placeholder="请输入列名" tag="列名" value='${column.label}'
                                    vertify="maxLength,minLength,required"/>
                    </div>
                </div>
                <div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md6">
                    <label class="layui-form-label polar-form-title">键名：</label>
                    <div class="layui-input-block polar-form-content">
                        <form:input name="key" placeholder="请输入键名" tag="键名" value='${column.key}'
                                    vertify="maxLength,minLength,required"/>
                    </div>
                </div>
                <div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md6">
                    <label class="layui-form-label polar-form-title">列类型：</label>
                    <div class="layui-input-block polar-form-content">
                        <form:select itemValues="${columnType}" name="type" value="${column.type}"></form:select>
                    </div>
                </div>


                <div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md6">
                    <label class="layui-form-label polar-form-title">提示语：</label>
                    <div class="layui-input-block polar-form-content">
                        <form:input name="placeholder" placeholder="请输入提示语" tag="提示语" value='${column.placeholder}'
                                    vertify="maxLength"/>
                    </div>
                </div>
                <div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md6">
                    <label class="layui-form-label polar-form-title">手机占比：</label>
                    <div class="layui-input-block polar-form-content">
                        <form:input name="small" placeholder="请输入手机占比" tag="手机占比" value='${column.small}'
                                    vertify="maxLength,minLength,required,number"/>
                    </div>
                </div>
                <div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md6">
                    <label class="layui-form-label polar-form-title">平板占比：</label>
                    <div class="layui-input-block polar-form-content">
                        <form:input name="middle" placeholder="请输入平板占比" tag="平板占比" value='${column.middle}'
                                    vertify="maxLength,minLength,required,number"/>
                    </div>
                </div>
                <div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md6">
                    <label class="layui-form-label polar-form-title">电脑占比：</label>
                    <div class="layui-input-block polar-form-content">
                        <form:input name="big" placeholder="请输入电脑占比" tag="电脑占比" value='${column.big}'
                                    vertify="maxLength,minLength,required,number"/>
                    </div>
                </div>
                <div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md6">
                    <label class="layui-form-label polar-form-title">大屏占比：</label>
                    <div class="layui-input-block polar-form-content">
                        <form:input name="larger" placeholder="请输入大屏占比" tag="大屏占比" value='${column.larger}'
                                    vertify="maxLength,minLength,required,number"/>
                    </div>
                </div>
                <div class="layui-inline layui-col-xs12 layui-col-sm6 layui-col-md6">
                    <label class="layui-form-label polar-form-title">排序号：</label>
                    <div class="layui-input-block polar-form-content">
                        <form:input name="orderNum" placeholder="请输入排序号" tag="排序号" value='${column.orderNum}'
                                    vertify="maxLength,minLength,required,number"/>
                    </div>
                </div>
                <div class="layui-inline layui-col-xs12 layui-col-sm12 layui-col-md12">
                    <label class="layui-form-label polar-form-title">扩展参数：</label>
                    <div class="layui-input-block polar-form-content">
                        <form:input name="extendsJson" placeholder="请输入扩展参数" tag="扩展参数" value='${column.extendsJson}'
                                    vertify="maxLength"/>
                    </div>
                </div>
                <div class="layui-inline layui-col-xs12 layui-col-sm12 layui-col-md12">
                    <label class="layui-form-label polar-form-title">校验方式：</label>
                    <div class="layui-input-block polar-form-content">
                        <c:forEach var="item" items="${validateType}">
                            <c:choose>
                                <c:when test="${fn:contains(column.validateType, item.value)}">
                                    <form:ck name="validateType" value="${item.value}" checkedValue="${item.value}"
                                             title="${item.text}"></form:ck>
                                </c:when>
                                <c:otherwise>
                                    <form:ck name="validateType" checkedValue="${item.value}"
                                             title="${item.text}"></form:ck>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    $(function () {
        var options = {
            addUrl: "/column/json/add",//定义新增的url
            upDateUrl: "/column/json/updateAllById",//定义修改的url
            id: "columnForm",//定义表单编号,要求lay-flter也为此
            unionId: "id"//主键编号
        };
        var form = polar.form.TmpForm(options);
        form.onFormVlidate = function (that) {
            var keyValue = $('input[name="key"]').val();
            if (keyValue == "displayJson" || keyValue == "title" || keyValue == "id" || keyValue == "flowId") {
                layui.layer.msg(keyValue + "为系统名称，请勿占用", {
                    icon: 5,
                    shift: 6
                });
                return false;
            }
            return true;
        };
        form.done = function (that, layuiForm) {//加载完成的回调
            var layedit = layui.layedit;
            that.indexs = {};
        };
        form.beforeValidate = function (that) {
            var layedit = layui.layedit;
        }
        form.onSubmit = function (that, data) {
            var layedit = layui.layedit;
            if (!polar.isNull(data.validateType)) {
                var str = '';
                for (var position in data.validateType) {
                    if (str.length == 0) {
                        str = data.validateType[position];
                    } else {
                        str = str + "," + data.validateType[position];
                    }
                }
                data.validateType = str;
            }

            return data;
        }
        form.init(form);
    });
</script>
