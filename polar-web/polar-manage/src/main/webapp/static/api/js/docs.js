$.views.settings.allowCode(true);
$.views.converters("getResponseModelName", function (val) {
    return getResponseModelName(val);
});

var tempBody = $.templates('#temp_body');
var tempBodyResponseModel = $.templates('#temp_body_response_model');

//获取context path
var contextPath = getContextPath();

function getContextPath() {
    var pathName = document.location.pathname;
    var index = pathName.substr(1).indexOf("/");
    var result = pathName.substr(0, index + 1);
    return result;
}

$(function () {

    $.ajax({
        url: ctx+"/v2/api-docs",
        dataType: "json",
        type: "get",
        async: true,
        error:function (XMLHttpRequest, textStatus, errorThrown) {

        },
        success: function (data) {
            //layui init
            var jsonData = eval(data);
            $("#title").html(jsonData.info.title);
            $("#api-div").html($("#template").render(jsonData));

            $('.navMenu li a').on('click',function(){
                var parent = $(this).parent().parent();//获取当前页签的父级的父级
                var labeul =$(this).parent("li").find(">ul")
                if ($(this).parent().hasClass('open') == false) {
                    //展开未展开
                    parent.find('ul').slideUp(300);
                    parent.find("li").removeClass("open")
                    parent.find('li a').removeClass("active").find(".arrow").removeClass("open")
                    $(this).parent("li").addClass("open").find(labeul).slideDown(300);
                    $(this).addClass("active").find(".arrow").addClass("open")
                }else{
                    $(this).parent("li").removeClass("open").find(labeul).slideUp(300);
                    if($(this).parent().find("ul").length>0){
                        $(this).removeClass("active").find(".arrow").removeClass("open")
                    }else{
                        $(this).addClass("active")
                    }
                }

            });

            $("[name='a_path']").click(function () {
                var path = $(this).attr("path");
                var method = $(this).attr("method");
                var operationId = $(this).attr("operationId");
                $.each(jsonData.paths[path], function (i, d) {
                    if (d.operationId == operationId) {
                        d.path = path;
                        d.method = method;
                        $("#path-body").html(tempBody.render(d));
                        var m=d.responses["200"]["schema"]["$ref"];
                        console.log(m);
                        if(d.responses["200"]["schema"]["type"]!=null&&d.responses["200"]["schema"]["type"]!='undefined'){
                            try {
                                m=d.responses["200"]["schema"]["items"]["$ref"];
                            }catch (e){

                            }
                        }
                        rendered=[];
                        var modelName = getResponseModelName(m);
                        renderResponseModel(jsonData, modelName);
                        rendered=[];
                    }
                });
            });

            //提交测试按钮
            $("[name='btn_submit']").click(function () {
                var operationId = $(this).attr("operationId");
                var parameterJson = {};
                $("input[operationId='" + operationId + "']").each(function (index, domEle) {
                    var k = $(domEle).attr("name");
                    var v = $(domEle).val();
                    parameterJson.push({k: v});
                });
            });

        }
    });

});

var rendered=[];
//渲染返回参数
function renderResponseModel(jsonData, modelName) {
    if (modelName) {
        var model = jsonData.definitions[modelName];
        model.name = modelName;
        //修改有嵌套对象的type
        $.each(model.properties, function (i, v) {
            if (v.items) {
                $.each(v.items, function (j, item) {
                    if(v.type=='array'){
                        model.properties[i].type = v.type + "[" + getResponseModelName(item) + "]"
                    }

                });
            }else{
                if(v["$ref"]!=null&&v["$ref"]!=null!="undefined"){
                    model.properties[i].type = "object[" + getResponseModelName(v["$ref"]) + "]"
                }
            }
        });
        var has=false;
        for(var position in rendered){
            if(model.name==rendered[position]){
                has=true;
                break;
            }
        }
        if(!has){
            $("#path-body-response-model").append(tempBodyResponseModel.render(model));
            rendered.push( model.name );
            //递归渲染多层对象嵌套
            $.each(model.properties, function (i, v) {
                if (v.items) {
                    $.each(v.items, function (j, item) {
                        renderResponseModel(jsonData, getResponseModelName(item));
                    });
                }else{
                    if(v["$ref"]!=null&&v["$ref"]!=null!="undefined"){
                        renderResponseModel(jsonData, getResponseModelName(v["$ref"]));
                    }
                }
            });
        }


    }
}

//获得返回模型名字
function getResponseModelName(val) {
    if (!val) {
        return null;
    }
    return val.substring(val.lastIndexOf("/") + 1, val.length);
}

//测试按钮，获取数据
function getData(operationId) {
    var path = contextPath + $("[m_operationId='" + operationId + "']").attr("path");
    //path 参数
    $("[p_operationId='" + operationId + "'][in='path']").each(function (index, domEle) {
        var k = $(domEle).attr("name");
        var v = $(domEle).val();
        if (v) {
            path = path.replace("{" + k + "}", v);
        }
    });
    var dataType="json";
    //header参数
    var headerJson = {};
    $("[p_operationId='" + operationId + "'][in='header']").each(function (index, domEle) {
        var k = $(domEle).attr("name");
        var v = $(domEle).val();
        if (v) {
            headerJson[k] = v;
        }
    });

    //请求方式
    var parameterType = $("#content_type_" + operationId).val();

    //query 参数
    var parameterJson = {};
    if ("form" == parameterType) {
        $("[p_operationId='" + operationId + "'][in='query']").each(function (index, domEle) {
            var k = $(domEle).attr("name");
            var v = $(domEle).val();
            if (v) {
                parameterJson[k] = v;
            }
        });
        dataType="html";
    } else if ("json" == parameterType) {
        var str = $("#text_tp_" + operationId).val();
        try {
            parameterJson = JSON.parse(str);
        } catch (error) {
            layer.msg("" + error, {icon: 5});
            return false;
        }
    }
    console.log(parameterType);
    //发送请求
    $.ajax({
        type: $("[m_operationId='" + operationId + "']").attr("method"),
        url: path,
        headers: headerJson,
        data: parameterJson,
        dataType: 'json',
        async:true,
        success: function (data) {
            var options = {
                withQuotes: true
            };
            $("#json-response").jsonViewer(data, options);
        }
    });
}


//请求类型
function changeParameterType(el) {
    var operationId = $(el).attr("operationId");
    var type = $(el).attr("type");
    $("#content_type_" + operationId).val(type);
    $(el).addClass("layui-btn-normal").removeClass("layui-btn-primary");
    if ("form" == type) {
        $("#text_tp_" + operationId).hide();
        $("#table_tp_" + operationId).show();
        $("#pt_json_" + operationId).addClass("layui-btn-primary").removeClass("layui-btn-normal");
    } else if ("json" == type) {
        $("#text_tp_" + operationId).show();
        $("#table_tp_" + operationId).hide();
        $("#pt_form_" + operationId).addClass("layui-btn-primary").removeClass("layui-btn-normal");
    }
}
