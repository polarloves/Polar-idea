package polar.island.flow.group.web;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import polar.island.core.config.Constants;
import polar.island.web.controller.BasicController;
import polar.island.core.entity.ResponseJson;
import polar.island.core.res.ResMsg;
import polar.island.core.res.ResType;
import polar.island.flow.group.entity.GroupEntity;
import polar.island.flow.group.service.GroupService;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * 用户组的接口，返回数据全部为json串。
 *
 * @author PolarLoves
 */
@Controller(value = "groupJsonController")
@RequestMapping(value = "/group/json")
public class GroupJsonController extends BasicController {
    @Resource(name = "groupService")
    private GroupService groupService;
    private static final String MODULE_NAME = "用户组";

    /**
     * 校验访问权限
     *
     * @return
     */
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:group:empty"})
    @ResMsg (tag = "校验用户用户组权限", type = ResType.JSON,writeLogs = false)
    @RequestMapping(value = "/checkPermission", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson checkPermission() {
        return new ResponseJson(Constants.CODE_SUCCESS);
    }

    /**
     * 依据条件查询分页数据，如果不传页码、每页条目，则默认分别为：1,10
     *
     * @param entity 查询条件
     * @return 列表Json串。
     * @see polar.island.flow.group.entity.GroupEntity
     */
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:group:view:list"})
    @ResMsg (tag = "查询用户组", type = ResType.JSON,writeLogs = false)
    @RequestMapping(value = "/pageList", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson pageList(GroupEntity entity) {
        Map<String, Object> condition = beanToMap(entity);
        return new ResponseJson(Constants.CODE_SUCCESS, groupService.selectPageList(condition), null,
                groupService.selectCount(condition));
    }

    /**
     * 查看用户组详情的json串
     *
     * @param id 数据编号
     * @return 详情json串
     */
    @ResponseBody
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:group:view:detail"})
    @ResMsg (tag = "查看用户组详情的json串", type = ResType.JSON,writeLogs = false)
    @RequestMapping(value = "/detail", produces = "application/json;charset=utf-8")
    public ResponseJson detail(String id) {
        idCheck(id, MODULE_NAME);
        Map<String, Object> entity = groupService.selectOneById(id);
        existCheck(entity, MODULE_NAME);
        return new ResponseJson(Constants.CODE_SUCCESS, entity);
    }

    @ResponseBody
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:group:edit"})
    @ResMsg (tag = "选择用户", type = ResType.JSON,writeLogs = false)
    @RequestMapping(value = "/userList", produces = "application/json;charset=utf-8")
    public ResponseJson userList(String state, String nickName, String phone, String email, Integer page, Integer rows) {
        if (page == null) {
            page = 1;
        }
        if (rows == null) {
            rows = 10;
        }
        Map<String, Object> condition = new HashMap<>();
        condition.put("state", state);
        condition.put("nickName", nickName);
        condition.put("phone", phone);
        condition.put("email", email);
        condition.put("pageStartNumber", (page - 1) * rows);
        condition.put("pageOffsetNumber", rows);
        return new ResponseJson(Constants.CODE_SUCCESS, groupService.selectUserPageList(condition), null,
                groupService.selectUserCount(condition));
    }

    /**
     * 依据数据编号更新此条数据的所有值。
     *
     * @param entity 需要更新的实体
     * @return 更新条目
     * @see polar.island.flow.group.entity.GroupEntity
     */
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:group:edit"})
    @ResMsg (tag = "更新用户组全部字段", type = ResType.JSON)
    @RequestMapping(value = "/updateAllById", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson updateAllById(@Valid GroupEntity entity, BindingResult bindingResult) {
        validate(GroupEntity.class, "updateAllById", bindingResult);
        Map<String, Object> condition = beanToMap(entity);
        Long result = groupService.updateAll(condition);
        return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功修改了" + result + "条数据。");
    }

    /**
     * 依据数据编号更新此条数据的所有值。
     *
     * @param entity 需要更新的实体
     * @return 更新条目
     * @see polar.island.flow.group.entity.GroupEntity
     */
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:group:edit"})
    @ResMsg (tag = "更新用户组单个字段", type = ResType.JSON)
    @RequestMapping(value = "/updateField", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson updateField(@Valid GroupEntity entity, BindingResult bindingResult) {
        validate(GroupEntity.class, "updateField", bindingResult);
        Map<String, Object> condition = beanToSingleField(entity);
        condition.put("id", entity.getId());
        Long result = groupService.updateField(condition);
        return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功修改了" + result + "条数据。");
    }

    /**
     * 根据数据编号物理删除数据。
     *
     * @param ids 数据编号
     * @return 删除结果。
     */
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:group:delete"})
    @ResMsg (tag = "删除多个用户组", type = ResType.JSON)
    @RequestMapping(value = "/deleteMulitById", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson deleteMulitById(@RequestParam(value = "ids[]", required = false) String[] ids) {
        idCheck(ids, MODULE_NAME);
        Long result = groupService.deleteMulitByIdPhysical(ids);
        return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功删除了" + result + "条数据。");
    }

    /**
     * 根据数据编号物理删除数据。
     *
     * @param id 数据编号
     * @return 删除结果。
     */
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:group:delete"})
    @ResMsg (tag = "删除一个用户组", type = ResType.JSON)
    @RequestMapping(value = "/deleteById", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson deleteById(String id) {
        idCheck(id, MODULE_NAME);
        Long result = groupService.deleteByIdPhysical(id);
        return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功删除了" + result + "条数据。");
    }

    /**
     * 新增一条数据。
     *
     * @param entity 数据
     * @return 新增结果。
     * @see polar.island.flow.group.entity.GroupEntity
     */
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:group:add"})
    @ResMsg (tag = "新增用户组", type = ResType.JSON)
    @RequestMapping(value = "/add", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson add(@Valid GroupEntity entity, BindingResult bindingResult) {
        validate(GroupEntity.class, "add", bindingResult);
        Map<String, Object> condition = beanToMap(entity);
        Object result = groupService.insert(condition);
        return new ResponseJson(Constants.CODE_SUCCESS, result, "新增成功。");
    }


}
