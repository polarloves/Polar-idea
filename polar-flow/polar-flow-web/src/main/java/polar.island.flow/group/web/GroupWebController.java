package polar.island.flow.group.web;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import polar.island.web.controller.BasicController;
import polar.island.core.res.ResMsg;
import polar.island.core.res.ResType;
import polar.island.core.util.GsonUtil;
import polar.island.flow.group.service.GroupService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * 操作组的控制器类，返回参数为页面，为配合加载，出错时为Json串，可修改@ErrorMsg标签，使其出错时返回页面。
 *
 * @author PolarLoves
 */
@Controller(value = "groupWebController")
@RequestMapping(value = "/group/web")
public class GroupWebController extends BasicController {
    @Resource(name = "groupService")
    private GroupService groupService;
    private static final String MODULE_NAME = "操作组";

    /**
     * 操作组列表页面
     *
     * @param request request对象
     * @return 列表页面，如出错，则返回Json串。
     */
    @ResMsg (tag = "操作组列表", type = ResType.JSON,writeLogs = false)
    @RequestMapping(value = {"/list", ""}, produces = "text/html;charset=utf-8")
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:group:view:list"})
    public String list(HttpServletRequest request) {
        return "/view/flow/group/groupList.jsp";
    }

    /**
     * 操作组新增页面
     *
     * @param request request对象
     * @return 新增页面，如出错，则返回Json串。
     */
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:group:add"})
    @ResMsg (tag = "新增操作组", type = ResType.JSON,writeLogs = false)
    @RequestMapping(value = "/add", produces = "text/html;charset=utf-8")
    public String add(HttpServletRequest request) {
        Map<String, Object> entity = new HashMap<String, Object>();
        request.setAttribute("group", entity);
        request.setAttribute("users", "[]");
        return "/view/flow/group/groupForm.jsp";
    }

    /**
     * 更新操作组页面
     *
     * @param request request对象
     * @param id      数据编号
     * @return 更新页面，如出错，则返回Json串。
     */
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:group:edit"})
    @ResMsg (tag = "更新操作组", type = ResType.JSON,writeLogs = false)
    @RequestMapping(value = "/update", produces = "text/html;charset=utf-8")
    public String updatePage(HttpServletRequest request, String id) {
        idCheck(id, MODULE_NAME);
        Map<String, Object> entity = groupService.selectOneById(id);
        existCheck(entity, MODULE_NAME);
        request.setAttribute("group", entity);
        request.setAttribute("users", GsonUtil.toJson(entity.get("users")));
        return "/view/flow/group/groupForm.jsp";
    }

    /**
     * 操作组的详情页面
     *
     * @param request request对象
     * @param id      数据编号
     * @return 列表页面，如出错，则返回Json串。
     */
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:group:view:detail"})
    @ResMsg (tag = "查看操作组", type = ResType.JSON,writeLogs = false)
    @RequestMapping(value = "/detail", produces = "text/html;charset=utf-8")
    public String detailPage(String id, HttpServletRequest request) {
        return updatePage(request, id);
    }

    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:group:edit"})
    @ResMsg (tag = "增加用户组", type = ResType.JSON,writeLogs = false)
    @RequestMapping(value = "/addGroupUser", produces = "text/html;charset=utf-8")
    public String addGroupUser() {
        return "/view/flow/group/userList.jsp";
    }
}
