package polar.island.flow.form.web;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import polar.island.core.config.Constants;
import polar.island.shiro.util.UserUtil;
import polar.island.web.controller.BasicController;
import polar.island.core.entity.ResponseJson;
import polar.island.core.res.ResMsg;
import polar.island.core.res.ResType;
import polar.island.flow.form.entity.FormEntity;
import polar.island.flow.form.service.FormService;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Date;
import java.util.Map;
/**
 * 流程表单的接口，返回数据全部为json串。
 * 
 * @author PolarLoves
 *
 */
@Controller(value = "formJsonController")
@RequestMapping(value = "/form/json")
public class FormJsonController extends BasicController {
	@Resource(name = "polarFormService")
	private FormService formService;
	private  static final  String MODULE_NAME="流程表单";
	/**
	 * 校验访问权限
	 * 
	 * @return
	 */
	@RequiresUser
	@RequiresPermissions(value = { "polar:backstage", "polar:form:empty" })
	@ResMsg (tag = "校验用户流程表单权限", type = ResType.JSON,writeLogs = false)
	@RequestMapping(value = "/checkPermission", produces = "application/json;charset=utf-8")
	@ResponseBody
	public ResponseJson checkPermission() {
		return new ResponseJson(Constants.CODE_SUCCESS);
	}
	/**
	 * 依据条件查询分页数据，如果不传页码、每页条目，则默认分别为：1,10
	 * 
	 * @param entity
	 *            查询条件
	 * @see polar.island.flow.form.entity.FormEntity
	 * @return 列表Json串。
	 */
	@RequiresUser
	@RequiresPermissions(value = { "polar:backstage","polar:form:view:list"})
	@ResMsg (tag = "查询流程表单", type = ResType.JSON,writeLogs = false)
	@RequestMapping(value = "/pageList", produces = "application/json;charset=utf-8")
	@ResponseBody
	public ResponseJson pageList(FormEntity entity) {
		Map<String, Object> condition = beanToMap(entity);
		return new ResponseJson(Constants.CODE_SUCCESS, formService.selectPageList(condition), null,
				formService.selectCount(condition));
	}
	/**
	 * 查看流程表单详情的json串
	 * 
	 * @param id
	 *            数据编号
	 * @return 详情json串
	 */
    @ResponseBody
	@RequiresUser
	@RequiresPermissions(value = { "polar:backstage", "polar:form:view:detail" })
	@ResMsg (tag = "查看流程表单详情的json串", type = ResType.JSON,writeLogs = false)
	@RequestMapping(value = "/detail", produces = "application/json;charset=utf-8")
	public ResponseJson detail(String id) {
		idCheck(id,MODULE_NAME);
		FormEntity entity = formService.selectOneById(id);
		existCheck(entity,MODULE_NAME);
		return new ResponseJson(Constants.CODE_SUCCESS, entity);
	}

	/**
	 * 依据数据编号更新此条数据的所有值。
	 * 
	 * @param entity
	 *            需要更新的实体
	 * @see polar.island.flow.form.entity.FormEntity
	 * @return 更新条目
	 */
	@RequiresUser
	@RequiresPermissions(value = { "polar:backstage","polar:form:edit"})
	@ResMsg (tag = "更新流程表单全部字段", type = ResType.JSON)
	@RequestMapping(value = "/updateAllById", produces = "application/json;charset=utf-8")
	@ResponseBody
	public ResponseJson updateAllById(@Valid FormEntity entity, BindingResult bindingResult) {
		validate(FormEntity.class, "updateAllById",bindingResult);
		Map<String, Object> condition = beanToMap(entity);
		Long result = formService.updateAll(condition);
		return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功修改了" + result + "条数据。");
	}

	/**
	 * 依据数据编号更新此条数据的所有值。
	 * 
	 * @param entity
	 *            需要更新的实体
	 * @see polar.island.flow.form.entity.FormEntity
	 * @return 更新条目
	 */
	@RequiresUser
	@RequiresPermissions(value = { "polar:backstage","form:edit"})
	@ResMsg (tag = "更新流程表单单个字段", type = ResType.JSON)
	@RequestMapping(value = "/updateField", produces = "application/json;charset=utf-8")
	@ResponseBody
	public ResponseJson updateField(@Valid FormEntity entity, BindingResult bindingResult) {
	    validate(FormEntity.class, "updateField",bindingResult);
		Map<String, Object> condition = beanToSingleField(entity);
		condition.put("id",entity.getId());
		Long result = formService.updateField(condition);
		return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功修改了" + result + "条数据。");
	}

	/**
	 * 根据数据编号物理删除数据。
	 * 
	 * @param ids
	 *            数据编号
	 * @return 删除结果。
	 */
	@RequiresUser
	@RequiresPermissions(value = { "polar:backstage","polar:form:delete"})
	@ResMsg (tag = "删除多个流程表单", type = ResType.JSON)
	@RequestMapping(value = "/deleteMulitById", produces = "application/json;charset=utf-8")
	@ResponseBody
	public ResponseJson deleteMulitById(@RequestParam(value = "ids[]", required = false) String[] ids) {
		idCheck(ids,MODULE_NAME);
		Long result = formService.deleteMulitByIdPhysical(ids);
		return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功删除了" + result + "条数据。");
	}

	/**
	 * 根据数据编号物理删除数据。
	 * 
	 * @param id
	 *            数据编号
	 * @return 删除结果。
	 */
	@RequiresUser
	@RequiresPermissions(value = { "polar:backstage","polar:form:delete"})
	@ResMsg (tag = "删除一个流程表单", type = ResType.JSON)
	@RequestMapping(value = "/deleteById", produces = "application/json;charset=utf-8")
	@ResponseBody
	public ResponseJson deleteById(String id) {
		idCheck(id,MODULE_NAME);
		Long result = formService.deleteByIdPhysical(id);
		return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功删除了" + result + "条数据。");
	}

	/**
	 * 新增一条数据。
	 * 
	 * @param entity
	 *            数据
	 * @see polar.island.flow.form.entity.FormEntity
	 * @return 新增结果。
	 */
	@RequiresUser
	@RequiresPermissions(value = { "polar:backstage","polar:form:add"})
	@ResMsg (tag = "新增流程表单", type = ResType.JSON)
	@RequestMapping(value = "/add", produces = "application/json;charset=utf-8")
	@ResponseBody
	public ResponseJson add(@Valid FormEntity entity, BindingResult bindingResult) {
		validate(FormEntity.class, "add",bindingResult);
		Map<String, Object> condition = beanToMap(entity);
		condition.put("createUser", UserUtil.getUserId());
		condition.put("createDate", new Date());
		Object result=formService.insert(condition);
		return new ResponseJson(Constants.CODE_SUCCESS,result, "新增成功。");
	}
	

}
