package polar.island.flow.form.web;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import polar.island.web.controller.BasicController;
import polar.island.core.res.ResMsg;
import polar.island.core.res.ResType;
import polar.island.core.util.GsonUtil;
import polar.island.flow.column.entity.ColumnEntity;
import polar.island.flow.form.entity.FormEntity;
import polar.island.flow.form.service.FormService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
/**
 * 流程表单的控制器类，返回参数为页面，为配合加载，出错时为Json串，可修改@ErrorMsg标签，使其出错时返回页面。
 * 
 * @author PolarLoves
 *
 */
@Controller(value = "formWebController")
@RequestMapping(value = "/form/web")
public class FormWebController extends BasicController {
	@Resource(name = "polarFormService")
	private FormService formService;
	private  static final  String MODULE_NAME="流程表单";
	/**
	 * 流程表单列表页面
	 *
	 * @param request
	 *            request对象
	 * @return 列表页面，如出错，则返回Json串。
	 */
	@ResMsg (tag = "流程表单列表", type = ResType.JSON,writeLogs = false)
	@RequestMapping(value = { "/list", "" }, produces = "text/html;charset=utf-8")
	@RequiresUser
	@RequiresPermissions(value = { "polar:backstage","polar:form:view:list"})
	public String list(HttpServletRequest request) {
		return "/view/flow/form/formList.jsp";
	}
	/**
	 * 流程表单新增页面
	 * 
	 * @param request
	 *            request对象
	 * @return 新增页面，如出错，则返回Json串。
	 */
	@RequiresUser
	@RequiresPermissions(value = { "polar:backstage","polar:form:add"})
	@ResMsg (tag = "新增流程表单", type = ResType.JSON,writeLogs = false)
	@RequestMapping(value = "/add", produces = "text/html;charset=utf-8")
	public String add(HttpServletRequest request) {
		FormEntity entity=new FormEntity();
		request.setAttribute("form", entity);
		request.setAttribute("columnEntity", "[]");
		return "/view/flow/form/formForm.jsp";
	}
	/**
	 * 更新流程表单页面
	 * 
	 * @param request
	 *            request对象
	 * @param id
	 *            数据编号
	 * @return 更新页面，如出错，则返回Json串。
	 */
	@RequiresUser
	@RequiresPermissions(value = { "polar:backstage","polar:form:edit"})
	@ResMsg (tag = "更新流程表单", type = ResType.JSON,writeLogs = false)
	@RequestMapping(value = "/update", produces = "text/html;charset=utf-8")
	public String updatePage(HttpServletRequest request, String id) {
		idCheck(id,MODULE_NAME);
		FormEntity entity = formService.selectOneById(id);
		existCheck(entity,MODULE_NAME);
		request.setAttribute("form", entity);
		request.setAttribute("columnEntity", GsonUtil.toJson(entity.getColumnEntity()));
		return "/view/flow/form/formForm.jsp";
	}
	/**
	 * 流程表单的详情页面
	 *
	 * @param request
	 *            request对象
	 * @param id
	 *            数据编号
	 * @return 列表页面，如出错，则返回Json串。
	 */
	@RequiresUser
	@RequiresPermissions(value = { "polar:backstage","polar:form:view:detail"})
	@ResMsg (tag = "查看流程表单", type = ResType.JSON,writeLogs = false)
	@RequestMapping(value = "/detail", produces = "text/html;charset=utf-8")
	public String detailPage(String id, HttpServletRequest request) {
  		return updatePage(request,id);
	}
	/**
	 * 表单列属性的详情页面
	 *
	 * @param request
	 *            request对象
	 * @param entity
	 *            数据
	 * @return 列表页面，如出错，则返回Json串。
	 */
	@RequiresUser
	@RequiresPermissions(value = { "polar:backstage","polar:form:view:detail"})
	@ResMsg (tag = "查看表单列属性", type = ResType.JSON,writeLogs = false)
	@RequestMapping(value = "/columnDetail", produces = "text/html;charset=utf-8")
	public String columnDetailPage(HttpServletRequest request,ColumnEntity entity) {
  		return columnUpdatePage(request,entity);
	}

	/**
	 * 更新表单列属性页面
	 *
	 * @param request
	 *            request对象
	 * @param entity
	 *
	 * @return 更新页面，如出错，则返回Json串。
	 */
	@RequiresUser
	@RequiresPermissions(value = { "polar:backstage","polar:form:edit"})
	@ResMsg (tag = "更新表单列属性", type = ResType.JSON,writeLogs = false)
	@RequestMapping(value = "/columnUpdate", produces = "text/html;charset=utf-8")
	public String columnUpdatePage(HttpServletRequest request,ColumnEntity entity) {
		request.setAttribute("column", entity);
		return "/view/flow/form/columnForm.jsp";
	}

	/**
	 * 表单列属性新增页面
	 *
	 * @param request
	 *            request对象
     *
	 * @return 新增页面，如出错，则返回Json串。
	 */
	@RequiresUser
	@RequiresPermissions(value = { "polar:backstage","polar:form:add"})
	@ResMsg (tag = "新增表单列属性", type = ResType.JSON,writeLogs = false)
	@RequestMapping(value = "/columnAdd", produces = "text/html;charset=utf-8")
	public String columnAdd(HttpServletRequest request) {
		return columnUpdatePage(request,new ColumnEntity());
	}
}
