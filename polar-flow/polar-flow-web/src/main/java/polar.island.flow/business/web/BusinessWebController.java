package polar.island.flow.business.web;

import com.google.gson.reflect.TypeToken;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import polar.island.web.controller.BasicController;
import polar.island.core.res.ResMsg;
import polar.island.core.res.ResType;
import polar.island.core.util.CommonUtil;
import polar.island.core.util.GsonUtil;
import polar.island.flow.business.entity.BusinessEntity;
import polar.island.flow.business.expection.BussinessException;
import polar.island.flow.business.service.BusinessService;
import polar.island.flow.business.util.FlowUtil;
import polar.island.flow.column.entity.ColumnEntity;
import polar.island.flow.flow.entity.FlowEntity;
import polar.island.flow.flow.service.FlowService;
import polar.island.flow.form.entity.FormEntity;
import polar.island.flow.form.service.FormService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Controller(value = "businessWebController")
@RequestMapping(value = "/business/web")
public class BusinessWebController extends BasicController {
    @Resource(name = "polarFlowService")
    private FlowService flowService;
    @Resource(name = "polarFormService")
    private FormService formService;
    @Resource(name = "businessService")
    private BusinessService businessService;

    private static final String MODULE_NAME = "业务流程";

    @RequiresUser
    @ResMsg (tag = "发起流程页面", type = ResType.JSON, writeLogs = false)
    @RequestMapping(value = "/launchPage/{id}", produces = "text/html;charset=utf-8")
    public String launchPage(@PathVariable String id, HttpServletRequest request) {
        FlowEntity flowEntity = flowService.selectOneById(id);
        FlowUtil.checkFlow(flowEntity);
        FormEntity formEntity = formService.selectOneById(CommonUtil.valueOf(flowEntity.getFormId()));
        request.setAttribute("flow", flowEntity);
        request.setAttribute("form", formEntity);
        List<Map<String, Object>> columns = findColumns(formEntity.getColumnEntity());
        request.setAttribute("columns", columns);
        return "/view/flow/business/launch.jsp";
    }

    private List<Map<String, Object>> findColumns(List<ColumnEntity> columnEntities) {
        List<Map<String, Object>> columns = new ArrayList<Map<String, Object>>();
        for (ColumnEntity entity : columnEntities) {
            Map<String, Object> column = new HashMap<String, Object>();
            column.put("key", entity.getKey());
            column.put("type", entity.getType() + "");
            column.put("label", entity.getLabel());
            column.put("placeholder", entity.getPlaceholder());
            column.put("small", entity.getSmall() + "");
            column.put("middle", entity.getMiddle() + "");
            column.put("big", entity.getBig() + "");
            column.put("larger", entity.getLarger() + "");
            String validate = "";
            String validateType = entity.getValidateType();
            if (!StringUtils.isEmpty(validateType)) {
                if (validateType.contains("1")) {
                    validate = validate + "required,";
                }
                if (validateType.contains("2")) {
                    validate = validate + "phone,";
                }
                if (validateType.contains("3")) {
                    validate = validate + "email,";
                }
                if (validateType.contains("4")) {
                    validate = validate + "identity,";
                }
                if (validateType.contains("5")) {
                    validate = validate + "maxLength,";
                }
                if (validateType.contains("6")) {
                    validate = validate + "minLength,";
                }
                if (validateType.contains("7")) {
                    validate = validate + "number,";
                }
                if (validateType.contains("8")) {
                    validate = validate + "numeric,";
                }
                if (validate.endsWith(",")) {
                    validate = validate.substring(0, validate.length() - 1);
                }
            }
            column.put("validate", validate);
            Map<String, String> extendsObject = new HashMap<String, String>();
            if (!StringUtils.isEmpty(entity.getExtendsJson())) {
                for (String m : entity.getExtendsJson().split(",")) {
                    extendsObject.put(m.split(":")[0], m.split(":")[1]);
                }
            }
            column.put("extendsObject", extendsObject);
            columns.add(column);
        }
        return columns;
    }

    @RequiresUser
    @ResMsg (tag = "处理流程页面", type = ResType.JSON, writeLogs = false)
    @RequestMapping(value = "/complete/{id}", produces = "text/html;charset=utf-8")
    public String complete(@PathVariable String id, HttpServletRequest request) {
        BusinessEntity businessEntity = businessService.selectOneById(id);
        if (businessEntity == null) {
            throw new BussinessException("流程不存在", null);
        }
        FormEntity formEntity = businessService.findCurrentFormEntity(businessEntity);
        request.setAttribute("form", formEntity);
        request.setAttribute("flowId", id);
        List<Map<String, Object>> columns = findColumns(formEntity.getColumnEntity());
        request.setAttribute("columns", columns);
        return "/view/flow/business/complete.jsp";
    }

    /**
     * 业务流程列表页面
     *
     * @param request request对象
     * @return 列表页面，如出错，则返回Json串。
     */
    @ResMsg (tag = "查询我发起的任务", type = ResType.JSON)
    @RequestMapping(value = "/myList/{flowId}", produces = "text/html;charset=utf-8")
    @RequiresUser
    public String myList(HttpServletRequest request, @PathVariable String flowId) {
        request.setAttribute("flowId", flowId);
        FlowEntity entity = flowService.selectOneById(flowId);
        existCheck(entity, "流程定义");
        FormEntity formEntity = formService.selectOneById(CommonUtil.valueOf(entity.getFormId()));
        request.setAttribute("entity", entity);
        request.setAttribute("form", formEntity);
        return "/view/flow/business/businessList.jsp";
    }

    @ResMsg (tag = "查看我的待办", type = ResType.JSON)
    @RequestMapping(value = "/toDoList/{flowId}", produces = "text/html;charset=utf-8")
    @RequiresUser
    public String toDoList(HttpServletRequest request, @PathVariable String flowId) {
        request.setAttribute("flowId", flowId);
        FlowEntity entity = flowService.selectOneById(flowId);
        existCheck(entity, "流程定义");
        FormEntity formEntity = formService.selectOneById(CommonUtil.valueOf(entity.getFormId()));
        request.setAttribute("entity", entity);
        request.setAttribute("form", formEntity);
        return "/view/flow/business/businessToDoList.jsp";
    }

    @ResMsg (tag = "查看我的已办", type = ResType.JSON,writeLogs = false)
    @RequestMapping(value = "/doneList/{flowId}", produces = "text/html;charset=utf-8")
    @RequiresUser
    public String doneList(HttpServletRequest request, @PathVariable String flowId) {
        request.setAttribute("flowId", flowId);
        FlowEntity entity = flowService.selectOneById(flowId);
        existCheck(entity, "流程定义");
        FormEntity formEntity = formService.selectOneById(CommonUtil.valueOf(entity.getFormId()));
        request.setAttribute("entity", entity);
        request.setAttribute("form", formEntity);
        return "/view/flow/business/doneList.jsp";
    }
    @ResMsg (tag = "查看所有流程", type = ResType.JSON,writeLogs = false)
    @RequestMapping(value = "/allList/{flowId}", produces = "text/html;charset=utf-8")
    @RequiresUser
    public String allList(HttpServletRequest request, @PathVariable String flowId) {
        request.setAttribute("flowId", flowId);
        FlowEntity entity = flowService.selectOneById(flowId);
        existCheck(entity, "流程定义");
        FormEntity formEntity = formService.selectOneById(CommonUtil.valueOf(entity.getFormId()));
        request.setAttribute("entity", entity);
        request.setAttribute("form", formEntity);
        return "/view/flow/business/allList.jsp";
    }
    /**
     * 业务流程的详情页面
     *
     * @param request request对象
     * @param id      数据编号
     * @return 列表页面，如出错，则返回Json串。
     */
    @RequiresUser
    @ResMsg (tag = "查看业务流程", type = ResType.JSON,writeLogs = false)
    @RequestMapping(value = "/detail", produces = "text/html;charset=utf-8")
    public String detailPage(String id, HttpServletRequest request) {
        idCheck(id, MODULE_NAME);
        BusinessEntity entity = businessService.selectOneById(id);
        existCheck(entity, MODULE_NAME);
        request.setAttribute("entity", entity);
        request.setAttribute("datas", GsonUtil.getGson().fromJson(StringEscapeUtils.unescapeHtml4(entity.getDisplayJson()), new TypeToken<ArrayList<HashMap<String, Object>>>() {
        }.getType()));
        return "/view/flow/business/businessForm.jsp";
    }

    @RequiresUser
    @ResMsg (tag = "查看业务流程历史", type = ResType.JSON,writeLogs = false)
    @RequestMapping(value = "/history", produces = "text/html;charset=utf-8")
    public String history(String id, HttpServletRequest request) {
        idCheck(id, MODULE_NAME);
        request.setAttribute("history", businessService.findHistory(id));
        return "/view/flow/business/history.jsp";
    }

    @RequiresUser
    @ResMsg (tag = "查看业务流程历史详情", type = ResType.JSON,writeLogs = false)
    @RequestMapping(value = "/historyDetail", produces = "text/html;charset=utf-8")
    public String historyDetail(String displayJson, HttpServletRequest request) {
        request.setAttribute("datas", GsonUtil.getGson().fromJson(StringEscapeUtils.unescapeHtml4(displayJson), new TypeToken<ArrayList<HashMap<String, Object>>>() {
        }.getType()));
        return "/view/flow/business/historyDetail.jsp";
    }
}
