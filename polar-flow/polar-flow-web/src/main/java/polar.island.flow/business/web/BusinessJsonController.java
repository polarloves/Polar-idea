package polar.island.flow.business.web;

import io.swagger.annotations.*;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import polar.island.core.config.Constants;
import polar.island.shiro.util.UserUtil;
import polar.island.web.controller.BasicController;
import polar.island.core.entity.ResponseJson;
import polar.island.core.res.ResMsg;
import polar.island.core.res.ResType;
import polar.island.core.util.CommonUtil;
import polar.island.flow.business.entity.BusinessEntity;
import polar.island.flow.business.expection.BussinessException;
import polar.island.flow.business.service.BusinessService;
import polar.island.flow.business.util.FlowUtil;
import polar.island.flow.flow.entity.FlowEntity;
import polar.island.flow.flow.service.FlowService;
import polar.island.flow.form.entity.FormEntity;
import polar.island.flow.form.service.FormService;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller(value = "businessJsonController")
@RequestMapping(value = "/business/json")
@Api(tags = "业务流程接口", description = "业务流程接口")
public class BusinessJsonController extends BasicController {
    @Resource(name = "polarFlowService")
    private FlowService flowService;
    @Resource(name = "polarFormService")
    private FormService formService;
    @Resource(name = "businessService")
    private BusinessService businessService;
    private static final String MODULE_NAME = "业务流程";

    @ApiOperation(value = "启动流程", httpMethod = "POST")
    @RequiresUser
    @ResMsg (tag = "启动流程", type = ResType.JSON)
    @RequestMapping(value = "/start", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson start(@ApiParam(value = "流程编号") String id, @ApiParam(value = "流程标题") String title, @ApiParam(value = "用于展示的json串数据") String displayJson, HttpServletRequest request) {
        FlowEntity flowEntity = flowService.selectOneById(id);
        FlowUtil.checkFlow(flowEntity);
        FormEntity formEntity = formService.selectOneById(CommonUtil.valueOf(flowEntity.getFormId()));
        BusinessEntity businessEntity = new BusinessEntity();
        businessEntity.setTitle(title);
        businessEntity.setDisplayJson(displayJson);
        businessEntity.setCreateUser( UserUtil.getUserId());
        businessService.start(businessEntity, flowEntity, formEntity, FlowUtil.findFlowColumnData(formEntity, request));
        return new ResponseJson(Constants.CODE_SUCCESS);
    }

    @ApiOperation(value = "完成流程", httpMethod = "POST")
    @RequiresUser
    @ResMsg (tag = "完成流程", type = ResType.JSON)
    @RequestMapping(value = "/complete", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson complete(@ApiParam(value = "流程编号") String id, @ApiParam(value = "用于展示的json串数据") String displayJson, HttpServletRequest request) {
        BusinessEntity businessEntity = businessService.selectOneById(id);
        if (businessEntity == null) {
            throw new BussinessException("当前流程不存在", null);
        }
        FormEntity formEntity = businessService.findCurrentFormEntity(businessEntity);
        businessService.complete(businessEntity, UserUtil.getUserId(), FlowUtil.findFlowColumnData(formEntity, request), displayJson);
        return new ResponseJson(Constants.CODE_SUCCESS);
    }

    /**
     * 依据条件查询分页数据，如果不传页码、每页条目，则默认分别为：1,10
     *
     * @param entity 查询条件
     * @return 列表Json串。
     * @see polar.island.flow.business.entity.BusinessEntity
     */
    @ApiOperation(value = "查询我发起的业务流程", httpMethod = "POST", response = BusinessEntity.class, responseContainer = "List")
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query",name = "searchDate", value = "开始时间-结束事件,格式：yyyy-MM-dd~yyyy-MM-dd"),
                    @ApiImplicitParam(paramType = "query",name = "page", value = "当前页,如不传,则默认为1", example = "1"),
                    @ApiImplicitParam(paramType = "query",name = "rows", value = "每页的数据条目,如不传,则默认为10", example = "10"),
                    @ApiImplicitParam(paramType = "query",name = "sort", value = "排序字段", example = "groupName"),
                    @ApiImplicitParam(paramType = "query",name = "order", value = "排序方式，可选值有：ASC DESC,当sort字段传入值时,其必须传入", example = "DESC")
            }
    )
    @RequiresUser
    @ResMsg (tag = "查询我发起的业务流程", type = ResType.JSON,writeLogs = false)
    @RequestMapping(value = "/myList/{flowId}", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson myList(@ApiIgnore BusinessEntity entity, String searchDate) {
        Map<String, Object> condition = beanToMap(entity);
        if (!StringUtils.isEmpty(searchDate)) {
            condition.put("startTime", searchDate.split("~")[0] + "00:00:00");
            condition.put("endTime", searchDate.split("~")[1] + " 23:59:59");
        }
        condition.put("createUser", UserUtil.getUserId());
        return new ResponseJson(Constants.CODE_SUCCESS, businessService.selectPageList(condition), null,
                businessService.selectCount(condition));
    }

    @ApiOperation(value = "查询所有的流程", httpMethod = "POST", response = BusinessEntity.class, responseContainer = "List")
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query",name = "searchDate", value = "开始时间-结束事件,格式：yyyy-MM-dd~yyyy-MM-dd"),
                    @ApiImplicitParam(paramType = "query",name = "page", value = "当前页,如不传,则默认为1", example = "1"),
                    @ApiImplicitParam(paramType = "query",name = "rows", value = "每页的数据条目,如不传,则默认为10", example = "10"),
                    @ApiImplicitParam(paramType = "query",name = "sort", value = "排序字段", example = "groupName"),
                    @ApiImplicitParam(paramType = "query",name = "order", value = "排序方式，可选值有：ASC DESC,当sort字段传入值时,其必须传入", example = "DESC")
            }
    )
    @RequiresUser
    @ResMsg (tag = "查询所有的流程", type = ResType.JSON,writeLogs = false)
    @RequestMapping(value = "/allList/{flowId}", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson allList(@ApiIgnore BusinessEntity entity,@ApiIgnore String searchDate) {
        Map<String, Object> condition = beanToMap(entity);
        if (!StringUtils.isEmpty(searchDate)) {
            condition.put("startTime", searchDate.split("~")[0] + "00:00:00");
            condition.put("endTime", searchDate.split("~")[1] + " 23:59:59");
        }

        return new ResponseJson(Constants.CODE_SUCCESS, businessService.selectPageList(condition), null,
                businessService.selectCount(condition));
    }

    @ApiOperation(value = "查询由我处理的业务流程", httpMethod = "POST", response = BusinessEntity.class, responseContainer = "List")
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query",name = "searchDate", value = "开始时间-结束事件,格式：yyyy-MM-dd~yyyy-MM-dd"),
                    @ApiImplicitParam(paramType = "query",name = "page", value = "当前页,如不传,则默认为1", example = "1"),
                    @ApiImplicitParam(paramType = "query",name = "rows", value = "每页的数据条目,如不传,则默认为10", example = "10"),
                    @ApiImplicitParam(paramType = "query",name = "sort", value = "排序字段", example = "groupName"),
                    @ApiImplicitParam(paramType = "query",name = "order", value = "排序方式，可选值有：ASC DESC,当sort字段传入值时,其必须传入", example = "DESC")
            }
    )
    @RequiresUser
    @ResMsg (tag = "查询由我处理的业务流程", type = ResType.JSON,writeLogs = false)
    @RequestMapping(value = "/todoList/{flowId}", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson todoList(@ApiIgnore BusinessEntity entity, @ApiIgnore String searchDate) {
        Map<String, Object> condition = beanToMap(entity);
        if (!StringUtils.isEmpty(searchDate)) {
            condition.put("startTime", searchDate.split("~")[0] + "00:00:00");
            condition.put("endTime", searchDate.split("~")[1] + " 23:59:59");
        }
        condition.put("currentUser", UserUtil.getUserId());
        return new ResponseJson(Constants.CODE_SUCCESS, businessService.selectToDoPageList(condition), null,
                businessService.selectToDoCount(condition));
    }

    @ApiOperation(value = "查询我处理过的业务流程", httpMethod = "POST", response = BusinessEntity.class, responseContainer = "List")
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query",name = "searchDate", value = "开始时间-结束事件,格式：yyyy-MM-dd~yyyy-MM-dd"),
                    @ApiImplicitParam(paramType = "query",name = "page", value = "当前页,如不传,则默认为1", example = "1"),
                    @ApiImplicitParam(paramType = "query",name = "rows", value = "每页的数据条目,如不传,则默认为10", example = "10"),
                    @ApiImplicitParam(paramType = "query",name = "sort", value = "排序字段", example = "groupName"),
                    @ApiImplicitParam(paramType = "query",name = "order", value = "排序方式，可选值有：ASC DESC,当sort字段传入值时,其必须传入", example = "DESC")
            }
    )
    @RequiresUser
    @ResMsg (tag = "查询我处理过的业务流程", type = ResType.JSON,writeLogs = false)
    @RequestMapping(value = "/doneList/{flowId}", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson doneList(@ApiIgnore BusinessEntity entity, @ApiIgnore String searchDate) {
        Map<String, Object> condition = beanToMap(entity);
        if (!StringUtils.isEmpty(searchDate)) {
            condition.put("startTime", searchDate.split("~")[0] + "00:00:00");
            condition.put("endTime", searchDate.split("~")[1] + " 23:59:59");
        }
        condition.put("currentUser", UserUtil.getUserId());
        return new ResponseJson(Constants.CODE_SUCCESS, businessService.selectDonePageList(condition), null,
                businessService.selectDoneCount(condition));
    }

    /**
     * 查看业务流程详情的json串
     *
     * @param id 数据编号
     * @return 详情json串
     */
    @ApiOperation(value = "查看业务流程详情", httpMethod = "POST", response = BusinessEntity.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query",name = "id", value = "表单编号"),
            }
    )
    @ResponseBody
    @RequiresUser
    @ResMsg (tag = "查看业务流程详情的json串", type = ResType.JSON,writeLogs = false)
    @RequestMapping(value = "/detail", produces = "application/json;charset=utf-8")
    public ResponseJson detail(@ApiIgnore String id) {
        idCheck(id, MODULE_NAME);
        BusinessEntity entity = businessService.selectOneById(id);
        existCheck(entity, MODULE_NAME);
        return new ResponseJson(Constants.CODE_SUCCESS, entity);
    }

    @ApiOperation(value = "召回业务流程", httpMethod = "POST", response = Void.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query",name = "id", value = "表单编号")
            }
    )
    @ResponseBody
    @RequiresUser
    @ResMsg (tag = "召回业务流程", type = ResType.JSON)
    @RequestMapping(value = "/cancel", produces = "application/json;charset=utf-8")
    public ResponseJson cancel(@ApiIgnore String id) {
        idCheck(id, MODULE_NAME);
        BusinessEntity entity = businessService.selectOneById(id);
        existCheck(entity, MODULE_NAME);
        if (entity.getState() != 0l) {
            throw new BussinessException("当前数据不允许召回", null);
        }
        String userId = UserUtil.getUserId();
        if (!userId.equals(entity.getCreateUser())) {
            throw new BussinessException("无法召回别人的数据", null);
        }
        businessService.cancel(entity, UserUtil.getUserId());
        return new ResponseJson(Constants.CODE_SUCCESS, entity);
    }


}
