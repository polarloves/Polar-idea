package polar.island.flow.business.util;

import polar.island.flow.business.expection.BussinessException;
import polar.island.flow.column.entity.ColumnEntity;
import polar.island.flow.flow.entity.FlowEntity;
import polar.island.flow.form.entity.FormEntity;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FlowUtil {
    private FlowUtil() {
    }

    public static void checkFlow(FlowEntity flowEntity) {
        if (flowEntity == null) {
            throw new BussinessException("流程未配置", null);
        }
        if (flowEntity.getFormId() == null) {
            throw new BussinessException("流程表单未配置", null);
        }
    }

    public static List<Map<String, Object>> findFlowColumnData(FormEntity formEntity,  HttpServletRequest request) {
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
        for (ColumnEntity columnEntity : formEntity.getColumnEntity()) {
            Map<String, Object> tmp = new HashMap<String, Object>();
            tmp.put("key", columnEntity.getKey());
            tmp.put("formId", formEntity.getId());
            tmp.put("formColumnId", columnEntity.getId());
            Object value = null;
            Object realValue = null;
            if (columnEntity.getType() == 11 || columnEntity.getType() == 9) {
                realValue = new ArrayList<String>();
                int index = 0;
                while (true) {
                    String tmpValue = request.getParameter(columnEntity.getKey() + "[" + index + "]");
                    if (tmpValue == null) {
                        break;
                    }
                    ((List<String>) realValue).add(tmpValue);
                    if (value == null) {
                        value = tmpValue;
                    } else {
                        value = value + "," + tmpValue;
                    }
                    index++;
                }
            } else {
                value = request.getParameter(columnEntity.getKey());
                realValue = value;
            }
            tmp.put("formValue", value);
            tmp.put("realValue", realValue);
            result.add(tmp);
        }
        return result;
    }

}
