package polar.island.flow.flow.web;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import polar.island.core.config.Constants;
import polar.island.shiro.util.UserUtil;
import polar.island.web.controller.BasicController;
import polar.island.core.entity.ResponseJson;
import polar.island.core.res.ResMsg;
import polar.island.core.res.ResType;
import polar.island.core.util.CommonUtil;
import polar.island.flow.flow.entity.AcceptFlowFormEntity;
import polar.island.flow.flow.entity.FlowEntity;
import polar.island.flow.flow.service.FlowService;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Date;
import java.util.Map;

/**
 * 流程定义的接口，返回数据全部为json串。
 *
 * @author PolarLoves
 */
@Controller(value = "flowJsonController")
@RequestMapping(value = "/flow/json")
public class FlowJsonController extends BasicController {
    @Resource(name = "polarFlowService")
    private FlowService flowService;
    private static final String MODULE_NAME = "流程";

    /**
     * 校验访问权限
     *
     * @return
     */
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:flow:empty"})
    @ResMsg (tag = "校验用户流程权限", type = ResType.JSON)
    @RequestMapping(value = "/checkPermission", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson checkPermission() {
        return new ResponseJson(Constants.CODE_SUCCESS);
    }

    /**
     * 依据条件查询分页数据，如果不传页码、每页条目，则默认分别为：1,10
     *
     * @param entity 查询条件
     * @return 列表Json串。
     * @see polar.island.flow.flow.entity.FlowEntity
     */
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:flow:view:list"})
    @ResMsg (tag = "查询流程", type = ResType.JSON,writeLogs = false)
    @RequestMapping(value = "/pageList", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson pageList(FlowEntity entity) {
        Map<String, Object> condition = beanToMap(entity);
        return new ResponseJson(Constants.CODE_SUCCESS, flowService.selectPageList(condition), null,
                flowService.selectCount(condition));
    }

    /**
     * 查看流程定义详情的json串
     *
     * @param id 数据编号
     * @return 详情json串
     */
    @ResponseBody
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:flow:view:detail"})
    @ResMsg (tag = "查看流程详情的json串", type = ResType.JSON,writeLogs = false)
    @RequestMapping(value = "/detail", produces = "application/json;charset=utf-8")
    public ResponseJson detail(String id) {
        idCheck(id, MODULE_NAME);
        FlowEntity entity = flowService.selectOneById(id);
        existCheck(entity, MODULE_NAME);
        return new ResponseJson(Constants.CODE_SUCCESS, entity);
    }

    /**
     * 依据数据编号更新此条数据的所有值。
     *
     * @param entity 需要更新的实体
     * @return 更新条目
     * @see polar.island.flow.flow.entity.FlowEntity
     */
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:flow:edit"})
    @ResMsg (tag = "更新流程定义全部字段", type = ResType.JSON)
    @RequestMapping(value = "/updateAllById", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson updateAllById(@Valid FlowEntity entity, BindingResult bindingResult) {
        validate(FlowEntity.class, "updateAllById", bindingResult);
        Map<String, Object> condition = beanToMap(entity);
        Long result = flowService.updateAll(condition);
        return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功修改了" + result + "条数据。");
    }

    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:flow:edit"})
    @ResMsg (tag = "更新流程图", type = ResType.JSON)
    @RequestMapping(value = "/updateBpmnData", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson updateBpmnData(String bpmnData, String id) {
        idCheck(id, MODULE_NAME);
        Long result = flowService.updateBpmnData(id, bpmnData);
        return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功修改了" + result + "条数据。");
    }

    /**
     * 根据数据编号物理删除数据。
     *
     * @param ids 数据编号
     * @return 删除结果。
     */
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:flow:delete"})
    @ResMsg (tag = "删除多个流程", type = ResType.JSON)
    @RequestMapping(value = "/deleteMulitById", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson deleteMulitById(@RequestParam(value = "ids[]", required = false) String[] ids) {
        idCheck(ids, MODULE_NAME);
        Long result = flowService.deleteMulitByIdPhysical(ids);
        return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功删除了" + result + "条数据。");
    }

    /**
     * 根据数据编号物理删除数据。
     *
     * @param id 数据编号
     * @return 删除结果。
     */
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:flow:delete"})
    @ResMsg (tag = "删除一个流程", type = ResType.JSON)
    @RequestMapping(value = "/deleteById", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson deleteById(String id) {
        idCheck(id, MODULE_NAME);
        Long result = flowService.deleteByIdPhysical(id);
        return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功删除了" + result + "条数据。");
    }

    /**
     * 新增一条数据。
     *
     * @param entity 数据
     * @return 新增结果。
     * @see polar.island.flow.flow.entity.FlowEntity
     */
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:flow:add"})
    @ResMsg (tag = "新增流程", type = ResType.JSON)
    @RequestMapping(value = "/add", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson add(@Valid FlowEntity entity, BindingResult bindingResult) {
        validate(FlowEntity.class, "add", bindingResult);
        Map<String, Object> condition = beanToMap(entity);
        condition.put("createUser", UserUtil.getUserId());
        condition.put("createDate", new Date());
        Object result = flowService.insert(condition);
        return new ResponseJson(Constants.CODE_SUCCESS, result, "新增成功。");
    }

    @ResponseBody
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:flow:view:detail"})
    @ResMsg (tag = "修改表单匹配规则", type = ResType.JSON)
    @RequestMapping(value = "/updateFlowMatches", produces = "application/json;charset=utf-8")
    public ResponseJson updateFlowMatches(@Valid AcceptFlowFormEntity entity, BindingResult bindingResult) {
        validate(bindingResult);
        flowService.upDateMatches(CommonUtil.valueOf(entity.getId()), entity.getMatches());
        return new ResponseJson(Constants.CODE_SUCCESS, entity);
    }

    @ResponseBody
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:flow:view:detail"})
    @ResMsg (tag = "发布流程", type = ResType.JSON)
    @RequestMapping(value = "/publish", produces = "application/json;charset=utf-8")
    public ResponseJson publish(String id) {
        idCheck(id, MODULE_NAME);
        flowService.publish(id);
        return new ResponseJson(Constants.CODE_SUCCESS, null);
    }
}
