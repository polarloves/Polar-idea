package polar.island.flow.flow.web;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import polar.island.web.controller.BasicController;
import polar.island.core.res.ResMsg;
import polar.island.core.res.ResType;
import polar.island.core.util.GsonUtil;
import polar.island.flow.flow.entity.FlowEntity;
import polar.island.flow.flow.entity.FlowFormMatches;
import polar.island.flow.flow.service.FlowService;
import polar.island.flow.form.service.FormService;
import polar.island.flow.group.service.GroupService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 流程定义表的控制器类，返回参数为页面，为配合加载，出错时为Json串，可修改@ErrorMsg标签，使其出错时返回页面。
 *
 * @author PolarLoves
 */
@Controller(value = "flowWebController")
@RequestMapping(value = "/flow/web")
public class FlowWebController extends BasicController {
    @Resource(name = "polarFlowService")
    private FlowService flowService;
    private static final String MODULE_NAME = "流程定义表";

    @Resource(name = "groupService")
    private GroupService groupService;
    @Resource(name = "polarFormService")
    private FormService formService;

    /**
     * 流程定义表列表页面
     *
     * @param request request对象
     * @return 列表页面，如出错，则返回Json串。
     */
    @ResMsg (tag = "流程定义表列表", type = ResType.JSON,writeLogs = false)
    @RequestMapping(value = {"/list", ""}, produces = "text/html;charset=utf-8")
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:flow:view:list"})
    public String list(HttpServletRequest request) {
        return "/view/flow/flow/flowList.jsp";
    }

    /**
     * 流程定义表新增页面
     *
     * @param request request对象
     * @return 新增页面，如出错，则返回Json串。
     */
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:flow:add"})
    @ResMsg (tag = "新增流程定义表", type = ResType.JSON,writeLogs = false)
    @RequestMapping(value = "/add", produces = "text/html;charset=utf-8")
    public String add(HttpServletRequest request) {
        FlowEntity entity = new FlowEntity();
        request.setAttribute("flow", entity);
        request.setAttribute("allGroups", groupService.allGroups());
        request.setAttribute("allForms", formService.allForms());
        return "/view/flow/flow/flowForm.jsp";
    }

    /**
     * 更新流程定义表页面
     *
     * @param request request对象
     * @param id      数据编号
     * @return 更新页面，如出错，则返回Json串。
     */
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:flow:edit"})
    @ResMsg (tag = "更新流程定义表", type = ResType.JSON,writeLogs = false)
    @RequestMapping(value = "/update", produces = "text/html;charset=utf-8")
    public String updatePage(HttpServletRequest request, String id) {
        idCheck(id, MODULE_NAME);
        FlowEntity entity = flowService.selectOneById(id);
        existCheck(entity, MODULE_NAME);
        request.setAttribute("flow", entity);
        request.setAttribute("allGroups", groupService.allGroups());
        request.setAttribute("allForms", formService.allForms());
        return "/view/flow/flow/flowForm.jsp";
    }

    /**
     * 流程定义表的详情页面
     *
     * @param request request对象
     * @param id      数据编号
     * @return 列表页面，如出错，则返回Json串。
     */
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:flow:view:detail"})
    @ResMsg (tag = "查看流程定义表", type = ResType.JSON,writeLogs = false)
    @RequestMapping(value = "/detail", produces = "text/html;charset=utf-8")
    public String detailPage(String id, HttpServletRequest request) {
        return updatePage(request, id);
    }


    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:flow:view:detail"})
    @ResMsg (tag = "查看流程图", type = ResType.JSON,writeLogs = false)
    @RequestMapping(value = "/bpmnData", produces = "text/html;charset=utf-8")
    public String bpmnData(String id, HttpServletRequest request) {
        idCheck(id, MODULE_NAME);
        FlowEntity entity = flowService.selectOneById(id);
        existCheck(entity, MODULE_NAME);
        String str = StringEscapeUtils.unescapeHtml4(entity.getBpmnData());
        if (str != null) {
            str = str.replace("\n", "");
        }
        request.setAttribute("bpmnData", str);
        request.setAttribute("id", entity.getId());
        return "/view/flow/flow/bpmnData.jsp";
    }

    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:flow:edit"})
    @ResMsg (tag = "匹配表单", type = ResType.JSON,writeLogs = false)
    @RequestMapping(value = "/matchForm", produces = "text/html;charset=utf-8")
    public String matchForm(String id, HttpServletRequest request) {
        idCheck(id, MODULE_NAME);
        request.setAttribute("flowMatches", GsonUtil.toJson(flowService.flowMatches(id)));
        request.setAttribute("allGroups", groupService.allGroups());
        request.setAttribute("allForms", formService.allForms());
        request.setAttribute("id", id);
        return "/view/flow/flow/flowtable.jsp";
    }

    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:flow:edit"})
    @ResMsg (tag = "匹配表单修改页面", type = ResType.JSON,writeLogs = false)
    @RequestMapping(value = "/matchFormUpate", produces = "text/html;charset=utf-8")
    public String matchFormUpate(FlowFormMatches entity, HttpServletRequest request) {
        request.setAttribute("flowMatches", entity);
        request.setAttribute("allGroups", groupService.allGroups());
        request.setAttribute("allForms", formService.allForms());
        return "/view/flow/flow/flowtable_edit.jsp";
    }
}
