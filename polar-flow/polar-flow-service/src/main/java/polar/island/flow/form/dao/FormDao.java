package polar.island.flow.form.dao;
import org.apache.ibatis.annotations.Param;
import polar.island.core.dao.BasicDao;
import polar.island.core.entity.DictEntity;
import polar.island.flow.form.entity.FormEntity;
import polar.island.mybatis.annotations.MybatisStore;
import polar.island.mybatis.common.MybatisDao;

import java.util.List;
import java.util.Map;

/**
 * 流程表单的持久化层。
 * 
 * @author PolarLoves
 *
 */
@MybatisStore (value="formDao")
public interface FormDao extends MybatisDao<FormEntity,FormEntity> {
	/**
	 * 由于仅有物理删除，此删除方法被移除
	 */
	@Deprecated
	public Long deleteByIdLogic(@Param(value = "id") String id);
	/**
	 * 由于仅有物理删除，此删除方法被移除
	 */
	@Deprecated
	public Long deleteByConditionLogic(Map<String, Object> condition);

	public List<DictEntity> allForms();
}
