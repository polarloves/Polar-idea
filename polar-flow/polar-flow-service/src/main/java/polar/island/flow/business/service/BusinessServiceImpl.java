package polar.island.flow.business.service;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import polar.island.core.util.ExceptionUtil;
import polar.island.core.util.GsonUtil;
import polar.island.flow.business.dao.BusinessDao;
import polar.island.flow.business.entity.BusinessEntity;
import polar.island.flow.business.expection.BussinessException;
import polar.island.flow.column.dao.ColumnDao;
import polar.island.flow.flow.entity.FlowEntity;
import polar.island.flow.form.entity.FormEntity;
import polar.island.mybatis.service.MybatisService;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(value = "businessService")
public class BusinessServiceImpl extends MybatisService<BusinessEntity, BusinessEntity, BusinessDao> implements BusinessService {
    @Resource(name = "businessDao")
    private BusinessDao businessDao;
    @Resource(name = "repositoryService")
    private RepositoryService repositoryService;
    @Resource(name = "runtimeService")
    private RuntimeService runtimeService;
    @Resource(name = "taskService")
    private TaskService taskService;
    @Resource(name = "columnDao")
    private ColumnDao columnDao;

    @Override
    public BusinessDao getDao() {
        return businessDao;
    }

    @Transactional(readOnly = false)
    @Override
    public Object insert(Map<String, Object> condition) {
        try {
            Object id = null;
            businessDao.insert(condition);
            id = condition.get("id");
            return id;
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Transactional(readOnly = false)
    @Override
    public void start(BusinessEntity businessEntity, FlowEntity flowEntity, FormEntity formEntity, List<Map<String, Object>> columnDatas) {
        try {
            Long count = businessDao.findUserCanOperateFlowCount(businessEntity.getCreateUser(), flowEntity.getId() + "");
            Date current = new Date();
            if (count == 0) {
                throw new BussinessException("当前用户无法执行此操作", null);
            }
            Map<String, Object> startMap = new HashMap<String, Object>();
            for (Map<String, Object> columnData : columnDatas) {
                startMap.put(columnData.get("key").toString(), columnData.get("realValue"));
            }
            //调用activiti，发布一个流程
            ProcessDefinition def = repositoryService.createProcessDefinitionQuery().deploymentId(flowEntity.getFlowDeployId()).singleResult();
            if (def == null) {
                throw new BussinessException("发布后的流程不存在，请重新发布", null);
            }
            ProcessInstance processInstance = runtimeService.startProcessInstanceById(def.getId(), startMap);
            Task task = taskService.createTaskQuery().processInstanceId(processInstance.getId()).singleResult();
            String currentOperateGroup = null;
            if (task != null) {
                String currentProcessKey = task.getTaskDefinitionKey();//当前任务的编号
                currentOperateGroup = businessDao.findGroupIdByKeyAndFlowId(flowEntity.getId() + "", currentProcessKey);
                if (currentOperateGroup == null) {
                    throw new BussinessException("key为" + currentProcessKey + "的流程未配置表单", null);
                }
            } else {
                //此处表示任务已完成
                throw new BussinessException("当前流程配置有误，不能刚刚开始就结束", null);
            }
            //存入主表数据
            Map<String, Object> condition = new HashMap<String, Object>();
            condition.put("title", businessEntity.getTitle());
            condition.put("displayJson", businessEntity.getDisplayJson());
            condition.put("createUser", businessEntity.getCreateUser());
            condition.put("createDate", current);
            condition.put("flowId", flowEntity.getId());
            condition.put("state", 0);
            condition.put("processId", processInstance.getId());
            condition.put("realJson", GsonUtil.toJson(startMap));
            condition.put("currentOperate", currentOperateGroup);
            condition.put("currentKey", task.getTaskDefinitionKey());
            insert(condition);
            //存入列数据
            for (Map<String, Object> columnData : columnDatas) {
                columnData.put("businessId", condition.get("id"));
                businessDao.insertColumnData(columnData);
            }
            Map<String, Object> log = new HashMap<String, Object>();
            log.put("flowDataId", condition.get("id"));
            log.put("flowId", flowEntity.getId());
            log.put("logContent", businessDao.findUserNickName(flowEntity.getCreateUser()) + "启动流程");
            log.put("createDate", current);
            log.put("createUser", flowEntity.getCreateUser());
            log.put("formWidth", formEntity.getFormWidth());
            log.put("formHeight", formEntity.getFormHeight());
            log.put("displayJson", businessEntity.getDisplayJson());
            log.put("type", 1);
            //插入日志
            businessDao.insertFlowLog(log);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Transactional(readOnly = false)
    @Override
    public void cancel(BusinessEntity entity, String userId) {
        try {
            runtimeService.deleteProcessInstance(entity.getProcessId(), "用户召回");
            businessDao.changeState("-1", entity.getId() + "");
            Map<String, Object> log = new HashMap<String, Object>();
            log.put("flowDataId", entity.getId());
            log.put("flowId", entity.getFlowId());
            log.put("logContent", businessDao.findUserNickName(userId) + "召回");
            log.put("createDate", new Date());
            log.put("createUser", userId);
            log.put("formWidth", null);
            log.put("formHeight", null);
            log.put("displayJson", null);
            log.put("type", 4);
            //插入日志
            businessDao.insertFlowLog(log);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    public FormEntity findCurrentFormEntity(BusinessEntity businessEntity) {
        try {
            Task task = taskService.createTaskQuery().processInstanceId(businessEntity.getProcessId()).singleResult();
            if (task == null) {
                throw new BussinessException("当前任务已被完成，无法执行此操作", null);
            }
            String key = task.getTaskDefinitionKey();
            FormEntity formEntity = businessDao.findFormByKeyAndFlowId(businessEntity.getFlowId() + "", key);
            if (formEntity == null) {
                throw new BussinessException("key为" + key + "的流程未配置表单", null);
            }
            Map<String, Object> condition = new HashMap<String, Object>();
            condition.put("formId", formEntity.getId());
            formEntity.setColumnEntity(columnDao.selectList(condition));
            return formEntity;
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Override
    public List<BusinessEntity> selectToDoPageList(Map<String, Object> condition) {
        try {
            return businessDao.selectToDoPageList(condition);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Override
    public Long selectToDoCount(Map<String, Object> condition) {
        try {
            return businessDao.selectToDoCount(condition);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Override
    public List<BusinessEntity> selectDonePageList(Map<String, Object> condition) {
        try {
            return businessDao.selectDonePageList(condition);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Override
    public Long selectDoneCount(Map<String, Object> condition) {
        try {
            return businessDao.selectDoneCount(condition);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Transactional(readOnly = false)
    @Override
    public void complete(BusinessEntity businessEntity, String userId, List<Map<String, Object>> columnDatas, String displayJson) {
        try {
            String state = "1", nextStep = null;
            Task task = taskService.createTaskQuery().processInstanceId(businessEntity.getProcessId()).singleResult();
            if (task == null) {
                throw new BussinessException("当前任务已被完成，无法执行此操作", null);
            }
            String key = task.getTaskDefinitionKey();
            Map<String, Object> value = new HashMap<String, Object>();
            Map<String, Object> current = new HashMap<String, Object>();
            for (Map<String, Object> columnData : columnDatas) {
                current.put(columnData.get("key").toString(), columnData.get("realValue"));
            }
            value.put("start", GsonUtil.jsonToObject(StringEscapeUtils.unescapeHtml4(businessEntity.getRealJson()), HashMap.class));
            value.put("current", current);
            taskService.complete(task.getId(), value);
            task = taskService.createTaskQuery().processInstanceId(businessEntity.getProcessId()).singleResult();
            String nextKey = null;
            if (task == null) {
                state = "2";
            } else {
                String currentProcessKey = task.getTaskDefinitionKey();//当前任务的编号
                nextStep = businessDao.findGroupIdByKeyAndFlowId(businessEntity.getFlowId() + "", currentProcessKey);
                nextKey = task.getTaskDefinitionKey();
                if (nextStep == null) {
                    throw new BussinessException("key为" + currentProcessKey + "的流程未配置表单", null);
                }
            }
            //改变当前任务状态以及执行人
            businessDao.changeStateAndNextStep(state, businessEntity.getId() + "", nextStep, nextKey);
            FormEntity formEntity = businessDao.findFormByKeyAndFlowId(businessEntity.getFlowId() + "", key);
            Map<String, Object> log = new HashMap<String, Object>();
            log.put("flowDataId", businessEntity.getId());
            log.put("flowId", businessEntity.getFlowId());
            log.put("logContent", businessDao.findUserNickName(userId) + formEntity.getFunctionName());
            log.put("createDate", new Date());
            log.put("createUser", userId);
            log.put("formWidth", formEntity.getFormWidth());
            log.put("formHeight", formEntity.getFormHeight());
            log.put("displayJson", displayJson);
            log.put("type", 2);
            businessDao.insertFlowLog(log);
            if (state.equals("2")) {
                //插入一条结束事件
                log.clear();
                log.put("flowDataId", businessEntity.getId());
                log.put("flowId", businessEntity.getFlowId());
                log.put("logContent", "结束");
                log.put("createDate", new Date());
                log.put("createUser", userId);
                log.put("formWidth", null);
                log.put("formHeight", null);
                log.put("displayJson", null);
                log.put("type", 3);
                businessDao.insertFlowLog(log);
            }
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }


    @Override
    public List<Map<String, Object>> findHistory(String id) {
        try {
            return businessDao.findHistory(id);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }
}