package polar.island.flow.flow.dao;

import org.apache.ibatis.annotations.Param;
import polar.island.flow.flow.entity.FlowEntity;
import polar.island.flow.flow.entity.FlowFormMatches;
import polar.island.mybatis.annotations.MybatisStore;
import polar.island.mybatis.common.MybatisDao;

import java.util.List;
import java.util.Map;

/**
 * 流程定义表的持久化层。
 *
 * @author PolarLoves
 */
@MybatisStore (value = "flowDao")
public interface FlowDao extends MybatisDao<FlowEntity, FlowEntity> {
    /**
     * 由于仅有物理删除，此删除方法被移除
     */
    @Deprecated
    public Long deleteByIdLogic(@Param(value = "id") String id);

    /**
     * 由于仅有物理删除，此删除方法被移除
     */
    @Deprecated
    public Long deleteByConditionLogic(Map<String, Object> condition);

    public List<FlowFormMatches> flowMatches(@Param(value = "flowId") String flowId);

    public void insertMatches(Map<String, Object> condition);

    public Long deleteFlowMatches(@Param(value = "flowId") String flowId);

    public Long updatePublishState(@Param(value = "id") String id, @Param(value = "state") String state, @Param(value = "flowDeployId") String flowDeployId);
}
