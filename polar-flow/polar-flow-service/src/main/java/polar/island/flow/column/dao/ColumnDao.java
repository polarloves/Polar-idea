package polar.island.flow.column.dao;

import org.apache.ibatis.annotations.Param;
import polar.island.core.dao.BasicDao;
import polar.island.flow.column.entity.ColumnEntity;
import polar.island.mybatis.annotations.MybatisStore;
import polar.island.mybatis.common.MybatisDao;

import java.util.Map;

/**
 * 表单列属性的持久化层,流程表单的子表，其列表字段与详情字段一致。
 * 
 * @author PolarLoves
 *
 */
@MybatisStore (value="columnDao")
public interface ColumnDao extends MybatisDao<ColumnEntity,ColumnEntity> {
	/**
	 * 由于仅有物理删除，此删除方法被移除
	 */
	@Deprecated
	public Long deleteByIdLogic(@Param(value = "id") String id);
	/**
	 * 由于仅有物理删除，此删除方法被移除
	 */
	@Deprecated
	public Long deleteByConditionLogic(Map<String, Object> condition);
}
