package polar.island.flow.form.service;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import polar.island.core.entity.DictEntity;
import polar.island.core.util.EntityUtil;
import polar.island.core.util.ExceptionUtil;
import polar.island.flow.column.dao.ColumnDao;
import polar.island.flow.column.entity.ColumnEntity;
import polar.island.flow.form.dao.FormDao;
import polar.island.flow.form.entity.FormEntity;
import polar.island.mybatis.service.MybatisService;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(value = "polarFormService")
public class FormServiceImpl extends MybatisService<FormEntity, FormEntity, FormDao> implements FormService {
    @Resource(name = "formDao")
    private FormDao formDao;
    //column的dao
    @Resource(name = "columnDao")
    private ColumnDao columnDao;

    @Override
    public FormDao getDao() {
        return formDao;
    }

    /**
     * 添加子表数据
     *
     * @param id        主表编号
     * @param condition 包含子表数据的实体类
     */
    private void addItems(Object id, Map<String, Object> condition) {
        //添加子表数据
        List<ColumnEntity> columnEntity = (List<ColumnEntity>) condition.get("columnEntity");
        if (!CollectionUtils.isEmpty(columnEntity)) {
            //添加表单列属性
            for (ColumnEntity item : columnEntity) {
                Map<String, Object> itemValue = EntityUtil.beanToMap(item);
                //设置表单列属性其父类外键
                itemValue.put("formId", id);
                columnDao.insert(itemValue);
            }
        }
    }

    @Override
    public FormEntity selectOneById(String id) {
        try {
            FormEntity entity = super.selectOneById(id);
            Map<String, Object> condition = new HashMap<String, Object>();
            //查询表单列属性
            condition.put("formId", id);
            entity.setColumnEntity(columnDao.selectList(condition));
            condition.clear();
            return entity;
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Override
    public FormEntity selectOneByCondition(Map<String, Object> condition) {
        try {
            FormEntity entity = super.selectOneByCondition(condition);
            Object id = entity.getId();
            //查询表单列属性
            condition.put("formId", id);
            entity.setColumnEntity(columnDao.selectList(condition));
            condition.clear();
            return entity;
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @CacheEvict(value = "dictCache", key = "#root.targetClass")
    @Transactional(readOnly = false)
    @Override
    public Object insert(Map<String, Object> condition) {
        try {
            Object id = null;
            formDao.insert(condition);
            id = condition.get("id");
            addItems(id, condition);
            return id;
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @CacheEvict(value = "dictCache", key = "#root.targetClass")
    @Override
    public Long deleteMulitByIdPhysical(String[] ids) {
        try {
            return super.deleteMulitByIdPhysical(ids);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @CacheEvict(value = "dictCache", key = "#root.targetClass")
    @Override
    @Transactional(readOnly = false)
    public Long updateAll(Map<String, Object> condition) {
        try {
            Object id = condition.get("id");
            //删除子表数据.
            Map<String, Object> deleteParams = new HashMap<String, Object>();
            deleteParams.put("formId", id);
            //删除表单列属性
            columnDao.deleteByConditionPhysical(deleteParams);
            deleteParams.clear();
            addItems(id, condition);
            return getDao().updateAll(condition);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @CacheEvict(value = "dictCache", key = "#root.targetClass")
    @Transactional(readOnly = false)
    public Long deleteByIdPhysical(String id) {
        try {//删除子表数据.
            Map<String, Object> deleteParams = new HashMap<String, Object>();
            deleteParams.put("formId", id);
            //删除表单列属性
            columnDao.deleteByConditionPhysical(deleteParams);
            deleteParams.clear();
            return super.deleteByIdPhysical(id);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Cacheable(value = "dictCache", key = "#root.targetClass")
    @Override
    public List<DictEntity> allForms() {
        try {
            return formDao.allForms();
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }
}