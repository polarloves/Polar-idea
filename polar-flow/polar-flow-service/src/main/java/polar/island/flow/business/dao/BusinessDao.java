package polar.island.flow.business.dao;

import org.apache.ibatis.annotations.Param;
import polar.island.flow.business.entity.BusinessEntity;
import polar.island.flow.form.entity.FormEntity;
import polar.island.mybatis.annotations.MybatisStore;
import polar.island.mybatis.common.MybatisDao;

import java.util.List;
import java.util.Map;

/**
 * 业务流程的持久化层。
 *
 * @author PolarLoves
 */
@MybatisStore (value = "businessDao")
public interface BusinessDao extends MybatisDao<BusinessEntity, BusinessEntity> {
    /**
     * 由于仅有物理删除，此删除方法被移除
     */
    @Deprecated
    public Long deleteByIdLogic(@Param(value = "id") String id);

    /**
     * 由于仅有物理删除，此删除方法被移除
     */
    @Deprecated
    public Long deleteByConditionLogic(Map<String, Object> condition);

    /**
     * 插入表单的列数据
     *
     * @param condition 列数据
     */
    public void insertColumnData(Map<String, Object> condition);

    /**
     * 插入流程的历史记录
     *
     * @param condition 历史数据
     */
    public void insertFlowLog(Map<String, Object> condition);

    /**
     * 根据用户编号查找用户的昵称
     *
     * @param userId 用户编号
     * @return 用户昵称
     */
    public String findUserNickName(@Param(value = "userId") String userId);

    /**
     * 修改业务的状态
     *
     * @param state 业务状态
     * @param id    业务编号
     */
    public void changeState(@Param(value = "state") String state, @Param(value = "id") String id);

    /**
     * 修改业务状态以及下一步处理人即表单
     *
     * @param state          业务状态
     * @param id             业务编号
     * @param currentOperate 当前操作的组
     * @param key            当前操作表单的key
     */
    public void changeStateAndNextStep(@Param(value = "state") String state, @Param(value = "id") String id, @Param(value = "currentOperate") String currentOperate, @Param(value = "key") String key);

    /**
     * 查询用户是否可以操作当前业务
     *
     * @param userId 用户编号
     * @param flowId 流程编号
     * @return 0或者1
     */
    public Long findUserCanOperateFlowCount(@Param(value = "userId") String userId, @Param(value = "flowId") String flowId);

    /**
     * 查询业务操作历史
     *
     * @param id 业务编号
     * @return 操作历史
     */
    public List<Map<String, Object>> findHistory(@Param(value = "id") String id);

    /**
     * 依据key以及流程编号，查询可操作的用户组编号
     *
     * @param flowId 流程编号
     * @param key    key
     * @return 可操作的用户组编号
     */
    public String findGroupIdByKeyAndFlowId(@Param(value = "flowId") String flowId, @Param(value = "key") String key);

    /**
     * 依据key以及流程编号，查询表单对象
     *
     * @param flowId 流程编号
     * @param key    key
     * @return 表单对象
     */
    public FormEntity findFormByKeyAndFlowId(@Param(value = "flowId") String flowId, @Param(value = "key") String key);

    /**
     * 查询待办列表，其中currentUser不能为空
     *
     * @param condition 查询条件。
     * @return 待办列表
     */
    public List<BusinessEntity> selectToDoPageList(Map<String, Object> condition);

    /**
     * 查询待办数量，其中currentUser不能为空
     *
     * @param condition 查询条件。
     * @return 待办数量
     */
    public Long selectToDoCount(Map<String, Object> condition);

    /**
     * 查询已办列表，其中currentUser不能为空
     *
     * @param condition 查询条件。
     * @return 待办列表
     */
    public List<BusinessEntity> selectDonePageList(Map<String, Object> condition);

    /**
     * 查询已办数量，其中currentUser不能为空
     *
     * @param condition 查询条件。
     * @return 待办数量
     */
    public Long selectDoneCount(Map<String, Object> condition);

}
