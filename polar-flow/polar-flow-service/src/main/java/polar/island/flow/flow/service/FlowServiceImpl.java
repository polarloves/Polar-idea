package polar.island.flow.flow.service;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.Deployment;
import org.apache.commons.lang3.StringEscapeUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import polar.island.core.exception.ValidationException;
import polar.island.core.util.EntityUtil;
import polar.island.core.util.ExceptionUtil;
import polar.island.flow.flow.dao.FlowDao;
import polar.island.flow.flow.entity.FlowEntity;
import polar.island.flow.flow.entity.FlowFormMatches;
import polar.island.mybatis.service.MybatisService;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(value = "polarFlowService")
public class FlowServiceImpl extends MybatisService<FlowEntity, FlowEntity, FlowDao> implements FlowService {
    @Resource(name = "flowDao")
    private FlowDao flowDao;
    @Resource(name = "repositoryService")
    private RepositoryService repositoryService;
    @Resource(name = "runtimeService")
    private RuntimeService runtimeService;
    @Resource(name = "taskService")
    private TaskService taskService;

    @Override
    public FlowDao getDao() {
        return flowDao;
    }

    @Transactional(readOnly = false)
    @Override
    public Object insert(Map<String, Object> condition) {
        try {
            Object id = null;
            condition.put("state", 0);
            flowDao.insert(condition);
            id = condition.get("id");
            return id;
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Transactional(readOnly = false)
    @Override
    public Long updateBpmnData(String id, String bpmnData) {
        try {
            Map<String, Object> condition = new HashMap<String, Object>();
            condition.put("id", id);
            condition.put("bpmnData", bpmnData);
            return flowDao.updateField(condition);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Override
    public List<FlowFormMatches> flowMatches(String flowId) {
        try {
            return flowDao.flowMatches(flowId);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Transactional(readOnly = false)
    @Override
    public void upDateMatches(String id, List<FlowFormMatches> matches) {
        try {
            flowDao.deleteFlowMatches(id);
            if (!CollectionUtils.isEmpty(matches)) {
                for (FlowFormMatches flowFormMatches : matches) {
                    Map<String, Object> condition = EntityUtil.beanToMap(flowFormMatches);
                    condition.put("flowId", id);
                    flowDao.insertMatches(condition);
                }
            }
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }

    }

    @Transactional(readOnly = false)
    @Override
    public void publish(String id) {
        try {
            FlowEntity entity = flowDao.selectOneById(id);
            if (entity == null) {
                throw new ValidationException("此流程不存在或者已被删除", null);
            }
            if (StringUtils.isEmpty(entity.getBpmnData())) {
                throw new ValidationException("此流程流程图未配置，无法发布", null);
            }
            if (StringUtils.isEmpty(entity.getFormId())) {
                throw new ValidationException("流程发起表单不能为空", null);
            }
            if (StringUtils.isEmpty(entity.getGroupId())) {
                throw new ValidationException("流程发起人不能为空", null);
            }
            try {
                Deployment deployment = repositoryService.createDeployment().addString("deploy.bpmn20.xml", StringEscapeUtils.unescapeHtml4(entity.getBpmnData())).deploy();
                flowDao.updatePublishState(id, "1", deployment.getId());
            } catch (Exception e) {
                throw new ValidationException("流程发起失败，请检查流程图是否正确", null);
            }
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }
}