package polar.island.flow.group.service;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import polar.island.core.entity.DictEntity;
import polar.island.core.util.ExceptionUtil;
import polar.island.flow.group.dao.GroupDao;
import polar.island.flow.group.entity.User;
import polar.island.mybatis.service.MybatisService;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(value = "groupService")
public class GroupServiceImpl extends MybatisService<Map<String, Object>, Map<String, Object>, GroupDao> implements GroupService {
    @Resource(name = "groupDao")
    private GroupDao groupDao;

    @Override
    public GroupDao getDao() {
        return groupDao;
    }

    @CacheEvict(value = "groupCache", key = "#root.targetClass")
    @Transactional(readOnly = false)
    @Override
    public Object insert(Map<String, Object> condition) {
        try {
            Object id = null;
            groupDao.insert(condition);
            id = condition.get("id");
            List<User> users = (List<User>) condition.get("users");
            Map<String, Object> condition2 = new HashMap<>();
            condition2.put("groupId", id);
            if (users != null) {
                for (User userId : users) {
                    condition2.put("userId", userId.getUserId());
                    groupDao.insertGroupUser(condition2);
                }
            }
            return id;
        } catch (
                Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Override
    public Map<String, Object> selectOneById(String id) {
        try {
            Map<String, Object> result = super.selectOneById(id);
            result.put("users", groupDao.selectGroupUser(id));
            return result;
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @CacheEvict(value = "groupCache", key = "#root.targetClass")
    @Override
    public Long deleteByIdPhysical(String id) {
        try {
            Long result = super.deleteByIdPhysical(id);
            groupDao.deleteGroupUserByGroupId(id);
            return result;
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @CacheEvict(value = "groupCache", key = "#root.targetClass")
    @Override
    public Long deleteMulitByIdPhysical(String[] ids) {
        try {
            return super.deleteMulitByIdPhysical(ids);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @CacheEvict(value = "groupCache", key = "#root.targetClass")
    @Override
    public Long updateAll(Map<String, Object> condition) {
        try {
            Long result = super.updateAll(condition);
            String id = condition.get("id").toString();
            groupDao.deleteGroupUserByGroupId(id);
            List<User> users = (List<User>) condition.get("users");
            Map<String, Object> condition2 = new HashMap<>();
            condition2.put("groupId", id);
            if (users != null) {
                for (User userId : users) {
                    condition2.put("userId", userId.getUserId());
                    groupDao.insertGroupUser(condition2);
                }
            }
            return result;
        } catch (
                Exception e) {
            throw ExceptionUtil.assembleException(e);
        }

    }


    @Override
    public List<Map<String, Object>> selectUserPageList(Map<String, Object> condition) {
        try {
            return groupDao.selectUserPageList(condition);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Override
    public Long selectUserCount(Map<String, Object> condition) {
        try {
            return groupDao.selectUserCount(condition);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Cacheable(value = "groupCache", key = "#root.targetClass")
    @Override
    public List<DictEntity> allGroups() {
        try {
            return groupDao.allGroups();
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }
}