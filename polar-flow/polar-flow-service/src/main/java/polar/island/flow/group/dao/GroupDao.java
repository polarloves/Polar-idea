package polar.island.flow.group.dao;

import org.apache.ibatis.annotations.Param;
import polar.island.core.entity.DictEntity;
import polar.island.flow.group.entity.User;
import polar.island.mybatis.annotations.MybatisStore;
import polar.island.mybatis.common.MybatisDao;

import java.util.List;
import java.util.Map;

/**
 * 操作组的持久化层。
 *
 * @author PolarLoves
 */
@MybatisStore (value = "groupDao")
public interface GroupDao extends MybatisDao<Map<String, Object>, Map<String, Object>> {
    /**
     * 由于仅有物理删除，此删除方法被移除
     */
    @Deprecated
    public Long deleteByIdLogic(@Param(value = "id") String id);

    /**
     * 由于仅有物理删除，此删除方法被移除
     */
    @Deprecated
    public Long deleteByConditionLogic(Map<String, Object> condition);

    public void insertGroupUser(Map<String, Object> condition);

    public Long deleteGroupUserByGroupId(@Param(value = "groupId") String groupId);

    public List<User> selectGroupUser(@Param(value = "groupId") String groupId);

    public List<Map<String, Object>> selectUserPageList(Map<String, Object> condition);

    public Long selectUserCount(Map<String, Object> condition);

    public List<DictEntity> allGroups();
}
