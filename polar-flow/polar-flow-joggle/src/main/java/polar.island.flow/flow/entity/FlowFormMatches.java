package polar.island.flow.flow.entity;

import org.apache.ibatis.type.Alias;
import polar.island.core.entity.BasicEntity;

import javax.validation.constraints.NotNull;

@Alias(value = "FlowFormMatches")
public class FlowFormMatches extends BasicEntity {
    private Long id;
    @NotNull(message = "流程编号不能为空")
    private Long flowId;
    @NotNull(message = "表单编号不能为空")
    private Long formId;
    @NotNull(message = "执行用户不能为空")
    private Long groupId;
    @NotNull(message = "匹配编号不能为空")
    private String key;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFlowId() {
        return flowId;
    }

    public void setFlowId(Long flowId) {
        this.flowId = flowId;
    }

    public Long getFormId() {
        return formId;
    }

    public void setFormId(Long formId) {
        this.formId = formId;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
