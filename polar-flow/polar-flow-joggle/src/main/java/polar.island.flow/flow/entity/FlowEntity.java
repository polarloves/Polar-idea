package polar.island.flow.flow.entity;
import org.apache.ibatis.type.Alias;
import polar.island.core.entity.BasicEntity;
import polar.island.core.validator.TAG;

import javax.validation.constraints.NotNull;
import java.util.Date;
/**
 *  流程定义
 *
 * @author  PolarLoves
 *
 */
@Alias(value = "FlowEntity")
public class FlowEntity extends BasicEntity {
	/** 主键编号	**/
	@TAG(value = { "updateAllById" , "updateField" })
	@NotNull(message = "编号不能为空")
	private Long id;
	/** 流程发布后的编号 **/
	private String flowDeployId;
	/** 流程名称 **/
	@TAG(value = { "updateAllById", "add" })
	@NotNull(message = "流程名称不能为空")
	private String flowName;
	/** 流程状态(0-未发布,1-已发布) **/
	private Integer state;
	/** 流程描述 **/
	private String flowDescribe;
	/** 创建人 **/
	private String createUser;
	/** 创建时间 **/
	private Date createDate;
	/** 流程图数据 **/
	private String bpmnData;
	@TAG(value = { "updateAllById", "add" })
	@NotNull(message = "表单不能为空")
	/** 表单编号 **/
	private Long formId;
	@TAG(value = { "updateAllById", "add" })
	@NotNull(message = "执行人不能为空")
	/** 可执行人 **/
	private Long groupId;

	private String userName;
	public Long getId(){
		return id;
	}
	
	public void setId(Long id){
		this.id = id;
	}
	
	public String getFlowDeployId(){
		return flowDeployId;
	}
	
	public void setFlowDeployId(String flowDeployId){
		this.flowDeployId = flowDeployId;
	}
	
	public String getFlowName(){
		return flowName;
	}
	
	public void setFlowName(String flowName){
		this.flowName = flowName;
	}
	
	public Integer getState(){
		return state;
	}
	
	public void setState(Integer state){
		this.state = state;
	}
	
	public String getFlowDescribe(){
		return flowDescribe;
	}
	
	public void setFlowDescribe(String flowDescribe){
		this.flowDescribe = flowDescribe;
	}
	
	public String getCreateUser(){
		return createUser;
	}
	
	public void setCreateUser(String createUser){
		this.createUser = createUser;
	}
	
	public Date getCreateDate(){
		return createDate;
	}
	
	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}
	
	public String getBpmnData(){
		return bpmnData;
	}
	
	public void setBpmnData(String bpmnData){
		this.bpmnData = bpmnData;
	}
	
	public Long getFormId(){
		return formId;
	}
	
	public void setFormId(Long formId){
		this.formId = formId;
	}
	
	public Long getGroupId(){
		return groupId;
	}
	
	public void setGroupId(Long groupId){
		this.groupId = groupId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}