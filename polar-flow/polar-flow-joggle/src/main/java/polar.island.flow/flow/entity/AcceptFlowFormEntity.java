package polar.island.flow.flow.entity;

import polar.island.core.entity.BasicEntity;

import javax.validation.constraints.NotNull;
import java.util.List;

public class AcceptFlowFormEntity extends BasicEntity {
    @NotNull(message = "编号不能为空")
    private Long id;
    private List<FlowFormMatches> matches;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<FlowFormMatches> getMatches() {
        return matches;
    }

    public void setMatches(List<FlowFormMatches> matches) {
        this.matches = matches;
    }
}
