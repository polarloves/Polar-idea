package polar.island.flow.flow.service;

import polar.island.core.service.BasicService;
import polar.island.flow.flow.entity.FlowEntity;
import polar.island.flow.flow.entity.FlowFormMatches;

import java.util.List;
import java.util.Map;

/**
 * 流程定义表的服务类，其实现类标签为：flowService,删除模式为：仅物理删除。
 *
 * @author PolarLoves
 */
public interface FlowService extends BasicService<FlowEntity, FlowEntity> {
    /**
     * 由于仅有物理删除，此删除方法被移除
     */
    @Deprecated
    public Long deleteByIdLogic(String id);

    /**
     * 由于仅有物理删除，此删除方法被移除
     */
    @Deprecated
    public Long deleteByConditionLogic(Map<String, Object> condition);

    public Long updateBpmnData(String id, String bpmnData);


    public List<FlowFormMatches> flowMatches(String flowId);

    public void upDateMatches(String id, List<FlowFormMatches> matches);

    public void publish(String id);


}