package polar.island.flow.business.expection;

import polar.island.core.config.Constants;
import polar.island.core.exception.FrameWorkException;

public class BussinessException extends FrameWorkException {
    public BussinessException(String message, Exception causeMessage) {
        super(Constants.CODE_FLOW, message, causeMessage,false);
    }

    public BussinessException(String message, Exception causeMessage, boolean log) {
        super(Constants.CODE_FLOW, message, causeMessage, log);
    }
}
