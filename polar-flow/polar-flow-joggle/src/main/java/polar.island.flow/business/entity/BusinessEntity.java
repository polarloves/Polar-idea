package polar.island.flow.business.entity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.ibatis.type.Alias;
import polar.island.core.entity.BasicEntity;
import polar.island.flow.form.entity.FormEntity;

import java.util.Date;
import java.util.List;

/**
 *  业务流程
 *
 * @author  PolarLoves
 *
 */
@Alias(value = "BusinessEntity")
@ApiModel(value = "polar.island.flow.business.entity.BusinessEntity", description = "业务流程")
public class BusinessEntity extends BasicEntity {
	/** 主键编号	**/
	@ApiModelProperty(value = "数据编号",example = "1")
	private Long id;
	/** 流程标题 **/
	@ApiModelProperty(value = "流程标题")
	private String title;
	/** 创建时间 **/
	@ApiModelProperty(value = "创建时间")
	private Date createDate;
	/** 创建人 **/
	@ApiModelProperty(value = "创建人")
	private String createUser;
	/** 流程编号 **/
	@ApiModelProperty(value = "activiti中发布后的编号")
	private Long flowId;
	/** activiti的processInstanceId **/
	@ApiModelProperty(value = "activiti的processInstanceId")
	private String processId;
	/** 当前状态（0-进行中，1-已完成） **/
	@ApiModelProperty(value = "当前状态（0-进行中，1-已完成）")
	private Long state;
	/** 展示的json **/
	@ApiModelProperty(value = "展示的json")
	private String displayJson;
	@ApiModelProperty(value = "真实数据的json")
	private String realJson;
	/** 当前操作的对象 **/
	@ApiModelProperty(value = "当前操作的对象")
	private String currentOperate;
	/** 当前的表单 **/
    @ApiModelProperty(value = "当前表单",reference = "polar.island.flow.form.entity.FormEntity")
	private FormEntity form;
	@ApiModelProperty(value = "创建人昵称")
	private String createUserNickName;



    public String getCreateUserNickName() {
		return createUserNickName;
	}

	public void setCreateUserNickName(String createUserNickName) {
		this.createUserNickName = createUserNickName;
	}

	public String getRealJson() {
		return realJson;
	}

	public void setRealJson(String realJson) {
		this.realJson = realJson;
	}

	public Long getId(){
		return id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public String getTitle(){
		return title;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public Date getCreateDate(){
		return createDate;
	}

	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}

	public String getCreateUser(){
		return createUser;
	}

	public void setCreateUser(String createUser){
		this.createUser = createUser;
	}

	public Long getFlowId(){
		return flowId;
	}

	public void setFlowId(Long flowId){
		this.flowId = flowId;
	}

	public String getProcessId(){
		return processId;
	}

	public void setProcessId(String processId){
		this.processId = processId;
	}

	public Long getState(){
		return state;
	}

	public void setState(Long state){
		this.state = state;
	}

	public String getDisplayJson(){
		return displayJson;
	}

	public void setDisplayJson(String displayJson){
		this.displayJson = displayJson;
	}

	public String getCurrentOperate() {
		return currentOperate;
	}

	public void setCurrentOperate(String currentOperate) {
		this.currentOperate = currentOperate;
	}

	public FormEntity getForm() {
		return form;
	}

	public void setForm(FormEntity form) {
		this.form = form;
	}
}