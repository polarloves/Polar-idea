package polar.island.flow.business.service;

import polar.island.core.service.BasicService;
import polar.island.flow.business.entity.BusinessEntity;
import polar.island.flow.flow.entity.FlowEntity;
import polar.island.flow.form.entity.FormEntity;

import java.util.List;
import java.util.Map;

/**
 * 业务流程的服务类，其实现类标签为：businessService,删除模式为：仅物理删除。
 *
 * @author PolarLoves
 */
public interface BusinessService extends BasicService<BusinessEntity, BusinessEntity> {
    /**
     * 由于仅有物理删除，此删除方法被移除
     */
    @Deprecated
    public Long deleteByIdLogic(String id);

    /**
     * 由于仅有物理删除，此删除方法被移除
     */
    @Deprecated
    public Long deleteByConditionLogic(Map<String, Object> condition);

    /**
     * 启动业务
     *
     * @param businessEntity 当前的业务实体
     * @param flowEntity     流程定义的实体
     * @param formEntity     表单数据
     * @param columnDatas    列数据
     */
    public void start(BusinessEntity businessEntity, FlowEntity flowEntity, FormEntity formEntity, List<Map<String, Object>> columnDatas);

    /**
     * 取消任务
     *
     * @param entity 当前的业务实体
     * @param userId 处理人
     */
    public void cancel(BusinessEntity entity, String userId);

    /**
     * 获取业务历史记录
     *
     * @param id 业务编号
     * @return 业务历史记录
     */
    public List<Map<String, Object>> findHistory(String id);

    /**
     * 完成任务
     *
     * @param businessEntity 当前的业务实体
     * @param userId         处理人
     * @param columnDatas    处理任务的数据
     * @param displayJson    处理任务用来展示的数据
     */
    public void complete(BusinessEntity businessEntity, String userId, List<Map<String, Object>> columnDatas, String displayJson);

    /**
     * 获取当前正在进行的表单
     *
     * @param businessEntity 当前的业务实体
     * @return 当前正在进行的表单
     */
    public FormEntity findCurrentFormEntity(BusinessEntity businessEntity);

    /**
     * 查询待办列表，其中currentUser不能为空
     *
     * @param condition 查询条件。
     * @return 待办列表
     */
    public List<BusinessEntity> selectToDoPageList(Map<String, Object> condition);

    /**
     * 查询待办数量，其中currentUser不能为空
     *
     * @param condition 查询条件。
     * @return 待办数量
     */
    public Long selectToDoCount(Map<String, Object> condition);

    /**
     * 查询已办列表，其中currentUser不能为空
     *
     * @param condition 查询条件。
     * @return 待办列表
     */
    public List<BusinessEntity> selectDonePageList(Map<String, Object> condition);

    /**
     * 查询已办数量，其中currentUser不能为空
     *
     * @param condition 查询条件。
     * @return 待办数量
     */
    public Long selectDoneCount(Map<String, Object> condition);
}