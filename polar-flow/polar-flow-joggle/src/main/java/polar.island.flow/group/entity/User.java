package polar.island.flow.group.entity;

import org.apache.ibatis.type.Alias;
import polar.island.core.entity.BasicEntity;
@Alias(value = "GroupUser")
public class User extends BasicEntity {
    private String userId;
    private String userName;
    private String nickName;
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
}
