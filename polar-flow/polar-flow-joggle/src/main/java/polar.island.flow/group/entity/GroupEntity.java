package polar.island.flow.group.entity;

import org.apache.ibatis.type.Alias;
import polar.island.core.entity.BasicEntity;
import polar.island.core.validator.TAG;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 *  操作组
 *
 * @author  PolarLoves
 *
 */
@Alias(value = "GroupEntity")
public class GroupEntity extends BasicEntity {
	/** 主键编号	**/
	@TAG(value = { "updateAllById" , "updateField" })
	@NotNull(message = "编号不能为空")
	private Long id;
	/** 分组名称 **/
	@TAG(value = { "updateAllById", "add" })
	@NotNull(message = "分组名称不能为空")
	private String groupName;
	/** 组描述 **/
	private String groupDecribe;
	private List<User> users;

	public Long getId(){
		return id;
	}
	
	public void setId(Long id){
		this.id = id;
	}
	
	public String getGroupName(){
		return groupName;
	}
	
	public void setGroupName(String groupName){
		this.groupName = groupName;
	}
	
	public String getGroupDecribe(){
		return groupDecribe;
	}
	
	public void setGroupDecribe(String groupDecribe){
		this.groupDecribe = groupDecribe;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}
}