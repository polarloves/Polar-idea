package polar.island.flow.group.service;

import polar.island.core.entity.DictEntity;
import polar.island.core.service.BasicService;

import java.util.List;
import java.util.Map;

/**
 * 操作组的服务类，其实现类标签为：groupService,删除模式为：仅物理删除。
 *
 * @author PolarLoves
 */
public interface GroupService extends BasicService<Map<String, Object>, Map<String, Object>> {
    /**
     * 由于仅有物理删除，此删除方法被移除
     */
    @Deprecated
    public Long deleteByIdLogic(String id);

    /**
     * 由于仅有物理删除，此删除方法被移除
     */
    @Deprecated
    public Long deleteByConditionLogic(Map<String, Object> condition);


    public List<Map<String, Object>> selectUserPageList(Map<String, Object> condition);

    public Long selectUserCount(Map<String, Object> condition);


    public List<DictEntity> allGroups();

}