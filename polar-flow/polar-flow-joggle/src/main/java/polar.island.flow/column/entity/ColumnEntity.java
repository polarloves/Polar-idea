package polar.island.flow.column.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.ibatis.type.Alias;
import polar.island.core.entity.BasicEntity;
import polar.island.core.validator.TAG;

import javax.validation.constraints.NotNull;

/**
 * 表单列属性的实体类,流程表单的子表
 *
 * @author PolarLoves
 */
@Alias(value = "ColumnEntity")
@ApiModel(value = "polar.island.flow.column.entity.ColumnEntity", description = "表单列属性")
public class ColumnEntity extends BasicEntity {

    /**
     * 主键编号
     **/
    @ApiModelProperty(value = "数据编号",example = "1")
    @TAG(value = {"updateAllById", "updateField"})
    @NotNull(message = "编号不能为空")
    private Long id;
    /**
     * 列名
     **/
    @ApiModelProperty(value = "列名")
    @TAG(value = {"updateAllById", "add"})
    @NotNull(message = "列名不能为空")
    private String label;
    /**
     * 列类型
     **/
    @ApiModelProperty(value = "列类型")
    @TAG(value = {"updateAllById", "add"})
    @NotNull(message = "列类型不能为空")
    private Long type;
    /**
     * 扩展参数
     **/
    @ApiModelProperty(value = "扩展参数")
    private String extendsJson;
    /**
     * 校验方式
     **/
    @ApiModelProperty(value = "校验方式")
    private String validateType;
    /**
     * 提示语
     **/
    @ApiModelProperty(value = "提示语")
    private String placeholder;
    /**
     * 手机占比
     **/
    @ApiModelProperty(value = "手机占比")
    @TAG(value = {"updateAllById", "add"})
    @NotNull(message = "手机占比不能为空")
    private Long small;
    /**
     * 平板占比
     **/
    @ApiModelProperty(value = "平板占比")
    @TAG(value = {"updateAllById", "add"})
    @NotNull(message = "平板占比不能为空")
    private Long middle;
    /**
     * 电脑占比
     **/
    @ApiModelProperty(value = "超大屏占比")
    @TAG(value = {"updateAllById", "add"})
    @NotNull(message = "超大屏占比不能为空")
    private Long larger;
    /**
     * 电脑占比
     **/
    @ApiModelProperty(value = "电脑占比")
    @TAG(value = {"updateAllById", "add"})
    @NotNull(message = "电脑占比不能为空")
    private Long big;
    /**
     * 排序号
     **/
    @ApiModelProperty(value = "排序号")
    @TAG(value = {"updateAllById", "add"})
    @NotNull(message = "排序号不能为空")
    private Long orderNum;
    /**
     * 表单编号
     **/
    @ApiModelProperty(value = "表单编号")
    @TAG(value = {"updateAllById", "add"})
    @NotNull(message = "表单编号不能为空")
    private Long formId;
    @ApiModelProperty(value = "键")
    @TAG(value = {"updateAllById", "add"})
    @NotNull(message = "键不能为空")
    private String key;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public String getExtendsJson() {
        return extendsJson;
    }

    public void setExtendsJson(String extendsJson) {
        this.extendsJson = extendsJson;
    }

    public String getValidateType() {
        return validateType;
    }

    public void setValidateType(String validateType) {
        this.validateType = validateType;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    public Long getSmall() {
        return small;
    }

    public void setSmall(Long small) {
        this.small = small;
    }

    public Long getMiddle() {
        return middle;
    }

    public void setMiddle(Long middle) {
        this.middle = middle;
    }

    public Long getBig() {
        return big;
    }

    public void setBig(Long big) {
        this.big = big;
    }

    public Long getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Long orderNum) {
        this.orderNum = orderNum;
    }

    public Long getFormId() {
        return formId;
    }

    public void setFormId(Long formId) {
        this.formId = formId;
    }


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Long getLarger() {
        return larger;
    }

    public void setLarger(Long larger) {
        this.larger = larger;
    }
}