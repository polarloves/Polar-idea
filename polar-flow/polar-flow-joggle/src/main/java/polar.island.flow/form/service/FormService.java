package polar.island.flow.form.service;
import polar.island.core.entity.DictEntity;
import polar.island.core.service.BasicService;
import polar.island.flow.form.entity.FormEntity;

import java.util.List;
import java.util.Map;
/**
 * 流程表单的服务类，其实现类标签为：formService,删除模式为：仅物理删除。
 *
 * @author  PolarLoves
 *
 */
public interface FormService extends BasicService<FormEntity,FormEntity> {
	/**
	 * 由于仅有物理删除，此删除方法被移除
	 */
	@Deprecated
	public Long deleteByIdLogic(String id);
	/**
	 * 由于仅有物理删除，此删除方法被移除
	 */
	@Deprecated
	public Long deleteByConditionLogic(Map<String, Object> condition);


	public List<DictEntity> allForms();
}