package polar.island.flow.form.entity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.ibatis.type.Alias;
import polar.island.core.entity.BasicEntity;
import polar.island.core.validator.TAG;
import polar.island.flow.column.entity.ColumnEntity;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;
/**
 *   流程表单
 *
 * @author  PolarLoves
 *
 */
@Alias(value = "FormEntity")
@ApiModel(value = "polar.island.flow.form.entity.FormEntity", description = "流程表单")
public class FormEntity extends BasicEntity {
	/** 主键编号	**/
	@TAG(value = { "updateAllById" , "updateField" })
	@NotNull(message = "编号不能为空")
	@ApiModelProperty(value = "数据编号",example = "1")
	private Long id;
	/** 表单名称 **/
	@TAG(value = { "updateAllById", "add" })
	@NotNull(message = "表单名称不能为空")
	@ApiModelProperty(value = "表单名称")
	private String formName;
	/** 表单类型(0-流程表单，1-操作表单) **/
	@ApiModelProperty(value = "表单类型(0-流程表单，1-操作表单)")
	private Integer formType;
	/** 表单宽度 **/
	@ApiModelProperty(value = "表单宽度")
	@TAG(value = { "updateAllById", "add" })
	@NotNull(message = "表单宽度不能为空")
	private String formWidth;
	/** 表单高度 **/
	@ApiModelProperty(value = "表单高度")
	@TAG(value = { "updateAllById", "add" })
	@NotNull(message = "表单高度不能为空")
	private String formHeight;
	/** 表单标题 **/
	@ApiModelProperty(value = "表单标题")
	@TAG(value = { "updateAllById", "add" })
	@NotNull(message = "表单标题不能为空")
	private String formTitle;
	/** 功能名称 **/
	@ApiModelProperty(value = "功能名称")
	@TAG(value = { "updateAllById", "add" })
	@NotNull(message = "功能名称不能为空")
	private String functionName;
	/** 操作按钮名称 **/
	@ApiModelProperty(value = "操作按钮名称")
	@TAG(value = { "updateAllById", "add" })
	@NotNull(message = "操作按钮名称不能为空")
	private String buttonName;
	/** 表单描述 **/
	@ApiModelProperty(value = "表单描述")
	private String formDescribe;
	/** 创建人 **/
	@ApiModelProperty(value = "创建人")
	private String createUser;
	/** 创建时间 **/
	@ApiModelProperty(value = "创建时间")
	private Date createDate;
	//column列表数据
	private List<ColumnEntity> columnEntity;


	public Long getId(){
		return id;
	}

	public void setId(Long id){
		this.id = id;
	}

	public String getFormName(){
		return formName;
	}

	public void setFormName(String formName){
		this.formName = formName;
	}

	public Integer getFormType(){
		return formType;
	}

	public void setFormType(Integer formType){
		this.formType = formType;
	}

	public String getFormWidth(){
		return formWidth;
	}

	public void setFormWidth(String formWidth){
		this.formWidth = formWidth;
	}

	public String getFormHeight(){
		return formHeight;
	}

	public void setFormHeight(String formHeight){
		this.formHeight = formHeight;
	}

	public String getFormTitle(){
		return formTitle;
	}

	public void setFormTitle(String formTitle){
		this.formTitle = formTitle;
	}

	public String getFunctionName(){
		return functionName;
	}

	public void setFunctionName(String functionName){
		this.functionName = functionName;
	}

	public String getButtonName(){
		return buttonName;
	}

	public void setButtonName(String buttonName){
		this.buttonName = buttonName;
	}

	public String getFormDescribe(){
		return formDescribe;
	}

	public void setFormDescribe(String formDescribe){
		this.formDescribe = formDescribe;
	}

	public String getCreateUser(){
		return createUser;
	}

	public void setCreateUser(String createUser){
		this.createUser = createUser;
	}

	public Date getCreateDate(){
		return createDate;
	}

	public void setCreateDate(Date createDate){
		this.createDate = createDate;
	}

	public List<ColumnEntity> getColumnEntity(){
		return columnEntity;
	}

	public void setColumnEntity(List<ColumnEntity> columnEntity){
		this.columnEntity=columnEntity;
	}
}