package polar.island.inlay.menuModel.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import polar.island.core.util.ExceptionUtil;
import polar.island.inlay.menu.dao.MenuDao;
import polar.island.inlay.menuModel.dao.MenuModelDao;
import polar.island.inlay.menuModel.entity.MenuModelEntity;
import polar.island.mybatis.service.MybatisService;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(value = "menuModelService")
public class MenuModelServiceImpl extends MybatisService<MenuModelEntity, MenuModelEntity, MenuModelDao> implements MenuModelService {
    @Resource(name = "menuModelDao")
    private MenuModelDao menuModelDao;
    @Resource(name = "menuDao")
    private MenuDao menuDao;

    @Override
    public MenuModelDao getDao() {
        return menuModelDao;
    }
    @Transactional
    @Override
    public Object insert(Map<String, Object> condition) {
        try {
            menuModelDao.insert(condition);
            return condition.get("id");
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Transactional(readOnly = false)
    @Override
    public void setDefault(String id) {
        try {
            menuModelDao.cleanDefault();
            Map<String, Object> condition = new HashMap<String, Object>();
            condition.put("id", id);
            condition.put("defaultMenu", 1);
            menuModelDao.updateField(condition);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Transactional(readOnly = false)
    public Long deleteByIdLogic(String id) {
        try {    //删除用户-菜单
            menuModelDao.deleteUserMenuByMenuModelId(id);
            //删除模板-菜单
            menuDao.deleteModelMenuByModelId(id);
            return super.deleteByIdLogic(id);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Override
    public List<MenuModelEntity> userMenuModels(String userId) {
        try {
            return menuModelDao.userMenuModels(userId);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Transactional(readOnly = false)
    @Override
    public void updateUserMenuModels(String userId, String menuModelId) {
        try {
            menuModelDao.deleteUserMenuByUserId(userId);
            if (menuModelId != null && !menuModelId.equals("")) {
                Map<String, Object> condition = new HashMap<String, Object>();
                condition.put("userId", userId);
                condition.put("menuModelId", menuModelId);
                menuModelDao.insertUserMenu(condition);
            }
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }
}