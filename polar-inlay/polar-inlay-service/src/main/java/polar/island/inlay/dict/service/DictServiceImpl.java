package polar.island.inlay.dict.service;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import polar.island.core.util.ExceptionUtil;
import polar.island.inlay.dict.dao.DictDao;
import polar.island.inlay.dict.entity.DictEntity;
import polar.island.mybatis.service.MybatisService;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service ( value = "dictService" )
public class DictServiceImpl extends MybatisService <DictEntity, DictEntity, DictDao> implements DictService {
    @Resource ( name = "dictDao" )
    private DictDao dictDao;

    @Override
    public DictDao getDao () {
        return dictDao;
    }

    @Transactional
    @CacheEvict ( value = "dictCache", key = "#condition['groupId']" )
    @Override
    public Object insert ( Map <String, Object> condition ) {
        dictDao.insert ( condition );
        return condition.get ( "id" );
    }

    @CacheEvict ( value = "dictCache", allEntries = true )
    @Override
    public Long updateAll ( Map <String, Object> condition ) {
        try {
            return super.updateAll ( condition );
        } catch (Exception e) {
            throw ExceptionUtil.assembleException ( e );
        }
    }

    @CacheEvict ( value = "dictCache", allEntries = true )
    @Override
    public Long deleteByIdPhysical ( String id ) {
        try {
            return super.deleteByIdPhysical ( id );
        } catch (Exception e) {
            throw ExceptionUtil.assembleException ( e );
        }
    }

    @CacheEvict ( value = "dictCache", allEntries = true )
    @Override
    public Long updateField ( Map <String, Object> condition ) {
        try {
            return super.updateField ( condition );
        } catch (
                Exception e) {
            throw ExceptionUtil.assembleException ( e );
        }
    }

    @CacheEvict ( value = "dictCache", allEntries = true )
    @Override
    public Long deleteMulitByIdPhysical ( String[] ids ) {
        try {
            return super.deleteMulitByIdPhysical ( ids );
        } catch (Exception e) {
            throw ExceptionUtil.assembleException ( e );
        }
    }

    @Override
    public List <polar.island.core.entity.DictEntity> allGroups () {
        try {
            return dictDao.allGroups ( );
        } catch (
                Exception e) {
            throw ExceptionUtil.assembleException ( e );
        }

    }

    @Cacheable ( value = "dictCache", key = "#id" )
    @Override
    public List <polar.island.core.entity.DictEntity> findDictById ( String id ) {
        try {
            return dictDao.findDictById ( id );
        } catch (Exception e) {
            throw ExceptionUtil.assembleException ( e );
        }
    }
}