package polar.island.inlay.menu.service;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import polar.island.core.entity.TreeEntity;
import polar.island.core.util.ExceptionUtil;
import polar.island.inlay.menu.dao.MenuDao;
import polar.island.inlay.menu.entity.MenuEntity;
import polar.island.mybatis.service.MybatisService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(value = "menuService")
public class MenuServiceImpl extends MybatisService<MenuEntity, MenuEntity, MenuDao> implements MenuService {
    @Resource(name = "menuDao")
    private MenuDao menuDao;

    @Override
    public MenuDao getDao() {
        return menuDao;
    }
    @Transactional
    @Override
    public Object insert(Map<String, Object> condition) {
        try {
            menuDao.insert(condition);
            return condition.get("id");
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Override
    public List<TreeEntity> allMenus() {
        try {
            return menuDao.allMenus();
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Override
    public List<MenuEntity> modelMenus(String menuModelId) {
        try {
            return menuDao.modelMenus(menuModelId);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Override
    public Long deleteByIdPhysical(String id) {
        try {
            return deleteMenu(id, menuDao.allMenus(), 0l);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    private Long deleteMenu(String id, List<TreeEntity> all, Long count) {
        count = count + menuDao.deleteByIdPhysical(id);
        menuDao.deleteModelMenuByMenuId(id);
        if (all != null && all.size() > 0) {
            for (TreeEntity entity : all) {
                if (id.equals(entity.getParentId() + "")) {
                    count = deleteMenu(entity.getId(), all, count);
                }
            }
        }
        return count;
    }

    @Transactional(readOnly = false)
    @Override
    public void updateModelMenus(String modelId, String[] menuId) {
        try {
            menuDao.deleteModelMenuByModelId(modelId);
            if (menuId != null && menuId.length > 0) {
                for (String id : menuId) {
                    Map<String, Object> condition = new HashMap<String, Object>();
                    condition.put("menuModelId", modelId);
                    condition.put("menuId", id);
                    menuDao.insertModelMenu(condition);
                }
            }
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Override
    public Long maxOrderNum() {
        try {
            return menuDao.maxOrderNum();
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Cacheable(value = "menuCache", key = "#userId")
    @Override
    public List<MenuEntity> userMenus(String userId) {
        try {
            List<MenuEntity> result = menuDao.userMenus(userId);
            if (result == null || result.size() == 0) {
                // 使用默认菜单
                result = menuDao.defaultMenus();
            }
            List<MenuEntity> temp = new ArrayList<MenuEntity>();
            if (result != null && result.size() > 0) {
                for (MenuEntity entity : result) {
                    if (entity.getParentId() == null) {
                        temp.add(entity);
                    }
                }
            }
            result.removeAll(temp);
            for (MenuEntity entity : temp) {
                addClidren(entity, result);
            }
            return temp;
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    private void addClidren(MenuEntity current, List<MenuEntity> datas) {
        Long parentId = current.getId();
        if (current.getChildren() == null) {
            current.setChildren(new ArrayList<MenuEntity>());
        }
        for (MenuEntity data : datas) {
            if (parentId.equals(data.getParentId())) {
                current.getChildren().add(data);
            }
        }
        //移除不需要的集合，提高效率
        datas.removeAll(current.getChildren());
        for (MenuEntity data : current.getChildren()) {
            addClidren(data, datas);
        }
    }

    @CacheEvict(value = "menuCache", key = "#userId")
    @Override
    public void clearUserMenusCache(String userId) {

    }

    @CacheEvict(value = "menuCache", allEntries = true)
    @Override
    public void clearAllMenusCache() {

    }
}