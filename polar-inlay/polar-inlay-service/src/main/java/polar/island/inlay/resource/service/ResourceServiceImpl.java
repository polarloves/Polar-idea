package polar.island.inlay.resource.service;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import polar.island.core.config.Constants;
import polar.island.core.entity.NameValueEntity;
import polar.island.core.exception.FrameWorkException;
import polar.island.core.util.ExceptionUtil;
import polar.island.inlay.permission.dao.PermissionDao;
import polar.island.inlay.resource.dao.ResourceDao;
import polar.island.inlay.resource.entity.ResourceEntity;
import polar.island.mybatis.service.MybatisService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service(value = "resourceService")
public class ResourceServiceImpl extends MybatisService<ResourceEntity, ResourceEntity, ResourceDao>
        implements ResourceService {
    @Resource(name = "resourceDao")
    private ResourceDao resourceDao;
    @Resource(name = "permissionDao")
    private PermissionDao permissionDao;

    @Override
    public ResourceDao getDao() {
        return resourceDao;
    }
    @Transactional
    @Override
    public Object insert(Map<String, Object> condition) {
        try {
            try {
                resourceDao.insert(condition);
            } catch (org.springframework.dao.DuplicateKeyException e) {
                throw new FrameWorkException(Constants.CODE_SERVER_COMMON, "访问路径为\"" + condition.get("path") + "\"的资源已存在",
                        null, false);
            }
            return condition.get("id");
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Override
    public Long deleteByIdPhysical(String id) {
        try {
            permissionDao.deleteResourcePermissionByResourceId(id);
            return super.deleteByIdPhysical(id);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Override
    public Map<String, List<String>> resourcePermissions() {
        try {
            Map<String, List<String>> result =  new ConcurrentHashMap<String, List<String>> ();
            List<NameValueEntity> resources = resourceDao.allResourcesAndPermissions();
            if (!CollectionUtils.isEmpty(resources)) {
                for (NameValueEntity entity : resources) {
                    String path = entity.getName();
                    List<String> permission;
                    if (result.containsKey(path)) {
                        permission = result.get(path);
                    } else {
                        permission = new ArrayList<String>();
                        result.put(path, permission);
                    }
                    permission.add(entity.getValue());
                }
            }
            return result;
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }
}