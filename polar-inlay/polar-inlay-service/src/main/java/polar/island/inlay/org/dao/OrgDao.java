package polar.island.inlay.org.dao;
import org.apache.ibatis.annotations.Param;
import polar.island.core.dao.BasicDao;
import polar.island.inlay.org.entity.OrgEntity;
import polar.island.mybatis.annotations.MybatisStore;
import polar.island.mybatis.common.MybatisDao;

import java.util.List;
import java.util.Map;
/**
 * 机构的持久化层。
 * 
 * @author PolarLoves
 *
 */
@MybatisStore (value="orgDao")
public interface OrgDao extends MybatisDao<OrgEntity,OrgEntity> {
	/**
	 * 由于仅有物理删除，此删除方法被移除
	 */
	@Deprecated
	public Long deleteByIdLogic(@Param(value = "id") String id);
	/**
	 * 由于仅有物理删除，此删除方法被移除
	 */
	@Deprecated
	public Long deleteByConditionLogic(Map<String, Object> condition);
	/**
	 * 查询所有的数据,并且按照parentId进行升序排序
     *
     * @return 所有的数据
	 */
	public List<OrgEntity> selectAllList();
	/**
	 * 依据部门编号删除部门的所有权限信息
	 *
	 * @param orgId 部门编号
	 * @return 删除结果
	 */
	public Long deleteOrgPermissionByOrgId(@Param(value = "orgId") String orgId);

}
