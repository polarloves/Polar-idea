package polar.island.inlay.records.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import polar.island.core.util.ExceptionUtil;
import polar.island.inlay.records.dao.RecordsDao;
import polar.island.inlay.records.entity.RecordsEntity;
import polar.island.mybatis.service.MybatisService;

import javax.annotation.Resource;
import java.util.Map;

@Service(value = "recordsService")
public class RecordsServiceImpl extends MybatisService<RecordsEntity, RecordsEntity, RecordsDao> implements RecordsService {
    @Resource(name = "recordsDao")
    private RecordsDao recordsDao;

    @Override
    public RecordsDao getDao() {
        return recordsDao;
    }

    @Transactional(readOnly = false)
    @Override
    public Object insert(Map<String, Object> condition) {
        try {
            recordsDao.insert(condition);
            return condition.get("id");
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }
}