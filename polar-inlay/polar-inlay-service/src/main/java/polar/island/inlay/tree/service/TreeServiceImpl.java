package polar.island.inlay.tree.service;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import polar.island.core.entity.DictEntity;
import polar.island.core.util.ExceptionUtil;
import polar.island.inlay.tree.dao.TreeDao;
import polar.island.inlay.tree.entity.TreeEntity;
import polar.island.mybatis.service.MybatisService;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service(value = "treeService")
public class TreeServiceImpl extends MybatisService<TreeEntity, TreeEntity, TreeDao> implements TreeService {
    @Resource(name = "treeDao")
    private TreeDao treeDao;

    @Override
    public TreeDao getDao() {
        return treeDao;
    }
    @Transactional
    @CacheEvict(value = "treeCache", key = "#condition['groupId']")
    @Override
    public Object insert(Map<String, Object> condition) {
        try {
            treeDao.insert(condition);
            return condition.get("id");
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @CacheEvict(value = "treeCache", key = "#condition['groupId']")
    @Override
    public Long updateAll(Map<String, Object> condition) {
        try {
            return super.updateAll(condition);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @CacheEvict(value = "treeCache", allEntries = true)
    @Override
    public Long deleteByIdPhysical(String id) {
        try {
            return super.deleteByIdPhysical(id);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @CacheEvict(value = "treeCache", allEntries = true)
    @Override
    public Long updateField(Map<String, Object> condition) {
        try {
            return super.updateField(condition);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @CacheEvict(value = "treeCache", allEntries = true)
    @Override
    public Long deleteMulitByIdPhysical(String[] ids) {
        try {
            return super.deleteMulitByIdPhysical(ids);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Override
    public List<DictEntity> allGroups() {
        try {
            return treeDao.allGroups();
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Cacheable(value = "treeCache", key = "#groupId")
    @Override
    public List<TreeEntity> getTree(String groupId) {
        try {
            return treeDao.getTree(groupId);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }
}