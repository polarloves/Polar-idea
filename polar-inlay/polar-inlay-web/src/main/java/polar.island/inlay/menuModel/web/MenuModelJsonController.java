package polar.island.inlay.menuModel.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import polar.island.core.config.Constants;
import polar.island.web.controller.BasicController;
import polar.island.core.entity.ResponseJson;
import polar.island.core.res.ResMsg;
import polar.island.core.res.ResType;
import polar.island.core.exception.ValidationException;
import polar.island.inlay.menuModel.entity.MenuModelEntity;
import polar.island.inlay.menuModel.service.MenuModelService;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Map;

/**
 * 菜单模板的接口，返回数据全部为json串。
 *
 * @author PolarLoves
 */
@Controller(value = "menuModelJsonController")
@RequestMapping(value = "/menuModel/json")
@Api(tags = "菜单模板接口", description = "菜单模板接口，其用于菜单模板的增删改查")
public class MenuModelJsonController extends BasicController {
    @Resource(name = "menuModelService")
    private MenuModelService menuModelService;
    private static final String MODULE_NAME = null;

    /**
     * 校验访问权限
     *
     * @return
     */
    @ApiOperation(value = "校验菜单模板权限", httpMethod = "POST",response = Void.class)
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:menumodel:empty"})
    @ResMsg (tag = "校验菜单模板权限", type = ResType.JSON,writeLogs = false)
    @RequestMapping(value = "/checkPermission", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson checkPermission() {
        return new ResponseJson(Constants.CODE_SUCCESS);
    }

    /**
     * 依据条件查询分页数据，如果不传页码、每页条目，则默认分别为：1,10
     *
     * @param entity 查询条件
     * @return 列表Json串。
     * @see polar.island.inlay.menuModel.entity.MenuModelEntity
     */
    @ApiOperation(value = "查询菜单模板分页列表", httpMethod = "POST",responseContainer = "List",response = MenuModelEntity.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query",name = "id",value = "编号",example = "1",dataType = "string"),
                    @ApiImplicitParam(paramType = "query",name = "name",value = "菜单模板名称"),
                    @ApiImplicitParam(paramType = "query",name = "info",value = "描述信息"),
                    @ApiImplicitParam(paramType = "query",name = "defaultMenu",value = "是否为默认模板",example = "1"),
                    @ApiImplicitParam(paramType = "query",name = "page",value = "当前页,如不传,则默认为1",example = "1"),
                    @ApiImplicitParam(paramType = "query",name = "rows",value = "每页的数据条目,如不传,则默认为10",example = "10"),
                    @ApiImplicitParam(paramType = "query",name = "sort",value = "排序字段",example = "groupName"),
                    @ApiImplicitParam(paramType = "query",name = "order",value = "排序方式，可选值有：ASC DESC,当sort字段传入值时,其必须传入",example = "DESC")
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:menumodel:view:list"})
    @ResMsg (tag = "查询菜单模板", type = ResType.JSON,writeLogs = false)
    @RequestMapping(value = "/pageList", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson pageList(@ApiIgnore MenuModelEntity entity) {
        Map<String, Object> condition = beanToMap(entity);
        return new ResponseJson(Constants.CODE_SUCCESS, menuModelService.selectPageList(condition), null,
                menuModelService.selectCount(condition));
    }
    @ApiOperation(value = "查看菜单模板详情", httpMethod = "POST",response = MenuModelEntity.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query",name = "id",value = "编号",example = "1",dataType = "string",required = true),
            }
    )
    @ResponseBody
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:menumodel:view:detail"})
    @ResMsg (tag = "查看菜单模板", type = ResType.JSON,writeLogs = false)
    @RequestMapping(value = "/detail", produces = "application/json;charset=utf-8")
    public ResponseJson detail(@ApiIgnore String id) {
        idCheck(id, MODULE_NAME);
        MenuModelEntity entity = menuModelService.selectOneById(id);
        existCheck(entity, MODULE_NAME);
        return new ResponseJson(Constants.CODE_SUCCESS, entity);
    }

    /**
     * 依据数据编号更新此条数据的所有值。
     *
     * @param entity 需要更新的实体
     * @return 更新条目
     * @see polar.island.inlay.menuModel.entity.MenuModelEntity
     */
    @ApiOperation(value = "更新菜单模板全部字段", httpMethod = "POST",response = int.class)
    @ApiImplicitParams(
            {

                    @ApiImplicitParam(paramType = "query",name = "id",value = "数据编号",required = true),
                    @ApiImplicitParam(paramType = "query",name = "name",value = "菜单模板名称",required = true),
                    @ApiImplicitParam(paramType = "query",name = "info",value = "描述信息",required = true),
                    @ApiImplicitParam(paramType = "query",name = "defaultMenu",value = "是否为默认模板",example = "1",required = true),

            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:menumodel:edit"})
    @ResMsg (tag = "更新菜单模板全部字段", type = ResType.JSON)
    @RequestMapping(value = "/updateAllById", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson update(@ApiIgnore @Valid MenuModelEntity entity, BindingResult bindingResult) {
        validate(MenuModelEntity.class, "updateAllById", bindingResult);
        Map<String, Object> condition = beanToMap(entity);
        Long result = menuModelService.updateAll(condition);
        return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功修改了" + result + "条数据。");
    }

    /**
     * 依据数据编号更新此条数据的所有值。
     *
     * @param entity 需要更新的实体
     * @return 更新条目
     * @see polar.island.inlay.menuModel.entity.MenuModelEntity
     */
    @ApiOperation(value = "更新字典单个字段", httpMethod = "POST",response = int.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query",name = "id",value = "数据编号",required = true),
                    @ApiImplicitParam(paramType = "query",name = "name",value = "菜单模板名称"),
                    @ApiImplicitParam(paramType = "query",name = "info",value = "描述信息"),
                    @ApiImplicitParam(paramType = "query",name = "defaultMenu",value = "是否为默认模板",example = "1",required = true),

            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:menumodel:edit"})
    @ResMsg (tag = "更新菜单模板单个字段", type = ResType.JSON)
    @RequestMapping(value = "/updateField", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson updateField(@ApiIgnore @Valid MenuModelEntity entity, BindingResult bindingResult) {
        validate(MenuModelEntity.class, "updateField", bindingResult);
        Map<String, Object> condition = beanToSingleField(entity);
        condition.put("id", entity.getId());
        Long result = menuModelService.updateField(condition);
        return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功修改了" + result + "条数据。");
    }

    /**
     * 根据数据编号物理删除数据。
     *
     * @param ids 数据编号
     * @return 删除结果。
     */
    @ApiOperation(value = "删除多条数据", httpMethod = "POST",response = int.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query",name = "ids",value = "数据编号,其为一组编号，传入时，以ids[]为key传入，如删除id为1和2的数据时，传入：id[]=1&id[]=2",required = true)
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:menumodel:delete"})
    @ResMsg (tag = "删除多个菜单模板", type = ResType.JSON)
    @RequestMapping(value = "/deleteMulitById", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson deleteMulitById(@ApiIgnore @RequestParam(value = "ids[]", required = false) String[] ids) {
        idCheck(ids, MODULE_NAME);
        Long result = menuModelService.deleteMulitByIdPhysical(ids);
        return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功删除了" + result + "条数据。");
    }

    /**
     * 根据数据编号物理删除数据。
     *
     * @param id 数据编号
     * @return 删除结果。
     */
    @ApiOperation(value = "删除一条数据", httpMethod = "POST",response = int.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query",name = "id",value = "数据编号",required = true)
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:menumodel:delete"})
    @ResMsg (tag = "删除一个菜单模板", type = ResType.JSON)
    @RequestMapping(value = "/deleteById", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson deleteById(@ApiIgnore String id) {
        idCheck(id, MODULE_NAME);
        Long result = menuModelService.deleteByIdPhysical(id);
        return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功删除了" + result + "条数据。");
    }

    /**
     * 新增一条数据。
     *
     * @param entity 数据
     * @return 新增结果。
     * @see polar.island.inlay.menuModel.entity.MenuModelEntity
     */
    @ApiOperation(value = "新增菜单模板", httpMethod = "POST",response = int.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query",name = "name",value = "菜单模板名称",required = true),
                    @ApiImplicitParam(paramType = "query",name = "info",value = "描述信息",required = true),
                    @ApiImplicitParam(paramType = "query",name = "defaultMenu",value = "是否为默认模板",example = "1",required = true),

            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:menumodel:add"})
    @ResMsg (tag = "新增菜单模板", type = ResType.JSON)
    @RequestMapping(value = "/add", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson add(@ApiIgnore @Valid MenuModelEntity entity, BindingResult bindingResult) {
        validate(MenuModelEntity.class, "add", bindingResult);
        Map<String, Object> condition = beanToMap(entity);
        condition.put("defaultMenu", 0L);
        Object result = menuModelService.insert(condition);
        return new ResponseJson(Constants.CODE_SUCCESS, result, "新增成功。");
    }
    @ApiOperation(value = "设置默认模板", httpMethod = "POST",response = int.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query",name = "id",value = "数据编号",required = true)
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:menumodel:default"})
    @ResMsg (tag = "设置默认模板", type = ResType.JSON)
    @RequestMapping(value = "/setDefault", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson setDefault(@ApiIgnore String id) {
        if (id == null || id.equals("")) {
            throw new ValidationException("数据编号不能为空", null);
        }
        menuModelService.setDefault(id);
        return new ResponseJson(Constants.CODE_SUCCESS, null, "设置默认模板成功");
    }
}
