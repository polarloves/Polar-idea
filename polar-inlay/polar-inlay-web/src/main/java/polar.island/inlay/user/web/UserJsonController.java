package polar.island.inlay.user.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import polar.island.core.config.Constants;
import polar.island.core.entity.ResponseJson;
import polar.island.core.res.ResMsg;
import polar.island.core.res.ResType;
import polar.island.core.exception.FrameWorkException;
import polar.island.core.exception.ValidationException;
import polar.island.core.security.entity.UserEntity;
import polar.island.core.security.service.SecurityService;
import polar.island.core.util.CommonUtil;
import polar.island.inlay.menu.service.MenuService;
import polar.island.inlay.menuModel.service.MenuModelService;
import polar.island.inlay.role.service.RoleService;

import polar.island.inlay.user.service.UserService;
import polar.island.shiro.encry.EncryManager;
import polar.island.shiro.util.UserUtil;
import polar.island.web.controller.BasicController;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Date;
import java.util.Map;

/**
 * 用户的接口，返回数据全部为json串。
 *
 * @author PolarLoves
 */
@Api(tags = "用户的接口", description = "用户的接口，返回数据全部为json串")
@Controller(value = "userJsonController")
@RequestMapping(value = "/user/json")
public class UserJsonController extends BasicController {
    @Resource(name = "userService")
    private UserService userService;
    @Resource(name = "encryManager")
    private EncryManager encryManager;
    @Resource(name = "roleService")
    private RoleService roleService;
    @Resource(name = "menuModelService")
    private MenuModelService menuModelService;
    @Resource(name = "menuService")
    private MenuService menuService;
    @Resource(name = "shiroSecurityService")
    private SecurityService securityService;

    private static final String MODULE_NAME = "用户";

    /**
     * 依据条件查询分页数据，如果不传页码、每页条目，则默认分别为：1,10
     *
     * @param entity 查询条件
     * @return 列表Json串。
     * @see polar.island.core.security.entity.UserEntity
     */
    @ApiOperation(value = "查询用户分页列表", httpMethod = "POST", responseContainer = "List", response = UserEntity.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query", name = "id", value = "数据编号", required = true),
                    @ApiImplicitParam(paramType = "query", name = "userName", value = "用户名称", required = true),
                    @ApiImplicitParam(paramType = "query", name = "headUrl", value = "头像"),
                    @ApiImplicitParam(paramType = "query", name = "nickName", value = "昵称"),
                    @ApiImplicitParam(paramType = "query", name = "phone", value = "手机号"),
                    @ApiImplicitParam(paramType = "query", name = "email", value = "邮箱"),
                    @ApiImplicitParam(paramType = "query", name = "org_id", value = "所属机构"),
                    @ApiImplicitParam(paramType = "query", name = "state", value = "用户状态"),
                    @ApiImplicitParam(paramType = "query", name = "page", value = "当前页,如不传,则默认为1", example = "1"),
                    @ApiImplicitParam(paramType = "query", name = "rows", value = "每页的数据条目,如不传,则默认为10", example = "10"),
                    @ApiImplicitParam(paramType = "query", name = "sort", value = "排序字段", example = "groupName"),
                    @ApiImplicitParam(paramType = "query", name = "order", value = "排序方式，可选值有：ASC DESC,当sort字段传入值时,其必须传入", example = "DESC")
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:user:view:list"})
    @ResMsg (tag = "查询用户", type = ResType.JSON, writeLogs = false)
    @RequestMapping(value = "/pageList", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson pageList(@ApiIgnore UserEntity entity) {
        Map<String, Object> condition = beanToMap(entity);
        return new ResponseJson(Constants.CODE_SUCCESS, userService.selectPageList(condition), null,
                userService.selectCount(condition));
    }

    @ApiOperation(value = "查看用户详情", httpMethod = "POST", response = UserEntity.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query", name = "id", value = "编号", example = "1", dataType = "string", required = true),
            }
    )
    @ResponseBody
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:user:view:detail"})
    @ResMsg (tag = "查看用户", type = ResType.JSON, writeLogs = false)
    @RequestMapping(value = "/detail", produces = "application/json;charset=utf-8")
    public ResponseJson detail(@ApiIgnore String id) {
        idCheck(id, MODULE_NAME);
        UserEntity entity = userService.selectOneById(id);
        existCheck(entity, MODULE_NAME);
        return new ResponseJson(Constants.CODE_SUCCESS, entity);
    }

    /**
     * 校验访问权限
     *
     * @return
     */
    @ApiOperation(value = "校验用户权限", httpMethod = "POST", response = Void.class)
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:user:empty"})
    @ResMsg (tag = "校验用户权限", type = ResType.JSON, writeLogs = false)
    @RequestMapping(value = "/checkPermission", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson checkPermission() {
        return new ResponseJson(Constants.CODE_SUCCESS);
    }

    /**
     * 依据数据编号更新此条数据的所有值其‘有效性’字段不会更新
     *
     * @param entity 需要更新的实体
     * @return 更新条目
     * @see polar.island.core.security.entity.UserEntity
     */
    @ApiOperation(value = "更新用户全部字段", httpMethod = "POST", response = int.class)
    @ApiImplicitParams(
            {

                    @ApiImplicitParam(paramType = "query", name = "id", value = "数据编号", required = true),
                    @ApiImplicitParam(paramType = "query", name = "headUrl", value = "头像"),
                    @ApiImplicitParam(paramType = "query", name = "nickName", value = "昵称"),
                    @ApiImplicitParam(paramType = "query", name = "phone", value = "手机号"),
                    @ApiImplicitParam(paramType = "query", name = "email", value = "邮箱"),
                    @ApiImplicitParam(paramType = "query", name = "org_id", value = "所属机构"),
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:user:edit"})
    @ResMsg (tag = "更新用户全部字段", type = ResType.JSON)
    @RequestMapping(value = "/updateAllById", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson updateAllById(@ApiIgnore @Valid UserEntity entity, BindingResult bindingResult) {
        validate(UserEntity.class, "updateAllById", bindingResult);
        Map<String, Object> condition = beanToMap(entity);
        Long result = userService.updateAll(condition);

        UserEntity user = userService.selectOneById(entity.getId() + "");
        String userName = user.getUserName();
        securityService.clearUserInfoCache(userName, userService.selectOneById(entity.getId() + ""));
//        if(entity.getId().equals(UserUtil.getUserId())){
//            ShiroPrincipal shiroPrincipal= (ShiroPrincipal) SecurityUtils.getSubject().getPrincipal();
//            shiroPrincipal.setUser(user);
//        }
        return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功修改了" + result + "条数据。");
    }

    @ApiOperation(value = "修改自己的个人资料", httpMethod = "POST", response = int.class)
    @ApiImplicitParams(
            {

                    @ApiImplicitParam(paramType = "query", name = "headUrl", value = "头像"),
                    @ApiImplicitParam(paramType = "query", name = "nickName", value = "昵称"),
                    @ApiImplicitParam(paramType = "query", name = "phone", value = "手机号"),
                    @ApiImplicitParam(paramType = "query", name = "email", value = "邮箱"),
                    @ApiImplicitParam(paramType = "query", name = "org_id", value = "所属机构"),
            }
    )
    @RequiresUser
    @ResMsg (tag = "修改自己的个人资料", type = ResType.JSON)
    @RequestMapping(value = "/updateSelf", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson updateSelf(@ApiIgnore @Valid UserEntity entity, BindingResult bindingResult) {
        entity.setId(CommonUtil.str2Long( UserUtil.getUserId()));
        entity.setUserName(UserUtil.getUserName());
        validate(UserEntity.class, "updateSelf", bindingResult);
        Map<String, Object> condition = beanToMap(entity);
        Long result = userService.updateSelf(condition);
        UserEntity user = userService.selectOneById(entity.getId() + "");
        String userName = user.getUserName();
        securityService.clearUserInfoCache(userName, user);
//        if(entity.getId().equals(UserUtil.getUserId())){
//            ShiroPrincipal shiroPrincipal= (ShiroPrincipal) SecurityUtils.getSubject().getPrincipal();
//            shiroPrincipal.setUser(entity);
//        }
        return new ResponseJson(Constants.CODE_SUCCESS, result, "修改成功");
    }

    @ApiOperation(value = "修改自己的密码", httpMethod = "POST", response = int.class)
    @ApiImplicitParams(
            {

                    @ApiImplicitParam(paramType = "query", name = "oldPwd", value = "原始密码"),
                    @ApiImplicitParam(paramType = "query", name = "newPwd", value = "新密码")
            }
    )
    @RequiresUser
    @ResMsg (tag = "修改密码", type = ResType.JSON)
    @RequestMapping(value = "/updatePwd", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson updatePwd(@ApiIgnore String oldPwd, @ApiIgnore String newPwd) {
        if (oldPwd == null) {
            throw new ValidationException("原始密码不能为空", null);
        }
        if (newPwd == null) {
            throw new ValidationException("新密码不能为空", null);
        }
        UserEntity user = userService.selectOneById(UserUtil.getUserId());
        if (!encryManager.encry(oldPwd, user.getSalt()).equals(user.getPassword())) {
            throw new FrameWorkException (Constants.CODE_UNCORRENT_CREDENTIALS, "原始密码输入错误。", null, false);
        }
        String salt = encryManager.generateSalt();
        userService.changePassword(user.getId() + "", encryManager.encry(newPwd, salt),salt);
        securityService.kickOutUser(user,"您的密码已被修改，您已被强制下线！");
        SecurityUtils.getSubject().logout();
        return new ResponseJson(Constants.CODE_SUCCESS, null, "密码修改成功");
    }

    /**
     * 依据数据编号更新此条数据的所有值其‘有效性’字段不会更新
     *
     * @param entity 需要更新的实体
     * @return 更新条目
     * @see polar.island.core.security.entity.UserEntity
     */
    @ApiOperation(value = "更新用户单个字段", httpMethod = "POST", response = int.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query", name = "fieldName", value = "更新字段名称，如更新字段名称为\"name\"，则传入值时，其传入的key为:\"name\"", example = "1", required = true),
                    @ApiImplicitParam(paramType = "query", name = "id", value = "数据编号", required = true),
                    @ApiImplicitParam(paramType = "query", name = "headUrl", value = "头像"),
                    @ApiImplicitParam(paramType = "query", name = "nickName", value = "昵称"),
                    @ApiImplicitParam(paramType = "query", name = "phone", value = "手机号"),
                    @ApiImplicitParam(paramType = "query", name = "email", value = "邮箱"),
                    @ApiImplicitParam(paramType = "query", name = "org_id", value = "所属机构"),
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:user:edit"})
    @ResMsg (tag = "更新用户单个字段", type = ResType.JSON)
    @RequestMapping(value = "/updateField", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson updateField(@ApiIgnore @Valid UserEntity entity, BindingResult bindingResult) {
        validate(UserEntity.class, "updateField", bindingResult);
        Map<String, Object> condition = beanToSingleField(entity);
        condition.put("id", entity.getId());
        Long result = userService.updateField(condition);
        UserEntity user = userService.selectOneById(entity.getId() + "");
        String userName = user.getUserName();
        securityService.clearUserInfoCache(userName, user);

        return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功修改了" + result + "条数据。");
    }

    /**
     * 根据数据编号逻辑删除数据。
     *
     * @param ids 数据编号
     * @return 删除结果。
     */
    @ApiOperation(value = "删除多条数据", httpMethod = "POST", response = int.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query", name = "ids", value = "数据编号,其为一组编号，传入时，以ids[]为key传入，如删除id为1和2的数据时，传入：id[]=1&id[]=2", required = true)
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:user:delete"})
    @ResMsg (tag = "删除多个用户", type = ResType.JSON)
    @RequestMapping(value = "/deleteMulitById", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson deleteMulitById(@ApiIgnore @RequestParam(value = "ids[]", required = false) String[] ids) {
        idCheck(ids, MODULE_NAME);
        for (String id : ids) {
            securityService.kickOutUser(userService.selectOneById(id), "此用户已被删除，您已被强制下线");
        }
        Long result = userService.deleteMulitByIdPhysical(ids);
        return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功删除了" + result + "条数据。");
    }

    /**
     * 根据数据编号逻辑删除数据。
     *
     * @param id 数据编号
     * @return 删除结果。
     */
    @ApiOperation(value = "删除一条数据", httpMethod = "POST", response = int.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query", name = "id", value = "数据编号", required = true)
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:user:delete"})
    @ResMsg (tag = "删除一个用户", type = ResType.JSON)
    @RequestMapping(value = "/deleteById", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson deleteById(@ApiIgnore String id) {
        idCheck(id, MODULE_NAME);
        securityService.kickOutUser(userService.selectOneById(id), "此用户已被删除，您已被强制下线");
        Long result = userService.deleteByIdPhysical(id);
        return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功删除了" + result + "条数据。");
    }

    @ApiOperation(value = "禁用用户", httpMethod = "POST", response = int.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query", name = "id", value = "数据编号", required = true)
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:user:disableUser"})
    @ResMsg (tag = "禁用用户", type = ResType.JSON)
    @RequestMapping(value = "/disableUser", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson disableUser(@ApiIgnore String id, @ApiIgnore String message) {
        idCheck(id, MODULE_NAME);
        securityService.kickOutUser(userService.selectOneById(id), "此用户已被禁用，您已被强制下线");
        Long result = userService.disableUser(id, message == null ? "系统禁用" : message);
        return new ResponseJson(Constants.CODE_SUCCESS, result, "禁用成功。");
    }

    @ApiOperation(value = "启用用户", httpMethod = "POST", response = int.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query", name = "id", value = "数据编号", required = true)
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:user:enableUser"})
    @ResMsg (tag = "启用用户", type = ResType.JSON)
    @RequestMapping(value = "/enableUser", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson enableUser(@ApiIgnore String id) {
        idCheck(id, MODULE_NAME);

        Long result = userService.enableUser(id);
        UserEntity user = userService.selectOneById(id);
        String userName = user.getUserName();
        securityService.clearUserInfoCache(userName, user);
        return new ResponseJson(Constants.CODE_SUCCESS, result, "启用成功。");
    }

    /**
     * 新增一条数据。
     *
     * @param entity 数据
     * @return 新增结果。
     * @see polar.island.core.security.entity.UserEntity
     */
    @ApiOperation(value = "新增用户", httpMethod = "POST", response = int.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query", name = "userName", value = "登录账户，密码默认为admin"),
                    @ApiImplicitParam(paramType = "query", name = "headUrl", value = "头像"),
                    @ApiImplicitParam(paramType = "query", name = "nickName", value = "昵称"),
                    @ApiImplicitParam(paramType = "query", name = "phone", value = "手机号"),
                    @ApiImplicitParam(paramType = "query", name = "email", value = "邮箱"),
                    @ApiImplicitParam(paramType = "query", name = "org_id", value = "所属机构"),
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:user:add"})
    @ResMsg (tag = "新增用户", type = ResType.JSON)
    @RequestMapping(value = "/add", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson add(@ApiIgnore @Valid UserEntity entity, BindingResult bindingResult) {
        entity.setCreateDate(new Date());
        entity.setLogCount(0L);
        entity.setSalt(encryManager.generateSalt());
        // 设置密码
        entity.setPassword(
                encryManager.encry(entity.getPassword(), entity.getSalt()));
        entity.setState(1);
        entity.setUserType(1);//平台用户
        validate(UserEntity.class, "add", bindingResult);
        Map<String, Object> condition = beanToMap(entity);
        Object result = userService.insert(condition);
        return new ResponseJson(Constants.CODE_SUCCESS, result, "新增成功。");
    }

    @ApiOperation(value = "修改用户角色", httpMethod = "POST", response = int.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query", name = "id", value = "用户编号", required = true),
                    @ApiImplicitParam(paramType = "query", name = "roleIds[]", value = "角色编号", required = true)
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:user:role:empty"})
    @ResMsg (tag = "修改用户角色", type = ResType.JSON)
    @RequestMapping(value = "/updateUserRole", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson updateUserRole(@ApiIgnore String id, @ApiIgnore @RequestParam(value = "roleIds[]", required = false) String[] roleIds) {
        idCheck(id, MODULE_NAME);
        securityService.clearUserPermissionCache(id);
        roleService.updateUserRoles(id, roleIds);
        return new ResponseJson(Constants.CODE_SUCCESS, null, "用户角色修改成功");
    }

    @ApiOperation(value = "修改用户菜单", httpMethod = "POST", response = int.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query", name = "id", value = "用户编号", required = true),
                    @ApiImplicitParam(paramType = "query", name = "menuModelId", value = "菜单模板编号", required = true)
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:user:menu"})
    @ResMsg (tag = "修改用户菜单", type = ResType.JSON)
    @RequestMapping(value = "/updateUserMenuModels", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson updateUserMenuModels(@ApiIgnore String id, @ApiIgnore String menuModelId) {
        idCheck(id, MODULE_NAME);
        menuService.clearUserMenusCache(id);
        menuModelService.updateUserMenuModels(id, menuModelId);
        return new ResponseJson(Constants.CODE_SUCCESS, null, "用户菜单修改成功");
    }

    /**
     * 清空用户菜单缓存
     *
     * @param userIds 用户编号
     * @return 成功或者失败的json串
     */
    @ApiOperation(value = "清空用户菜单缓存", httpMethod = "POST", response = int.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query", name = "userIds", value = "用户编号", required = true)
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:user:clear:userMenu"})
    @ResMsg (tag = "清除用户菜单缓存", type = ResType.JSON)
    @RequestMapping(value = "/clearUserMenuCache", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson clearUserMenuCache(@ApiIgnore @RequestParam(value = "userIds[]", required = false) String[] userIds) {
        idCheck(userIds, MODULE_NAME);
        for (String userId : userIds) {
            menuService.clearUserMenusCache(userId);
        }
        return new ResponseJson(Constants.CODE_SUCCESS, null, "缓存清除成功。");
    }

    /**
     * 清空用户菜单所有缓存
     *
     * @return 成功或者失败的json串
     */
    @ApiOperation(value = "清空用户菜单所有缓存", httpMethod = "POST", response = int.class)
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:user:clear:allMenu"})
    @ResMsg (tag = "清除用户菜单缓存", type = ResType.JSON)
    @RequestMapping(value = "/clearAllUserMenuCache", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson clearAllUserMenuCache() {
        menuService.clearAllMenusCache();
        return new ResponseJson(Constants.CODE_SUCCESS, null, "缓存清除成功。");
    }

    /**
     * 清除用户权限缓存
     *
     * @param userIds 用户编号
     * @return 成功或者失败的json串
     */
    @ApiOperation(value = "清除用户权限缓存", httpMethod = "POST", response = int.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query", name = "userIds", value = "用户编号", required = true)
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:user:clear:userInfo"})
    @ResMsg (tag = "清除用户权限缓存", type = ResType.JSON)
    @RequestMapping(value = "/clearUserInfo", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson clearUserInfo(@ApiIgnore @RequestParam(value = "userIds[]", required = false) String[] userIds) {
        idCheck(userIds, MODULE_NAME);
        for (String userId : userIds) {
            securityService.clearUserPermissionCache(userId);
        }
        return new ResponseJson(Constants.CODE_SUCCESS, null, "缓存清除成功。");
    }

    /**
     * 清除用户所有权限缓存
     *
     * @return 成功或者失败的json串
     */
    @ApiOperation(value = "清除所有用户权限缓存", httpMethod = "POST", response = int.class)
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:user:clear:allInfo"})
    @ResMsg (tag = "清除用户权限缓存", type = ResType.JSON)
    @RequestMapping(value = "/clearAllUserInfo", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson clearAllUserInfo() {
        securityService.clearAllUserPermissionCache();
        return new ResponseJson(Constants.CODE_SUCCESS, null, "缓存清除成功。");
    }


}
