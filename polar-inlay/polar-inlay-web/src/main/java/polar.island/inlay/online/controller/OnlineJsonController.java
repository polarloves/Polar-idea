package polar.island.inlay.online.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.session.ExpiredSessionException;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.DefaultSessionKey;
import org.apache.shiro.session.mgt.SessionManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import polar.island.core.config.Constants;
import polar.island.core.security.service.SecurityService;
import polar.island.web.controller.BasicController;
import polar.island.core.entity.ResponseJson;
import polar.island.core.res.ResMsg;
import polar.island.core.res.ResType;
import polar.island.core.exception.ValidationException;
import polar.island.inlay.online.entity.OnlineUserEntity;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Deque;
import java.util.List;

@Controller(value = "onlineJsonController")
@RequestMapping(value = "/online/json")
@Api(tags = "在线用户接口", description = "在线用户接口")
public class OnlineJsonController extends BasicController {
    @Resource(name = "shiroCacheManager")
    private CacheManager cacheManager;
    @Resource(name = "sessionManager")
    private SessionManager sessionManager;
    private static final String MODULE_NAME = "在线用户";
    @Resource(name = "shiroSecurityService")
    private SecurityService securityService;
    @Value( value = "${shiro.cache.onlineUserCacheName}" )
    private String onlineUserCacheName;
    @ApiOperation(value = "查询在线用户分页列表", httpMethod = "POST", responseContainer = "List", response = OnlineUserEntity.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query", name = "userId", value = "用户编号", example = "1", dataType = "string", required = true),
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:online:view"})
    @ResMsg (tag = "查询用户", type = ResType.JSON, writeLogs = false)
    @RequestMapping(value = "/pageList", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson pageList(@ApiIgnore OnlineUserEntity entity) {
        if (StringUtils.isEmpty(entity.getUserId())) {
            throw new ValidationException("用户编号不能为空", null);
        }
        Cache<String, Deque<Serializable>> activeUserCache = cacheManager.getCache(onlineUserCacheName);
        Deque<Serializable> caches = activeUserCache.get(entity.getUserId());
        List<OnlineUserEntity> sessionList = new ArrayList<OnlineUserEntity>();
        if (caches != null && caches.size() > 0) {
            for (Serializable sessionId : caches) {
                OnlineUserEntity tmp = new OnlineUserEntity();
                try {
                    Session session = sessionManager.getSession(new DefaultSessionKey(sessionId));
                    if (session == null) {
                        continue;
                    }
                    Object flag = session.getAttribute(Constants.FORCE_DOWNLINE_FLAG);
                    if (flag != null && flag.equals(true)) {
                        continue;
                    }
                    tmp.setUserId(entity.getUserId());
                    tmp.setSessionId((String) sessionId);
                    tmp.setLastAccessTime(session.getLastAccessTime());
                    tmp.setLoginTime(session.getStartTimestamp());
                    tmp.setLoginIp(session.getHost());
                    if (session.getTimeout() > 0) {
                        tmp.setExpireTime(new Date(session.getLastAccessTime().getTime() + session.getTimeout()));
                    }
                } catch (ExpiredSessionException e) {
                    tmp.setExpire(true);
                }
                sessionList.add(tmp);
            }
        }
        return new ResponseJson(Constants.CODE_SUCCESS, sessionList, null, null);
    }

    @ApiOperation(value = "强制下线", httpMethod = "POST")
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query", name = "sessionId", value = "会话编号", example = "1", dataType = "string", required = true),
                    @ApiImplicitParam(paramType = "query", name = "message", value = "下线消息内容", example = "1", dataType = "string", required = true),
                    @ApiImplicitParam(paramType = "query", name = "userId", value = "用户编号", example = "1", dataType = "string", required = true),
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:online:downline"})
    @ResMsg (tag = "强制下线", type = ResType.JSON)
    @RequestMapping(value = "/forceDownLine", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson forceDownLine(@ApiIgnore String sessionId, @ApiIgnore String message, @ApiIgnore String userId) {
        idCheck(sessionId, MODULE_NAME);
        securityService.kickOutSession(sessionId, message);
        return new ResponseJson(Constants.CODE_SUCCESS, 1, "强制下线成功。");
    }

}
