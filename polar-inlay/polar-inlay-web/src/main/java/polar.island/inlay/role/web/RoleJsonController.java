package polar.island.inlay.role.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import polar.island.core.config.Constants;
import polar.island.web.controller.BasicController;
import polar.island.core.entity.ResponseJson;
import polar.island.core.res.ResMsg;
import polar.island.core.res.ResType;
import polar.island.inlay.role.entity.RoleEntity;
import polar.island.inlay.role.service.RoleService;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * 角色的接口，返回数据全部为json串。
 * 
 * @author PolarLoves
 *
 */
@Controller(value = "roleJsonController")
@RequestMapping(value = "/role/json")
@Api(tags = "角色接口", description = "角色接口，其用于角色的增删改查")
public class RoleJsonController extends BasicController {
	@Resource(name = "roleService")
	private RoleService roleService;
	private  static final  String MODULE_NAME="角色";
	/**
	 * 校验访问权限
	 * 
	 * @return
	 */
	@ApiOperation(value = "校验角色权限", httpMethod = "POST",response = Void.class)
	@RequiresUser
	@RequiresPermissions(value = { "polar:backstage", "polar:role:empty" })
	@ResMsg (tag = "校验角色权限", type = ResType.JSON,writeLogs = false)
	@RequestMapping(value = "/checkPermission", produces = "application/json;charset=utf-8")
	@ResponseBody
	public ResponseJson checkPermission() {
		return new ResponseJson(Constants.CODE_SUCCESS);
	}
	/**
	 * 依据条件查询数据
	 * 
	 *            查询条件
	 * @see polar.island.inlay.role.entity.RoleEntity
	 * @return 列表Json串。
	 */
	@ApiOperation(value = "查询角色", httpMethod = "POST",responseContainer = "List",response = RoleEntity.class)
	@RequiresUser
	@RequiresPermissions(value = { "polar:backstage", "polar:role:view:list" })
	@ResMsg (tag = "查询角色", type = ResType.JSON,writeLogs = false)
	@RequestMapping(value = "/pageList", produces = "application/json;charset=utf-8")
	@ResponseBody
	public ResponseJson pageList() {
		Map<String, Object> condition =new HashMap<>();
		return new ResponseJson(Constants.CODE_SUCCESS, roleService.selectList(condition));
	}
	@ApiOperation(value = "查看角色详情", httpMethod = "POST",response = RoleEntity.class)
	@ApiImplicitParams(
			{
					@ApiImplicitParam(paramType = "query",name = "id",value = "编号",example = "1",dataType = "string",required = true),
			}
	)
	@ResponseBody
	@RequiresUser
	@RequiresPermissions(value = { "polar:backstage", "polar:role:view:detail" })
	@ResMsg (tag = "查看角色", type = ResType.JSON,writeLogs = false)
	@RequestMapping(value = "/detail", produces = "application/json;charset=utf-8")
	public ResponseJson detail(@ApiIgnore String id) {
		idCheck(id,MODULE_NAME);
		RoleEntity entity = roleService.selectOneById(id);
		existCheck(entity,MODULE_NAME);
		return new ResponseJson(Constants.CODE_SUCCESS, entity);
	}
	/**
	 * 依据数据编号更新此条数据的所有值。
	 * 
	 * @param entity
	 *            需要更新的实体
	 * @see polar.island.inlay.role.entity.RoleEntity
	 * @return 更新条目
	 */
	@ApiOperation(value = "更新角色全部字段", httpMethod = "POST",response = int.class)
	@ApiImplicitParams(
			{

					@ApiImplicitParam(paramType = "query",name = "id",value = "数据编号",required = true),
					@ApiImplicitParam(paramType = "query",name = "parentId",value = "父级角色编号"),
					@ApiImplicitParam(paramType = "query",name = "name",value = "角色名称",required = true),
					@ApiImplicitParam(paramType = "query",name = "text",value = "角色中文名称",required = true),
					@ApiImplicitParam(paramType = "query",name = "info",value = "角色描述",required = true),
					@ApiImplicitParam(paramType = "query",name = "orderNum",value = "排序号"),
			}
	)
	@RequiresUser
	@RequiresPermissions(value = { "polar:backstage", "polar:role:edit" })
	@ResMsg (tag = "更新角色全部字段", type = ResType.JSON)
	@RequestMapping(value = "/updateAllById", produces = "application/json;charset=utf-8")
	@ResponseBody
	public ResponseJson update(@ApiIgnore @Valid RoleEntity entity, BindingResult bindingResult) {
		validate(RoleEntity.class, "updateAllById",bindingResult);
		Map<String, Object> condition = beanToMap(entity);
		Long result = roleService.updateAll(condition);
		return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功修改了" + result + "条数据。");
	}

	/**
	 * 根据数据编号物理删除数据。
	 * 
	 * @param id
	 *            数据编号
	 * @return 删除结果。
	 */
	@ApiOperation(value = "删除一条数据", httpMethod = "POST",response = int.class)
	@ApiImplicitParams(
			{
					@ApiImplicitParam(paramType = "query",name = "id",value = "数据编号",required = true)
			}
	)
	@RequiresUser
	@RequiresPermissions(value = { "polar:backstage", "polar:role:delete" })
	@ResMsg (tag = "删除一个角色", type = ResType.JSON)
	@RequestMapping(value = "/deleteById", produces = "application/json;charset=utf-8")
	@ResponseBody
	public ResponseJson deleteById(@ApiIgnore String id) {
		idCheck(id,MODULE_NAME);
		Long result = roleService.deleteByIdPhysical(id);
		return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功删除了" + result + "条数据。");
	}

	/**
	 * 新增一条数据。
	 * 
	 * @param entity
	 *            数据
	 * @see polar.island.inlay.role.entity.RoleEntity
	 * @return 新增结果。
	 */
	@ApiOperation(value = "新增角色", httpMethod = "POST",response = int.class)
	@ApiImplicitParams(
			{
					@ApiImplicitParam(paramType = "query",name = "parentId",value = "父级角色编号",required = true),
					@ApiImplicitParam(paramType = "query",name = "name",value = "角色名称"),
					@ApiImplicitParam(paramType = "query",name = "text",value = "角色中文名称",required = true),
					@ApiImplicitParam(paramType = "query",name = "info",value = "角色描述",required = true),
					@ApiImplicitParam(paramType = "query",name = "orderNum",value = "排序号"),
			}
	)
	@RequiresUser
	@RequiresPermissions(value = { "polar:backstage", "polar:role:add" })
	@ResMsg (tag = "新增角色", type = ResType.JSON)
	@RequestMapping(value = "/add", produces = "application/json;charset=utf-8")
	@ResponseBody
	public ResponseJson add(@ApiIgnore @Valid RoleEntity entity, BindingResult bindingResult) {
		validate(RoleEntity.class, "add",bindingResult);
		Map<String, Object> condition = beanToMap(entity);
		Object result = roleService.insert(condition);
		return new ResponseJson(Constants.CODE_SUCCESS, result, "新增成功。");
	}

	/**
	 * 修改用户的角色信息
	 * 
	 * @param id
	 *            用户编号
	 * @param roleIds
	 *            角色列表
	 * @return 修改结果
	 */
	@ApiOperation(value = "修改用户角色", httpMethod = "POST",response = int.class)
	@ApiImplicitParams(
			{
					@ApiImplicitParam(paramType = "query",name = "id",value = "用户编号",required = true),
					@ApiImplicitParam(paramType = "query",name = "roleIds[]",value = "角色编号",required = true)
			}
	)
	@RequiresUser
	@RequiresPermissions(value = { "polar:backstage", "polar:user:role" })
	@ResMsg (tag = "修改用户角色", type = ResType.JSON)
	@RequestMapping(value = "/updateUserRole", produces = "application/json;charset=utf-8")
	@ResponseBody
	public ResponseJson updateUserRole(@ApiIgnore String id,@ApiIgnore  @RequestParam(value = "roleIds[]", required = false) String[] roleIds) {
		idCheck(id,MODULE_NAME);
		roleService.updateUserRoles(id, roleIds);
		return new ResponseJson(Constants.CODE_SUCCESS, null, "用户角色分配成功");
	}
}
