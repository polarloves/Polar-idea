package polar.island.inlay.resource.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import polar.island.core.config.Constants;
import polar.island.core.security.service.SecurityService;
import polar.island.inlay.resource.entity.ResourceEntity;
import polar.island.inlay.resource.service.ResourceService;
import polar.island.web.controller.BasicController;
import polar.island.core.entity.ResponseJson;
import polar.island.core.res.ResMsg;
import polar.island.core.res.ResType;
import polar.island.core.util.PropertieUtil;
import polar.island.inlay.permission.service.PermissionService;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Map;


/**
 * 资源的接口，返回数据全部为json串。
 *
 * @author PolarLoves
 */
@Api(tags = "资源的接口", description = "资源的接口，返回数据全部为json串")
@Controller(value = "resourceJsonController")
@RequestMapping(value = "/resource/json")
public class ResourceJsonController extends BasicController {
    @Resource(name = "resourceService")
    private ResourceService resourceService;
    @Resource(name = "permissionService")
    private PermissionService permissionService;
    @Resource(name = "shiroSecurityService")
    private SecurityService securityService;
    private static final String MODULE_NAME = "资源";

    /**
     * 校验访问权限
     *
     * @return
     */
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:resource:empty"})
    @ResMsg (tag = "校验资源权限", type = ResType.JSON,writeLogs = false)
    @ApiOperation(value = "校验字典权限", httpMethod = "POST", response = Void.class)
    @RequestMapping(value = "/checkPermission", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson checkPermission() {
        return new ResponseJson(Constants.CODE_SUCCESS);
    }

    /**
     * 依据条件查询分页数据，如果不传页码、每页条目，则默认分别为：1,10
     *
     * @param entity 查询条件
     * @return 列表Json串。
     */
    @ApiOperation(value = "查询资源分页列表", httpMethod = "POST", responseContainer = "List", response = ResourceEntity.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query",name = "id", value = "编号", example = "1", dataType = "string"),
                    @ApiImplicitParam(paramType = "query",name = "name", value = "资源名称"),
                    @ApiImplicitParam(paramType = "query",name = "text", value = "资源中文名称"),
                    @ApiImplicitParam(paramType = "query",name = "path", value = "访问路径"),
                    @ApiImplicitParam(paramType = "query",name = "orderNum", value = "排序号"),
                    @ApiImplicitParam(paramType = "query",name = "info", value = "描述"),

                    @ApiImplicitParam(paramType = "query",name = "page", value = "当前页,如不传,则默认为1", example = "1"),
                    @ApiImplicitParam(paramType = "query",name = "rows", value = "每页的数据条目,如不传,则默认为10", example = "10"),
                    @ApiImplicitParam(paramType = "query",name = "sort", value = "排序字段", example = "groupName"),
                    @ApiImplicitParam(paramType = "query",name = "order", value = "排序方式，可选值有：ASC DESC,当sort字段传入值时,其必须传入", example = "DESC")
            }
    )

    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:resource:view:list"})
    @ResMsg (tag = "查询资源", type = ResType.JSON,writeLogs = false)
    @RequestMapping(value = "/pageList", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson pageList(@ApiIgnore ResourceEntity entity) {
        Map<String, Object> condition = beanToMap(entity);
        return new ResponseJson(Constants.CODE_SUCCESS, resourceService.selectPageList(condition), null,
                resourceService.selectCount(condition));
    }

    @ApiOperation(value = "查看资源详情", httpMethod = "POST", response = ResourceEntity.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query",name = "id", value = "编号", example = "1", dataType = "string", required = true),
            }
    )
    @ResponseBody
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:resource:view:detail"})
    @ResMsg (tag = "查看资源", type = ResType.JSON)
    @RequestMapping(value = "/detail", produces = "application/json;charset=utf-8")
    public ResponseJson detail(@ApiIgnore String id) {
        idCheck(id, MODULE_NAME);
        ResourceEntity entity = resourceService.selectOneById(id);
        existCheck(entity, MODULE_NAME);
        return new ResponseJson(Constants.CODE_SUCCESS, entity);
    }

    /**
     * 依据数据编号更新此条数据的所有值。
     *
     * @param entity 需要更新的实体
     * @return 更新条目
     */
    @ApiOperation(value = "更新资源全部字段", httpMethod = "POST", response = int.class)
    @ApiImplicitParams(
            {

                    @ApiImplicitParam(paramType = "query",name = "id", value = "数据编号", required = true),
                    @ApiImplicitParam(paramType = "query",name = "name", value = "资源名称", required = true),
                    @ApiImplicitParam(paramType = "query",name = "text", value = "资源中文名称", required = true),
                    @ApiImplicitParam(paramType = "query",name = "path", value = "访问路径", required = true),
                    @ApiImplicitParam(paramType = "query",name = "orderNum", value = "排序号", required = true),
                    @ApiImplicitParam(paramType = "query",name = "info", value = "描述", required = true),
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:resource:edit"})
    @ResMsg (tag = "更新资源全部字段", type = ResType.JSON)
    @RequestMapping(value = "/updateAllById", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson update(@ApiIgnore @Valid ResourceEntity entity, BindingResult bindingResult) {
        validate(ResourceEntity.class, "updateAllById", bindingResult);
        Map<String, Object> condition = beanToMap(entity);
        condition.put("id", entity.getId());
        Long result = resourceService.updateAll(condition);
        return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功修改了" + result + "条数据。");
    }

    /**
     * 依据数据编号更新此条数据的所有值。
     *
     * @param entity 需要更新的实体
     * @return 更新条目
     */
    @ApiOperation(value = "更新资源单个字段", httpMethod = "POST", response = int.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query",name = "fieldName", value = "更新字段名称，如更新字段名称为\"name\"，则传入值时，其传入的key为:\"name\"", example = "1", required = true),
                    @ApiImplicitParam(paramType = "query",name = "id", value = "数据编号", required = true),
                    @ApiImplicitParam(paramType = "query",name = "name", value = "资源名称"),
                    @ApiImplicitParam(paramType = "query",name = "text", value = "资源中文名称"),
                    @ApiImplicitParam(paramType = "query",name = "path", value = "访问路径"),
                    @ApiImplicitParam(paramType = "query",name = "orderNum", value = "排序号"),
                    @ApiImplicitParam(paramType = "query",name = "info", value = "描述"),
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:resource:edit"})
    @ResMsg (tag = "更新资源单个字段", type = ResType.JSON)
    @RequestMapping(value = "/updateField", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson updateField(@ApiIgnore @Valid ResourceEntity entity, BindingResult bindingResult) {
        validate(ResourceEntity.class, "updateField", bindingResult);
        Map<String, Object> condition = beanToSingleField(entity);
        condition.put("id", entity.getId());
        Long result = resourceService.updateField(condition);
        return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功修改了" + result + "条数据。");
    }

    /**
     * 根据数据编号物理删除数据。
     *
     * @param ids 数据编号
     * @return 删除结果。
     */
    @ApiOperation(value = "删除多条数据", httpMethod = "POST", response = int.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query",name = "ids", value = "数据编号,其为一组编号，传入时，以ids[]为key传入，如删除id为1和2的数据时，传入：id[]=1&id[]=2", required = true)
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:resource:delete"})
    @ResMsg (tag = "删除多个资源", type = ResType.JSON)
    @RequestMapping(value = "/deleteMulitById", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson deleteMulitById(@ApiIgnore @RequestParam(value = "ids[]", required = false) String[] ids) {
        idCheck(ids, MODULE_NAME);
        Long result = resourceService.deleteMulitByIdPhysical(ids);
        return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功删除了" + result + "条数据。");
    }

    /**
     * 根据数据编号物理删除数据。
     *
     * @param id 数据编号
     * @return 删除结果。
     */
    @ApiOperation(value = "删除一条数据", httpMethod = "POST", response = int.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query",name = "id", value = "数据编号", required = true)
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:resource:delete"})
    @ResMsg (tag = "删除一个资源", type = ResType.JSON)
    @RequestMapping(value = "/deleteById", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson deleteById(@ApiIgnore String id) {
        idCheck(id, MODULE_NAME);
        Long result = resourceService.deleteByIdPhysical(id);
        return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功删除了" + result + "条数据。");
    }

    /**
     * 新增一条数据。
     *
     * @param entity 数据
     * @return 新增结果。
     */
    @ApiOperation(value = "新增一条字典数据", httpMethod = "POST", response = int.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query",name = "name", value = "资源名称", required = true),
                    @ApiImplicitParam(paramType = "query",name = "text", value = "资源中文名称", required = true),
                    @ApiImplicitParam(paramType = "query",name = "path", value = "访问路径", required = true),
                    @ApiImplicitParam(paramType = "query",name = "orderNum", value = "排序号", required = true),
                    @ApiImplicitParam(paramType = "query",name = "info", value = "描述", required = true),
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:resource:add"})
    @ResMsg (tag = "新增资源", type = ResType.JSON)
    @RequestMapping(value = "/add", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson add(@ApiIgnore @Valid ResourceEntity entity, BindingResult bindingResult) {
        validate(ResourceEntity.class, "add", bindingResult);
        Map<String, Object> condition = beanToMap(entity);
        Object result = resourceService.insert(condition);
        return new ResponseJson(Constants.CODE_SUCCESS, result, "新增成功。");
    }

    @ApiOperation(value = "修改资源权限", httpMethod = "POST", response = Integer.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query",name = "id", value = "资源编号", required = true),
                    @ApiImplicitParam(paramType = "query",name = "permissionIds[]", value = "权限编号", required = true)
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:resource:permission"})
    @ResMsg (tag = "修改资源权限", type = ResType.JSON)
    @RequestMapping(value = "/updateResourcePermissions", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson updateResourcePermissions(@ApiIgnore String id,
                                                  @ApiIgnore @RequestParam(value = "permissionIds[]", required = false) String[] permissionIds) {
        idCheck(id, MODULE_NAME);
        permissionService.updateResourcePermissions(id, permissionIds);
        return new ResponseJson(Constants.CODE_SUCCESS, null, "资源权限设置成功");
    }

    @ApiOperation(value = "重置资源", httpMethod = "POST", response = Integer.class)
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:resource:reload"})
    @ResMsg (tag = "重置资源", type = ResType.JSON)
    @RequestMapping(value = "/reloadResource", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson reloadResource() {
        securityService.reloadResource(Boolean.parseBoolean(PropertieUtil.getSetting("reload.publish")));
        return new ResponseJson(Constants.CODE_SUCCESS, null, "重置资源成功");
    }

}
