package polar.island.inlay.tree.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import polar.island.core.config.Constants;
import polar.island.web.controller.BasicController;
import polar.island.core.entity.ResponseJson;
import polar.island.core.res.ResMsg;
import polar.island.core.res.ResType;
import polar.island.inlay.tree.entity.TreeEntity;
import polar.island.inlay.tree.service.TreeService;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Map;

/**
 * 树结构的接口，返回数据全部为json串。
 * 
 * @author PolarLoves
 *
 */
@Controller(value = "treeJsonController")
@RequestMapping(value = "/tree/json")
@Api(tags = "树结构的接口", description = "树结构的接口，其用于树结构的增删改查")
public class TreeJsonController extends BasicController {
	@Resource(name = "treeService")
	private TreeService treeService;
	private  static final  String MODULE_NAME="树结构";
	/**
	 * 校验访问权限
	 * 
	 * @return
	 */
	@ApiOperation(value = "校验树结构权限", httpMethod = "POST",response = Void.class)
	@RequiresUser
	@RequiresPermissions(value = { "polar:backstage", "polar:tree:empty" })
	@ResMsg (tag = "校验树结构权限", type = ResType.JSON,writeLogs = false)
	@RequestMapping(value = "/checkPermission", produces = "application/json;charset=utf-8")
	@ResponseBody
	public ResponseJson checkPermission() {
		return new ResponseJson(Constants.CODE_SUCCESS);
	}

	/**
	 * 依据条件查询分页数据，如果不传页码、每页条目，则默认分别为：1,10
	 * 
	 * @param entity
	 *            查询条件
	 * @see polar.island.inlay.tree.entity.TreeEntity
	 * @return 列表Json串。
	 */
	@ApiOperation(value = "查询树结构分页列表", httpMethod = "POST",responseContainer = "List",response = TreeEntity.class)
	@ApiImplicitParams(
			{
					@ApiImplicitParam(paramType = "query",name = "id",value = "编号",example = "1",dataType = "string"),
					@ApiImplicitParam(paramType = "query",name = "groupName",value = "组名，支持模糊查询",example = "性别"),
					@ApiImplicitParam(paramType = "query",name = "groupId",value = "组编号",example = "SEX"),
					@ApiImplicitParam(paramType = "query",name = "text",value = "文本内容",example = "男"),
					@ApiImplicitParam(paramType = "query",name = "value",value = "值",example = "01"),
					@ApiImplicitParam(paramType = "query",name = "textAlias",value = "别名"),
					@ApiImplicitParam(paramType = "query",name = "textId",value = "编号"),
					@ApiImplicitParam(paramType = "query",name = "parentId",value = "父级编号"),
					@ApiImplicitParam(paramType = "query",name = "type",value = "类型"),

					@ApiImplicitParam(paramType = "query",name = "page",value = "当前页,如不传,则默认为1",example = "1"),
					@ApiImplicitParam(paramType = "query",name = "rows",value = "每页的数据条目,如不传,则默认为10",example = "10"),
					@ApiImplicitParam(paramType = "query",name = "sort",value = "排序字段",example = "groupName"),
					@ApiImplicitParam(paramType = "query",name = "order",value = "排序方式，可选值有：ASC DESC,当sort字段传入值时,其必须传入",example = "DESC")
			}
	)
	@RequiresUser
	@RequiresPermissions(value = { "polar:backstage", "polar:tree:view:list" })
	@ResMsg (tag = "查询树结构", type = ResType.JSON,writeLogs = false)
	@RequestMapping(value = "/pageList", produces = "application/json;charset=utf-8")
	@ResponseBody
	public ResponseJson pageList(@ApiIgnore TreeEntity entity) {
		Map<String, Object> condition = beanToMap(entity);
		return new ResponseJson(Constants.CODE_SUCCESS,  treeService.selectPageList(condition),null,
				treeService.selectCount(condition));
	}

	/**
	 * 查看树结构详情的json串
	 * 
	 * @param id
	 *            数据编号
	 * @return 详情json串
	 */
	@ApiOperation(value = "查看树结构详情", httpMethod = "POST",response = TreeEntity.class)
	@ApiImplicitParams(
			{
					@ApiImplicitParam(paramType = "query",name = "id",value = "编号",example = "1",dataType = "string",required = true),
			}
	)
	@ResponseBody
	@RequiresUser
	@RequiresPermissions(value = { "polar:backstage", "polar:tree:view:detail" })
	@ResMsg (tag = "查看树结构详情的json串", type = ResType.JSON,writeLogs = false)
	@RequestMapping(value = "/detail", produces = "application/json;charset=utf-8")
	public ResponseJson detail(@ApiIgnore String id) {
		idCheck(id,MODULE_NAME);
		TreeEntity entity = treeService.selectOneById(id);
		existCheck(entity,MODULE_NAME);
		return new ResponseJson(Constants.CODE_SUCCESS, entity);
	}

	/**
	 * 依据数据编号更新此条数据的所有值。
	 * 
	 * @param entity
	 *            需要更新的实体
	 * @see polar.island.inlay.tree.entity.TreeEntity
	 * @return 更新条目
	 */
	@ApiOperation(value = "更新树结构全部字段", httpMethod = "POST",response = int.class)
	@ApiImplicitParams(
			{

					@ApiImplicitParam(paramType = "query",name = "id",value = "数据编号",required = true),
					@ApiImplicitParam(paramType = "query",name = "groupName",value = "组名",example = "性别",required = true),
					@ApiImplicitParam(paramType = "query",name = "groupId",value = "组编号",example = "SEX",required = true),
					@ApiImplicitParam(paramType = "query",name = "text",value = "文本内容",example = "男",required = true),
					@ApiImplicitParam(paramType = "query",name = "value",value = "值",example = "01",required = true),
					@ApiImplicitParam(paramType = "query",name = "textAlias",value = "别名",required = true),
					@ApiImplicitParam(paramType = "query",name = "textId",value = "编号",required = true),
					@ApiImplicitParam(paramType = "query",name = "parentId",value = "父级编号"),
					@ApiImplicitParam(paramType = "query",name = "type",value = "类型",required = true),
			}
	)
	@RequiresUser
	@RequiresPermissions(value = { "polar:backstage", "polar:tree:edit" })
	@ResMsg (tag = "更新树结构全部字段", type = ResType.JSON)
	@RequestMapping(value = "/updateAllById", produces = "application/json;charset=utf-8")
	@ResponseBody
	public ResponseJson update(@ApiIgnore @Valid TreeEntity entity, BindingResult bindingResult) {
		validate(TreeEntity.class, "updateAllById",bindingResult);
		Map<String, Object> condition = beanToMap(entity);
		Long result = treeService.updateAll(condition);
		return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功修改了" + result + "条数据。");
	}

	/**
	 * 依据数据编号更新此条数据的所有值。
	 * 
	 * @param entity
	 *            需要更新的实体
	 * @see polar.island.inlay.tree.entity.TreeEntity
	 * @return 更新条目
	 */
	@ApiOperation(value = "更新字典单个字段", httpMethod = "POST",response = int.class)
	@ApiImplicitParams(
			{
					@ApiImplicitParam(paramType = "query",name = "fieldName",value = "更新字段名称，如更新字段名称为\"name\"，则传入值时，其传入的key为:\"name\"",example = "1",required = true),
					@ApiImplicitParam(paramType = "query",name = "id",value = "数据编号",required = true),
					@ApiImplicitParam(paramType = "query",name = "groupName",value = "组名",example = "性别"),
					@ApiImplicitParam(paramType = "query",name = "groupId",value = "组编号",example = "SEX"),
					@ApiImplicitParam(paramType = "query",name = "text",value = "文本内容",example = "男"),
					@ApiImplicitParam(paramType = "query",name = "value",value = "值",example = "01"),
					@ApiImplicitParam(paramType = "query",name = "textAlias",value = "别名"),
					@ApiImplicitParam(paramType = "query",name = "textId",value = "编号"),
					@ApiImplicitParam(paramType = "query",name = "parentId",value = "父级编号"),
					@ApiImplicitParam(paramType = "query",name = "type",value = "类型")
			}
	)
	@RequiresUser
	@RequiresPermissions(value = { "polar:backstage", "polar:tree:edit" })
	@ResMsg (tag = "更新树结构单个字段", type = ResType.JSON)
	@RequestMapping(value = "/updateField", produces = "application/json;charset=utf-8")
	@ResponseBody
	public ResponseJson updateField(@ApiIgnore @Valid TreeEntity entity, BindingResult bindingResult){
		validate(TreeEntity.class, "updateField",bindingResult);
		Map<String, Object> condition = beanToSingleField(entity);
		condition.put("id", entity.getId());
		Long result = treeService.updateField(condition);
		return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功修改了" + result + "条数据。");
	}

	/**
	 * 根据数据编号物理删除数据。
	 * 
	 * @param ids
	 *            数据编号
	 * @return 删除结果。
	 */
	@ApiOperation(value = "删除多条数据", httpMethod = "POST",response = int.class)
	@ApiImplicitParams(
			{
					@ApiImplicitParam(paramType = "query",name = "ids",value = "数据编号,其为一组编号，传入时，以ids[]为key传入，如删除id为1和2的数据时，传入：id[]=1&id[]=2",required = true)
			}
	)
	@RequiresUser
	@RequiresPermissions(value = { "polar:backstage", "polar:tree:delete" })
	@ResMsg (tag = "删除多个树结构", type = ResType.JSON)
	@RequestMapping(value = "/deleteMulitById", produces = "application/json;charset=utf-8")
	@ResponseBody
	public ResponseJson deleteMulitById(@ApiIgnore @RequestParam(value = "ids[]", required = false) String[] ids) {
		idCheck(ids,MODULE_NAME);
		Long result = treeService.deleteMulitByIdPhysical(ids);
		return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功删除了" + result + "条数据。");
	}

	/**
	 * 根据数据编号物理删除数据。
	 * 
	 * @param id
	 *            数据编号
	 * @return 删除结果。
	 */
	@ApiOperation(value = "删除一条数据", httpMethod = "POST",response = int.class)
	@ApiImplicitParams(
			{
					@ApiImplicitParam(paramType = "query",name = "id",value = "数据编号",required = true)
			}
	)
	@RequiresUser
	@RequiresPermissions(value = { "polar:backstage", "polar:tree:delete" })
	@ResMsg (tag = "删除一个树结构", type = ResType.JSON)
	@RequestMapping(value = "/deleteById", produces = "application/json;charset=utf-8")
	@ResponseBody
	public ResponseJson deleteById(@ApiIgnore String id) {
		idCheck(id,MODULE_NAME);
		Long result = treeService.deleteByIdPhysical(id);
		return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功删除了" + result + "条数据。");
	}

	/**
	 * 新增一条数据。
	 * 
	 * @param entity
	 *            数据
	 * @see polar.island.inlay.tree.entity.TreeEntity
	 * @return 新增结果。
	 */
	@ApiOperation(value = "新增树结构", httpMethod = "POST",response = int.class)
	@ApiImplicitParams(
			{
					@ApiImplicitParam(paramType = "query",name = "groupName",value = "组名",example = "性别",required = true),
					@ApiImplicitParam(paramType = "query",name = "groupId",value = "组编号",example = "SEX",required = true),
					@ApiImplicitParam(paramType = "query",name = "text",value = "文本内容",example = "男",required = true),
					@ApiImplicitParam(paramType = "query",name = "value",value = "值",example = "01",required = true),
					@ApiImplicitParam(paramType = "query",name = "textAlias",value = "别名",required = true),
					@ApiImplicitParam(paramType = "query",name = "textId",value = "编号",required = true),
					@ApiImplicitParam(paramType = "query",name = "parentId",value = "父级编号"),
					@ApiImplicitParam(paramType = "query",name = "type",value = "类型",required = true)
			}
	)
	@RequiresUser
	@RequiresPermissions(value = { "polar:backstage", "polar:tree:add" })
	@ResMsg (tag = "新增树结构", type = ResType.JSON)
	@RequestMapping(value = "/add", produces = "application/json;charset=utf-8")
	@ResponseBody
	public ResponseJson add(@ApiIgnore @Valid TreeEntity entity, BindingResult bindingResult) {
		validate(TreeEntity.class, "add",bindingResult);
		Map<String, Object> condition = beanToMap(entity);
		Object result = treeService.insert(condition);
		return new ResponseJson(Constants.CODE_SUCCESS, result, "新增成功。");
	}
}
