package polar.island.inlay.records.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import polar.island.core.config.Constants;
import polar.island.web.controller.BasicController;
import polar.island.core.entity.ResponseJson;
import polar.island.core.res.ResMsg;
import polar.island.core.res.ResType;
import polar.island.core.util.DateUtils;
import polar.island.inlay.records.entity.RecordsEntity;
import polar.island.inlay.records.service.RecordsService;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 访问记录的接口，返回数据全部为json串。
 * 
 * @author PolarLoves
 *
 */
@Controller(value = "recordsJsonController")
@RequestMapping(value = "/records/json")
@Api(tags = "访问记录接口", description = "访问记录接口")
public class RecordsJsonController extends BasicController {
	@Resource(name = "recordsService")
	private RecordsService recordsService;
	private  static final  String MODULE_NAME=null;
	/**
	 * 校验访问权限
	 * 
	 * @return
	 */
	@ApiOperation(value = "校验访问记录权限", httpMethod = "POST",response = Void.class)
	@RequiresUser
	@RequiresPermissions(value = { "polar:backstage", "polar:records:empty" })
	@ResMsg (tag = "校验访问记录权限", type = ResType.JSON,writeLogs = false)
	@RequestMapping(value = "/checkPermission", produces = "application/json;charset=utf-8")
	@ResponseBody
	public ResponseJson checkPermission() {
		return new ResponseJson(Constants.CODE_SUCCESS);
	}
	/**
	 * 依据条件查询分页数据，如果不传页码、每页条目，则默认分别为：1,10
	 * 
	 * @param entity
	 *            查询条件
	 * @see polar.island.inlay.records.entity.RecordsEntity
	 * @return 列表Json串。
	 */
	@ApiOperation(value = "分页查询访问记录", httpMethod = "POST",responseContainer = "List",response = RecordsEntity.class)
	@ApiImplicitParams(
			{
					@ApiImplicitParam(paramType = "query",name = "id",value = "编号",example = "1",dataType = "string"),
					@ApiImplicitParam(paramType = "query",name = "pageName",value = "页面名称"),
					@ApiImplicitParam(paramType = "query",name = "vistUrl",value = "访问地址"),
					@ApiImplicitParam(paramType = "query",name = "vistPlatform",value = "访问平台"),

					@ApiImplicitParam(paramType = "query",name = "page",value = "当前页,如不传,则默认为1",example = "1"),
					@ApiImplicitParam(paramType = "query",name = "rows",value = "每页的数据条目,如不传,则默认为10",example = "10"),
					@ApiImplicitParam(paramType = "query",name = "sort",value = "排序字段",example = "groupName"),
					@ApiImplicitParam(paramType = "query",name = "order",value = "排序方式，可选值有：ASC DESC,当sort字段传入值时,其必须传入",example = "DESC")
			}
	)
	@RequiresUser
	@RequiresPermissions(value = { "polar:backstage", "polar:records:list" })
	@ResMsg (tag = "查询访问记录", type = ResType.JSON,writeLogs = false)
	@RequestMapping(value = "/pageList", produces = "application/json;charset=utf-8")
	@ResponseBody
	public ResponseJson pageList(@ApiIgnore RecordsEntity entity) {
		Map<String, Object> condition = beanToMap(entity);
		if (entity.getVistSearchDate() != null) {
			try {
				String args[] = entity.getVistSearchDate().split("~");
				condition.put("startDate", DateUtils.parseDate(args[0].trim()));
				condition.put("endDate", DateUtils.parseDate(args[1].trim()));
			} catch (Exception e) {
			}
		}
		return new ResponseJson(Constants.CODE_SUCCESS, recordsService.selectPageList(condition),null,
				recordsService.selectCount(condition));
	}
	@ApiOperation(value = "查看访问记录详情", httpMethod = "POST",response = RecordsEntity.class)
	@ApiImplicitParams(
			{
					@ApiImplicitParam(paramType = "query",name = "id",value = "编号",example = "1",dataType = "string",required = true),
			}
	)
	@ResponseBody
	@RequiresUser
	@RequiresPermissions(value = { "polar:backstage", "polar:records:detail" })
	@ResMsg (tag = "查看访问记录", type = ResType.JSON,writeLogs = false)
	@RequestMapping(value = "/detail", produces = "application/json;charset=utf-8")
	public ResponseJson detail(@ApiIgnore String id) {
		idCheck(id,MODULE_NAME);
		RecordsEntity entity = recordsService.selectOneById(id);
		existCheck(entity,MODULE_NAME);
		return new ResponseJson(Constants.CODE_SUCCESS, entity);
	}
	/**
	 * 根据数据编号物理删除数据。
	 * 
	 * @param ids
	 *            数据编号
	 * @return 删除结果。
	 */
	@ApiOperation(value = "删除多条数据", httpMethod = "POST",response = int.class)
	@ApiImplicitParams(
			{
					@ApiImplicitParam(paramType = "query",name = "ids",value = "数据编号,其为一组编号，传入时，以ids[]为key传入，如删除id为1和2的数据时，传入：id[]=1&id[]=2",required = true)
			}
	)
	@RequiresUser
	@RequiresPermissions(value = { "polar:backstage", "polar:records:delete" })
	@ResMsg (tag = "删除多个访问记录", type = ResType.JSON,writeLogs = false)
	@RequestMapping(value = "/deleteMulitById", produces = "application/json;charset=utf-8")
	@ResponseBody
	public ResponseJson deleteMulitById(@ApiIgnore @RequestParam(value = "ids[]", required = false) String[] ids) {
		idCheck(ids,MODULE_NAME);
		Long result = recordsService.deleteMulitByIdPhysical(ids);
		return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功删除了" + result + "条数据。");
	}

	/**
	 * 根据数据编号物理删除数据。
	 * 
	 * @param id
	 *            数据编号
	 * @return 删除结果。
	 */
	@ApiOperation(value = "删除一条数据", httpMethod = "POST",response = int.class)
	@ApiImplicitParams(
			{
					@ApiImplicitParam(paramType = "query",name = "id",value = "数据编号",required = true)
			}
	)
	@RequiresUser
	@RequiresPermissions(value = { "polar:backstage", "polar:records:delete" })
	@ResMsg (tag = "删除一个访问记录", type = ResType.JSON,writeLogs = false)
	@RequestMapping(value = "/deleteById", produces = "application/json;charset=utf-8")
	@ResponseBody
	public ResponseJson deleteById(@ApiIgnore String id) {
		idCheck(id,MODULE_NAME);
		Long result = recordsService.deleteByIdPhysical(id);
		return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功删除了" + result + "条数据。");
	}

}
