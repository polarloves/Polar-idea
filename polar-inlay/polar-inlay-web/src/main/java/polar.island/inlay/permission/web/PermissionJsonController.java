package polar.island.inlay.permission.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import polar.island.core.config.Constants;
import polar.island.web.controller.BasicController;
import polar.island.core.entity.ResponseJson;
import polar.island.core.res.ResMsg;
import polar.island.core.res.ResType;
import polar.island.inlay.permission.entity.PermissionEntity;
import polar.island.inlay.permission.service.PermissionService;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * 权限的接口，返回数据全部为json串。
 *
 * @author PolarLoves
 */
@Controller(value = "permissionJsonController")
@RequestMapping(value = "/permission/json")
@Api(tags = "权限管理接口", description = "权限管理接口，其用于权限的增删改查")
public class PermissionJsonController extends BasicController {
    @Resource(name = "permissionService")
    private PermissionService permissionService;
    private static final String MODULE_NAME = null;

    /**
     * 校验访问权限
     *
     * @return
     */
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:permission:empty"})
    @ResMsg (tag = "校验权限管理权限", type = ResType.JSON,writeLogs = false)
    @RequestMapping(value = "/checkPermission", produces = "application/json;charset=utf-8")
    @ResponseBody
    @ApiOperation(value = "校验权限管理权限", httpMethod = "POST", response = Void.class)
    public ResponseJson checkPermission() {
        return new ResponseJson(Constants.CODE_SUCCESS, null);
    }

    /**
     * 依据条件查询数据
     * <p>
     * 查询条件
     *
     * @return 列表Json串。
     * @see polar.island.inlay.permission.entity.PermissionEntity
     */
    @ApiOperation(value = "查询权限", httpMethod = "POST", responseContainer = "List", response = PermissionEntity.class)
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:permission:view:list"})
    @ResMsg (tag = "查询权限", type = ResType.JSON,writeLogs = false)
    @RequestMapping(value = "/pageList", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson pageList() {
        Map<String, Object> condition = new HashMap<>();
        return new ResponseJson(Constants.CODE_SUCCESS, permissionService.selectList(condition), null,
                permissionService.selectCount(condition));
    }

    @ApiOperation(value = "查看权限详情", httpMethod = "POST", response = PermissionEntity.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query",name = "id", value = "编号", example = "1", dataType = "string", required = true),
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:permission:view:detail"})
    @ResMsg (tag = "查看权限", type = ResType.JSON,writeLogs = false)
    @RequestMapping(value = "/detail", produces = "application/json;charset=utf-8")
    public String detail(@ApiIgnore String id) {
        idCheck(id, MODULE_NAME);
        PermissionEntity entity = permissionService.selectOneById(id);
        existCheck(entity, MODULE_NAME);
        return "/view/inlay/permission/permissionForm.jsp";
    }

    /**
     * 依据数据编号更新此条数据的所有值。
     *
     * @param entity 需要更新的实体
     * @return 更新条目
     */
    @ApiOperation(value = "更新机构全部字段", httpMethod = "POST", response = int.class)
    @ApiImplicitParams(
            {

                    @ApiImplicitParam(paramType = "query",name = "id", value = "数据编号", required = true),
                    @ApiImplicitParam(paramType = "query",name = "name", value = "权限名称", required = true),
                    @ApiImplicitParam(paramType = "query",name = "text", value = "中文名称", required = true),
                    @ApiImplicitParam(paramType = "query",name = "info", value = "权限描述", required = true),
                    @ApiImplicitParam(paramType = "query",name = "orderNum", value = "排序号", required = true),
                    @ApiImplicitParam(paramType = "query",name = "parentId", value = "父级权限")
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:permission:edit"})
    @ResMsg (tag = "更新权限全部字段", type = ResType.JSON)
    @RequestMapping(value = "/updateAllById", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson update(@ApiIgnore @Valid PermissionEntity entity, BindingResult bindingResult) {
        validate(PermissionEntity.class, "updateAllById", bindingResult);
        Map<String, Object> condition = beanToMap(entity);
        Long result = permissionService.updateAll(condition);
        return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功修改了" + result + "条数据。");
    }

    /**
     * 依据数据编号更新此条数据的所有值。
     *
     * @param entity 需要更新的实体
     * @return 更新条目
     * @see polar.island.inlay.permission.entity.PermissionEntity
     */
    @ApiOperation(value = "更新机构单个字段", httpMethod = "POST", response = int.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query",name = "fieldName", value = "更新字段名称，如更新字段名称为\"name\"，则传入值时，其传入的key为:\"name\"", example = "1", required = true),
                    @ApiImplicitParam(paramType = "query",name = "id", value = "数据编号", required = true),
                    @ApiImplicitParam(paramType = "query",name = "name", value = "权限名称"),
                    @ApiImplicitParam(paramType = "query",name = "text", value = "中文名称"),
                    @ApiImplicitParam(paramType = "query",name = "info", value = "权限描述"),
                    @ApiImplicitParam(paramType = "query",name = "orderNum", value = "排序号"),
                    @ApiImplicitParam(paramType = "query",name = "parentId", value = "父级权限")
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:permission:edit"})
    @ResMsg (tag = "更新权限单个字段", type = ResType.JSON)
    @RequestMapping(value = "/updateField", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson updateField(@ApiIgnore @Valid PermissionEntity entity, BindingResult bindingResult) {
        validate(PermissionEntity.class, "updateField", bindingResult);
        Map<String, Object> condition = beanToSingleField(entity);
        condition.put("id", entity.getId());
        Long result = permissionService.updateField(condition);
        return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功修改了" + result + "条数据。");
    }

    /**
     * 根据数据编号物理删除数据。
     *
     * @param ids 数据编号
     * @return 删除结果。
     */
    @ApiOperation(value = "删除多条数据", httpMethod = "POST", response = int.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query",name = "ids", value = "数据编号,其为一组编号，传入时，以ids[]为key传入，如删除id为1和2的数据时，传入：id[]=1&id[]=2", required = true)
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:permission:delete"})
    @ResMsg (tag = "删除多个权限", type = ResType.JSON)
    @RequestMapping(value = "/deleteMulitById", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson deleteMulitById(@ApiIgnore @RequestParam(value = "ids[]", required = false) String[] ids) {
        idCheck(ids, MODULE_NAME);
        Long result = permissionService.deleteMulitByIdPhysical(ids);
        return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功删除了" + result + "条数据。");
    }

    /**
     * 根据数据编号物理删除数据。
     *
     * @param id 数据编号
     * @return 删除结果。
     */
    @ApiOperation(value = "删除一条数据", httpMethod = "POST", response = int.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query",name = "id", value = "数据编号", required = true)
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:permission:delete"})
    @ResMsg (tag = "删除一个权限", type = ResType.JSON)
    @RequestMapping(value = "/deleteById", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson deleteById(@ApiIgnore String id) {
        idCheck(id, MODULE_NAME);
        Long result = permissionService.deleteByIdPhysical(id);
        return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功删除了" + result + "条数据。");
    }

    /**
     * 新增一条数据。
     *
     * @param entity 数据
     * @return 新增结果。
     * @see polar.island.inlay.permission.entity.PermissionEntity
     */
    @ApiOperation(value = "新增权限", httpMethod = "POST", response = int.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query",name = "name", value = "权限名称", required = true),
                    @ApiImplicitParam(paramType = "query",name = "text", value = "中文名称", required = true),
                    @ApiImplicitParam(paramType = "query",name = "info", value = "权限描述", required = true),
                    @ApiImplicitParam(paramType = "query",name = "orderNum", value = "排序号", required = true),
                    @ApiImplicitParam(paramType = "query",name = "parentId", value = "父级权限")
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:permission:add"})
    @ResMsg (tag = "新增权限", type = ResType.JSON)
    @RequestMapping(value = "/add", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson add(@ApiIgnore @Valid PermissionEntity entity, BindingResult bindingResult) {
        validate(PermissionEntity.class, "add", bindingResult);
        Map<String, Object> condition = beanToMap(entity);
        Object result = permissionService.insert(condition);
        return new ResponseJson(Constants.CODE_SUCCESS, result, "新增成功。");
    }

    @ApiOperation(value = "修改角色权限", httpMethod = "POST", response = int.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query",name = "id", value = "角色编号", required = true),
                    @ApiImplicitParam(paramType = "query",name = "permissionIds[]", value = "权限编号", required = true),
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:role:permission"})
    @ResMsg (tag = "修改角色权限", type = ResType.JSON)
    @RequestMapping(value = "/updateRolePermissions", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson updateRolePermissions(@ApiIgnore String id,
                                              @ApiIgnore @RequestParam(value = "permissionIds[]", required = false) String[] permissionIds) {
        idCheck(id, MODULE_NAME);
        permissionService.updateRolePermissions(id, permissionIds);
        return new ResponseJson(Constants.CODE_SUCCESS, null, "角色权限分配成功");
    }

    @ApiOperation(value = "修改部门权限", httpMethod = "POST", response = int.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query",name = "id", value = "部门编号", required = true),
                    @ApiImplicitParam(paramType = "query",name = "permissionIds[]", value = "权限编号", required = true),
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:org:permission"})
    @ResMsg (tag = "修改部门权限", type = ResType.JSON)
    @RequestMapping(value = "/updateOrgPermissions", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson updateOrgPermissions(@ApiIgnore String id,
                                             @ApiIgnore @RequestParam(value = "permissionIds[]", required = false) String[] permissionIds) {
        idCheck(id, MODULE_NAME);
        permissionService.updateOrgPermissions(id, permissionIds);
        return new ResponseJson(Constants.CODE_SUCCESS, null, "部门权限分配成功");
    }
}
