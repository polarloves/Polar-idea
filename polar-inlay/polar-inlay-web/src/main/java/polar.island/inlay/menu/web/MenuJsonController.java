package polar.island.inlay.menu.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import polar.island.core.config.Constants;
import polar.island.web.controller.BasicController;
import polar.island.core.entity.ResponseJson;
import polar.island.core.res.ResMsg;
import polar.island.core.res.ResType;
import polar.island.inlay.menu.entity.MenuEntity;
import polar.island.inlay.menu.service.MenuService;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.Map;

/**
 * 菜单的接口，返回数据全部为json串。
 *
 * @author PolarLoves
 */
@Api(tags = "菜单接口", description = "菜单接口，其用于菜单的增删改查")
@Controller(value = "menuJsonController")
@RequestMapping(value = "/menu/json")
public class MenuJsonController extends BasicController {
    @Resource(name = "menuService")
    private MenuService menuService;
    private static final String MODULE_NAME = "菜单";

    /**
     * 校验访问权限
     *
     * @return
     */
    @ApiOperation(value = "校验菜单权限", httpMethod = "POST",response = Void.class)
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:menu:empty"})
    @ResMsg (tag = "校验菜单权限", type = ResType.JSON,writeLogs = false)
    @RequestMapping(value = "/checkPermission", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson checkPermission() {
        return new ResponseJson(Constants.CODE_SUCCESS);
    }

    /**
     * 依据条件查询数据
     *
     * @param entity 查询条件
     * @return 列表Json串。
     * @see polar.island.inlay.menu.entity.MenuEntity
     */
    @ApiOperation(value = "查询菜单分页列表", httpMethod = "POST",responseContainer = "List",response = MenuEntity.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query",name = "id",value = "编号",example = "1",dataType = "string"),
                    @ApiImplicitParam(paramType = "query",name = "name",value = "菜单名称"),
                    @ApiImplicitParam(paramType = "query",name = "path",value = "菜单访问路径"),
                    @ApiImplicitParam(paramType = "query",name = "orderNum",value = "菜单排序号"),
                    @ApiImplicitParam(paramType = "query",name = "parentId",value = "父级菜单编号"),
                    @ApiImplicitParam(paramType = "query",name = "page",value = "当前页,如不传,则默认为1",example = "1"),
                    @ApiImplicitParam(paramType = "query",name = "rows",value = "每页的数据条目,如不传,则默认为10",example = "10"),
                    @ApiImplicitParam(paramType = "query",name = "sort",value = "排序字段",example = "groupName"),
                    @ApiImplicitParam(paramType = "query",name = "order",value = "排序方式，可选值有：ASC DESC,当sort字段传入值时,其必须传入",example = "DESC")
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:menu:view:list"})
    @ResMsg (tag = "查询菜单", type = ResType.JSON,writeLogs = false)
    @RequestMapping(value = "/pageList", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson pageList(@ApiIgnore MenuEntity entity) {
        Map<String, Object> condition = beanToMap(entity);
        return new ResponseJson(Constants.CODE_SUCCESS, menuService.selectList(condition), null,
                menuService.selectCount(condition));
    }
    @ApiOperation(value = "查询菜单分页详情", httpMethod = "POST",response = MenuEntity.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query",name = "id",value = "编号",example = "1",dataType = "string"),
            }
    )
    @ResponseBody
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:menu:view:detail"})
    @ResMsg (tag = "查看菜单", type = ResType.JSON)
    @RequestMapping(value = "/detail", produces = "application/json;charset=utf-8")
    public ResponseJson detail(@ApiIgnore String id) {
        idCheck(id, MODULE_NAME);
        MenuEntity entity = menuService.selectOneById(id);
        existCheck(entity, MODULE_NAME);
        return new ResponseJson(Constants.CODE_SUCCESS, entity);
    }
    @ApiOperation(value = "更新菜单全部字段", httpMethod = "POST",response = int.class)
    @ApiImplicitParams(
            {

                    @ApiImplicitParam(paramType = "query",name = "id",value = "数据编号",required = true),
                    @ApiImplicitParam(paramType = "query",name = "name",value = "菜单名称"),
                    @ApiImplicitParam(paramType = "query",name = "path",value = "菜单访问路径"),
                    @ApiImplicitParam(paramType = "query",name = "orderNum",value = "菜单排序号"),
                    @ApiImplicitParam(paramType = "query",name = "parentId",value = "父级菜单编号"),
                    @ApiImplicitParam(paramType = "query",name = "icon",value = "菜单图标")
            }
    )
    /**
     * 依据数据编号更新此条数据的所有值。
     *
     * @param entity 需要更新的实体
     * @return 更新条目
     * @see polar.island.inlay.menu.entity.MenuEntity
     */
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:menu:edit"})
    @ResMsg (tag = "更新菜单全部字段", type = ResType.JSON)
    @RequestMapping(value = "/updateAllById", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson update(@ApiIgnore @Valid MenuEntity entity, BindingResult bindingResult) {
        validate(MenuEntity.class, "updateAllById", bindingResult);
        Map<String, Object> condition = beanToMap(entity);
        Long result = menuService.updateAll(condition);
        return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功修改了" + result + "条数据。");
    }

    /**
     * 依据数据编号更新此条数据的所有值。
     *
     * @param entity 需要更新的实体
     * @return 更新条目
     * @see polar.island.inlay.menu.entity.MenuEntity
     */
    @ApiOperation(value = "更新菜单单个字段", httpMethod = "POST",response = int.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query",name = "fieldName",value = "更新字段名称，如更新字段名称为\"name\"，则传入值时，其传入的key为:\"name\"",example = "1",required = true),
                    @ApiImplicitParam(paramType = "query",name = "id",value = "数据编号",required = true),
                    @ApiImplicitParam(paramType = "query",name = "name",value = "菜单名称"),
                    @ApiImplicitParam(paramType = "query",name = "path",value = "菜单访问路径"),
                    @ApiImplicitParam(paramType = "query",name = "orderNum",value = "菜单排序号"),
                    @ApiImplicitParam(paramType = "query",name = "parentId",value = "父级菜单编号"),
                    @ApiImplicitParam(paramType = "query",name = "icon",value = "菜单图标")
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:menu:edit"})
    @ResMsg (tag = "更新菜单单个字段", type = ResType.JSON)
    @RequestMapping(value = "/updateField", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson updateField(@ApiIgnore MenuEntity entity, BindingResult bindingResult) {
        validate(MenuEntity.class, "updateField", bindingResult);
        Map<String, Object> condition = beanToSingleField(entity);
        condition.put("id", entity.getId());
        Long result = menuService.updateField(condition);
        return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功修改了" + result + "条数据。");
    }
    /**
     * 根据数据编号物理删除数据。
     *
     * @param ids 数据编号
     * @return 删除结果。
     */
    @ApiOperation(value = "删除多条数据", httpMethod = "POST",response = int.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query",name = "ids",value = "数据编号,其为一组编号，传入时，以ids[]为key传入，如删除id为1和2的数据时，传入：id[]=1&id[]=2",required = true)
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:menu:delete"})
    @ResMsg (tag = "删除多个菜单", type = ResType.JSON)
    @RequestMapping(value = "/deleteMulitById", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson deleteMulitById(@ApiIgnore @RequestParam(value = "ids[]", required = false) String[] ids) {
        idCheck(ids, MODULE_NAME);
        Long result = menuService.deleteMulitByIdPhysical(ids);
        return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功删除了" + result + "条数据。");
    }

    /**
     * 根据数据编号物理删除数据。
     *
     * @param id 数据编号
     * @return 删除结果。
     */
    @ApiOperation(value = "删除一条数据", httpMethod = "POST",response = int.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query",name = "id",value = "数据编号",required = true)
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:menu:delete"})
    @ResMsg (tag = "删除一个菜单", type = ResType.JSON)
    @RequestMapping(value = "/deleteById", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson deleteById(@ApiIgnore String id) {
        idCheck(id, MODULE_NAME);
        Long result = menuService.deleteByIdPhysical(id);
        return new ResponseJson(Constants.CODE_SUCCESS, result, "您成功删除了" + result + "条数据。");
    }

    /**
     * 新增一条数据。
     *
     * @param entity 菜单数据
     * @return 新增结果。
     * @see polar.island.inlay.menu.entity.MenuEntity
     */
    @ApiOperation(value = "新增菜单", httpMethod = "POST",response = int.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query",name = "name",value = "菜单名称"),
                    @ApiImplicitParam(paramType = "query",name = "path",value = "菜单访问路径"),
                    @ApiImplicitParam(paramType = "query",name = "orderNum",value = "菜单排序号"),
                    @ApiImplicitParam(paramType = "query",name = "parentId",value = "父级菜单编号"),
                    @ApiImplicitParam(paramType = "query",name = "icon",value = "菜单图标")
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:menu:add"})
    @ResMsg (tag = "新增菜单", type = ResType.JSON)
    @RequestMapping(value = "/add", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson add(@ApiIgnore @Valid MenuEntity entity, BindingResult bindingResult) {
        validate(MenuEntity.class, "add", bindingResult);
        Map<String, Object> condition = beanToMap(entity);
        Object result = menuService.insert(condition);
        return new ResponseJson(Constants.CODE_SUCCESS, result, "新增成功。");
    }
    @ApiOperation(value = "修改菜单模板中的菜单", httpMethod = "POST",response = int.class)
    @ApiImplicitParams(
            {
                    @ApiImplicitParam(paramType = "query",name = "modelId",value = "菜单模板编号",required = true),
                    @ApiImplicitParam(paramType = "query",name = "menuId[]",value = "菜单编号",required = true)
            }
    )
    @RequiresUser
    @RequiresPermissions(value = {"polar:backstage", "polar:menumodel:menu"})
    @ResMsg (tag = "修改模板菜单", type = ResType.JSON)
    @RequestMapping(value = "/updateModelMenus", produces = "application/json;charset=utf-8")
    @ResponseBody
    public ResponseJson updateUserRole(@ApiIgnore String modelId,@ApiIgnore  @RequestParam(value = "menuId[]", required = false) String[] menuId) {
        idCheck(modelId, null);
        menuService.updateModelMenus(modelId, menuId);
        return new ResponseJson(Constants.CODE_SUCCESS, null, "模板菜单分配成功");
    }

}
