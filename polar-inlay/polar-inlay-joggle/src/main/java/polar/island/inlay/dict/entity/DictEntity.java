package polar.island.inlay.dict.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.ibatis.type.Alias;
import polar.island.core.entity.BasicEntity;
import polar.island.core.validator.TAG;

import javax.validation.constraints.NotNull;

/**
 * 字典的实体类。 字典数据
 *
 * @author PolarLoves
 *
 */
@Alias(value = "DictEntity")
@ApiModel(value = "polar.island.inlay.dict.entity.DictEntity", description = "字典信息")
public class DictEntity extends BasicEntity {
	private static final long serialVersionUID = -3021051445418572480L;
	/** 主键编号 **/
	@ApiModelProperty(value = "数据编号",example = "1")
	@TAG(value = { "updateAllById", "updateField" })
	@NotNull(message = "编号不能为空")
	private Long id;
	/** 组名 **/
	@ApiModelProperty(value = "字典所属组组名",example = "性别")
	private String groupName;
	/** 组编号 **/
	@ApiModelProperty(value = "字典所属组组名编号",example = "SEX")
	@TAG(value = { "updateAllById", "add" })
	@NotNull(message = "所属组不能为空")
	private String groupId;
	/** 文本内容 **/
	@ApiModelProperty(value = "字典内容",example = "男")
	@TAG(value = { "updateAllById", "add" })
	@NotNull(message = "文本内容不能为空")
	private String text;
	/** 值 **/
	@ApiModelProperty(value = "字典值",example = "1")
	@TAG(value = { "updateAllById", "add" })
	@NotNull(message = "值不能为空")
	private String value;
	/** 备注 **/
	@ApiModelProperty(value = "字典备注",example = "")
	private String remark;
	/** 排序号 **/
	@ApiModelProperty(value = "字典排序号",example = "1")
	@TAG(value = { "updateAllById", "add" })
	@NotNull(message = "排序号不能为空")
	private Long orderNum;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Long getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(Long orderNum) {
		this.orderNum = orderNum;
	}
}