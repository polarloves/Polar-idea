package polar.island.inlay.logs.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.ibatis.type.Alias;
import polar.island.core.entity.BasicEntity;
import polar.island.core.validator.TAG;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * 日志记录表的实体类。 用来记录通用产出的错误信息。
 *
 * @author PolarLoves
 *
 */
@Alias(value = "LogsEntity")
@ApiModel(value = "polar.island.inlay.logs.entity.LogsEntity", description = "错误日志信息")
public class LogsEntity extends BasicEntity {
	private static final long serialVersionUID = -4611262567428426961L;
	/** 主键编号 **/
	@ApiModelProperty(value = "数据编号",example = "1")
	@TAG(value = { "updateAllById", "updateField" })
	@NotNull(message = "编号不能为空")
	private Long id;
	/** 当前登录用户 **/
	@ApiModelProperty(value = "操作用户",example = "1")
	private String userId;
	/** 创建时间 **/
	@ApiModelProperty(value = "操作时间",example = "1")
	private Date createTime;
	/** 接口名称 **/
	@ApiModelProperty(value = "接口名称",example = "1")
	private String interfaceName;
	/** 错误信息 **/
	@ApiModelProperty(value = "错误信息",example = "1")
	private String message;
	/** 引起的原因 **/
	@ApiModelProperty(value = "引起的原因,列表不返回",example = "1")
	private String caseBy;
	/** 创建时间毫秒 **/
	@ApiModelProperty(hidden = true)
	private Long createTimeMillions;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getInterfaceName() {
		return interfaceName;
	}

	public void setInterfaceName(String interfaceName) {
		this.interfaceName = interfaceName;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getCaseBy() {
		return caseBy;
	}

	public void setCaseBy(String caseBy) {
		this.caseBy = caseBy;
	}

	public Long getCreateTimeMillions() {
		return createTimeMillions;
	}

	public void setCreateTimeMillions(Long createTimeMillions) {
		this.createTimeMillions = createTimeMillions;
	}
}