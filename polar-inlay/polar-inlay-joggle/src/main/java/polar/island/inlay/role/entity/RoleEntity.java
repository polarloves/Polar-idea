package polar.island.inlay.role.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.ibatis.type.Alias;
import polar.island.core.entity.BasicEntity;
import polar.island.core.validator.TAG;

import javax.validation.constraints.NotNull;

/**
 * 角色的实体类。 角色管理
 *
 * @author PolarLoves
 *
 */
@Alias(value = "RoleEntity")
@ApiModel(value = "polar.island.inlay.role.entity.RoleEntity", description = "角色信息")
public class RoleEntity extends BasicEntity {
	private static final long serialVersionUID = -1873840638467309305L;
	/** 主键编号 **/
	@ApiModelProperty(value = "数据编号",example = "1")
	@TAG(value = { "updateAllById", "updateField" })
	@NotNull(message = "编号不能为空")
	private Long id;
	/** 父类id **/
	@ApiModelProperty(value = "父级角色编号")
	private Long parentId;
	/** 角色名称 **/
	@TAG(value = { "updateAllById", "add" })
	@NotNull(message = "角色名称不能为空")
	@ApiModelProperty(value = "角色名称")
	private String name;
	/** 角色中文名称 **/
	@TAG(value = { "updateAllById", "add" })
	@NotNull(message = "角色中文名称不能为空")
	@ApiModelProperty(value = "角色中文名称")
	private String text;
	/** 角色描述 **/
	@ApiModelProperty(value = "角色描述")
	private String info;
	/** 排序 **/
	@ApiModelProperty(value = "排序号")
	@TAG(value = { "updateAllById", "add" })
	@NotNull(message = "排序不能为空")
	private Long orderNum;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public Long getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(Long orderNum) {
		this.orderNum = orderNum;
	}
}