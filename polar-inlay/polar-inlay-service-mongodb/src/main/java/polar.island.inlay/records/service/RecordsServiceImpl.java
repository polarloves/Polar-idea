package polar.island.inlay.records.service;

import org.springframework.stereotype.Service;
import polar.island.core.util.ExceptionUtil;
import polar.island.inlay.records.dao.RecordsDao;
import polar.island.inlay.records.entity.RecordsEntity;
import polar.island.mongo.service.MongoBasicService;

import javax.annotation.Resource;
import java.util.Map;

@Service(value = "recordsService")
public class RecordsServiceImpl extends MongoBasicService<RecordsEntity, RecordsEntity, RecordsDao> implements RecordsService {
    @Resource(name = "recordsDao")
    private RecordsDao recordsDao;

    @Override
    public RecordsDao getDao() {
        return recordsDao;
    }

    @Override
    public Object insert(Map<String, Object> condition) {
        try {
            recordsDao.insert(condition);
            return condition.get("id");
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }
}