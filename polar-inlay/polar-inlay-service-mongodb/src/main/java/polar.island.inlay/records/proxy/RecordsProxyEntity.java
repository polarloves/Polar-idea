package polar.island.inlay.records.proxy;

import org.springframework.data.annotation.Id;
import polar.island.core.dao.annotation.AutoIncrease;
import polar.island.core.dao.annotation.ClassConfig;
import polar.island.core.dao.annotation.FieldConfig;
import polar.island.core.dao.annotation.MatchStyle;
import polar.island.core.dao.types.Style;
import polar.island.core.dao.types.StyleType;
import polar.island.inlay.records.entity.RecordsEntity;

import java.util.Date;

@ClassConfig(tableName = polar.island.inlay.config.Table.INLAY_RECORDS, listClass = RecordsEntity.class, detailClass = RecordsEntity.class)
public class RecordsProxyEntity {
    /** 主键编号 **/
    @Id
    @AutoIncrease
    @FieldConfig
    private Long id;
    /** 页面名称 **/
    @FieldConfig(styles = {   //作为查询列表时的条件
            @MatchStyle(type = StyleType.SELECT_MULTI,matchStyle = Style.LIKE),
            //作为查询单个时的条件
            @MatchStyle(type = StyleType.SELECT_ONE),
            //作为删除时的条件
            @MatchStyle(type = StyleType.DELETE),
            //作为更新时的条件
            @MatchStyle(type = StyleType.UPDATE)})
    private String pageName;
    /** 访问路径 **/
    @FieldConfig(styles = {   //作为查询列表时的条件
            @MatchStyle(type = StyleType.SELECT_MULTI,matchStyle = Style.LIKE),
            //作为查询单个时的条件
            @MatchStyle(type = StyleType.SELECT_ONE),
            //作为删除时的条件
            @MatchStyle(type = StyleType.DELETE),
            //作为更新时的条件
            @MatchStyle(type = StyleType.UPDATE)})
    private String vistUrl;
    /** 访问平台 **/
    @FieldConfig
    private Integer vistPlatform;
    /** 访问时间 **/
    @FieldConfig
    private Date vistDate;
    /** 访问人 **/
    @FieldConfig(styles = {   //作为查询列表时的条件
            @MatchStyle(type = StyleType.SELECT_MULTI,matchStyle = Style.LIKE),
            //作为查询单个时的条件
            @MatchStyle(type = StyleType.SELECT_ONE),
            //作为删除时的条件
            @MatchStyle(type = StyleType.DELETE),
            //作为更新时的条件
            @MatchStyle(type = StyleType.UPDATE)})
    private String vistPeople;
    /** 访问Ip **/
    @FieldConfig(styles = {   //作为查询列表时的条件
            @MatchStyle(type = StyleType.SELECT_MULTI,matchStyle = Style.LIKE),
            //作为查询单个时的条件
            @MatchStyle(type = StyleType.SELECT_ONE),
            //作为删除时的条件
            @MatchStyle(type = StyleType.DELETE),
            //作为更新时的条件
            @MatchStyle(type = StyleType.UPDATE)})
    private String vistIp;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public String getVistUrl() {
        return vistUrl;
    }

    public void setVistUrl(String vistUrl) {
        this.vistUrl = vistUrl;
    }

    public Integer getVistPlatform() {
        return vistPlatform;
    }

    public void setVistPlatform(Integer vistPlatform) {
        this.vistPlatform = vistPlatform;
    }

    public Date getVistDate() {
        return vistDate;
    }

    public void setVistDate(Date vistDate) {
        this.vistDate = vistDate;
    }

    public String getVistPeople() {
        return vistPeople;
    }

    public void setVistPeople(String vistPeople) {
        this.vistPeople = vistPeople;
    }

    public String getVistIp() {
        return vistIp;
    }

    public void setVistIp(String vistIp) {
        this.vistIp = vistIp;
    }
}
