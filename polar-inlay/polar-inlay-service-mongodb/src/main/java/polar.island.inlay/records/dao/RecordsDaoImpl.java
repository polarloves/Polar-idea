package polar.island.inlay.records.dao;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import polar.island.inlay.records.entity.RecordsEntity;
import polar.island.inlay.records.proxy.RecordsProxyEntity;
import polar.island.mongo.dao.MongoBasicDao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component(value = "recordsDao")
public class RecordsDaoImpl extends MongoBasicDao<RecordsEntity, RecordsEntity> implements RecordsDao {


    @Override
    public Class<?> proxyClass() {
        return RecordsProxyEntity.class;
    }

    @Override
    public void insert(Map<String, Object> condition) {
        super.insert(condition);
    }

    @Override
    public List<RecordsEntity> convertToList(List source) {
        List<RecordsEntity> result = new ArrayList<>();
        for (RecordsProxyEntity entity : (List<RecordsProxyEntity>) source) {
            RecordsEntity recordsEntity = convertToDetail(entity);
            result.add(recordsEntity);
        }
        return result;
    }

    @Override
    public RecordsEntity convertToDetail(Object source) {
        RecordsEntity entity = super.convertToDetail(source);
        return entity;
    }

    @Override
    public Query selectListQuery(Map<String, Object> condition) {
        Query query = super.selectListQuery(condition);
        boolean ok = false;
        Criteria criteria = Criteria.where("vistDate");
        if (condition.get("startDate") != null) {
            criteria = criteria.gt(condition.get("startDate"));
            ok = true;
        }
        if (condition.get("endDate") != null) {
            criteria = criteria.lt(condition.get("endDate"));
            ok = true;
        }
        if (ok) {
            query.addCriteria(criteria);
        }
        return query;
    }

    @Override
    public List<RecordsEntity> selectPageList(Map<String, Object> condition) {
        return super.selectPageList(condition);
    }
}
