package polar.island.inlay.permission.proxy;

import org.springframework.data.annotation.Id;
import polar.island.core.dao.annotation.AutoIncrease;
import polar.island.core.dao.annotation.ClassConfig;
import polar.island.core.dao.annotation.FieldConfig;
import polar.island.inlay.permission.entity.PermissionEntity;

@ClassConfig(tableName = polar.island.inlay.config.Table.INLAY_PERMISSION, listClass = PermissionEntity.class, detailClass = PermissionEntity.class)
public class PermissionProxyEntity {
    /** 注解编号 **/
    @Id
    @AutoIncrease
    @FieldConfig
    private Long id;
    /** 权限名称  **/
    @FieldConfig
    private String name;
    /** 中文名称  **/
    @FieldConfig
    private String text;
    /** 权限描述  **/
    @FieldConfig
    private String info;
    /** 排序号  **/
    @FieldConfig
    private Long orderNum;
    /** 父类编号  **/
    @FieldConfig
    private Long parentId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Long getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Long orderNum) {
        this.orderNum = orderNum;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }
}
