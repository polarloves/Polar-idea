package polar.island.inlay.permission.dao;


import org.bson.Document;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import polar.island.core.entity.TreeEntity;
import polar.island.core.exception.ValidationException;
import polar.island.core.util.CommonUtil;
import polar.island.core.util.EntityUtil;
import polar.island.inlay.org.proxy.OrgProxyEntity;
import polar.island.inlay.permission.entity.PermissionEntity;
import polar.island.inlay.permission.proxy.PermissionProxyEntity;
import polar.island.inlay.resource.proxy.ResourceProxyEntity;
import polar.island.inlay.role.proxy.RoleProxyEntity;
import polar.island.inlay.util.SortUtil;
import polar.island.mongo.dao.MongoBasicDao;
import polar.island.mongo.util.MongoDBUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository(value = "permissionDao")
public class PermissionDaoImpl extends MongoBasicDao<PermissionEntity, PermissionEntity> implements PermissionDao {
    @Override
    public Class<?> proxyClass() {
        return PermissionProxyEntity.class;
    }

    @Override
    public List<PermissionEntity> convertToList(List source) {
        List<PermissionEntity> result= super.convertToList(source);
        SortUtil.sortPermission(result);
        return result;
    }

    @Override
    public List<TreeEntity> allPermission() {
        Aggregation agg = Aggregation.newAggregation(
                Aggregation.project("id", "name", "parentId", "orderNum", "text"),
                Aggregation.sort(Sort.Direction.ASC, "parentId", "orderNum")
        );
        List<TreeEntity> details = new ArrayList<TreeEntity>();
        AggregationResults<Document> outputType = mongoTemplate.aggregate(agg, tableName(), Document.class);
        List<Document> resultList = outputType.getMappedResults();

        for (Document obj : resultList) {
            TreeEntity entity = new TreeEntity();
            entity.setText(obj.getString("text"));
            entity.setId(CommonUtil.valueOf(obj.getLong("_id")));
            entity.setParentId(CommonUtil.valueOf(obj.getLong("parentId")));
            entity.setValue(obj.getString("name"));
            entity.setOrderNum(obj.getLong("orderNum"));
            details.add(entity);
        }
        SortUtil.sortTree(details);
        return details;
    }

    @Override
    public Long deleteByConditionPhysical(Map<String, Object> condition) {
        throw new ValidationException("无法根据条件删除数据", null);
    }

    public void updateCollectionPermissions(Object collectionId, String[] permissionIds, String collectionName) {
        List<Long> ids = new ArrayList<>();
        List<PermissionProxyEntity> menuProxyEntities = new ArrayList<>();
        if (permissionIds != null && permissionIds.length > 0) {
            for (String id : permissionIds) {
                ids.add(Long.parseLong(id));
            }
            Query query = listQueryFields().addCriteria(Criteria.where("_id").in(ids));
            menuProxyEntities = mongoTemplate.find(query, PermissionProxyEntity.class, polar.island.inlay.config.Table.INLAY_PERMISSION);
        }

        Query query = new Query(Criteria.where("_id").is(collectionId));
        Update update = new Update();
        update.set("permissions", menuProxyEntities);
        mongoTemplate.updateMulti(query, update, collectionName);
    }

    public void updateRolePermissions(String roleId, String[] permissionIds) {
        updateCollectionPermissions(Long.parseLong(roleId), permissionIds, polar.island.inlay.config.Table.INLAY_ROLE);
    }

    @Override
    public void updateResourcePermissions(String resourceId, String[] permissionIds) {
        updateCollectionPermissions(Long.parseLong(resourceId), permissionIds, polar.island.inlay.config.Table.INLAY_RESOURCES);
    }

    @Override
    public void updateOrgPermissions(String orgId, String[] permissionIds) {
        updateCollectionPermissions(Long.parseLong(orgId), permissionIds, polar.island.inlay.config.Table.INLAY_ORG);
    }

    @Override
    public Long deleteByIdPhysical(String id) {
        //删除角色-权限
        MongoDBUtil.deleteCollectionItem(mongoTemplate, polar.island.inlay.config.Table.INLAY_ROLE, "permissions", Long.parseLong(id));
        //删除部门-权限
        MongoDBUtil.deleteCollectionItem(mongoTemplate, polar.island.inlay.config.Table.INLAY_ORG, "permissions", Long.parseLong(id));
        //删除资源-权限
        MongoDBUtil.deleteCollectionItem(mongoTemplate, polar.island.inlay.config.Table.INLAY_RESOURCES, "permissions", Long.parseLong(id));

        return super.deleteByIdPhysical(id);
    }

    @Override
    public Long updateAll(Map<String, Object> condition) {
        Long result=super.updateAll(condition);
        Object id = realIdValue(findId(condition));
        PermissionEntity entity = selectOneById(id.toString());
        updatePermission(condition, polar.island.inlay.config.Table.INLAY_ROLE, entity);
        updatePermission(condition, polar.island.inlay.config.Table.INLAY_ORG, entity);
        updatePermission(condition, polar.island.inlay.config.Table.INLAY_RESOURCES, entity);
        return result;
    }

    @Override
    public Long updateField(Map<String, Object> condition) {
        Long result=super.updateField(condition);
        Object id = realIdValue(findId(condition));
        PermissionEntity entity = selectOneById(id.toString());
        updatePermission(condition, polar.island.inlay.config.Table.INLAY_ROLE, entity);
        updatePermission(condition, polar.island.inlay.config.Table.INLAY_ORG, entity);
        updatePermission(condition, polar.island.inlay.config.Table.INLAY_RESOURCES, entity);
        return result;
    }


    private void updatePermission(Map<String, Object> condition, String collectionName, PermissionEntity entity) {
        Object id = realIdValue(findId(condition));
        Update update = new Update();
        update.set("permissions.$.name", entity.getName());
        update.set("permissions.$.text", entity.getText());
        update.set("permissions.$.info", entity.getInfo());
        update.set("permissions.$.orderNum", entity.getOrderNum());
        update.set("permissions.$.parentId", entity.getParentId());
        mongoTemplate.updateMulti(Query.query(Criteria.where("permissions._id").is(id)), update, collectionName);
    }

    @Override
    public List<PermissionEntity> rolePermissions(String roleId) {
        List<PermissionEntity> result = null;
        RoleProxyEntity menuModelProxyEntity = mongoTemplate.findOne(Query.query(Criteria.where("_id").is(Long.parseLong(roleId))), RoleProxyEntity.class, polar.island.inlay.config.Table.INLAY_ROLE);
        if (menuModelProxyEntity != null) {
            if (menuModelProxyEntity.getPermissions() != null) {
                result = EntityUtil.copyArray(menuModelProxyEntity.getPermissions(), PermissionEntity.class);
            }
        }
        if (result == null) {
            result = new ArrayList<PermissionEntity>();
        }
        SortUtil.sortPermission(result);
        return result;

    }

    @Override
    public List<PermissionEntity> orgPermissions(String orgId) {
        List<PermissionEntity> result = null;
        OrgProxyEntity menuModelProxyEntity = mongoTemplate.findOne(Query.query(Criteria.where("_id").is(Long.parseLong(orgId))), OrgProxyEntity.class, polar.island.inlay.config.Table.INLAY_ORG);
        if (menuModelProxyEntity != null) {
            if (menuModelProxyEntity.getPermissions() != null) {
                result = EntityUtil.copyArray(menuModelProxyEntity.getPermissions(), PermissionEntity.class);
            }
        }
        if (result == null) {
            result = new ArrayList<PermissionEntity>();
        }
        SortUtil.sortPermission(result);
        return result;
    }

    @Override
    public List<PermissionEntity> resourcePermissions(String resourceId) {
        List<PermissionEntity> result = null;
        ResourceProxyEntity menuModelProxyEntity = mongoTemplate.findOne(Query.query(Criteria.where("_id").is(Long.parseLong(resourceId))), ResourceProxyEntity.class, polar.island.inlay.config.Table.INLAY_RESOURCES);
        if (menuModelProxyEntity != null) {
            if (menuModelProxyEntity.getPermissions() != null) {
                result = EntityUtil.copyArray(menuModelProxyEntity.getPermissions(), PermissionEntity.class);
            }
        }
        if (result == null) {
            result = new ArrayList<PermissionEntity>();
        }
        SortUtil.sortPermission(result);
        return result;
    }


}
