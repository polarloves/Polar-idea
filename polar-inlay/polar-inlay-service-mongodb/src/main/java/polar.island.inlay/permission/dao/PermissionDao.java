package polar.island.inlay.permission.dao;

import polar.island.core.dao.BasicDao;
import polar.island.inlay.permission.entity.PermissionEntity;

import java.util.List;
import java.util.Map;

/**
 * 权限的持久化层。
 *
 * @author PolarLoves
 */

public interface PermissionDao extends BasicDao<PermissionEntity, PermissionEntity> {
    /**
     * 由于仅有物理删除，此删除方法被移除
     */
    @Deprecated
    public Long deleteByIdLogic(String id);

    /**
     * 由于仅有物理删除，此删除方法被移除
     */
    @Deprecated
    public Long deleteByConditionLogic(Map<String, Object> condition);

    /**
     * 修改角色-权限
     *
     * @param roleId        角色编号
     * @param permissionIds 权限编号
     */
    public void updateRolePermissions(String roleId, String[] permissionIds);

    /**
     * 修改资源-权限
     *
     * @param resourceId    资源编号
     * @param permissionIds 权限编号
     */
    public void updateResourcePermissions(String resourceId, String[] permissionIds);

    /**
     * 修改部门-权限
     *
     * @param orgId         部门编号
     * @param permissionIds 权限编号
     */
    public void updateOrgPermissions(String orgId, String[] permissionIds);

    /**
     * 获取所有的权限
     *
     * @return 所有的权限
     */
    public List<polar.island.core.entity.TreeEntity> allPermission();


    /**
     * 获取角色的所有权限
     *
     * @param roleId 角色编号
     * @return 查询到的权限
     */
    public List<PermissionEntity> rolePermissions(String roleId);

    /**
     * 获取部门的所有权限
     *
     * @param orgId 部门编号
     * @return 查询到的权限
     */
    public List<PermissionEntity> orgPermissions(String orgId);

    /**
     * 获取资源的所有权限
     *
     * @param resourceId 资源编号
     * @return 资源权限
     */
    public List<PermissionEntity> resourcePermissions(String resourceId);

}
