package polar.island.inlay.permission.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import polar.island.core.config.Constants;
import polar.island.core.entity.TreeEntity;
import polar.island.core.exception.FrameWorkException;
import polar.island.core.util.ExceptionUtil;
import polar.island.inlay.permission.dao.PermissionDao;
import polar.island.inlay.permission.entity.PermissionEntity;
import polar.island.mongo.service.MongoBasicService;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service(value = "permissionService")
public class PermissionServiceImpl extends MongoBasicService<PermissionEntity, PermissionEntity, PermissionDao>
        implements PermissionService {
    @Resource(name = "permissionDao")
    private PermissionDao permissionDao;

    @Override
    public PermissionDao getDao() {
        return permissionDao;
    }

    @Override
    public Object insert(Map<String, Object> condition) {
        try {
            Map<String, Object> condition2 = new HashMap<String, Object>();
            condition2.put("name", condition.get("name"));
            if (getDao().selectOneByCondition(condition2) != null) {
                throw new FrameWorkException(Constants.CODE_SERVER_COMMON, "名称为\"" + condition.get("name") + "\"的权限已存在",
                        null, false);
            }
            permissionDao.insert(condition);
            return condition.get("id");
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Override
    public List<TreeEntity> allPermission() {
        try {
            return permissionDao.allPermission();
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Override
    public Long deleteByIdPhysical(String id) {
        try {
            List<TreeEntity> all = permissionDao.allPermission();
            Long count = deletePermission(id, all, 0l);
            return count;
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    private Long deletePermission(String id, List<TreeEntity> all, Long count) {
        // 删除此id的数据
        count = count + permissionDao.deleteByIdPhysical(id);
        if (all != null && all.size() > 0) {
            for (TreeEntity entity : all) {
                if (id.equals(entity.getParentId() + "")) {
                    count = deletePermission(entity.getId(), all, count);
                }
            }
        }
        return count;
    }

    @Override
    public List<PermissionEntity> rolePermissions(String roleId) {
        try {
            return permissionDao.rolePermissions(roleId);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Transactional(readOnly = false, transactionManager = "mongoTransactionManager")
    @Override
    public void updateRolePermissions(String roleId, String[] permissionIds) {
        try {
            permissionDao.updateRolePermissions(roleId, permissionIds);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Transactional(readOnly = false, transactionManager = "mongoTransactionManager")
    @Override
    public void updateResourcePermissions(String resourceId, String[] permissionIds) {
        try {
            permissionDao.updateResourcePermissions(resourceId, permissionIds);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Override
    public List<PermissionEntity> orgPermissions(String orgId) {
        try {
            return permissionDao.orgPermissions(orgId);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Transactional(readOnly = false, transactionManager = "mongoTransactionManager")
    @Override
    public void updateOrgPermissions(String orgId, String[] permissionIds) {
        try {
            permissionDao.updateOrgPermissions(orgId, permissionIds);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Transactional(readOnly = false, transactionManager = "mongoTransactionManager")
    @Override
    public List<PermissionEntity> resourcePermissions(String resourceId) {
        try {
            return permissionDao.resourcePermissions(resourceId);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }
}