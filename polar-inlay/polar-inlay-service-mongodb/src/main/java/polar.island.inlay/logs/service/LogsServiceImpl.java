package polar.island.inlay.logs.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import polar.island.core.util.ExceptionUtil;
import polar.island.inlay.logs.dao.LogsDao;
import polar.island.inlay.logs.entity.LogsEntity;
import polar.island.mongo.service.MongoBasicService;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Map;

@Transactional(transactionManager = "mongoTransactionManager")
@Service(value = "logsService")
public class LogsServiceImpl extends MongoBasicService<LogsEntity, LogsEntity, LogsDao> implements LogsService {
    @Resource(name = "logsDao")
    private LogsDao logsDao;

    @Override
    public LogsDao getDao() {
        return logsDao;
    }

    @Override
    public Object insert(Map<String, Object> condition) {
        try {
            Date date = new Date();
            condition.put("createTime", date);
            condition.put("createTimeMillions", date.getTime());
            logsDao.insert(condition);
            return condition.get("id");
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }
}