package polar.island.inlay.logs.proxy;


import org.springframework.data.annotation.Id;
import polar.island.core.dao.annotation.AutoIncrease;
import polar.island.core.dao.annotation.ClassConfig;
import polar.island.core.dao.annotation.FieldConfig;
import polar.island.core.dao.annotation.MatchStyle;
import polar.island.core.dao.types.Style;
import polar.island.core.dao.types.StyleType;
import polar.island.inlay.logs.entity.LogsEntity;

import java.util.Date;

@ClassConfig(tableName = polar.island.inlay.config.Table.INLAY_LOGS, listClass = LogsEntity.class, detailClass = LogsEntity.class)
public class LogProxyEntity {
    /** 主键编号 **/
    @Id
    @AutoIncrease
    @FieldConfig
    private Long id;
    /** 当前登录用户 **/
    @FieldConfig
    private String userId;
    /** 创建时间 **/
    @FieldConfig
    private Date createTime;
    /** 接口名称 **/
    @FieldConfig(styles = {   //作为查询列表时的条件
            @MatchStyle(type = StyleType.SELECT_MULTI,matchStyle = Style.LIKE),
            //作为查询单个时的条件
            @MatchStyle(type = StyleType.SELECT_ONE),
            //作为删除时的条件
            @MatchStyle(type = StyleType.DELETE),
            //作为更新时的条件
            @MatchStyle(type = StyleType.UPDATE)})
    private String interfaceName;
    /** 错误信息 **/
    @FieldConfig
    private String message;
    /** 引起的原因 **/
    @FieldConfig
    private String caseBy;
    /** 创建时间毫秒 **/
    @FieldConfig
    private Long createTimeMillions;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getInterfaceName() {
        return interfaceName;
    }

    public void setInterfaceName(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCaseBy() {
        return caseBy;
    }

    public void setCaseBy(String caseBy) {
        this.caseBy = caseBy;
    }

    public Long getCreateTimeMillions() {
        return createTimeMillions;
    }

    public void setCreateTimeMillions(Long createTimeMillions) {
        this.createTimeMillions = createTimeMillions;
    }
}
