package polar.island.inlay.logs.dao;

import org.springframework.stereotype.Component;
import polar.island.inlay.logs.entity.LogsEntity;
import polar.island.inlay.logs.proxy.LogProxyEntity;
import polar.island.mongo.dao.MongoBasicDao;

@Component(value = "logsDao")
public class LogsDaoImpl extends MongoBasicDao<LogsEntity, LogsEntity> implements LogsDao {
    @Override
    public Class<?> proxyClass() {
        return LogProxyEntity.class;
    }
}
