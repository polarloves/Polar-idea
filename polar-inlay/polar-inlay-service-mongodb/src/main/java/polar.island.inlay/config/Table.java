package polar.island.inlay.config;

public class Table {
    public static final String INLAY_DICT="t_polar_dict";
    public static final String INLAY_LOGS="t_polar_logs";
    public static final String INLAY_MENU="t_polar_menu";
    public static final String INLAY_MENUMODEL="t_polar_menumodel";
    public static final String INLAY_ORG="t_polar_org";
    public static final String INLAY_PERMISSION="t_polar_permission";
    public static final String INLAY_RECORDS="t_polar_visit_records";
    public static final String INLAY_RESOURCES="t_polar_resource";
    public static final String INLAY_ROLE="t_polar_role";
    public static final String INLAY_TREE="t_polar_tree";
    public static final String INLAY_USER="t_polar_user";
}
