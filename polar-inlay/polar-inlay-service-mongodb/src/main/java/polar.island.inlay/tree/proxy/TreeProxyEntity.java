package polar.island.inlay.tree.proxy;

import org.springframework.data.annotation.Id;
import polar.island.core.dao.annotation.AutoIncrease;
import polar.island.core.dao.annotation.ClassConfig;
import polar.island.core.dao.annotation.FieldConfig;
import polar.island.inlay.tree.entity.TreeEntity;


@ClassConfig(tableName = polar.island.inlay.config.Table.INLAY_TREE, listClass = TreeEntity.class, detailClass = TreeEntity.class)
public class TreeProxyEntity {
    /** 主键编号 **/
    @Id
    @AutoIncrease
    @FieldConfig
    private Long id;
    /** 组编号 **/
    @FieldConfig
    private String groupId;
    /** 组名 **/
    @FieldConfig
    private String groupName;
    /** 文本内容 **/
    @FieldConfig
    private String text;
    /** 值 **/
    @FieldConfig
    private String value;
    /** 别名 **/
    @FieldConfig
    private String textAlias;
    /** 编号 **/
    @FieldConfig
    private String textId;
    /** 父级编号 **/
    @FieldConfig
    private String parentId;
    /** 类型 **/
    @FieldConfig
    private Integer type;
    @FieldConfig
    private Integer orderNum;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getTextAlias() {
        return textAlias;
    }

    public void setTextAlias(String textAlias) {
        this.textAlias = textAlias;
    }

    public String getTextId() {
        return textId;
    }

    public void setTextId(String textId) {
        this.textId = textId;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Integer orderNum) {
        this.orderNum = orderNum;
    }
}
