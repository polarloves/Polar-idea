package polar.island.inlay.tree.dao;

import org.bson.Document;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import polar.island.core.entity.DictEntity;
import polar.island.inlay.tree.entity.TreeEntity;
import polar.island.inlay.tree.proxy.TreeProxyEntity;
import polar.island.mongo.dao.MongoBasicDao;

import java.util.ArrayList;
import java.util.List;

@Component(value = "treeDao")
public class TreeDaoImpl extends MongoBasicDao<TreeEntity, TreeEntity> implements TreeDao {
    @Override
    public List<DictEntity> allGroups() {
        Aggregation agg = Aggregation.newAggregation(
                Aggregation.project("groupName", "groupId"),
                Aggregation.group("groupName", "groupId"),
                Aggregation.sort(Sort.by(Sort.Direction.ASC, "groupName"))
        );
        List<DictEntity> details = new ArrayList<DictEntity>();
        AggregationResults<Document> outputType = mongoTemplate.aggregate(agg, tableName(), Document.class);
        List<Document> resultList = outputType.getMappedResults();
        for (Document obj : resultList) {
            polar.island.core.entity.DictEntity entity = new polar.island.core.entity.DictEntity();
            entity.setText(obj.getString("groupName"));
            entity.setValue(obj.getString("groupId"));
            details.add(entity);
        }
        return details;
    }

    @Override
    public List<TreeEntity> getTree(String groupId) {
        return mongoTemplate.find(Query.query(Criteria.where("groupId").is(groupId)).with(Sort.by("orderNum")), TreeEntity.class, tableName());

    }

    @Override
    public Class<?> proxyClass() {
        return TreeProxyEntity.class;
    }
}
