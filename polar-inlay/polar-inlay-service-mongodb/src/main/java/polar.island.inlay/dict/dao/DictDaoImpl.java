package polar.island.inlay.dict.dao;

import org.bson.Document;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import polar.island.inlay.dict.entity.DictEntity;
import polar.island.inlay.dict.proxy.DictProxyEntity;
import polar.island.mongo.dao.MongoBasicDao;

import java.util.ArrayList;
import java.util.List;

@Component(value = "dictDao")
public class DictDaoImpl extends MongoBasicDao<DictEntity, DictEntity> implements DictDao {

    @Override
    public List<polar.island.core.entity.DictEntity> allGroups() {
        Aggregation agg = Aggregation.newAggregation(
                Aggregation.project("groupName", "groupId"),
                Aggregation.group("groupName", "groupId"),
                Aggregation.sort(Sort.by(Sort.Direction.ASC, "groupName"))
        );
        List<polar.island.core.entity.DictEntity> details = new ArrayList<polar.island.core.entity.DictEntity>();
        AggregationResults<Document> outputType = mongoTemplate.aggregate(agg, tableName(), Document.class);
        List<Document> resultList = outputType.getMappedResults();
        for (Document obj : resultList) {
            polar.island.core.entity.DictEntity entity = new polar.island.core.entity.DictEntity();
            entity.setText(obj.getString("groupName"));
            entity.setValue(obj.getString("groupId"));
            details.add(entity);
        }
        return details;
    }



    @Override
    public List<polar.island.core.entity.DictEntity> findDictById(String id) {
        return mongoTemplate.find(Query.query(Criteria.where("groupId").is(id)).with(Sort.by(Sort.Direction.ASC, "orderNum")), polar.island.core.entity.DictEntity.class, tableName());
    }

    @Override
    public Class<?> proxyClass() {
        return DictProxyEntity.class;
    }
}
