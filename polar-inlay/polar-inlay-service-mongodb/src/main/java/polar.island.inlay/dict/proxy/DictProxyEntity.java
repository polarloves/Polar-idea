package polar.island.inlay.dict.proxy;

import org.springframework.data.annotation.Id;
import polar.island.core.dao.annotation.AutoIncrease;
import polar.island.core.dao.annotation.ClassConfig;
import polar.island.core.dao.annotation.FieldConfig;
import polar.island.core.dao.annotation.MatchStyle;
import polar.island.core.dao.types.Style;
import polar.island.core.dao.types.StyleType;
import polar.island.inlay.dict.entity.DictEntity;


@ClassConfig(tableName = polar.island.inlay.config.Table.INLAY_DICT, listClass = DictEntity.class, detailClass = DictEntity.class)
public class DictProxyEntity {
    /** 主键编号 **/
    @Id
    @AutoIncrease
    @FieldConfig
    private Long id;
    /** 组名 **/
    @FieldConfig(styles = {   //作为查询列表时的条件
            @MatchStyle(type = StyleType.SELECT_MULTI,matchStyle = Style.LIKE),
            //作为查询单个时的条件
            @MatchStyle(type = StyleType.SELECT_ONE),
            //作为删除时的条件
            @MatchStyle(type = StyleType.DELETE),
            //作为更新时的条件
            @MatchStyle(type = StyleType.UPDATE)})
    private String groupName;
    /** 组编号 **/
    @FieldConfig
    private String groupId;
    /** 文本内容 **/
    @FieldConfig
    private String text;
    /** 值 **/
    @FieldConfig(styles = {   //作为查询列表时的条件
            @MatchStyle(type = StyleType.SELECT_MULTI,matchStyle = Style.LIKE),
            //作为查询单个时的条件
            @MatchStyle(type = StyleType.SELECT_ONE),
            //作为删除时的条件
            @MatchStyle(type = StyleType.DELETE),
            //作为更新时的条件
            @MatchStyle(type = StyleType.UPDATE)})
    private String value;
    /** 备注 **/
    @FieldConfig(styles = {   //作为查询列表时的条件
            @MatchStyle(type = StyleType.SELECT_MULTI,matchStyle = Style.LIKE),
            //作为查询单个时的条件
            @MatchStyle(type = StyleType.SELECT_ONE),
            //作为删除时的条件
            @MatchStyle(type = StyleType.DELETE),
            //作为更新时的条件
            @MatchStyle(type = StyleType.UPDATE)})
    private String remark;
    /** 排序号 **/
    @FieldConfig
    private Long orderNum;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Long orderNum) {
        this.orderNum = orderNum;
    }
}
