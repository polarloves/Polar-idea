package polar.island.inlay.org.dao;

import com.mongodb.client.result.UpdateResult;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;
import polar.island.inlay.org.entity.OrgEntity;
import polar.island.inlay.org.proxy.OrgProxyEntity;
import polar.island.inlay.util.SortUtil;
import polar.island.mongo.dao.MongoBasicDao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component(value = "orgDao")
public class OrgDaoImpl extends MongoBasicDao<OrgEntity, OrgEntity> implements OrgDao {
    @Override
    public Query defaultOrder(Query query, Map<String, Object> condition, Class<?> proxyClass) {
        query.with(Sort.by(Sort.Direction.ASC, "parentId"));
        return query;
    }

    @Override
    public List<OrgEntity> selectAllList() {
        return super.selectList(new HashMap<String, Object>());
    }

    @Override
    public List<OrgEntity> convertToList(List source) {
        List<OrgEntity> result= super.convertToList(source);
        sort(result);
        return result;
    }
    private void sort(List<OrgEntity> entities) {
        SortUtil.sortOrg(entities);
    }

    @Override
    public Long deleteOrgPermissionByOrgId(String orgId) {
        Update update = Update.update("permission", null);
        Query query = new Query(Criteria.where("_id").is(Long.parseLong(orgId)));
        UpdateResult result = mongoTemplate.updateMulti(query, update, tableName());
        return result.getMatchedCount();
    }

    @Override
    public Class<?> proxyClass() {
        return OrgProxyEntity.class;
    }
}
