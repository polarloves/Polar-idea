package polar.island.inlay.org.proxy;


import org.springframework.data.annotation.Id;
import polar.island.core.dao.annotation.AutoIncrease;
import polar.island.core.dao.annotation.ClassConfig;
import polar.island.core.dao.annotation.FieldConfig;
import polar.island.core.dao.annotation.MatchStyle;
import polar.island.core.dao.types.Style;
import polar.island.core.dao.types.StyleType;
import polar.island.inlay.org.entity.OrgEntity;
import polar.island.inlay.permission.proxy.PermissionProxyEntity;

import java.util.List;

@ClassConfig(tableName = polar.island.inlay.config.Table.INLAY_ORG, listClass = OrgEntity.class, detailClass = OrgEntity.class)
public class OrgProxyEntity {
    /** 主键编号	**/
    @Id
    @AutoIncrease
    @FieldConfig
    private Long id;
    /** 机构名称 **/
    @FieldConfig(styles = {   //作为查询列表时的条件
            @MatchStyle(type = StyleType.SELECT_MULTI,matchStyle = Style.LIKE),
            //作为查询单个时的条件
            @MatchStyle(type = StyleType.SELECT_ONE),
            //作为删除时的条件
            @MatchStyle(type = StyleType.DELETE),
            //作为更新时的条件
            @MatchStyle(type = StyleType.UPDATE)})
    private String name;
    /** 上级部门 **/
    @FieldConfig
    private Long parentId;
    /** 机构编码 **/
    @FieldConfig
    private String orgCode;
    /** 机构介绍 **/
    @FieldConfig
    private String orgDescribe;
    /** 联系人 **/
    @FieldConfig
    private String contactPeople;
    /** 联系电话 **/
    @FieldConfig
    private String contactPhone;
    /** 联系邮箱 **/
    @FieldConfig
    private String contactEmail;
    @FieldConfig(scope = {}, styles = {}, sortable = false,updateAll = false,updateField = false)
    private List<PermissionProxyEntity> permissions;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<PermissionProxyEntity> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<PermissionProxyEntity> permissions) {
        this.permissions = permissions;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getOrgDescribe() {
        return orgDescribe;
    }

    public void setOrgDescribe(String orgDescribe) {
        this.orgDescribe = orgDescribe;
    }

    public String getContactPeople() {
        return contactPeople;
    }

    public void setContactPeople(String contactPeople) {
        this.contactPeople = contactPeople;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }
}
