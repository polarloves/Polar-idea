package polar.island.inlay.org.service;

import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import polar.island.core.exception.ValidationException;
import polar.island.core.util.CommonUtil;
import polar.island.core.util.ExceptionUtil;
import polar.island.inlay.org.dao.OrgDao;
import polar.island.inlay.org.entity.OrgEntity;
import polar.island.mongo.service.MongoBasicService;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(value = "orgService")
public class OrgServiceImpl extends MongoBasicService<OrgEntity, OrgEntity, OrgDao> implements OrgService {
    @Resource(name = "orgDao")
    private OrgDao orgDao;

    @Override
    public OrgDao getDao() {
        return orgDao;
    }

    @Override
    public Object insert(Map<String, Object> condition) {
        try {
            Object name = condition.get("orgCode");
            Map<String, Object> condition2 = new HashMap<String, Object>();
            condition2.put("orgCode", name);
            OrgEntity entity = getDao().selectOneByCondition(condition);
            if (entity != null) {
                throw new ValidationException("部门编号已存在", null);
            }
            Object id = null;
            orgDao.insert(condition);
            id = condition.get("id");
            return id;
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }


    @Override
    public List<OrgEntity> selectAllList() {
        return getDao().selectAllList();
    }

    public Long deleteByIdPhysical(String id) {
        try {
            return deleteChildrenPhysical(getDao().selectOneById(id), getDao().selectAllList(), 0l);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    /**
     * 物理删除当前的数据以及其子数据。
     *
     * @param current  当前需要删除的数据.
     * @param entities 所有的数据.
     * @param count    计数开始值.
     * @return 删除的总条目
     */
    private Long deleteChildrenPhysical(OrgEntity current, List<OrgEntity> entities, Long count) {
        String id = CommonUtil.valueOf(current.getId());
        count = count + getDao().deleteByIdPhysical(id);
        orgDao.deleteOrgPermissionByOrgId(current.getId() + "");
        if (CollectionUtils.isEmpty(entities)) {
            String childId = CommonUtil.valueOf(current.getParentId());
            for (OrgEntity entity : entities) {
                String parentId = CommonUtil.valueOf(entity.getParentId());
                if (childId.equals(parentId)) {
                    count = deleteChildrenPhysical(entity, entities, count);
                }
            }
        }
        return count;
    }

    @Override
    public OrgEntity selectOneById(String id) {
        try {
            OrgEntity result = super.selectOneById(id);
            return result;
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Override
    public OrgEntity selectOneByCondition(Map<String, Object> condition) {
        try {
            OrgEntity result = super.selectOneByCondition(condition);
            return result;
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

}