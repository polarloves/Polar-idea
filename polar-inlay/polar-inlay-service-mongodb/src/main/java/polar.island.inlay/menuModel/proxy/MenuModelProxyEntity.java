package polar.island.inlay.menuModel.proxy;

import org.springframework.data.annotation.Id;
import polar.island.core.dao.annotation.AutoIncrease;
import polar.island.core.dao.annotation.ClassConfig;
import polar.island.core.dao.annotation.FieldConfig;
import polar.island.core.dao.annotation.MatchStyle;
import polar.island.core.dao.types.Style;
import polar.island.core.dao.types.StyleType;
import polar.island.inlay.menu.proxy.MenuProxyEntity;
import polar.island.inlay.menuModel.entity.MenuModelEntity;

import java.util.List;


@ClassConfig(tableName = polar.island.inlay.config.Table.INLAY_MENUMODEL, listClass = MenuModelEntity.class, detailClass = MenuModelEntity.class)
public class MenuModelProxyEntity {
    /**
     * 主键编号
     **/
    @Id
    @AutoIncrease
    @FieldConfig
    private Long id;
    /**
     * 模板名称
     **/
    @FieldConfig(styles = {   //作为查询列表时的条件
            @MatchStyle(type = StyleType.SELECT_MULTI, matchStyle = Style.LIKE),
            //作为查询单个时的条件
            @MatchStyle(type = StyleType.SELECT_ONE),
            //作为删除时的条件
            @MatchStyle(type = StyleType.DELETE),
            //作为更新时的条件
            @MatchStyle(type = StyleType.UPDATE)})
    private String name;
    /**
     * 模板描述
     **/
    @FieldConfig(styles = {   //作为查询列表时的条件
            @MatchStyle(type = StyleType.SELECT_MULTI, matchStyle = Style.LIKE),
            //作为查询单个时的条件
            @MatchStyle(type = StyleType.SELECT_ONE),
            //作为删除时的条件
            @MatchStyle(type = StyleType.DELETE),
            //作为更新时的条件
            @MatchStyle(type = StyleType.UPDATE)})
    private String info;
    /**
     * 默认模板（0-否，1-是）
     **/
    @FieldConfig
    private Long defaultMenu;
    @FieldConfig(updateAll = false, updateField = false, sortable = false, queryDetail = true, queryList = false)
    private List<MenuProxyEntity> menus;

    public Long getId() {
        return id;
    }

    public List<MenuProxyEntity> getMenus() {
        return menus;
    }

    public void setMenus(List<MenuProxyEntity> menus) {
        this.menus = menus;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Long getDefaultMenu() {
        return defaultMenu;
    }

    public void setDefaultMenu(Long defaultMenu) {
        this.defaultMenu = defaultMenu;
    }
}
