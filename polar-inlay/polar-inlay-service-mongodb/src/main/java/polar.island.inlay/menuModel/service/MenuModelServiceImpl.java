package polar.island.inlay.menuModel.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import polar.island.core.util.ExceptionUtil;
import polar.island.inlay.menuModel.dao.MenuModelDao;
import polar.island.inlay.menuModel.entity.MenuModelEntity;
import polar.island.mongo.service.MongoBasicService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(value = "menuModelService")
public class MenuModelServiceImpl extends MongoBasicService<MenuModelEntity, MenuModelEntity, MenuModelDao> implements MenuModelService {
    @Resource(name = "menuModelDao")
    private MenuModelDao menuModelDao;


    @Override
    public MenuModelDao getDao() {
        return menuModelDao;
    }

    @Override
    public Object insert(Map<String, Object> condition) {
        try {
            menuModelDao.insert(condition);
            return condition.get("id");
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Transactional(transactionManager = "mongoTransactionManager", readOnly = false)
    @Override
    public void setDefault(String id) {
        try {
            menuModelDao.cleanDefault();
            Map<String, Object> condition = new HashMap<String, Object>();
            condition.put("id", id);
            condition.put("defaultMenu", 1);
            menuModelDao.updateField(condition);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }


    @Override
    public List<MenuModelEntity> userMenuModels(String userId) {
        try {
            List<MenuModelEntity> result = new ArrayList<MenuModelEntity>();
            MenuModelEntity entity = menuModelDao.userMenuModel(userId);
            if (entity != null) {
                result.add(entity);
            }
            return result;
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Transactional(transactionManager = "mongoTransactionManager", readOnly = false)
    @Override
    public void updateUserMenuModels(String userId, String menuModelId) {
        try {
            menuModelDao.updateUserMenuModels(userId, menuModelId);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }
}