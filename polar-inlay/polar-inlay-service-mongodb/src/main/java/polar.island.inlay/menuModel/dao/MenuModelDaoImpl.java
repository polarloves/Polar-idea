package polar.island.inlay.menuModel.dao;

import org.bson.Document;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import polar.island.core.exception.ValidationException;
import polar.island.inlay.menuModel.entity.MenuModelEntity;
import polar.island.inlay.menuModel.proxy.MenuModelProxyEntity;
import polar.island.mongo.dao.MongoBasicDao;

import java.util.List;
import java.util.Map;

@Component(value = "menuModelDao")
public class MenuModelDaoImpl extends MongoBasicDao<MenuModelEntity, MenuModelEntity> implements MenuModelDao {
    @Override
    public void cleanDefault() {
        Query query = new Query();
        Update update = new Update();
        update.set("defaultMenu", 0l);
        mongoTemplate.updateMulti(query, update, polar.island.inlay.config.Table.INLAY_MENUMODEL);
    }

    @Override
    public MenuModelEntity userMenuModel(String userId) {
        Aggregation agg = Aggregation.newAggregation(
                Aggregation.project("menuModel"),
                Aggregation.match(Criteria.where("_id").is(Long.parseLong(userId)))
        );
        AggregationResults<Document> outputType = mongoTemplate.aggregate(agg, polar.island.inlay.config.Table.INLAY_USER, Document.class);
        List<Document> resultList = outputType.getMappedResults();
        if (!CollectionUtils.isEmpty(resultList)) {
            MenuModelEntity entity = new MenuModelEntity();
            entity.setId(resultList.get(0).getLong("menuModel"));
            if (entity.getId() != null) {
                return entity;
            }
        }
        return null;
    }

    @Override
    public Long deleteByConditionPhysical(Map<String, Object> condition) {
        throw new ValidationException("无法根据条件删除数据", null);
    }

    @Override
    public Long deleteByIdPhysical(String id) {
        Query query = Query.query(Criteria.where("menuModel").is(Long.parseLong(id)));
        Update update = new Update();
        update.set("menuModel", null);
        mongoTemplate.updateMulti(query, update, polar.island.inlay.config.Table.INLAY_USER);
        return super.deleteByIdPhysical(id);
    }


    @Override
    public void updateUserMenuModels(String userId, String menuModelId) {
        Query query = Query.query(Criteria.where("_id").is(Long.parseLong(userId)));
        Update update = new Update();
        update.set("menuModel", Long.parseLong(menuModelId));
        mongoTemplate.updateMulti(query, update, polar.island.inlay.config.Table.INLAY_USER);
    }

    @Override
    public Class<?> proxyClass() {
        return MenuModelProxyEntity.class;
    }
}
