package polar.island.inlay.menuModel.dao;

import polar.island.core.dao.BasicDao;
import polar.island.inlay.menuModel.entity.MenuModelEntity;

import java.util.Map;

/**
 * 菜单模板的持久化层。
 *
 * @author PolarLoves
 */
public interface MenuModelDao extends BasicDao<MenuModelEntity, MenuModelEntity> {
    /**
     * 由于仅有物理删除，此删除方法被移除
     */
    @Deprecated
    public Long deleteByIdLogic(String id);

    /**
     * 由于仅有物理删除，此删除方法被移除
     */
    @Deprecated
    public Long deleteByConditionLogic(Map<String, Object> condition);

    public void cleanDefault();

    /**
     * 获取用户的所有菜单
     *
     * @param userId 用户编号
     * @return 用户菜单
     */
    public MenuModelEntity userMenuModel(String userId);




    public void updateUserMenuModels(String userId, String menuModelId);
}
