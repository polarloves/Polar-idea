package polar.island.inlay.menu.service;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import polar.island.core.entity.TreeEntity;
import polar.island.core.util.ExceptionUtil;
import polar.island.inlay.menu.dao.MenuDao;
import polar.island.inlay.menu.entity.MenuEntity;
import polar.island.inlay.menuModel.dao.MenuModelDao;
import polar.island.inlay.menuModel.entity.MenuModelEntity;
import polar.island.mongo.service.MongoBasicService;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service(value = "menuService")
public class MenuServiceImpl extends MongoBasicService<MenuEntity, MenuEntity, MenuDao> implements MenuService {
    @Resource(name = "menuDao")
    private MenuDao menuDao;
    @Resource(name = "menuModelDao")
    private MenuModelDao menuModelDao;

    @Override
    public MenuDao getDao() {
        return menuDao;
    }

    @Transactional(transactionManager = "mongoTransactionManager", readOnly = false)
    @Override
    public Object insert(Map<String, Object> condition) {
        try {
            menuDao.insert(condition);
            return condition.get("id");
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Override
    public List<TreeEntity> allMenus() {
        try {
            return menuDao.allMenus();
        } catch (Exception e) {
            e.printStackTrace();
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Override
    public List<MenuEntity> modelMenus(String menuModelId) {
        try {
            return menuDao.modelMenus(menuModelId);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }
    @Override
    public Long deleteByIdPhysical(String id) {
        try {
            return deleteMenu(id, menuDao.allMenus(), 0l);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    private Long deleteMenu(String id, List<TreeEntity> all, Long count) {
        count = count + menuDao.deleteByIdPhysical(id);
        if (all != null && all.size() > 0) {
            for (TreeEntity entity : all) {
                if (id.equals(entity.getParentId() + "")) {
                    count = deleteMenu(entity.getId(), all, count);
                }
            }
        }
        return count;
    }

    @Transactional(transactionManager = "mongoTransactionManager", readOnly = false)
    @Override
    public void updateModelMenus(String modelId, String[] menuId) {
        try {
            menuDao.upsetMenuModelMenus(modelId, menuId);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Override
    public Long maxOrderNum() {
        return null;
    }


    @Cacheable(value = "menuCache", key = "#userId")
    @Override
    public ArrayList<MenuEntity> userMenus(String userId) {
        try {
            MenuModelEntity menuModelEntity = menuModelDao.userMenuModel(userId);
            List<MenuEntity> result = null;
            if (menuModelEntity != null) {
                result = menuDao.selectMenusByMenuModelId(menuModelEntity.getId());
            } else {
                // 使用默认菜单
                result = menuDao.defaultMenus();
            }
            ArrayList<MenuEntity> temp = new ArrayList<MenuEntity>();
            if (result != null && result.size() > 0) {
                for (MenuEntity entity : result) {
                    if (entity.getParentId() == null) {
                        temp.add(entity);
                    }
                }
            }
            result.removeAll(temp);
            for (MenuEntity entity : temp) {
                addClidren(entity, result);
            }
            return temp;
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    private void addClidren(MenuEntity current, List<MenuEntity> datas) {
        Long parentId = current.getId();
        if (current.getChildren() == null) {
            current.setChildren(new ArrayList<MenuEntity>());
        }
        for (MenuEntity data : datas) {
            if (parentId.equals(data.getParentId())) {
                current.getChildren().add(data);
            }
        }
        //移除不需要的集合，提高效率
        datas.removeAll(current.getChildren());
        for (MenuEntity data : current.getChildren()) {
            addClidren(data, datas);
        }
    }

    @CacheEvict(value = "menuCache", key = "#userId")
    @Override
    public void clearUserMenusCache(String userId) {

    }

    @CacheEvict(value = "menuCache", allEntries = true)
    @Override
    public void clearAllMenusCache() {

    }
}