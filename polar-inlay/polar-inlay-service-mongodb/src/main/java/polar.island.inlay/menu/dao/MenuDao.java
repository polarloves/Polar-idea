package polar.island.inlay.menu.dao;

import polar.island.core.dao.BasicDao;
import polar.island.inlay.menu.entity.MenuEntity;

import java.util.List;
import java.util.Map;

/**
 * 菜单的持久化层。
 *
 * @author PolarLoves
 */
public interface MenuDao extends BasicDao<MenuEntity, MenuEntity> {
    /**
     * 由于仅有物理删除，此删除方法被移除
     */
    @Deprecated
    public Long deleteByIdLogic(String id);

    /**
     * 由于仅有物理删除，此删除方法被移除
     */
    @Deprecated
    public Long deleteByConditionLogic(Map<String, Object> condition);

    /**
     * 获取所有的菜单
     *
     * @return 所有的菜单
     */
    public List<polar.island.core.entity.TreeEntity> allMenus();

    /**
     * 获取菜单模板的所有菜单
     *
     * @param menuModelId 菜单模板编号
     * @return 查询到的菜单
     */
    public List<MenuEntity> modelMenus(String menuModelId);


    /**
     * 设置菜单模板的菜单
     *
     * @param modelId 模板编号
     * @param modelId 菜单编号
     */
    public void upsetMenuModelMenus(String modelId, String[] menuId);


    /**
     * 获取默认菜单
     *
     * @return 默认菜单
     */
    public List<MenuEntity> defaultMenus();

    public List<MenuEntity> selectMenusByMenuModelId(Long menuModelId);

}
