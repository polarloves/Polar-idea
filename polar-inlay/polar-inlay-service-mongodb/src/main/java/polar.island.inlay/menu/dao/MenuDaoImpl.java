package polar.island.inlay.menu.dao;

import org.bson.Document;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import polar.island.core.entity.TreeEntity;
import polar.island.core.exception.ValidationException;
import polar.island.core.util.CommonUtil;
import polar.island.core.util.EntityUtil;
import polar.island.inlay.menu.entity.MenuEntity;
import polar.island.inlay.menu.proxy.MenuProxyEntity;
import polar.island.inlay.menuModel.proxy.MenuModelProxyEntity;
import polar.island.inlay.util.SortUtil;
import polar.island.mongo.dao.MongoBasicDao;
import polar.island.mongo.util.MongoDBUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository(value = "menuDao")
public class MenuDaoImpl extends MongoBasicDao<MenuEntity, MenuEntity> implements MenuDao {
    @Override
    public Query defaultOrder(Query query, Map<String, Object> condition, Class<?> proxyClass) {
        return query.with(Sort.by(Sort.Direction.ASC, "parentId")).with(Sort.by(Sort.Direction.ASC, "orderNum"));
    }

    @Override
    public Query orderCondition(Query query, Map<String, Object> condition) {
        return defaultOrder(query, condition, proxyClass());
    }

    @Override
    public List<MenuEntity> convertToList(List source) {
        List<MenuEntity> result = super.convertToList(source);
        sortMenus(result);
        return result;
    }

    @Override
    public List<TreeEntity> allMenus() {
        Aggregation agg = Aggregation.newAggregation(
                Aggregation.project("id", "name", "parentId", "orderNum"),
                Aggregation.sort(Sort.Direction.ASC, "parentId", "orderNum")
        );
        List<TreeEntity> details = new ArrayList<TreeEntity>();
        AggregationResults<Document> outputType = mongoTemplate.aggregate(agg, tableName(), Document.class);
        List<Document> resultList = outputType.getMappedResults();

        for (Document obj : resultList) {
            TreeEntity entity = new TreeEntity();
            entity.setText(obj.getString("name"));
            entity.setId(CommonUtil.valueOf(obj.getLong("_id") + ""));
            entity.setParentId(CommonUtil.valueOf(obj.getLong("parentId")));
            entity.setOrderNum(obj.getLong("orderNum"));
            details.add(entity);
        }
        SortUtil.sortTree(details);
        return details;
    }

    private void sortMenus(List<MenuEntity> entities) {
        SortUtil.sortMenu(entities);
    }

    @Override
    public Long deleteByConditionPhysical(Map<String, Object> condition) {
        throw new ValidationException("无法根据条件删除数据", null);
    }

    @Override
    public List<MenuEntity> modelMenus(String menuModelId) {
        List<MenuEntity> result = null;
        MenuModelProxyEntity menuModelProxyEntity = mongoTemplate.findOne(Query.query(Criteria.where("_id").is(Long.parseLong(menuModelId))), MenuModelProxyEntity.class, polar.island.inlay.config.Table.INLAY_MENUMODEL);
        if (menuModelProxyEntity != null) {
            if (menuModelProxyEntity.getMenus() != null) {
                result = EntityUtil.copyArray(menuModelProxyEntity.getMenus(), MenuEntity.class);
            }
        }
        if (result == null) {
            result = new ArrayList<MenuEntity>();
        }
        sortMenus(result);

        return result;
    }

    @Override
    public void upsetMenuModelMenus(String modelId, String[] menuId) {
        List<Long> ids = new ArrayList<>();
        List<MenuProxyEntity> menuProxyEntities = new ArrayList<>();
        if (menuId != null && menuId.length > 0) {
            for (String id : menuId) {
                ids.add(Long.parseLong(id));
            }
            Query query = listQueryFields().addCriteria(Criteria.where("_id").in(ids));
            menuProxyEntities = mongoTemplate.find(query, MenuProxyEntity.class, polar.island.inlay.config.Table.INLAY_MENU);
        }

        Query query = new Query(Criteria.where("_id").is(Long.parseLong(modelId)));
        Update update = new Update();
        update.set("menus", menuProxyEntities);
        mongoTemplate.updateMulti(query, update, polar.island.inlay.config.Table.INLAY_MENUMODEL);
    }

    private void updateMenuModelMenus(Map<String, Object> condition) {
        Object id = realIdValue(findId(condition));
        MenuEntity entity = selectOneById(id.toString());
        Update update = new Update();
        update.set("menus.$.name", entity.getName());
        update.set("menus.$.icon", entity.getIcon());
        update.set("menus.$.path", entity.getPath());
        update.set("menus.$.orderNum", entity.getOrderNum());
        update.set("menus.$.parentId", entity.getParentId());
        mongoTemplate.updateMulti(Query.query(Criteria.where("menus._id").is(id)), update, polar.island.inlay.config.Table.INLAY_MENUMODEL);
    }

    @Override
    public Long updateField(Map<String, Object> condition) {
        Long result = super.updateField(condition);
        updateMenuModelMenus(condition);
        return result;
    }

    @Override
    public Long updateAll(Map<String, Object> condition) {
        Long result = super.updateAll(condition);
        updateMenuModelMenus(condition);
        return result;
    }

    @Override
    public Long deleteByIdPhysical(String id) {
        //删除模板-菜单...
        MongoDBUtil.deleteCollectionItem(mongoTemplate, polar.island.inlay.config.Table.INLAY_MENUMODEL, "menus", Long.parseLong(id));
        return super.deleteByIdPhysical(id);
    }

    @Override
    public List<MenuEntity> defaultMenus() {
        List<MenuEntity> result = null;
        MenuModelProxyEntity menuModelProxyEntity = mongoTemplate.findOne(Query.query(Criteria.where("defaultMenu").is(1)), MenuModelProxyEntity.class, polar.island.inlay.config.Table.INLAY_MENUMODEL);
        if (menuModelProxyEntity != null) {
            if (menuModelProxyEntity.getMenus() != null) {
                result = EntityUtil.copyArray(menuModelProxyEntity.getMenus(), MenuEntity.class);
            }
        }
        if (result == null) {
            result = new ArrayList<MenuEntity>();
        }
        sortMenus(result);
        return result;
    }

    @Override
    public List<MenuEntity> selectMenusByMenuModelId(Long menuModelId) {
        return modelMenus(menuModelId + "");
    }

    @Override
    public Class<?> proxyClass() {
        return MenuProxyEntity.class;
    }
}
