package polar.island.inlay.menu.proxy;

import org.springframework.data.annotation.Id;
import polar.island.core.dao.annotation.AutoIncrease;
import polar.island.core.dao.annotation.ClassConfig;
import polar.island.core.dao.annotation.FieldConfig;
import polar.island.inlay.menu.entity.MenuEntity;


@ClassConfig(tableName = polar.island.inlay.config.Table.INLAY_MENU, listClass = MenuEntity.class, detailClass = MenuEntity.class)
public class MenuProxyEntity {
    // 主键编号
    @Id
    @AutoIncrease
    @FieldConfig
    private Long id;
    // 名称
    @FieldConfig
    private String name;
    // 图标
    @FieldConfig
    private String icon;
    // 访问路径
    @FieldConfig
    private String path;
    // 排序号
    @FieldConfig
    private Long orderNum;
    // 父级编号
    @FieldConfig
    private Long parentId;
    // 是否默认展开
    @FieldConfig
    private Integer defaultOpen;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Long getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Long orderNum) {
        this.orderNum = orderNum;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Integer getDefaultOpen() {
        return defaultOpen;
    }

    public void setDefaultOpen(Integer defaultOpen) {
        this.defaultOpen = defaultOpen;
    }
}
