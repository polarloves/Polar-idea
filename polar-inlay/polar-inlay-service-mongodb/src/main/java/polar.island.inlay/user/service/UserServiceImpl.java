package polar.island.inlay.user.service;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import polar.island.core.config.Constants;
import polar.island.core.exception.FrameWorkException;
import polar.island.core.security.entity.UserEntity;
import polar.island.core.util.ExceptionUtil;
import polar.island.inlay.user.dao.UserDao;
import polar.island.mongo.service.MongoBasicService;

import javax.annotation.Resource;
import java.util.*;

@Service(value = "userService")
public class UserServiceImpl extends MongoBasicService<UserEntity, UserEntity, UserDao> implements UserService {
    @Resource(name = "userDao")
    private UserDao userDao;


    @Override
    public UserDao getDao() {
        return userDao;
    }

    @CacheEvict(value = "userDbCache", allEntries = true)
    @Override
    public Object insert(Map<String, Object> condition) {
        try {
            if (selectUserByUserName(condition.get("userName").toString()) != null) {
                throw new FrameWorkException(Constants.CODE_COMMON, "当前用户已被注册", null,false);
            }
            userDao.insert(condition);
            return condition.get("id");
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Override
    public UserEntity selectUserByUserName(String userName) {
        try {
            if (StringUtils.isEmpty(userName)) {
                throw new FrameWorkException(Constants.CODE_COMMON, "用户名不能为空", null,false);
            }
            Map<String, Object> condition = new HashMap<String, Object>();
            condition.put("userName", userName);
            return selectOneByCondition(condition);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }


    @Transactional(transactionManager = "mongoTransactionManager", readOnly = false)
    @Override
    public void logIn(Long id, String host) {
        try {
            Map<String, Object> condition = new HashMap<String, Object>();
            condition.put("id", id);
            condition.put("host", host);
            condition.put("logInDate", new Date());
            userDao.logIn(condition);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Cacheable(value = "userDbCache", key = "#id")
    @Override
    public UserEntity selectOneById(String id) {
        try {
            return super.selectOneById(id);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Override
    public Set<String> userRoles(String userId) {
        try {
            List<String> result = userDao.userRoles(userId);
            Set<String> set = new HashSet<String>();
            set.addAll(result);
            return set;
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @CacheEvict(value = "userDbCache", key = "#id")
    @Override
    public Long deleteByIdPhysical(String id) {
        try {
            return super.deleteByIdPhysical(id);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @CacheEvict(value = "userDbCache", key = "#id")
    @Transactional(transactionManager = "mongoTransactionManager", readOnly = false)
    public Long disableUser(String id,String disableReason) {
        try {
            return getDao().disableUser(id,disableReason);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @CacheEvict(value = "userDbCache", key = "#id")
    @Transactional(transactionManager = "mongoTransactionManager", readOnly = false)
    public Long enableUser(String id) {
        try {
            return getDao().enableUser(id);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @CacheEvict(value = "userDbCache", key = "#condition['id'].toString()")
    @Override
    public Long updateAll(Map<String, Object> condition) {
        try {
            return super.updateAll(condition);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @CacheEvict(value = "userDbCache", key = "#condition['id'].toString()")
    @Override
    public Long updateField(Map<String, Object> condition) {
        try {
            return super.updateField(condition);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @CacheEvict(value = "userDbCache", key = "#condition['id'].toString()")
    @Transactional(transactionManager = "mongoTransactionManager", readOnly = false)
    @Override
    public Long updateSelf(Map<String, Object> condition) {
        try {
            return getDao().updateSelf(condition);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @CacheEvict(value = "userDbCache", allEntries = true)
    @Override
    public Long deleteMulitByIdPhysical(String[] ids) {
        try {
            return super.deleteMulitByIdPhysical(ids);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @CacheEvict(value = "userDbCache", key = "#id")
    @Override
    public Long deleteByIdLogic(String id) {
        try {
            return super.deleteByIdLogic(id);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @CacheEvict(value = "userDbCache", allEntries = true)
    @Override
    public Long deleteByConditionPhysical(Map<String, Object> condition) {
        try {
            return super.deleteByConditionPhysical(condition);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @CacheEvict(value = "userDbCache", allEntries = true)
    @Override
    public Long deleteMulitByIdLogic(String[] ids) {
        try {
            return super.deleteMulitByIdLogic(ids);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @CacheEvict(value = "userDbCache", allEntries = true)
    @Override
    public Long deleteByConditionLogic(Map<String, Object> condition) {
        try {
            return super.deleteByConditionLogic(condition);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Override
    public Set<String> userPermissions(String userId) {
        try {
            List<String> result = userDao.userPermissions(userId);
            Set<String> set = new HashSet<String>();
            set.addAll(result);
            List<String> orgPermissions = userDao.orgPermissions(userId);
            set.addAll(orgPermissions);
            return set;
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @CacheEvict(value = "userDbCache", key = "#userId")
    @Transactional(transactionManager = "mongoTransactionManager", readOnly = false)
    @Override
    public void changePassword(String userId , String password , String salt) {
        try {
            Map <String, Object> condition = new HashMap <String, Object> ( );
            condition.put ( "id" , userId );
            condition.put ( "password" , password );
            condition.put ( "salt" , salt );
            updateField(condition);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }
}