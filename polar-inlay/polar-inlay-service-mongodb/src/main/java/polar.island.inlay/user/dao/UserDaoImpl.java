package polar.island.inlay.user.dao;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;
import polar.island.core.security.entity.UserEntity;
import polar.island.inlay.org.proxy.OrgProxyEntity;
import polar.island.inlay.permission.proxy.PermissionProxyEntity;
import polar.island.inlay.role.proxy.RoleProxyEntity;
import polar.island.inlay.user.proxy.UserProxyEntity;
import polar.island.mongo.dao.MongoBasicDao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

@Repository(value = "userDao")
public class UserDaoImpl extends MongoBasicDao<UserEntity, UserEntity> implements UserDao {
    @Override
    public void logIn(Map<String, Object> condition) {
        Query query = Query.query(Criteria.where("_id").is(condition.get("id")));
        Update update = Update.update("logInIp", condition.get("host"))
                .set("logInDate", condition.get("logInDate")).inc("logCount", 1);
        mongoTemplate.updateFirst(query, update, tableName());
    }

    @Override
    public List<UserEntity> selectPageList(Map<String, Object> condition) {
        return super.selectPageList(condition);
    }

    @Override
    public void insert(Map<String, Object> condition) {
        super.insert(condition);
    }

    @Override
    public List<String> userRoles(String userId) {
        List<String> result = new ArrayList<>();
        UserProxyEntity menuModelProxyEntity = mongoTemplate.findOne(Query.query(Criteria.where("_id").is(Long.parseLong(userId))), UserProxyEntity.class, tableName());
        if (menuModelProxyEntity != null) {
            if (menuModelProxyEntity.getRoles() != null) {
                for (RoleProxyEntity roleProxyEntity : menuModelProxyEntity.getRoles()) {
                    result.add(roleProxyEntity.getName());
                }
            }
        }
        if (result == null) {
            result = new ArrayList<String>();
        } else {
            HashSet h = new HashSet(result);
            result.clear();
            result.addAll(h);
        }
        return result;
    }

    @Override
    public List<String> userPermissions(String userId) {
        List<String> result = new ArrayList<String>();
        UserProxyEntity menuModelProxyEntity = mongoTemplate.findOne(Query.query(Criteria.where("_id").is(Long.parseLong(userId))), UserProxyEntity.class, tableName());
        if (menuModelProxyEntity != null) {
            if (menuModelProxyEntity.getRoles() != null) {
                List<Long> roleIds = new ArrayList<Long>();
                for (RoleProxyEntity roleProxyEntity : menuModelProxyEntity.getRoles()) {
                    //根据roleId，查找权限...
                    roleIds.add(roleProxyEntity.getId());
                }
                Query query = listQueryFields().addCriteria(Criteria.where("_id").in(roleIds));
                List<RoleProxyEntity> roleProxyEntities = mongoTemplate.find(query, RoleProxyEntity.class, polar.island.inlay.config.Table.INLAY_ROLE);
                if (!CollectionUtils.isEmpty(roleProxyEntities)) {
                    for (RoleProxyEntity entity : roleProxyEntities) {
                        if (!CollectionUtils.isEmpty(entity.getPermissions())) {
                            for (PermissionProxyEntity permissionProxyEntity : entity.getPermissions()) {
                                result.add(permissionProxyEntity.getName());
                            }
                        }
                    }
                }

            }
        }
        HashSet h = new HashSet(result);
        result.clear();
        result.addAll(h);
        return result;
    }

    @Override
    public Long disableUser(String id, String disableReason) {
        Query query = Query.query(Criteria.where("_id").is(Long.parseLong(id)));
        Update update = Update.update("state", 0);
        update.set("disableReason",disableReason);
        return mongoTemplate.updateFirst(query, update, tableName()).getMatchedCount();
    }



    @Override
    public Long enableUser(String id) {
        Query query = Query.query(Criteria.where("_id").is(Long.parseLong(id)));
        Update update = Update.update("state", 1);
        update.set("disableReason",null);
        return mongoTemplate.updateFirst(query, update, tableName()).getMatchedCount();
    }

    @Override
    public Long updateSelf(Map<String, Object> condition) {
        Query query = Query.query(Criteria.where("_id").is(realIdValue(findId(condition))));
        Update update = new Update();
        update.set("headUrl", condition.get("headUrl"));
        update.set("nickName", condition.get("nickName"));
        update.set("phone", condition.get("phone"));
        update.set("email", condition.get("email"));
        return mongoTemplate.updateFirst(query, update, tableName()).getMatchedCount();
    }

    @Override
    public List<String> orgPermissions(String userId) {
        List<String> result = null;
        OrgProxyEntity menuModelProxyEntity = mongoTemplate.findOne(Query.query(Criteria.where("_id").is(Long.parseLong(userId))), OrgProxyEntity.class, polar.island.inlay.config.Table.INLAY_ORG);
        if (menuModelProxyEntity != null) {
            if (menuModelProxyEntity.getPermissions() != null) {
                for (PermissionProxyEntity roleProxyEntity : menuModelProxyEntity.getPermissions()) {
                    result.add(roleProxyEntity.getName());
                }
            }
        }
        if (result == null) {
            result = new ArrayList<String>();
        } else {
            HashSet h = new HashSet(result);
            result.clear();
            result.addAll(h);
        }
        return result;
    }

    @Override
    public Class<?> proxyClass() {
        return UserProxyEntity.class;
    }
}
