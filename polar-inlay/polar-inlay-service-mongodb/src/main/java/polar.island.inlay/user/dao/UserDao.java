package polar.island.inlay.user.dao;

import polar.island.core.dao.BasicDao;
import polar.island.core.security.entity.UserEntity;

import java.util.List;
import java.util.Map;

/**
 * 用户的持久化层。
 *
 * @author PolarLoves
 */

public interface UserDao extends BasicDao<UserEntity, UserEntity> {
    /**
     * 用户登录后，修改数据
     *
     * @param condition 用户信息
     */
    public void logIn(Map<String, Object> condition);

    /**
     * 获取用户的所有角色
     *
     * @param userId 用户信息
     * @return 用户角色
     */
    public List<String> userRoles(String userId);

    /**
     * 获取用户的所有权限
     *
     * @param userId 用户信息
     * @return 用户的权限
     */
    public List<String> userPermissions(String userId);

    /**
     * 禁用用户
     *
     * @param id            用户编号
     * @param disableReason 禁用原因
     * @return 禁用用户数量
     */
    public Long disableUser(String id, String disableReason);

    /**
     * 启用用户
     *
     * @param id 用户编号
     * @return 启用用户数量
     */
    public Long enableUser(String id);

    /**
     * 更新用户基本信息
     *
     * @param condition 用户信息
     * @return 更新数量
     */
    public Long updateSelf(Map<String, Object> condition);

    /**
     * 获取用户的所有部门权限
     *
     * @param userId 用户信息
     * @return 用户的所有部门权限
     */
    public List<String> orgPermissions(String userId);

}
