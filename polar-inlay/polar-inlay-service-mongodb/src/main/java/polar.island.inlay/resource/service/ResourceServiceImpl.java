package polar.island.inlay.resource.service;

import org.springframework.stereotype.Service;
import polar.island.core.config.Constants;
import polar.island.core.exception.FrameWorkException;
import polar.island.core.security.service.ResourceService;
import polar.island.core.util.ExceptionUtil;
import polar.island.inlay.resource.dao.ResourceDao;
import polar.island.inlay.resource.entity.ResourceEntity;
import polar.island.mongo.service.MongoBasicService;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(value = "resourceService")
public class ResourceServiceImpl extends MongoBasicService<ResourceEntity, ResourceEntity, ResourceDao>
        implements ResourceService {
    @Resource(name = "resourceDao")
    private ResourceDao resourceDao;

    @Override
    public ResourceDao getDao() {
        return resourceDao;
    }

    @Override
    public Object insert(Map<String, Object> condition) {
        try {
            Map<String, Object> condition2 = new HashMap<String, Object>();
            condition2.put("path", condition.get("path"));
            if (getDao().selectOneByCondition(condition2) !=null) {
                throw new FrameWorkException(Constants.CODE_SERVER_COMMON, "访问路径为\"" + condition.get("path") + "\"的资源已存在",
                        null, false);
            }
            resourceDao.insert(condition);
            return condition.get("id");
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }


    @Override
    public Map<String, List<String>> resourcePermissions() {
        return resourceDao.resourcePermissions();
    }
}