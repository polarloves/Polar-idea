package polar.island.inlay.resource.dao;

import com.mongodb.BasicDBObject;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import polar.island.inlay.permission.proxy.PermissionProxyEntity;
import polar.island.inlay.resource.entity.ResourceEntity;
import polar.island.inlay.resource.proxy.ResourceProxyEntity;
import polar.island.mongo.dao.MongoBasicDao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component(value = "resourceDao")
public class ResourceDaoImpl extends MongoBasicDao<ResourceEntity, ResourceEntity> implements ResourceDao {
    @Override
    public Map<String, List<String>> resourcePermissions() {
        BasicDBObject fieldsObject = new BasicDBObject();
        fieldsObject.put("path", true);
        fieldsObject.put("permissions", true);
        List<ResourceProxyEntity> entities = mongoTemplate.find(new BasicQuery(null, fieldsObject.toJson()), proxyClass(), tableName());
        Map<String, List<String>> result =  new ConcurrentHashMap<String, List<String>> ();
        for (ResourceProxyEntity e : entities) {
            List<String> str = new ArrayList<>();
            if (!CollectionUtils.isEmpty(e.getPermissions())) {
                for (PermissionProxyEntity pe : e.getPermissions()) {
                    str.add(pe.getName());
                }
            }
            result.put(e.getPath(), str);
        }
        return result;
    }

    @Override
    public void insert(Map<String, Object> condition) {
        super.insert(condition);
    }

    @Override
    public Class<ResourceProxyEntity> proxyClass() {
        return ResourceProxyEntity.class;
    }
}
