package polar.island.inlay.util;

import org.springframework.util.CollectionUtils;
import polar.island.core.entity.TreeEntity;
import polar.island.inlay.menu.entity.MenuEntity;
import polar.island.inlay.org.entity.OrgEntity;
import polar.island.inlay.permission.entity.PermissionEntity;
import polar.island.inlay.role.entity.RoleEntity;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SortUtil {
    public static void sortTree(List<TreeEntity> details) {
        if (!CollectionUtils.isEmpty(details)) {
            Collections.sort(details, new Comparator<TreeEntity>() {
                @Override
                public int compare(TreeEntity o1, TreeEntity o2) {
                    return sortResult(o1.getParentId(), o2.getParentId(), o1.getOrderNum(), o2.getOrderNum());
                }
            });
        }
    }

    private static int sortResult(String parentId1, String parentId2, Long orderNum1, Long orderNum2) {

        long orderDiff = orderNum1 - orderNum2;
        int parentIdDiff = 0;
        int orderCompares = 0;
        if (orderDiff > 0) {
            orderCompares = 1;
        } else {
            orderCompares = -1;
        }
        if (parentId1 == null) {
            parentId1 = "-1";
        }
        if (parentId2 == null) {
            parentId2 = "-1";
        }
        parentIdDiff = Long.compare(Long.parseLong(parentId1), Long.parseLong(parentId2));
        if (parentIdDiff == 0) {
            return orderCompares;
        }
        return parentIdDiff;
    }

    private static int sortResult(Long parentId1, Long parentId2, Long orderNum1, Long orderNum2) {
        long orderDiff = orderNum1 - orderNum2;
        int parentIdDiff = 0;
        int orderCompares = 0;
        if (orderDiff > 0) {
            orderCompares = 1;
        } else {
            orderCompares = -1;
        }
        if (parentId1 == null) {
            parentId1 = -1l;
        }
        if (parentId2 == null) {
            parentId2 =  -1l;
        }
        parentIdDiff = Long.compare(parentId1, parentId2);
        if (parentIdDiff == 0) {
            return orderCompares;
        }
        return parentIdDiff;
    }

    public static void sortRole(List<RoleEntity> details) {
        if (!CollectionUtils.isEmpty(details)) {
            Collections.sort(details, new Comparator<RoleEntity>() {
                @Override
                public int compare(RoleEntity o1, RoleEntity o2) {
                    return sortResult(o1.getParentId(), o2.getParentId(), o1.getOrderNum(), o2.getOrderNum());
                }
            });
        }
    }

    public static void sortOrg(List<OrgEntity> details) {
        if (!CollectionUtils.isEmpty(details)) {
            Collections.sort(details, new Comparator<OrgEntity>() {
                @Override
                public int compare(OrgEntity o1, OrgEntity o2) {
                    return sortResult(o1.getParentId(), o2.getParentId(), o1.getId(), o2.getId());
                }
            });
        }
    }

    public static void sortPermission(List<PermissionEntity> details) {
        if (!CollectionUtils.isEmpty(details)) {
            Collections.sort(details, new Comparator<PermissionEntity>() {
                @Override
                public int compare(PermissionEntity o1, PermissionEntity o2) {

                    return sortResult(o1.getParentId(), o2.getParentId(), o1.getOrderNum(), o2.getOrderNum());
                }
            });
        }
    }

    public static void sortMenu(List<MenuEntity> entities) {
        if (!CollectionUtils.isEmpty(entities)) {
            Collections.sort(entities, new Comparator<MenuEntity>() {
                @Override
                public int compare(MenuEntity o1, MenuEntity o2) {
                    return sortResult(o1.getParentId(), o2.getParentId(), o1.getOrderNum(), o2.getOrderNum());
                }
            });
        }
    }
}
