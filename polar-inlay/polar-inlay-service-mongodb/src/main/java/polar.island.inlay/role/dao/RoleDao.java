package polar.island.inlay.role.dao;

import polar.island.core.dao.BasicDao;
import polar.island.inlay.role.entity.RoleEntity;

import java.util.List;
import java.util.Map;

/**
 * 角色的持久化层。
 *
 * @author PolarLoves
 */
public interface RoleDao extends BasicDao<RoleEntity, RoleEntity> {
    /**
     * 由于仅有物理删除，此删除方法被移除
     */
    @Deprecated
    public Long deleteByIdLogic(String id);

    /**
     * 由于仅有物理删除，此删除方法被移除
     */
    @Deprecated
    public Long deleteByConditionLogic(Map<String, Object> condition);

    /**
     * 获取所有的角色
     *
     * @return 所有的角色
     */
    public List<polar.island.core.entity.TreeEntity> allRoles();

    /**
     * 修改用户的角色
     *
     * @param userId 用户编号
     * @param roleId 角色编号
     */
    public void updateUserRoles(String userId, String[] roleId);

    /**
     * 获取用户所有的角色
     *
     * @param userId 用户编号
     * @return 用户角色
     */
    public List<RoleEntity> userRoles(String userId);

}
