package polar.island.inlay.role.dao;

import org.bson.Document;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import polar.island.core.entity.TreeEntity;
import polar.island.core.exception.ValidationException;
import polar.island.core.util.CommonUtil;
import polar.island.core.util.EntityUtil;
import polar.island.inlay.role.entity.RoleEntity;
import polar.island.inlay.role.proxy.RoleProxyEntity;
import polar.island.inlay.user.proxy.UserProxyEntity;
import polar.island.inlay.util.SortUtil;
import polar.island.mongo.dao.MongoBasicDao;
import polar.island.mongo.util.MongoDBUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository(value = "roleDao")
public class RoleDaoImpl extends MongoBasicDao<RoleEntity, RoleEntity> implements RoleDao {
    @Override
    public List<TreeEntity> allRoles() {
        Aggregation agg = Aggregation.newAggregation(
                Aggregation.project("id", "name", "parentId", "orderNum", "text"),
                Aggregation.sort(Sort.Direction.ASC, "parentId", "orderNum")
        );
        List<TreeEntity> details = new ArrayList<TreeEntity>();
        AggregationResults<Document> outputType = mongoTemplate.aggregate(agg, tableName(), Document.class);
        List<Document> resultList = outputType.getMappedResults();

        for (Document obj : resultList) {
            TreeEntity entity = new TreeEntity();
            entity.setText(obj.getString("text"));
            entity.setId(CommonUtil.valueOf(obj.getLong("_id")));
            entity.setParentId(CommonUtil.valueOf(obj.getLong("parentId")));
            entity.setValue(obj.getString("name"));
            entity.setOrderNum(obj.getLong("orderNum"));
            details.add(entity);
        }
        SortUtil.sortTree(details);
        return details;
    }

    @Override
    public void updateUserRoles(String userId, String[] roleId) {
        updateCollectionRoles(Long.parseLong(userId), roleId, polar.island.inlay.config.Table.INLAY_USER);
    }

    @Override
    public Long deleteByIdPhysical(String id) {
        //删除用户-角色
        MongoDBUtil.deleteCollectionItem(mongoTemplate, polar.island.inlay.config.Table.INLAY_USER, "roles", Long.parseLong(id));
        return super.deleteByIdPhysical(id);
    }


    @Override
    public Long deleteByConditionPhysical(Map<String, Object> condition) {
        throw new ValidationException("无法根据条件删除数据", null);
    }

    public void updateCollectionRoles(Object collectionId, String[] roleIds, String collectionName) {
        List<Long> ids = new ArrayList<>();
        List<RoleProxyEntity> menuProxyEntities = new ArrayList<>();
        if (roleIds != null && roleIds.length > 0) {
            for (String id : roleIds) {
                ids.add(Long.parseLong(id));
            }
            Query query = listQueryFields().addCriteria(Criteria.where("_id").in(ids));
            menuProxyEntities = mongoTemplate.find(query, RoleProxyEntity.class, polar.island.inlay.config.Table.INLAY_ROLE);
        }
        Query query = new Query(Criteria.where("_id").is(collectionId));
        Update update = new Update();
        update.set("roles", menuProxyEntities);
        mongoTemplate.updateMulti(query, update, collectionName);
    }

    private void updateCollectionRole(Map<String, Object> condition, String collectionName) {
        Object id = realIdValue(findId(condition));
        RoleEntity entity = selectOneById(id.toString());
        Update update = new Update();
        update.set("roles.$.name", entity.getName());
        update.set("roles.$.text", entity.getText());
        update.set("roles.$.info", entity.getInfo());
        update.set("roles.$.orderNum", entity.getOrderNum());
        update.set("roles.$.parentId", entity.getParentId());
        mongoTemplate.updateMulti(Query.query(Criteria.where("roles._id").is(id)), update, collectionName);
    }

    @Override
    public Long updateField(Map<String, Object> condition) {
        Long result=super.updateField(condition);
        updateCollectionRole(condition, polar.island.inlay.config.Table.INLAY_USER);
        return result;
    }

    @Override
    public Long updateAll(Map<String, Object> condition) {
        Long result=super.updateAll(condition);
        updateCollectionRole(condition, polar.island.inlay.config.Table.INLAY_USER);
        return result;
    }

    @Override
    public List<RoleEntity> userRoles(String userId) {
        List<RoleEntity> result = null;
        UserProxyEntity menuModelProxyEntity = mongoTemplate.findOne(Query.query(Criteria.where("_id").is(Long.parseLong(userId))), UserProxyEntity.class, polar.island.inlay.config.Table.INLAY_USER);
        if (menuModelProxyEntity != null) {
            if (menuModelProxyEntity.getRoles() != null) {
                result = EntityUtil.copyArray(menuModelProxyEntity.getRoles(), RoleEntity.class);
            }
        }
        if (result == null) {
            result = new ArrayList<RoleEntity>();
        }
        SortUtil.sortRole(result);
        return result;
    }


    @Override
    public Class<?> proxyClass() {
        return RoleProxyEntity.class;
    }
}
