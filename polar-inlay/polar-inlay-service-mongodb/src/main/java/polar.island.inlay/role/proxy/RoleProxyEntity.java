package polar.island.inlay.role.proxy;


import org.springframework.data.annotation.Id;
import polar.island.core.dao.annotation.AutoIncrease;
import polar.island.core.dao.annotation.ClassConfig;
import polar.island.core.dao.annotation.FieldConfig;
import polar.island.inlay.permission.proxy.PermissionProxyEntity;
import polar.island.inlay.role.entity.RoleEntity;

import java.util.List;

@ClassConfig(tableName = polar.island.inlay.config.Table.INLAY_ROLE, listClass = RoleEntity.class, detailClass = RoleEntity.class)
public class RoleProxyEntity {
    /** 主键编号 **/
    @Id
    @AutoIncrease
    @FieldConfig
    private Long id;
    /** 父类id **/
    @FieldConfig
    private Long parentId;
    /** 角色名称 **/
    @FieldConfig
    private String name;
    /** 角色中文名称 **/
    @FieldConfig
    private String text;
    /** 角色描述 **/
    @FieldConfig
    private String info;
    /** 排序 **/
    @FieldConfig
    private Long orderNum;
    @FieldConfig(updateAll = false,updateField = false,scope = {}, styles = {}, sortable = false)
    private List<PermissionProxyEntity> permissions;

    public List<PermissionProxyEntity> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<PermissionProxyEntity> permissions) {
        this.permissions = permissions;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Long getOrderNum() {
        return orderNum;
    }

    public void setOrderNum(Long orderNum) {
        this.orderNum = orderNum;
    }
}
