package polar.island.inlay.role.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import polar.island.core.config.Constants;
import polar.island.core.entity.TreeEntity;
import polar.island.core.exception.FrameWorkException;
import polar.island.core.util.ExceptionUtil;
import polar.island.inlay.permission.dao.PermissionDao;
import polar.island.inlay.role.dao.RoleDao;
import polar.island.inlay.role.entity.RoleEntity;
import polar.island.mongo.service.MongoBasicService;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(value = "roleService")
public class RoleServiceImpl extends MongoBasicService<RoleEntity, RoleEntity, RoleDao> implements RoleService {
    @Resource(name = "roleDao")
    private RoleDao roleDao;
    @Resource(name = "permissionDao")
    private PermissionDao permissionDao;

    @Override
    public RoleDao getDao() {
        return roleDao;
    }

    @Override
    public Object insert(Map<String, Object> condition) {
        try {
            Map<String, Object> condition2 = new HashMap<String, Object>();
            condition2.put("name", condition.get("name"));
            if (getDao().selectOneByCondition(condition2) != null) {
                throw new FrameWorkException(Constants.CODE_SERVER_COMMON, "名称为\"" + condition.get("name") + "\"的角色已存在",
                        null, false);
            }
            roleDao.insert(condition);
            return condition.get("id");
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Override
    public List<TreeEntity> allRoles() {
        try {
            return roleDao.allRoles();
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Transactional(transactionManager = "mongoTransactionManager", readOnly = false)
    @Override
    public void updateUserRoles(String userId, String[] roleId) {
        try {
            roleDao.updateUserRoles(userId, roleId);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    @Transactional(transactionManager = "mongoTransactionManager", readOnly = false)
    @Override
    public Long deleteByIdPhysical(String id) {
        try {
            List<TreeEntity> all = roleDao.allRoles();
            Long count = deleteRoles(id, all, 0l);
            return count;
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }

    private Long deleteRoles(String id, List<TreeEntity> all, Long count) {
        // 删除此id的数据
        count = count + roleDao.deleteByIdPhysical(id + "");
        if (all != null && all.size() > 0) {
            for (TreeEntity entity : all) {
                if (id.equals(entity.getParentId() + "")) {
                    count = deleteRoles(entity.getId(), all, count);
                }
            }
        }
        return count;
    }

    @Override
    public List<RoleEntity> userRoles(String userId) {
        try {
            return roleDao.userRoles(userId);
        } catch (Exception e) {
            throw ExceptionUtil.assembleException(e);
        }
    }
}