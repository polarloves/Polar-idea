package polar.island.inlay.code.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import polar.island.core.exception.ValidationException;
import polar.island.inlay.code.dao.CodeDao;
import polar.island.inlay.code.entity.CodeColumn;
import polar.island.inlay.code.entity.CodeEntity;
import polar.island.mongo.service.MongoBasicService;

import java.util.List;
import java.util.Map;


@Service(value = "codeService")
public class CodeServiceImpl extends MongoBasicService<CodeEntity, CodeEntity, CodeDao> implements CodeService {


    @Override
    public CodeDao getDao() {
        return null;
    }

    @Transactional(readOnly = false)
    @Override
    public Long deleteMulitByIdPhysical(String[] ids) {
        throw new ValidationException("Mongodb版本不支持代码生成器！", null);

    }

    @Transactional(readOnly = false)
    @Override
    public Long deleteByIdPhysical(String id) {
        throw new ValidationException("Mongodb版本不支持代码生成器！", null);
    }

    public List<Map<String, Object>> allTableNames() {
        throw new ValidationException("Mongodb版本不支持代码生成器！", null);
    }

    @Transactional(readOnly = false)
    @Override
    public void importTable(String tableName, String commont, String moduleName) {
        throw new ValidationException("Mongodb版本不支持代码生成器！", null);
    }

    @Transactional(readOnly = false)
    public void genColumns(Long tableId, CodeEntity entity) {
        throw new ValidationException("Mongodb版本不支持代码生成器！", null);
    }

    @Transactional(readOnly = false)
    @Override
    public void insertColumns(List<CodeColumn> codeColumn, Long tableId) {
        throw new ValidationException("Mongodb版本不支持代码生成器！", null);
    }

    @Override
    public List<CodeColumn> selectColumns(Long tableId) {
        throw new ValidationException("Mongodb版本不支持代码生成器！", null);
    }

    @Transactional(readOnly = false)
    @Override
    public Object insert(Map<String, Object> condition) {
        throw new ValidationException("Mongodb版本不支持代码生成器！", null);
    }

    @Transactional(readOnly = false)
    @Override
    public void genSettings(CodeEntity entity) {
        throw new ValidationException("Mongodb版本不支持代码生成器！", null);

    }

    @Override
    public List<Map<String, Object>> allChildrens(String parentTableId) {
        throw new ValidationException("Mongodb版本不支持代码生成器！", null);
    }

    @Override
    public CodeEntity selectOneById(String id) {
        throw new ValidationException("Mongodb版本不支持代码生成器！", null);
    }

    @Override
    public void importExcell(List<Map<String, Object>> condition) {
        throw new ValidationException("Mongodb版本不支持代码生成器！", null);
    }

    @Override
    public CodeEntity selectOneByCondition(Map<String, Object> condition) {
        throw new ValidationException("Mongodb版本不支持代码生成器！", null);
    }

    @Override
    public List<CodeEntity> selectList(Map<String, Object> condition) {
        throw new ValidationException("Mongodb版本不支持代码生成器！", null);
    }

    @Override
    public List<CodeEntity> selectPageList(Map<String, Object> condition) {
        throw new ValidationException("Mongodb版本不支持代码生成器！", null);
    }

    @Override
    public Long selectCount(Map<String, Object> condition) {
        throw new ValidationException("Mongodb版本不支持代码生成器！", null);
    }

    @Override
    public Long updateAll(Map<String, Object> condition) {
        throw new ValidationException("Mongodb版本不支持代码生成器！", null);
    }

    @Override
    public Long updateField(Map<String, Object> condition) {
        throw new ValidationException("Mongodb版本不支持代码生成器！", null);
    }

    @Override
    public Long deleteByIdLogic(String id) {
        throw new ValidationException("Mongodb版本不支持代码生成器！", null);
    }

    @Override
    public Long deleteMulitByIdLogic(String[] ids) {
        throw new ValidationException("Mongodb版本不支持代码生成器！", null);
    }

    @Override
    public Long deleteByConditionPhysical(Map<String, Object> condition) {
        throw new ValidationException("Mongodb版本不支持代码生成器！", null);
    }

    @Override
    public Long deleteByConditionLogic(Map<String, Object> condition) {
        throw new ValidationException("Mongodb版本不支持代码生成器！", null);
    }

    @Override
    public List<Map<String, Object>> selectExportList(Map<String, Object> condition) {
        throw new ValidationException("Mongodb版本不支持代码生成器！", null);
    }
}
